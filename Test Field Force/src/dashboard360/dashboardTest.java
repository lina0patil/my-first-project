package dashboard360;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class dashboardTest {
	public static WebDriver driver;

	@BeforeTest
	public void launchingBrowser() throws InterruptedException {
  //launching the browser
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
	ChromeOptions options= new ChromeOptions();
	options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
	driver = new ChromeDriver(options);
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
  //navigate to the web page
	driver.get("https://testffc.nimapinfotech.com/auth/login");	    
		  
  //login -enter the email and password & click on sign in button 
	driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
	Thread.sleep(2000);
	driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
  //Thread.sleep(2000);
	driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	System.out.println("login done successfully");
	 }
	@Test(priority=0)
	public void dashboard360() throws InterruptedException  {
  //driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	WebDriverWait wait=new WebDriverWait(driver,30);	
  //Click on the dashboard360
	WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Dashboard 360')]")));
	element.click();
	Thread.sleep(5000);
	}
	@Test(priority=1)
	public void verifyTaskAgainstLead() throws InterruptedException {
  //move down by using interface
	JavascriptExecutor js=(JavascriptExecutor)driver;
	//js.executeScript("window.scrollBy(0,2500)");
	//navigate to Task against Lead tab
	WebElement element = driver.findElement(By.xpath("//div[@class='row']/child::div[17]"));
	js.executeScript("arguments[0].scrollIntoView(true);", element);
	Thread.sleep(5000);
  //Task Against Lead
	driver.findElement(By.xpath("//div[contains(text(),'Tasks Against Lead')]"));
	Thread.sleep(3000);
	WebDriverWait wait=new WebDriverWait(driver,30);
	
	WebElement customer = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[.='Customer2 5']")));
	customer.click();
	Thread.sleep(3000);
	}
	@Test(priority=2)
	public void verify_data_FromTask () throws InterruptedException {
		WebDriverWait wait=new WebDriverWait(driver,20);
		
		WebElement task = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//mat-cell[normalize-space()='test2']")));
		task.click();
		Thread.sleep(3000);
		String title=driver.findElement(By.xpath("//p[normalize-space()='test2']")).getText();
	 	 System.out.println("Title = "+title);
	 	Thread.sleep(3000);
	 
	 	String description=driver.findElement(By.xpath("//p[normalize-space()='dashboard360']")).getText();
	 	 System.out.println("Description = "+description);
	 	Thread.sleep(2000);
	 	String priority=driver.findElement(By.xpath("//p[normalize-space()='Low Priority']")).getText();
	 	 System.out.println("Priority = "+priority);
	 	Thread.sleep(2000);
	 	String assignedDate=driver.findElement(By.xpath("//p[normalize-space()='21 Feb 2022 01:40 PM']")).getText();
	 	 System.out.println("Assigned Date = "+assignedDate);
	 	Thread.sleep(2000);
	 	String dueDate=driver.findElement(By.xpath("//p[normalize-space()='23 Feb 2022']")).getText();
	 	 System.out.println("Due Date = "+dueDate);
	 	Thread.sleep(2000);
	 	String assignedTo=driver.findElement(By.xpath("//p[contains(@class,'kt-pointer')]")).getText();
	 	 System.out.println("Assigned To = "+assignedTo);
	 	Thread.sleep(2000);
	 	String customerName=driver.findElement(By.xpath("//p[normalize-space()='Customer2']")).getText();
	 	 System.out.println("Customer Name = "+customerName);
	 	Thread.sleep(2000);
	 	String mobileNo=driver.findElement(By.xpath("//div[contains(@class,'col-4')]//p[contains(text(),'-')]")).getText();
	 	 System.out.println("Mobile No = "+mobileNo);
	}
	@Test(priority=3)
	public void verify_CancelButton() throws InterruptedException {
      driver.findElement(By.xpath("//span[normalize-space()='Cancel']")).click();
		Thread.sleep(3000);
	}
	   @AfterTest
	     public void closeBrowser() throws InterruptedException {
	   	 Thread.sleep(2000);
	   	  driver.close();
	   	  System.out.println("AfterTest is running");
	     }
}
