package myCustomerTestNg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class myCustomerMTest {
	 public static WebDriver driver;
		
	    @BeforeTest
		public void beforeTest() throws InterruptedException {
	//launching the browser
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
		ChromeOptions options= new ChromeOptions();
	 
	    options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		     
	//navigate to the web page
		driver.get("https://app.fieldforceconnect.com/auth/login");
		//to verify the title and url of login page
		/*  String url= driver.getCurrentUrl();
		  if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
			  Assert.fail("url is not matched "); }
		  else {
			  System.out.println("url matched");
		  }
		  String title= driver.getTitle();
		  if(!title.equalsIgnoreCase("TEST Field Force")) {
			  Assert.fail("title is not matched"); }
		  else {
			  System.out.println("title matched");
	  }
		  System.out.println("1st Test case is done successfully");
	  */
			  
	//login -enter the email and password & click on sign in button 
		driver.findElement(By.id("mat-input-0")).sendKeys("9519519519"); //email
		Thread.sleep(2000);
		driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
		Thread.sleep(2000);
		driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
		System.out.println("login done successfully");
		Thread.sleep(4000);
	    }
	    
        @Test(priority=0)
        public void myCustomerTest() throws InterruptedException {
	//navigate to the my customer
	    driver.findElement(By.xpath("//span[text()='My Customers']")).click();
	    Thread.sleep(3000);
	    System.out.println("navigate to my customer");    
        }
        
       
        @Test(priority=1)
        public void export() throws InterruptedException {
    //click on export 
        driver.findElement(By.xpath("//span[text()='Export']")).click();
        Thread.sleep(8000);
        System.out.println("Export- test case done successfully");
        }
        
       @Test(priority=2)
        public void bulkUpload() throws InterruptedException, AWTException {
	//click on bulk upload 
	    driver.findElement(By.xpath("//span[contains(text(),'Bulk Upload')]")).click();
	    Thread.sleep(4000);
 /*
	// Click  to download All Customers 
	    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-bulk-upload-customer/kt-portlet/div/kt-portlet-body/div/div[1]/a/b")).click();
	    Thread.sleep(4000);
	   // driver.switchTo().alert().accept();
	    Thread.sleep(4000);
	//Click to download sample file
	    driver.findElement(By.xpath("//a[contains(text(),'Click to download sample file')]")).click();
	    Thread.sleep(4000);	    
	//Get the input field "Attach Excel File"   
	    WebElement attachExcelFile= driver.findElement(By.xpath("//button[@class='mat-raised-button mat-stroked-button mat-button-base ng-star-inserted']//span[contains(text(),' Attach Excel File')]"));
	    attachExcelFile.click();
	    
	    StringSelection strSel = new StringSelection("BulkUpload-Leads-customer");
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
	    Thread.sleep(3000);
	    Robot robot = new Robot();
	    Thread.sleep(3000);
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);

	    Thread.sleep(3000);
	    robot.keyPress(KeyEvent.VK_ENTER);
	    Thread.sleep(3000);
	    robot.keyRelease(KeyEvent.VK_ENTER);
	    Thread.sleep(3000);
	//click the upload button
		driver.findElement(By.xpath("//span[contains(text(),'Upload')]")).click();
	//printing message
		//  String toastMessage = driver.findElement(By.id("toast-container")).getText();
	   //   System.out.println(toastMessage);*/
	    Thread.sleep(5000);  
	//click on show report
		driver.findElement(By.xpath("//span[contains(text(), 'Show Report')]")).click();
		Thread.sleep(5000);
        
	  /*//click to search
		WebElement searchUser=driver.findElement(By.xpath("//*[@id=\"mat-input-400\"]"));
		searchUser.sendKeys("rijo");
		Thread.sleep(3000); 
		searchUser.clear();
		Thread.sleep(5000);
	//click on items per page
		driver.findElement(By.xpath("//*[@id=\"mat-select-48\"]/div/div[1]")).click();
		driver.findElement(By.xpath("//*[@id=\"mat-option-441\"]/span")).click();*/
        
	//click to download bulkupload report
		driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-bulk-report-customer/kt-portlet/div/kt-portlet-body/div/table/tbody/tr[3]/td[5]/button[2]/span/i")).click();
		Thread.sleep(5000);
	//click to download report file
		driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-bulk-report-customer/kt-portlet/div/kt-portlet-body/div/table/tbody/tr[3]/td[5]/button[3]/span/i")).click();
		Thread.sleep(5000);
	//click on view report
		driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-bulk-report-customer/kt-portlet/div/kt-portlet-body/div/table/tbody/tr[1]/td[5]/button[1]/span")).click();
		Thread.sleep(6000);
        }
        
        
		@Test(priority=3)
		public void viewBulkReport() throws InterruptedException {
		JavascriptExecutor js=(JavascriptExecutor)driver;
	//click to search
		WebElement searchUser=driver.findElement(By.id("mat-input-401"));
	    searchUser.sendKeys("rijo");
		Thread.sleep(3000);
		js.executeScript("window.scrollBy(0,500)");
		WebElement slider=driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-view-bulk-report/kt-portlet/div/kt-portlet-body/div/mat-table"));
		
		Actions act=new Actions(driver);
		Thread.sleep(3000);
	//sliding on horizontal scale
		act.clickAndHold(slider).moveByOffset(400, 0).release().perform();
		Thread.sleep(5000);
	//moving back to original position
		act.clickAndHold(slider).moveByOffset(-400, 0).release().perform();
		driver.navigate().back();
        Thread.sleep(5000);
        driver.navigate().back();
        Thread.sleep(3000);
        driver.navigate().back();
        Thread.sleep(5000);
		}
		@Test(priority=4)
	    public void searchField() throws InterruptedException {
	    WebElement searchUser=driver.findElement(By.id("mat-input-0"));
	    searchUser.sendKeys("lina");
	    Thread.sleep(3000);
	    searchUser.clear();
	    Thread.sleep(3000);   
	    }
        @Test(priority=5)
        public void filter() throws InterruptedException {
        Thread.sleep(5000);
    //click on filter
	    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-filter/div[1]/button/span")).click();
	    Thread.sleep(5000);
     //country
	    driver.findElement(By.xpath("//Span[@class='mat-select-placeholder ng-tns-c32-511 ng-star-inserted']")).click();
	    Thread.sleep(4000);                     
	    WebElement searchCountry = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/div[1]/mat-form-field/div/div[1]/div/input"));
	    searchCountry.sendKeys("India");
	    Thread.sleep(4000);
	    driver.findElement(By.xpath("//span[contains(text(),' India ')]")).click();
	    Thread.sleep(5000);
    //State  
	    driver.findElement(By.xpath("//Span[@class='mat-select-placeholder ng-tns-c32-13 ng-star-inserted']")).click();
	    Thread.sleep(3000);
	    WebElement searchState = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/div[1]/mat-form-field/div/div[1]/div/input"));
	    searchState.sendKeys("Maharashtra");
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//span[contains(text(),' Maharashtra ')]")).click();
	    Thread.sleep(3000);
    //city  
	    driver.findElement(By.xpath("//Span[@class='mat-select-placeholder ng-tns-c32-16 ng-star-inserted']")).click();
	    Thread.sleep(3000);
	    WebElement searchCity = driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/div[1]/mat-form-field/div/div[1]/div/input"));
	    searchCity.sendKeys("Pune");
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//span[contains(text(),' Pune ')]")).click();
	    Thread.sleep(3000);
	 //Locality  
	    driver.findElement(By.xpath("//Span[@class='mat-select-placeholder ng-tns-c32-19 ng-star-inserted']")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/mat-option[2]/span")).click();//pune
	    Thread.sleep(3000);  
	 //By status
	    Select byStatus=new Select(driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[1]/div[2]/div[1]/div/div/mat-form-field/div/div[1]/div/mat-select/div/div[1]")));
	    //select require value from dropdown
	    byStatus.selectByVisibleText("Success");
	    Thread.sleep(2000);
	    byStatus.selectByVisibleText("Cold");
	    Thread.sleep(2000);
	 //By source
	    Select bySource=new Select(driver.findElement(By.xpath("//*[@id=\"mat-select-48\"]/div/div[1]/span")));
	    //select require value from dropdown
	    bySource.selectByVisibleText("Email newsletter");
	    Thread.sleep(2000);
	    bySource.selectByVisibleText("Cold Call");
	    Thread.sleep(2000);
	 //By Stage
	    Select byStage=new Select(driver.findElement(By.xpath("//*[@id=\"mat-select-49\"]/div/div[1]/span")));
	    //select require value from dropdown
	    byStage.selectByVisibleText("Opportunity");
	    Thread.sleep(2000);
	    byStage.selectByVisibleText("Enquiry");
	    Thread.sleep(2000);
	 //By Type
	    Select byType=new Select(driver.findElement(By.xpath("//*[@id=\"mat-select-50\"]/div/div[1]/span")));
	    //select require value from dropdown
	    byType.selectByVisibleText("Distributor / Super Stockist");
	    Thread.sleep(2000);
	    byType.selectByVisibleText("Wholesaler / Dealer");
	    Thread.sleep(2000);
	  //By Territory
	    Select byTerritory=new Select(driver.findElement(By.xpath("//*[@id=\"mat-select-51\"]/div/div[1]/span")));
	    //select require value from dropdown
	    byTerritory.selectByVisibleText("new");
	    Thread.sleep(2000);
	  //By Member
	    Select byMember=new Select(driver.findElement(By.xpath("//*[@id=\"mat-select-52\"]/div/div[1]/span")));
	    //select require value from dropdown
	    byMember.selectByVisibleText("BulkName2");
	    Thread.sleep(2000);
	    byMember.selectByVisibleText("testuser");
	    Thread.sleep(2000);
	    byMember.selectByVisibleText("dhanashree");
	    Thread.sleep(2000);
	 //By Tag
	    driver.findElement(By.xpath("//*[@id=\"mat-input-408\"]")).sendKeys("android");
	    Thread.sleep(5000);
     //apply filter
	    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[2]/div/button[2]/span")).click();
		Thread.sleep(5000);
	//click on clear filter button
	    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-filter/div[2]/div/div/div/div/div[9]/button/h6")).click();	  
	    Thread.sleep(3000);
}
        }
