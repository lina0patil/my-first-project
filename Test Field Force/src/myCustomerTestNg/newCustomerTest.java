package myCustomerTestNg;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class newCustomerTest {
	public static WebDriver driver;
	
    @BeforeTest
	public void beforeTest() throws InterruptedException {
//launching the browser
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
	ChromeOptions options= new ChromeOptions();
    Map<String, Object>prefs=new HashMap<String, Object>();
    prefs.put("credentials_enable_service", false);
    prefs.put("profile.password_manager_enabled", false);
    options.setExperimentalOption("prefs", prefs);
    options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
	driver = new ChromeDriver(options);
	driver.manage().window().maximize();
	     
//navigate to the web page
	driver.get("https://testffc.nimapinfotech.com/auth/login");
	//to verify the title and url of login page
	/*  String url= driver.getCurrentUrl();
	  if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
		  Assert.fail("url is not matched "); }
	  else {
		  System.out.println("url matched");
	  }
	  String title= driver.getTitle();
	  if(!title.equalsIgnoreCase("TEST Field Force")) {
		  Assert.fail("title is not matched"); }
	  else {
		  System.out.println("title matched");
  }
	  System.out.println("1st Test case is done successfully");
  */
		  
//login -enter the email and password & click on sign in button 
	driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
	Thread.sleep(2000);
	driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
	Thread.sleep(2000);
	driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	System.out.println("login done successfully");
	Thread.sleep(4000);
    }
    
 @Test(priority=0)
 public void myCustomerTest() throws InterruptedException {
//navigate to the my customer
    driver.findElement(By.xpath("//span[text()='My Customers']")).click();
    Thread.sleep(5000);
    System.out.println("navigate to my customer");    
 } 
 @Test(priority=2)
 public void newCustomer() throws InterruptedException, AWTException {
	 JavascriptExecutor js=(JavascriptExecutor)driver;
   // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	//Click on new customer
	driver.findElement(By.xpath("//span[text()=' New Customer ']")).click();
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-input-399\"]")).sendKeys("Test1");  //customer name    //*[@id="mat-input-22"]
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-input-400\"]")).sendKeys("87654"); //Ref no. //*[@id="mat-input-23"]
	Thread.sleep(5000);
    driver.findElement(By.xpath("//*[@id=\"mat-input-401\"]")).sendKeys("xyz "); //Contact person name
    Thread.sleep(5000);
    driver.findElement(By.xpath("//*[@id=\"mat-input-402\"]")).sendKeys("7634567890");  //mobile no
    Thread.sleep(5000);
	driver.findElement(By.id("mat-input-403")).sendKeys("234565");       //telephone no.
    driver.findElement(By.id("mat-input-404")).sendKeys("test1@gmail.com");  //email
	driver.findElement(By.id("mat-input-405")).sendKeys("manager"); //designation
	Thread.sleep(5000);
//click on add more
	driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-customers-edit-dialog/div[1]/form/div/div[4]/div[6]/button/span")).click();
	Thread.sleep(5000);
	//click on remove
	driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-customers-edit-dialog/div[1]/form/div/div[5]/div/div[2]/button/span")).click();
	Thread.sleep(5000);
	//Customer address
    driver.findElement(By.xpath("//*[@id=\"mat-input-406\"]")).click();
	Thread.sleep(10000);
	driver.findElement(By.xpath("//*[@id=\"mat-dialog-1\"]/kt-address-dialog/kt-portlet/div/kt-portlet-body/div[2]/button/span")).click();    
// WebElement country= driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit dialog/div[1]/form/div/div[6]/div[2]/mat-form-field"));
// country.click();            
//  Select sel=new Select(country);
//  sel.selectByVisibleText("India");
/*//Country
    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[6]/div[2]/mat-form-field")).click();
	Thread.sleep(5000);
	driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/mat-option[101]/span")).click();
	Thread.sleep(5000);
//State
	driver.findElement(By.xpath("//*[@id=\"mat-select-43\"]/div/div[1]/span")).click();
	Thread.sleep(8000);
	driver.findElement(By.xpath("//*[@id=\"mat-option-730\"]/span")).click();
	Thread.sleep(5000);
 //City
	driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-customers-edit-dialog/div[1]/form/div/div[6]/div[4]/mat-form-field")).click();
	Thread.sleep(6000);
	driver.findElement(By.xpath("//*[@id=\"mat-option-1039\"]/span")).click();
	Thread.sleep(2000);
 //Locality
	driver.findElement(By.xpath("//*[@id=\"mat-select-45\"]/div/div[1]/span")).click();
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-option-1161\"]/span")).click();*/
 //Pincode
	driver.findElement(By.xpath("//*[@id=\"mat-input-407\"]")).sendKeys("411021");
	Thread.sleep(5000);
//Scroll down    
	 js.executeScript("window.scrollBy(0,500)");
	 Thread.sleep(3000);
//More_Info
	driver.findElement(By.xpath("//*[@id=\"mat-select-46\"]/div/div[1]/span")).click(); //customer source
	Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@id=\"mat-option-694\"]/span")).click();//Personal Contact
	Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@id=\"mat-select-47\"]/div/div[1]/span")).click(); //customer stage
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-option-699\"]/span")).click(); //opportunity
	Thread.sleep(3000);
	WebElement tag= driver.findElement(By.xpath("//*[@id=\"mat-input-410\"]"));  //tags
	tag.sendKeys("customer service");
	tag.sendKeys(Keys.ENTER);
	Thread.sleep(5000);
// Attachment
	WebElement attachment= driver.findElement(By.xpath("//*[@id=\"mat-input-411\"]"));
    attachment.click();
	Thread.sleep(5000);
    StringSelection strSel = new StringSelection("nimap.png");
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
    Thread.sleep(3000);
    Robot robot = new Robot();
    Thread.sleep(3000);
    robot.keyPress(KeyEvent.VK_CONTROL);
    robot.keyPress(KeyEvent.VK_V);
    robot.keyRelease(KeyEvent.VK_V);
    robot.keyRelease(KeyEvent.VK_CONTROL);

    Thread.sleep(3000);
    robot.keyPress(KeyEvent.VK_ENTER);
    Thread.sleep(3000);
    robot.keyRelease(KeyEvent.VK_ENTER);
    Thread.sleep(3000);
	driver.findElement(By.xpath("//*[@id=\"mat-checkbox-32\"]/label/div")).click(); //check box
//primary-secondary sales info
	driver.findElement(By.xpath("//*[@id=\"mat-select-49\"]/div/div[1]/span")).click();//customer type
	driver.findElement(By.xpath("//*[@id=\"mat-option-706\"]/span")).click(); //distributor
	driver.findElement(By.xpath("//*[@id=\"mat-select-50\"]/div/div[1]/span")).click(); //customer parent name
	Thread.sleep(5000);
	js.executeScript("window.scrollBy(0,200)");
//custom fields
	driver.findElement(By.xpath("//*[@id=\"mat-input-413\"]")).sendKeys("100%");	//enter marks
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-select-51\"]/div/div[1]/span")).click();//sub1
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-option-707\"]/span")).click();//sub2
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-radio-17\"]/label/div[1]/div[1]")).click();//radio button yes
	Thread.sleep(5000);
	//driver.findElement(By.xpath("//*[@id=\"mat-select-52\"]/div/div[1]/span")).click();//click on project1
	Thread.sleep(5000);
	driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-customers-edit-dialog/div[2]/div/div/div/button[2]/span")).click();//save
	System.out.println("New customer save successfully");
	Thread.sleep(5000);
//	String toastMessage = driver.findElement(By.className("toast-container")).getText();
 //   System.out.println(toastMessage);
 }
//	  @AfterTest
	  public void closeBrowser() throws InterruptedException {
	  	  Thread.sleep(5000);
	  	  driver.close();
	  	  System.out.println("AfterTest is running");
	  }


}
