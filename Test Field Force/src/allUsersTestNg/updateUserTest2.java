package allUsersTestNg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class updateUserTest2 {
	public static String ReturnMonthSelected(int mnth) {
	      switch (mnth) {
	          case 1:
	              return "JAN";
	          case 2:
	              return "FEB";
	          case 3:
	              return "MAR";
	          case 4:
	              return "APR";
	          case 5:
	              return "MAY";
	          case 6:
	              return "JUN";
	          case 7:
	              return "JUL";
	          case 8:
	              return "AUG";
	          case 9:
	              return "SEP";
	          case 10:
	              return "OCT";
	          case 11:
	              return "NOV";
	          case 12:
	              return "DEC";
	          default:
	              return "Invalid input - Wrong month number.";
	      }
	  }
	public static WebDriver driver;

	 @BeforeTest
		    public void beforeClass() throws InterruptedException {
		//launching the browser
		    System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
			ChromeOptions options= new ChromeOptions();
			options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
			driver = new ChromeDriver(options);
			driver.manage().window().maximize();
			
		//navigate to the web page
			driver.get("https://testffc.nimapinfotech.com/auth/login");	    
			
		//to verify the title and url of login page
			String url= driver.getCurrentUrl();
			System.out.println(url);
			if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
			Assert.fail("url is not matched "); }
			else {
			System.out.println("url matched");
			}
		    String title= driver.getTitle();
			System.out.println(title);
			if(!title.equalsIgnoreCase("Test Field Force")) {
			Assert.fail("title is not matched"); }
			else {
			System.out.println("title matched");
			}
			  
		//login -enter the email and password & click on sign in button 
			driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
			Thread.sleep(2000);
			driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
			Thread.sleep(2000);
			driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
			System.out.println("login done successfully");
			Thread.sleep(6000);
			}
	 @Test(priority=-1, description="Click on Setting/All user")
	 public void TC_all_User() throws InterruptedException {	
	//navigate to homepage
		    WebElement slider = driver.findElement(By.className("ps__thumb-y"));
		    Actions act = new Actions(driver);
		    Thread.sleep(4000);
	//move down the slider by using method
		    act.clickAndHold(slider).moveByOffset(0, 350).release().perform();
		    Thread.sleep(4000);
	//click on settings
		    driver.findElement(By.xpath("//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")).click();
		    Thread.sleep(3000);
	//move down the slider by using method
		    act.clickAndHold(slider).moveByOffset(0, 50).release().perform();
		    Thread.sleep(3000);
	//click on all users
		    driver.findElement(By.xpath("//*[@id=\"kt_aside_menu\"]/ul/li[18]/div/ul/li[1]/a/span")).click();
		    Thread.sleep(10000);
		    System.out.println("Before Test is running"); 
		  }
	 @Test(priority=0, description="Click on edit user")
	   	public void TC_01() throws InterruptedException {
   //Edit user
	    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-body/div[1]/mat-table/mat-row[4]/mat-cell[8]/button/span")).click();   
	    Thread.sleep(3000);
   }
	/* @Test(priority=1, description="Edit the other fields")
    public void TC_02() throws InterruptedException{
	   JavascriptExecutor js = (JavascriptExecutor) driver;
	 //Name field
   WebElement name=driver.findElement(By.id("mat-input-6"));
   Thread.sleep(3000);
    name.clear();
    Thread.sleep(3000);
    name.sendKeys("test lead12");
    Thread.sleep(4000);
    //Employee no
    WebElement eNo= driver.findElement(By.id("mat-input-7"));
    eNo.clear();
    Thread.sleep(3000);
    eNo.sendKeys("456732");
    Thread.sleep(5000);
    //country code
   // driver.findElement(By.xpath("//*[@id=\"mat-select-7\"]/div/div[1]")).click();
  //  Thread.sleep(5000);
   // driver.findElement(By.xpath("//*[@id=\"mat-option-1363\"]/span")).click();
   // Thread.sleep(3000);
       
    //mob no.
    WebElement mNo= driver.findElement(By.xpath("//*[@id=\"mat-input-8\"]"));
    mNo.clear();
    Thread.sleep(3000);
    mNo.sendKeys("9879674456");
    Thread.sleep(3000);
   
    //contact no.
    WebElement cNo= driver.findElement(By.xpath("//*[@id=\"mat-input-9\"]"));
    cNo.clear();
    Thread.sleep(3000);
    cNo.sendKeys("5678674490");
    Thread.sleep(3000);
   
    //user email
    WebElement email= driver.findElement(By.xpath("//*[@id=\"mat-input-10\"]"));
    email.clear();
    Thread.sleep(3000);
    email.sendKeys("testlead12@nimapinfotech.com");
    Thread.sleep(3000);

    //assign role
    driver.findElement(By.xpath("//*[@id=\"mat-select-8\"]/div/div[1]/span")).click();
    Thread.sleep(2000);
   driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/mat-option[1]/span")).click();//test
    Thread.sleep(3000);
    //gender
    driver.findElement(By.xpath("//*[@id=\"mat-select-9\"]/div/div[1]")).click();
    driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/mat-option[3]/span")).click();

    //date of birth
    boolean IsBirthDateClicked = false; 
    int BirthYear = 1997;
    driver.findElement(By.xpath("//*[@id=\"mat-input-11\"]")).click();
    driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[1]")).click();
    
    while(true) {
    	if(IsBirthDateClicked) {
    		break;
    	}
    	WebElement Calelement = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[1]/span"));
        String dtr = Calelement.getText();
        System.out.println(dtr);
        String[] DateRangepartss = dtr.split(" � ");
       // System.out.println(DateRangepartss[0]);
       // System.out.println(DateRangepartss[1]);
        outerloop:       
    	if(BirthYear >= Integer.parseInt(DateRangepartss[0]) && BirthYear <= Integer.parseInt(DateRangepartss[1]) && !IsBirthDateClicked ) {
                //Store each year in string array
                List < WebElement > cols;
                //Get value from Table each cell
                WebElement t = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-multi-year-view/table/tbody"));
                // count rows with size() method
                List < WebElement > rws = t.findElements(By.tagName("tr"));                          		
                for (WebElement row: rws) {
                    List < WebElement > Cells = row.findElements(By.tagName("td"));
                    for (WebElement Cell: Cells) {
                        if (Cell.getText().contains(Integer.toString(BirthYear))) {
                            Cell.click();
                            Thread.sleep(5000);
                            int BirthMonth = 9;
                            String bMonth = ReturnMonthSelected(BirthMonth);
                            WebElement tm = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-year-view/table/tbody"));
                            // count rows with size() method
                            List <WebElement> monthsrows = tm.findElements(By.tagName("tr"));
                            for (WebElement monthrow: monthsrows) {
                                List < WebElement > MonthCells = monthrow.findElements(By.tagName("td"));
                                for (WebElement Monthcell: MonthCells) {
                                    if (Monthcell.getText().contains(bMonth)) {
                                    	Monthcell.click();
                                        Thread.sleep(5000);
                                        int BirthDay = 19;
                                        WebElement bdatecalender = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody"));
                                        // count rows with size() method
                                        List <WebElement> brows = bdatecalender.findElements(By.tagName("tr"));
                                        for(WebElement brow: brows){
                                            List<WebElement> bCells = brow.findElements(By.tagName("td"));
                                            for(WebElement bCell:bCells){
                                            	if (bCell.getText().contains(Integer.toString(BirthDay)))
                                            	{
                                            		bCell.click();
                                                 	IsBirthDateClicked = true;
                                                 	break outerloop;
                                            	}
                                               
                                            }
                                        }                                                                                                                  
                                    }
                                }
                            }
                        }
                    }
                }
          		break;     
    	}
    	else {
    		if((Integer.parseInt(DateRangepartss[0])-BirthYear)>0){
    			driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[2]")).click();
        		continue;
    		}
    		else {
    			driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[3]")).click();
        		continue;
    		}
    	
    	}
    }
       Thread.sleep(5000);
    }
*/
   	@Test(priority=2, description=" Verify that on clicking the date field, a calendar should open(Date of Joining)")
   	public void TC_03() throws InterruptedException {
   	driver.findElement(By.xpath("//*[@id=\"mat-input-12\"]")).click();
   	     
   	}
   	@Test(priority=3, description="to verify the default date and system date (Date of Joining)")
   	public void TC_04() throws InterruptedException {
   		WebElement joinCalelement = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/mat-calendar-header/div/div/button[1]"));
   	    String defaultDOJ = joinCalelement.getText();
   	    System.out.println("default date: " + defaultDOJ);
   	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");
   	    LocalDate now = LocalDate.now();
   	    System.out.println("System date: " + now);
   	    String date1 = formatter.format(now).toUpperCase();
   	    System.out.println(date1);
   	    
   	  //to verify the default date and system date
   		if(!defaultDOJ.equals(date1)) {
   		Assert.fail("date is not matched "); }
   		else {
   		System.out.println("date matched");
   		}    
   	}
   	@Test(priority=4,description=" Ckeck that user is able to select the Date of Joining")
   	public void TC_05() throws InterruptedException {
       
    //Date of joining
    boolean IsJoinDateClicked=false;
    int joinYear = 2021;
    driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[1]")).click();
    
    while(true) {
    	if(IsJoinDateClicked) {
    		break;
    	}
    	WebElement joinCalelement = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[1]/span"));
        String jdtr = joinCalelement.getText();
        System.out.println(jdtr);
        String[] jDateRangepartss = jdtr.split(" � ");
        System.out.println(jDateRangepartss[0]);
        System.out.println(jDateRangepartss[1]);
        outerloop:       
    	if(joinYear >= Integer.parseInt(jDateRangepartss[0]) && joinYear <= Integer.parseInt(jDateRangepartss[1]) && !IsJoinDateClicked ) {
                //Store each year in string array
                List < WebElement > jcols;
                //Get value from Table each cell
                WebElement jt = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/div/mat-multi-year-view/table/tbody"));
                // count rows with size() method
                List < WebElement > jrws = jt.findElements(By.tagName("tr"));                          		
                for (WebElement row: jrws) {
                    List < WebElement > jCells = row.findElements(By.tagName("td"));
                    for (WebElement jCell: jCells) {
                        if (jCell.getText().contains(Integer.toString(joinYear))) {
                            jCell.click();
                            Thread.sleep(5000);
                            int joinMonth = 12;
                            String jMonth = ReturnMonthSelected(joinMonth);
                            WebElement tm = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-year-view/table/tbody"));
                            // count rows with size() method
                            List <WebElement> jmonthsrows = tm.findElements(By.tagName("tr"));
                            for (WebElement jmonthrow: jmonthsrows) {
                                List < WebElement > jMonthCells = jmonthrow.findElements(By.tagName("td"));
                                for (WebElement jMonthcell: jMonthCells) {
                                    if (jMonthcell.getText().contains(jMonth)) {
                                    	jMonthcell.click();
                                        Thread.sleep(5000);
                                        int joinDay = 1;
                                        WebElement jdatecalender = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody"));
                                        // count rows with size() method
                                        List <WebElement> jnrows = jdatecalender.findElements(By.tagName("tr"));
                                        for(WebElement jnrow: jnrows){
                                            List<WebElement> jnCells = jnrow.findElements(By.tagName("td"));
                                            for(WebElement jnCell:jnCells){
                                            	if (jnCell.getText().contains(Integer.toString(joinDay)))
                                            	{
                                            		jnCell.click();
                                            		IsJoinDateClicked = true;
                                                 	break outerloop;
                                            	}
                                               
                                            }
                                        }                                                                                                                  
                                    }
                                }
                            }
                        }
                    }
                }
          		break;     
    	}
    	else {
    		if((Integer.parseInt(jDateRangepartss[0])-joinYear)>0){
    			driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[2]")).click();
        		continue;
    		}
    		else {
    			driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[3]")).click();
        		continue;
    		}                 
    	}
    }
    
    Thread.sleep(5000);
   	}
	@Test(priority=5, description=" Verify that on clicking the date field, a calendar should open(last date of working)")
   	public void TC_06() throws InterruptedException {
   	driver.findElement(By.xpath("//*[@id=\"mat-input-20\"]")).click();
   	     
   	}
	@Test(priority=6, description="to verify the current month date and system date(last date of working)")
	public void TC_07() throws InterruptedException {
		WebElement lastCsalelement = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/mat-calendar-header/div/div/button[1]/span"));
	    String defaultLDW = lastCsalelement.getText();
	    System.out.println("default date: " + defaultLDW);
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");
	    LocalDate now = LocalDate.now();
	    System.out.println("System date: " + now);
	    String date1 = formatter.format(now).toUpperCase();
	    System.out.println(date1);
	    
	  //to verify the current month date and system date
		if(!defaultLDW.equals(date1)) {
		Assert.fail("date is not matched "); }
		else {
		System.out.println("date matched");
		}    
	}
	@Test(priority=7, description= "Ckeck that user is able to select the Last date of working")
	public void TC_08() throws InterruptedException {
    //Last date of working
    boolean IsLastDateClicked=false;
    int lastYear = 2022;
    driver.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/mat-calendar-header/div/div/button[1]")).click();
    
    while(true) {
    	if(IsLastDateClicked) {
    		break;
    	}
    	WebElement lastCalelement = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/mat-calendar-header/div/div/button[1]/span"));
        String ldtr = lastCalelement.getText();
        System.out.println(ldtr);
        String[] lDateRangepartss = ldtr.split(" � ");
        System.out.println(lDateRangepartss[0]);
        System.out.println(lDateRangepartss[1]);
        outerloop:       
    	if(lastYear >= Integer.parseInt(lDateRangepartss[0]) && lastYear <= Integer.parseInt(lDateRangepartss[1]) && !IsLastDateClicked ) {
                //Store each year in string array
                List < WebElement > lcols;
                //Get value from Table each cell
                WebElement lt = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/div/mat-multi-year-view/table/tbody"));
                // count rows with size() method
                List < WebElement > lrws = lt.findElements(By.tagName("tr"));                          		
                for (WebElement row: lrws) {
                    List < WebElement > lCells = row.findElements(By.tagName("td"));
                    for (WebElement lCell: lCells) {
                        if (lCell.getText().contains(Integer.toString(lastYear))) {
                            lCell.click();
                            Thread.sleep(5000);
                            int lastMonth = 1;
                            String lMonth = ReturnMonthSelected(lastMonth);
                            WebElement tm = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-year-view/table/tbody"));
                            // count rows with size() method
                            List <WebElement> lmonthsrows = tm.findElements(By.tagName("tr"));
                            for (WebElement lmonthrow: lmonthsrows) {
                                List < WebElement > lMonthCells = lmonthrow.findElements(By.tagName("td"));
                                for (WebElement lMonthcell: lMonthCells) {
                                    if (lMonthcell.getText().contains(lMonth)) {
                                    	lMonthcell.click();
                                        Thread.sleep(5000);
                                        int lastDay = 10;
                                        WebElement ldatecalender = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div"));
                                        // count rows with size() method
                                        List <WebElement> lrows = ldatecalender.findElements(By.tagName("tr"));
                                        for(WebElement lrow: lrows){
                                            List<WebElement> ltCells = lrow.findElements(By.tagName("td"));
                                            for(WebElement ltCell:ltCells){
                                            	if (ltCell.getText().contains(Integer.toString(lastDay)))
                                            	{
                                            		ltCell.click();
                                            		IsLastDateClicked = true;
                                                 	break outerloop;
                                            	}
                                               
                                            }
                                        }                                                                                                                  
                                    }
                                }
                            }
                        }
                    }
                }
          		break;     
    	}
    	else {
    		if((Integer.parseInt(lDateRangepartss[0])-lastYear)>0){
    			driver.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/mat-calendar-header/div/div/button[2]")).click();
        		continue;
    		}
    		else {
    			driver.findElement(By.xpath("//*[@id=\"mat-datepicker-2\"]/mat-calendar-header/div/div/button[3]")).click();
        		continue;
    		}                      
    	}
    }
    
    Thread.sleep(5000);
   	}
    @Test(priority=8, description="enter the other fields")
   	public void TC_09() throws InterruptedException {
   //country
    driver.findElement(By.xpath("//*[@id=\"mat-select-10\"]/div/div[1]/span")).click();
    WebElement searchCountry= driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/div[1]/mat-form-field/div/div[1]/div/input"));
    searchCountry.sendKeys("India");
    Thread.sleep(5000);
    driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/div[2]/div/mat-option[2]/span")).click();  
    Thread.sleep(5000);
    
   //State  
    driver.findElement(By.xpath("//*[@id=\"mat-select-11\"]/div/div[1]/span")).click();
    Thread.sleep(5000);
    WebElement searchelement = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/div[1]/mat-form-field/div/div[1]/div/input"));
    searchelement.sendKeys("Maharashtra");
    Thread.sleep(2000);
    driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/div[2]/div/mat-option[2]/span")).click();
    Thread.sleep(5000);
    
    //city
    driver.findElement(By.xpath("//*[@id=\"mat-select-12\"]/div/div[1]/span")).click(); 
    Thread.sleep(5000);
    WebElement searchCity = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/div[1]/mat-form-field/div/div[1]/div/input"));
    searchCity.sendKeys("Mumbai");
    Thread.sleep(5000);
    driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/div/div/div[2]/div/mat-option[2]/span")).click();//mumbai
    Thread.sleep(5000);
 /*   
    //zip code
    WebElement zCode=driver.findElement(By.xpath("//*[@id=\"mat-input-16\"]"));
    zCode.clear();
    Thread.sleep(3000);
    zCode.sendKeys("420320");
    Thread.sleep(5000);

    //street address
    WebElement streetAddress=driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-users-edit/div/form/div/div/div[16]/mat-form-field/div/div[1]/div/input"));
    streetAddress.clear();
    Thread.sleep(3000);
    streetAddress.sendKeys("zxcvbnmm");
    Thread.sleep(5000);

    //Assign manager  
    driver.findElement(By.xpath("//*[@id=\"mat-select-14\"]/div/div[1]/span")).click();
    driver.findElement(By.xpath("//*[@id=\"mat-option-756\"]/span")).click();
    Thread.sleep(5000);
    //territory
    driver.findElement(By.xpath("//*[@id=\"mat-select-13\"]/div/div[1]/span")).click(); 
    Thread.sleep(5000); */
    }
    
    @Test(priority=9, description="Verify user is able to click 'access web login' toggle button")
   	public void TC_10() throws InterruptedException {
    	driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-users-edit/div/form/div/div/div[18]/div/mat-slide-toggle/label/div/div/div[1]")).click();
    	Thread.sleep(5000); 
    }
    @Test(priority=10, description="Verify user is able to click the update button successfully")
   	public void TC_11() throws InterruptedException {
    //update button
    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-users-edit/div/div/div/button[2]/span")).click(); 
    Thread.sleep(5000);        
    String toastMessage = driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-users-edit/div/div/div")).getText();
    System.out.println(toastMessage);
    Thread.sleep(5000);
}
    @Test(priority=11,description="sign out")
    public void TC_12() throws InterruptedException {
  	  driver.findElement(By.xpath("//div[@class='kt-header__topbar-user']")).click();
  	  Thread.sleep(5000);
  	  WebElement signOut=driver.findElement(By.xpath("//a[@class='btn btn-outline-brand btn-upper btn-sm btn-bold']"));
  	  Actions action=new Actions(driver);
  	  action.moveToElement(signOut).perform();
  	  Thread.sleep(3000);
  	  signOut.click();
  	  Thread.sleep(5000);
  	  System.out.println("signout successfully");
  	Thread.sleep(5000);
    }
    
    @Test(priority=12,description="Sign In")
    public void TC_13() throws InterruptedException {
    	Thread.sleep(4000);
    //web access login is off , Enter that email
  	driver.findElement(By.id("mat-input-21")).sendKeys("testlead12@nimapinfotech.com"); //email
  	Thread.sleep(2000);
  	driver.findElement(By.id("mat-input-22")).sendKeys("123456789"); //password
  	Thread.sleep(2000);
  //Enter the valid Credentials in the field and Click on Sign In button
  	driver.findElement(By.id("mat-input-23")).click(); //sign in button
  	Thread.sleep(5000);
  //Enter the valid Credentials in the field and Click on Sign In with OTP button
  	driver.findElement(By.xpath("//button[contains(text(),'Sign In With OTP')]")).click();
  	//OTP
  	Scanner sc = new Scanner(System.in);
	System.out.println("Please enter the Userinput");
	String userInput= sc.nextLine();
	System.out.println("userInput is" + userInput);
	WebElement otp=driver.findElement(By.id("mat-input-8")); 
	otp.sendKeys(userInput);
	Thread.sleep(2000);
	//submit button
	driver.findElement(By.id("kt_login_signin_submit")).click();	
	Thread.sleep(5000);
    }
 //@AfterTest
public void closeBrowser() throws InterruptedException {
	  Thread.sleep(5000);
	  driver.close();
	  System.out.println("AfterTest is running");
}
}
