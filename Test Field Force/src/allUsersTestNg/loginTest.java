package allUsersTestNg;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class loginTest {
       @Test
  public void verifyLoginPage() throws InterruptedException {
    	   System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
   		ChromeOptions options= new ChromeOptions();
   		options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});

   		WebDriver driver =new ChromeDriver(options);
   		driver.manage().window().maximize();
		driver.get("https://testffc.nimapinfotech.com/auth/login");
		System.out.println("Verify login page test started");
		driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
		Thread.sleep(3000);
	    driver.findElement(By.id("mat-input-1")).sendKeys("12345678");    //password
		Thread.sleep(5000);
		//sign in button
		driver.findElement(By.id("kt_login_signin_submit")).click();  
		Thread.sleep(5000);
		//Sign In With OTP
		driver.findElement(By.xpath("//button[contains(text(),'Sign In With OTP')]")).click();
	  	//OTP
	  	Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the OTP");
		String userInput= sc.nextLine();
		System.out.println("userInput is" + userInput);
		WebElement otp=driver.findElement(By.id("mat-input-8")); 
		otp.sendKeys(userInput);
		Thread.sleep(2000);
		//submit button
		driver.findElement(By.id("kt_login_signin_submit")).click();	
		Thread.sleep(5000);
  }
     @Test
  public void verifyForgotPasswordPage() throws InterruptedException {
	  System.out.println("Verify Forgot password page test started");
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
		ChromeOptions options= new ChromeOptions();
		options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});

		WebDriver driver =new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://testffc.nimapinfotech.com/auth/login");

		driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
		Thread.sleep(3000);
		//Forgot password 
	    driver.findElement(By.xpath("//*[@id=\"kt_login\"]/div/div[2]/kt-login/div[2]/div/form/div[3]/a")).click();	
	    Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("9301398747");//mob
		Thread.sleep(5000);
		driver.findElement(By.id("kt_login_signin_submit")).click(); 	// submit button
		//OTP
		Scanner sc1 = new Scanner(System.in);
		System.out.println("Please enter the OTP");
		String OTP= sc1.nextLine();
		System.out.println("OTP is" + OTP);
		WebElement otp1=driver.findElement(By.id("mat-input-3")); 
		otp1.sendKeys(OTP);
		//newpassword
		System.out.println("Please enter the new password");
		String newpassword= sc1.nextLine();
		System.out.println("newpassword is" + newpassword);
		WebElement newpass=driver.findElement(By.id("mat-input-4")); 
		newpass.sendKeys(newpassword);
		//confirm password
		System.out.println("Please enter the Confirm password");
		String confirmPassword= sc1.nextLine();
		System.out.println("password is" + confirmPassword);
		WebElement confirmPass=driver.findElement(By.id("mat-input-5")); 
		confirmPass.sendKeys(confirmPassword);
		Thread.sleep(5000);
		//submit button
		driver.findElement(By.xpath("//*[@id=\"kt_login_signin_submit\"]")).click();	
  }
}
