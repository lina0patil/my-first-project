package allUsersTestNg;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class myTeamIETest {
	 public static WebDriver driver;
	 @BeforeTest
	 public void startBrowser() throws InterruptedException {
     // System Property for IEDriver   
     System.setProperty("webdriver.ie.driver", "C:\\Users\\lenovo\\Driver/IEDriverServer.exe");  
      
     // Instantiate a IEDriver class.       
     driver=new InternetExplorerDriver(); 
     driver.manage().window().maximize();
	 }

    @Test
    public void navigateToUrl() throws InterruptedException {
    //navigate to the web page
    driver.get("https://testffc.nimapinfotech.com/auth/login");	    
    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    //to verify the title and url of login page
    String url= driver.getCurrentUrl();
    System.out.println(url);
    if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
    Assert.fail("url is not matched "); }
    else {
    System.out.println("url matched");
    }
    String title= driver.getTitle();
    System.out.println(title);
    if(!title.equalsIgnoreCase("Test Field Force")) {
    Assert.fail("title is not matched"); }
    else {
    System.out.println("title matched");
    }
  
   //login -enter the email and password & click on sign in button 
   driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
   Thread.sleep(2000);
   driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
   Thread.sleep(2000);
   driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
   System.out.println("login done successfully");
   Thread.sleep(3000);
   }

    @Test(priority=0)
    public void myTeam() throws InterruptedException  {
    driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
    WebElement myTeam= driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/kt-aside-left/div/div/div/ul/li[3]/a/span"));
    Actions act=new Actions(driver);             
    act.moveToElement(myTeam).perform();       
    Thread.sleep(3000);
    myTeam.click();
    Thread.sleep(3000);
    }
    @Test(priority=1)
    public void myTeam_location() throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    WebElement myTeam_location= driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/mat-tab-header/div[2]/div/div/div[3]/div"));
    Actions act=new Actions(driver);
    act.moveToElement(myTeam_location).perform(); 
    Thread.sleep(3000);
    myTeam_location.click();
    Thread.sleep(3000);				
    }
    @Test(priority=2)
    public void nameofUser() throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    WebElement nameofUser= driver.findElement(By.xpath("//div[contains(text(),('Lina Patil'))]"));
    Actions act=new Actions(driver);
    act.moveToElement(nameofUser).perform(); 
    Thread.sleep(3000);
    nameofUser.click();
    Thread.sleep(3000);	 

    String punchINStatus=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[2]/td[4]/div/span")).getText();
    System.out.println("Punch In Status= "+punchINStatus);

    String PunchInDate=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[2]/td[1]/span")).getText();
    System.out.println("Punch In Date= "+PunchInDate);

    String PunchInTime=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[2]/td[2]/span")).getText();
    System.out.println("Punch In Time= "+PunchInTime);

    String PunchInLocation=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[2]/td[3]/span")).getText();
    System.out.println("Punch In Location= "+PunchInLocation);


    /*
    String punchOutStatus=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[1]/td[4]/div/span")).getText();
    System.out.println("Punch Out Status= "+punchOutStatus);

    String PunchOutDate=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[1]/td[1]/span")).getText();
    System.out.println("Punch Out Date= "+PunchOutDate);

    String PunchOutTime=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[1]/td[2]/span")).getText();
    System.out.println("Punch Out Time= "+PunchOutTime);

    String PunchOutLocation=driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-team-list/div/div[2]/kt-portlet/div/kt-portlet-body/kt-team-actions/kt-portlet/div/kt-portlet-body/mat-tab-group/div/mat-tab-body[3]/div/kt-location/div/div/div/table/tbody/tr[1]/td[3]/span")).getText();
    System.out.println("Punch Out Location= "+PunchOutLocation);
     */
     }
     }
