package allUsersTestNg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class logout {
	public static WebDriver driver;
	
  @BeforeTest
  public void beforeTest() throws InterruptedException {
	//launching the browser
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
	  ChromeOptions options= new ChromeOptions();
	  Map<String, Object>prefs=new HashMap<String, Object>();
	  prefs.put("credentials_enable_service", false);
	  prefs.put("profile.password_manager_enabled", false);
	  options.setExperimentalOption("prefs", prefs);
	  options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
      driver = new ChromeDriver(options);
      driver.manage().window().maximize();
      
//navigate to the web page
      driver.get("https://app.fieldforceconnect.com/auth/login");//to verify the title and url of login page
	  String url= driver.getCurrentUrl();
	  System.out.println(url);
	  if(!url.equalsIgnoreCase("https://app.fieldforceconnect.com/auth/login")) {
		  Assert.fail("url is not matched "); }
	  else {
		  System.out.println("url matched"); }
	  String title= driver.getTitle();
	  System.out.println(title);
	  if(!title.equalsIgnoreCase("Field Force Connect")) {
		  Assert.fail("title is not matched"); }
	  else {
		  System.out.println("title matched");}
	  
//login -enter the email and password & click on sign in button 
	  driver.findElement(By.id("mat-input-0")).sendKeys("9519519519"); //email
	  Thread.sleep(2000);
	  driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
	  Thread.sleep(2000);
	  driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	  System.out.println("login done successfully");
	  Thread.sleep(4000);
  }
 // @Test(priority=0)
  public void countField() {
	  List<WebElement>elements=driver.findElements(By.xpath("//*"));
	     System.out.println("total field=" + Integer.toString(elements.size()));
  }
  @Test(priority=1)
  public void signout() throws InterruptedException {
	  driver.findElement(By.xpath("//div[@class='kt-header__topbar-user']")).click();
	  Thread.sleep(5000);
	  WebElement signOut=driver.findElement(By.xpath("//a[@class='btn btn-outline-brand btn-upper btn-sm btn-bold']"));
	  Actions action=new Actions(driver);
	  action.moveToElement(signOut).perform();
	  Thread.sleep(3000);
	  signOut.click();
	  System.out.println("signout successfully");
  }

  /*
  @AfterTest
  public void closeBrowser() throws InterruptedException {
  	   Thread.sleep(5000);
  	   driver.close();
  	   System.out.println("AfterTest is running");
  }*/
}
