package allUsersTestNg;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class bulkUploadTest3 {
	    public static WebDriver driver;
	
	    @BeforeTest
		public void beforeTest() throws InterruptedException {
	//launching the browser
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
		ChromeOptions options= new ChromeOptions();
	    Map<String, Object>prefs=new HashMap<String, Object>();
	    prefs.put("credentials_enable_service", false);
	    prefs.put("profile.password_manager_enabled", false);
	    options.setExperimentalOption("prefs", prefs);
	    options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		      
	//navigate to the web page
		driver.get("https://app.fieldforceconnect.com/auth/login");//to verify the title and url of login page
	    String url= driver.getCurrentUrl();
		System.out.println(url);
		if(!url.equalsIgnoreCase("https://app.fieldforceconnect.com/auth/login")) {
		Assert.fail("url is not matched "); }
		else {
		System.out.println("url matched"); }
		String title= driver.getTitle();
		System.out.println(title);
		if(!title.equalsIgnoreCase("Field Force Connect")) {
		Assert.fail("title is not matched"); }
		else {
		System.out.println("title matched");}
			  
	//login -enter the email and password & click on sign in button 
		driver.findElement(By.id("mat-input-0")).sendKeys("9519519519"); //email
		Thread.sleep(2000);
		driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
		Thread.sleep(2000);
		
		driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
		System.out.println("login done successfully");
		Thread.sleep(4000);
		//navigate to homepage
	    WebElement slider = driver.findElement(By.className("ps__thumb-y"));
	    Actions act = new Actions(driver);
	    Thread.sleep(4000);
   //move down the slider by using method
	    act.clickAndHold(slider).moveByOffset(0, 300).release().perform();
	    Thread.sleep(4000);
   //click on settings
	    driver.findElement(By.xpath("//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")).click();
	    Thread.sleep(3000);
   //move down the slider by using method
	    act.clickAndHold(slider).moveByOffset(0, 50).release().perform();
	    Thread.sleep(3000);
   //click on all users
	    driver.findElement(By.xpath("//*[@id=\"kt_aside_menu\"]/ul/li[18]/div/ul/li[1]/a/span")).click();
	    Thread.sleep(4000);
	    System.out.println("Before Test is running"); 
	    Thread.sleep(4000);
	  }
		  
  @Test
  public void bulkUpload() throws InterruptedException, AWTException {
	//click on bulk upload 
	    driver.findElement(By.xpath("//span[contains(text(),'Bulk Upload')]")).click();
	    Thread.sleep(4000);
    
	// Click to download All Users
	    driver.findElement(By.xpath("//div[@class='col-15 text-center py-2']//b[contains(text(),'Click Here')]")).click();
	    Thread.sleep(4000);
	   // driver.switchTo().alert().accept();
	    Thread.sleep(4000);
	//Click to download sample file
	    driver.findElement(By.xpath("//a[contains(text(),'Click to download sample file')]")).click();
	    Thread.sleep(4000);	    
/*	//Get the input field "Attach Excel File"   
	    WebElement attachExcelFile= driver.findElement(By.xpath("//button[@class='mat-raised-button mat-stroked-button mat-button-base ng-star-inserted']//span[contains(text(),' Attach Excel File')]"));
	    attachExcelFile.click();
	    
	 /*   // String path= "C:\\Users\\Nimap\\Users.xlsx";
        //attachExcelFile.sendKeys(path);
		Thread.sleep(5000);
	//Enter the file path onto the file selection input field
		attachExcelFile.sendKeys("C:/Users/Nimap/Bulk Upload - CRM Team With Managers.xlsx");
		attachExcelFile.sendKeys(Keys.ENTER);*/
	 /*   
	    StringSelection strSel = new StringSelection("Bulk Upload - CRM Team With Managers");
	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
	    Thread.sleep(3000);
	    Robot robot = new Robot();
	    Thread.sleep(3000);
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);

	    Thread.sleep(3000);
	    robot.keyPress(KeyEvent.VK_ENTER);
	    Thread.sleep(3000);
	    robot.keyRelease(KeyEvent.VK_ENTER);
	    Thread.sleep(3000);
	//click the upload button
		driver.findElement(By.xpath("//span[contains(text(),'Upload')]")).click();*/
	//printing message
		 //String toastMessage = driver.findElement(By.xpath("//*[@id=\"toast-container\"]")).getText();
	     //System.out.println(toastMessage);
	     Thread.sleep(5000);
	//click on show report
		driver.findElement(By.xpath("//span[contains(text(), 'Show Report')]")).click();
		Thread.sleep(5000);
	//click to search
		WebElement searchUser=driver.findElement(By.xpath("//*[@id=\"mat-input-7\"]"));
	    searchUser.sendKeys("lina");
	    Thread.sleep(3000);  
	    searchUser.clear();
	/*//click to download bulkupload report
		driver.findElement(By.xpath("//i[@class='fas fa-download' and @mattooltip='Dowload BulkUpload Report']")).click();
		Thread.sleep(5000);
	//click to download report file
		driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-show-users-report/kt-portlet/div/kt-portlet-body/div/mat-table/mat-row[6]/mat-cell[5]/i[2]")).click();
	//click on view report
		driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-show-users-report/kt-portlet/div/kt-portlet-body/div/mat-table/mat-row[1]/mat-cell[5]/button[1]/span")).click();
 */
	    
  }
}
