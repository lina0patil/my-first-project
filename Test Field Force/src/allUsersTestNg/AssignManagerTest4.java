package allUsersTestNg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class AssignManagerTest4 {
	public static WebDriver driver;

 @BeforeTest
	    public void beforeClass() throws InterruptedException {
//launching the browser
		  System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
		  ChromeOptions options= new ChromeOptions();
		  Map<String, Object>prefs=new HashMap<String, Object>();
		  prefs.put("credentials_enable_service", false);
		  prefs.put("profile.password_manager_enabled", false);
		  options.setExperimentalOption("prefs", prefs);
		  options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
	      driver = new ChromeDriver(options);
	      driver.manage().window().maximize();
//navigate to the web page
	      driver.get("https://app.fieldforceconnect.com/auth/login");
	                  
//to verify the title and url of login page
		  String url= driver.getCurrentUrl();
		  System.out.println(url);
		  if(!url.equalsIgnoreCase("https://app.fieldforceconnect.com/auth/login")) {
			  Assert.fail("url is not matched "); }
		  else {
			  System.out.println("url matched");
		  }
		  String title= driver.getTitle();
		  System.out.println(title);
		  if(!title.equalsIgnoreCase("Field Force Connect")) {
			  Assert.fail("title is not matched"); }
		  else {
			  System.out.println("title matched");
	  }
	  
//login -enter the email and password & click on sign in button 
		driver.findElement(By.id("mat-input-0")).sendKeys("9519519519"); //email
		Thread.sleep(2000);
	    driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
	    Thread.sleep(2000);
	    driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	    System.out.println("login done successfully");
	    Thread.sleep(5000);

//navigate to homepage
	    WebElement slider = driver.findElement(By.className("ps__thumb-y"));
	    Actions act = new Actions(driver);
	    Thread.sleep(4000);
//move down the slider by using method
	    act.clickAndHold(slider).moveByOffset(0, 300).release().perform();
	    Thread.sleep(4000);
//click on settings
	    driver.findElement(By.xpath("//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")).click();
	    Thread.sleep(3000);
//move down the slider by using method
	    act.clickAndHold(slider).moveByOffset(0, 50).release().perform();
	    Thread.sleep(3000);
//click on all users
	    driver.findElement(By.xpath("//*[@id=\"kt_aside_menu\"]/ul/li[18]/div/ul/li[1]/a/span")).click();
	    Thread.sleep(4000);
	    System.out.println("Before Test is running"); 
	  }
 
      //  @Test(priority=0)
        public void export() throws InterruptedException {
		driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-header/div[4]/button[3]/span")).click();
        Thread.sleep(5000);
        System.out.println("Export- test case done successfully");
}

   //     @Test(priority=1)
        public void assignTeam() throws InterruptedException {
        WebElement assignTeam=driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-body/div[1]/mat-table/mat-row[1]/mat-cell[6]/button/span"));
        Actions action=new Actions(driver);
	    action.moveToElement(assignTeam).perform();
	    Thread.sleep(3000);
        assignTeam.click();
        //select the checkbox  
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-assign-manager-dialog/div/form/div[1]/div/div/div[2]/label/span[2]")).click();
        Thread.sleep(5000);
        //click on assign button        
        driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-assign-manager-dialog/div/form/div[2]/div/button[2]/span")).click();
        Thread.sleep(5000);
        /*//verify the message
	    String teamconfirmationMessage = driver.findElement(By.xpath("/html/body/div[4]/div")).getText();
	    System.out.println("teamconfirmationMessage");
	    if(teamconfirmationMessage.equals("Team assigned Successfully")) {
	    System.out.println("assign team successful-passed");}
	    else{
		System.out.println("assign team unsuccessful-failed");
	    }
	    Thread.sleep(5000);*/
        }
 //@Test(priority=2)
        public void assignManager() throws InterruptedException {
     //click on assign manager
        WebElement element =driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-body/div[1]/mat-table/mat-row[1]/mat-cell[8]/div/i"));
        Thread.sleep(2000);
        Actions action=new Actions(driver);
        action.moveToElement(element).perform();
        Thread.sleep(3000);
        element.click();
        WebElement searchAssign=driver.findElement(By.xpath("//*[@id=\"mat-input-6\"]"));
        searchAssign.sendKeys("rijo");
       driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/mat-dialog-container/kt-assign-manager-dialog/div/form/div[1]/div/div/mat-radio-group/div/label/span[2]/mat-radio-button/label/div[1]/div[2]")).click();//radiobutton
    //click on assign button            
       driver.findElement(By.xpath("//*[@id=\"mat-dialog-2\"]/kt-assign-manager-dialog/div/form/div[2]/div/button[2]/span")).click();
 }
        @Test(priority=4)
        public void userStatus() throws InterruptedException {
        //click on user status	
        driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-body/div[1]/mat-table/mat-row[1]/mat-cell[7]/mat-slide-toggle/label/div/div/div[1]")).click();
        Thread.sleep(5000);
        //click on ok button
        driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-users-edit/div/div[2]/div/button[2]/span")).click();
        Thread.sleep(5000);
}
        @Test(priority=5)
        public void searchField() throws InterruptedException {
 	    WebElement searchUser=driver.findElement(By.xpath("//*[@id=\"mat-input-0\"]"));
 	    searchUser.sendKeys("lina");
 	    Thread.sleep(3000);
 	    searchUser.clear();
        Thread.sleep(8000);
        }
       @Test(priority=6)
        public void filter() throws InterruptedException {
      //click on filter
	    driver.findElement(By.xpath("//*[@id=\"filterDropdown\"]/span")).click();
	    Thread.sleep(5000);
    //click on By user status
		WebElement status=driver.findElement(By.xpath("//*[@id=\"mat-select-4\"]/div/div[1]/span"));
		status.click();
		Thread.sleep(3000);    
     //click on inactive user
	    driver.findElement(By.xpath("//*[@id=\"mat-option-2\"]/span")).click();
	    Thread.sleep(3000);  
      //click on apply filter button
	    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[2]/div/button[2]/span")).click();
	    Thread.sleep(5000);
	 //user status
	    driver.findElement(By.xpath("//*[@id=\"mat-slide-toggle-4\"]/label/div/div/div[1]")).click();
        Thread.sleep(5000);
    //click on ok button
        driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-users-edit/div/div[2]/div/button[2]/span")).click();
        Thread.sleep(5000); 
    //click on clear filter button
	    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-filter/div[2]/div/div/div/div/div[2]/button/h6")).click();	  
	    Thread.sleep(3000);          
	/*    
//click on filter
	
	    driver.findElement(By.xpath("//*[@id=\"filterDropdown\"]/span")).click();
	    Thread.sleep(3000);
//country
	    driver.findElement(By.xpath("//*[@id=\"mat-select-0\"]/div/div[1]/span")).click();
	    Thread.sleep(5000);
	    WebElement searchCountry = driver.findElement(By.xpath("//*[@id=\"mat-input-1\"]"));
	    searchCountry.sendKeys("India");
	    Thread.sleep(4000);
	    //driver.findElement(By.className("mat-option-ripple mat-ripple")).click();
	    driver.findElement(By.xpath("//*[@id=\"mat-option-112\"]/span")).click();
	    Thread.sleep(5000);
//State  
	    driver.findElement(By.xpath("//*[@id=\"mat-select-1\"]/div/div[1]/span")).click();
	    Thread.sleep(3000);
	    WebElement searchState = driver.findElement(By.xpath("//*[@id=\"mat-input-2\"]"));
	    searchState.sendKeys("Maharashtra");
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//*[@id=\"mat-option-282\"]/span")).click();
	    Thread.sleep(3000);
//city  
	    driver.findElement(By.xpath("//*[@id=\"mat-select-2\"]/div/div[1]/span")).click();
	    Thread.sleep(3000);
	    WebElement searchCity = driver.findElement(By.xpath("//*[@id=\"mat-input-3\"]"));
	    searchCity.sendKeys("Pune");
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//*[@id=\"mat-option-591\"]/span")).click();
	    Thread.sleep(3000);
//manager
        driver.findElement(By.xpath("//*[@id=\"mat-select-3\"]/div/div[1]/span")).click();
		Thread.sleep(3000);
		WebElement searchManager = driver.findElement(By.xpath("//*[@id=\"mat-input-5\"]"));
		searchManager.sendKeys("Rijo");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"mat-option-259\"]/span")).click();
		Thread.sleep(3000);
//click on By user status
		//WebElement status=driver.findElement(By.xpath("//*[@id=\"mat-select-4\"]/div/div[1]/span"));
		status.click();
		Thread.sleep(3000);
//click on active user
	    driver.findElement(By.xpath("//*[@id=\"mat-option-1\"]/span")).click();
	  Thread.sleep(3000);
//My roles
	    driver.findElement(By.xpath("//*[@id=\"mat-select-5\"]/div/div[1]/span")).click();
		driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/div/div/mat-option[2]/mat-pseudo-checkbox")).click();//click on the checkbox
		driver.findElement(By.xpath("//*[@id=\"mat-option-10\"]/mat-pseudo-checkbox")).click();
		Thread.sleep(5000); 
//side click
		driver.findElement(By.xpath("/html/body/div[3]/div[1]")).click();
		
//apply filter
	    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[2]/div/button[2]/span")).click();
		Thread.sleep(5000);*/
}
/*
@AfterTest
public void closeBrowser() throws InterruptedException {
	   Thread.sleep(5000);
	   driver.close();
	   System.out.println("AfterTest is running");
}*/
}


