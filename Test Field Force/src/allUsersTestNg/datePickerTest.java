package allUsersTestNg;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

    public class datePickerTest {
	public static String returnMonthSelected(int mnth) {
	      switch (mnth) {
	          case 1:
	              return "JAN";
	          case 2:
	              return "FEB";
	          case 3:
	              return "MAR";
	          case 4:
	              return "APR";
	          case 5:
	              return "MAY";
	          case 6:
	              return "JUN";
	          case 7:
	              return "JUL";
	          case 8:
	              return "AUG";
	          case 9:
	              return "SEP";
	          case 10:
	              return "OCT";
	          case 11:
	              return "NOV";
	          case 12:
	              return "DEC";
	          default:
	              return "Invalid input - Wrong month number.";
	      }
	  }
	public static WebDriver driver;
	@BeforeTest
	public void launchingBrowser() throws InterruptedException {
//launching the browser
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
	ChromeOptions options= new ChromeOptions();
	Map<String, Object>prefs=new HashMap<String, Object>();
	prefs.put("credentials_enable_service", false);
	prefs.put("profile.password_manager_enabled", false);
	options.setExperimentalOption("prefs", prefs);
	options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
	driver = new ChromeDriver(options);
	driver.manage().window().maximize();
	
//navigate to the web page
	driver.get("https://testffc.nimapinfotech.com/auth/login");	    
	
//to verify the title and url of login page
	String url= driver.getCurrentUrl();
	System.out.println(url);
	if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
	Assert.fail("url is not matched "); }
	else {
	System.out.println("url matched");
	}
    String title= driver.getTitle();
	System.out.println(title);
	if(!title.equalsIgnoreCase("Test Field Force")) {
	Assert.fail("title is not matched"); }
	else {
	System.out.println("title matched");
	}
	  
//login -enter the email and password & click on sign in button 
	driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
	Thread.sleep(2000);
	driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
	Thread.sleep(2000);
	driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	System.out.println("login done successfully");
	Thread.sleep(6000);
	}
	@Test(priority=0, description="navigateToAllUser")
	public void TC_01() throws InterruptedException {
//navigate to homepage
	WebElement slider = driver.findElement(By.className("ps__thumb-y"));
	Actions act = new Actions(driver);
	Thread.sleep(4000);//move down the slider by using method
	act.clickAndHold(slider).moveByOffset(0, 300).release().perform();
	Thread.sleep(4000);
//click on settings
	driver.findElement(By.xpath("//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")).click();
	Thread.sleep(3000);
//move down the slider by using method
	act.clickAndHold(slider).moveByOffset(0, 50).release().perform();
	Thread.sleep(3000);
//click on all users
	driver.findElement(By.xpath("//*[@id=\"kt_aside_menu\"]/ul/li[18]/div/ul/li[1]/a/span")).click();
	Thread.sleep(10000);
	System.out.println("Before Test is running"); 
//add user
    driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-header/div[4]/button[1]/span")).click();
    Thread.sleep(5000);
	  }
	@Test(priority=1, description=" Verify that on clicking the date field, a calendar should open")
	public void TC_02() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"mat-input-12\"]")).click();
	     
	}
	@Test(priority=2, description="to verify the default date and system date")
	public void TC_03() throws InterruptedException {
		WebElement joinCalelement = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/mat-calendar-header/div/div/button[1]/span"));
	    String defaultDOJ = joinCalelement.getText();
	    System.out.println("default date: " + defaultDOJ);
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd yyyy");
	    LocalDate now = LocalDate.now();
	    System.out.println("System date: " + now);
	    String date1 = formatter.format(now).toUpperCase();
	    System.out.println(date1);
	    
	  //to verify the default date and system date
		if(!defaultDOJ.equals(date1)) {
		Assert.fail("date is not matched "); }
		else {
		System.out.println("date matched");
		}    
	}
	@Test(priority=3)
	public void TC_04() throws InterruptedException {
	driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[1]")).click();
	boolean IsJoinDateClicked=false;
	int joinYear = 2021;
	while(true) {
    if(IsJoinDateClicked) {
      		break;
    }
    WebElement joinCalelement = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[1]/span"));
    String jdtr = joinCalelement.getText();
    System.out.println(jdtr);
    String[] jDateRangepartss = jdtr.split(" � ");
    System.out.println(jDateRangepartss[0]);
    System.out.println(jDateRangepartss[1]);
    outerloop:       
    if(joinYear >= Integer.parseInt(jDateRangepartss[0]) && joinYear <= Integer.parseInt(jDateRangepartss[1]) && !IsJoinDateClicked ) {
//Store each year in string array
    List < WebElement > jcols;
//Get value from Table each cell
    WebElement jt = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/div/mat-multi-year-view/table/tbody"));
    // count rows with size() method
                  List < WebElement > jrws = jt.findElements(By.tagName("tr"));                          		
                  for (WebElement row: jrws) {
                      List < WebElement > jCells = row.findElements(By.tagName("td"));
                      for (WebElement jCell: jCells) {
                          if (jCell.getText().contains(Integer.toString(joinYear))) {
                              jCell.click();
                              Thread.sleep(5000);
                              int joinMonth = 12;
                              String jMonth = returnMonthSelected(joinMonth);
                              WebElement tm = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-year-view/table/tbody"));
                              // count rows with size() method
                              List <WebElement> jmonthsrows = tm.findElements(By.tagName("tr"));
                              for (WebElement jmonthrow: jmonthsrows) {
                                  List < WebElement > jMonthCells = jmonthrow.findElements(By.tagName("td"));
                                  for (WebElement jMonthcell: jMonthCells) {
                                      if (jMonthcell.getText().contains(jMonth)) {
                                      	jMonthcell.click();
                                          Thread.sleep(5000);
                                          int joinDay = 1;
                                          WebElement bdatecalender = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody"));
                                          // count rows with size() method
                                          List <WebElement> brows = bdatecalender.findElements(By.tagName("tr"));
                                          for(WebElement brow: brows){
                                              List<WebElement> bCells = brow.findElements(By.tagName("td"));
                                              for(WebElement bCell:bCells){
                                              	if (bCell.getText().contains(Integer.toString(joinDay)))
                                              	{
                                              		bCell.click();
                                              		IsJoinDateClicked = true;
                                                   	break outerloop;
                                              	}
                                                 
                                              }
                                          }                                                                                                                  
                                      }
                                  }
                              }
                          }
                      }
                  }
            		break;     
      	}
      	else {
      		driver.findElement(By.xpath("//*[@id=\"mat-datepicker-1\"]/mat-calendar-header/div/div/button[2]")).click();
      		continue;                 
      	}
      }
	}}

