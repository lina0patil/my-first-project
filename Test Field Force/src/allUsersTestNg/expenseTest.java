package allUsersTestNg;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.google.common.collect.Table.Cell;

public class expenseTest {
	public static WebDriver driver;
	@BeforeTest
	public void beforeClass() throws InterruptedException {
	//launching the browser
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
	ChromeOptions options= new ChromeOptions();
	options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
	driver = new ChromeDriver(options);
	driver.manage().window().maximize();
			
	//navigate to the web page
	driver.get("https://testffc.nimapinfotech.com/auth/login");	    
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	//to verify the title and url of login page
	String url= driver.getCurrentUrl();
	System.out.println(url);
	if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
	Assert.fail("url is not matched "); }
	else {
	System.out.println("url matched");
	}
	String title= driver.getTitle();
	System.out.println(title);
	if(!title.equalsIgnoreCase("Test Field Force")) {
	Assert.fail("title is not matched"); }
	else {
	System.out.println("title matched");
	}
	
	//login -enter the email and password & click on sign in button 
	driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
	Thread.sleep(2000);
	driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
	Thread.sleep(2000);
	driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	System.out.println("login done successfully");
	Thread.sleep(3000);
	}
	@Test(priority=0)
	public void myExpense() throws InterruptedException  {
	driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	WebElement myExpense= driver.findElement(By.xpath("//span[contains(text(),('My Expense'))]"));
	Actions act=new Actions(driver);
	act.moveToElement(myExpense).perform(); 
	Thread.sleep(3000);
	myExpense.click();
	Thread.sleep(3000);	
	}
    @Test(priority=1)
	public void export() throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	WebElement export= driver.findElement(By.xpath("//span[contains(text(),('Export'))]"));
	Actions act=new Actions(driver);
	act.moveToElement(export).perform(); 
	Thread.sleep(3000);
	export.click();
	Thread.sleep(3000);			
	}
    @Test(priority=2)
 	public static void exportReadData()  throws InterruptedException, IOException {
    //copy path of excel
   // String path="C:\\Users\\lenovo\\git\\my-first-project\\Test Field Force\\Expense - Expected.xlsx";
    String path="C:/Users/lenovo/Downloads/Expense(1).xlsx";
    //create the object of file class to open xlsx file 
    File file=new File(path);
    	
    //to fetch the date from excel sheet/ to read the file
    FileInputStream fs=new FileInputStream(file);
    	
    XSSFWorkbook wb=new XSSFWorkbook(fs);
    	
    XSSFSheet sheet=wb.getSheetAt(0);
    for(Row row :sheet) {
    if(row.getRowNum()>1) {
    break;
    }
    else {
    for(org.apache.poi.ss.usermodel.Cell cell :row) {
    System.out.println(cell.getStringCellValue());
    }
    }
    } 
    }

  //  @Test
 	public void sameContentOfExcelFiles() throws IOException {
 	//String userDir = System.getProperty("user.dir");
 	Workbook wb1 = WorkbookFactory.create(new File("C:\\Users\\lenovo\\git\\my-first-project\\Test Field Force\\Expense - Expected.xlsx"));
 	Workbook wb2 = WorkbookFactory.create(new File("C:\\Users\\lenovo\\git\\my-first-project\\Test Field Force\\Expense - Actual.xlsx"));
 	//Workbook wb2 = WorkbookFactory.create(new File("C:/Users/lenovo/Downloads/Expense (1).xlsx"));
 	compareExcelFile compareExcelFiles = new compareExcelFile();
 	compareExcelFiles.verifyDataInExcelBookAllSheets(wb1, wb2);
 	}
    @Test(priority=3)
    public void verify_filter_for_pendingData() throws InterruptedException {
    //click on filter
    driver.findElement(By.xpath("//span[contains(text(),('Filter'))]")).click();
    Thread.sleep(5000);
    //click on By status
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[1]/div/div[2]/div[2]/div/div/mat-form-field/div/div[1]/div/mat-select/div/div[1]/span")).click();
    Thread.sleep(3000);  
    //Pending
    driver.findElement(By.xpath("//*[@id=\"mat-option-1\"]/span")).click();
    Thread.sleep(2000);
    Actions act= new Actions(driver);
    act.sendKeys(Keys.ESCAPE).build().perform();
    Thread.sleep(3000);  
    
    //click on apply filter button
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[2]/div/button[2]/span")).click();
    Thread.sleep(5000);
    driver.findElement(By.xpath("//span[contains(text(),('Export'))]")).click();
    compareExcelFile readXlsx = new compareExcelFile();
	System.out.println("Pending Data");
	readXlsx.readXLSXFile("C:\\Users\\lenovo\\git\\my-first-project\\Test Field Force\\Expense Pending Data.xlsx");
	Thread.sleep(5000);
	 //click on clear filter button
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[2]/div/div/div/div/div[2]/button")).click();	  
    Thread.sleep(3000); 
    }
    @Test(priority=4)
    public void verify_filter_for_rejectedData() throws InterruptedException {
    //click on filter
    driver.findElement(By.xpath("//span[contains(text(),('Filter'))]")).click();
    Thread.sleep(5000);
    //click on By status
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[1]/div/div[2]/div[2]/div/div/mat-form-field/div/div[1]/div/mat-select/div/div[1]/span")).click();
    Thread.sleep(3000);  
    //Rejected
    driver.findElement(By.xpath("//*[@id=\"mat-option-4\"]/span")).click();
    Thread.sleep(2000);
    Actions act= new Actions(driver);
    act.sendKeys(Keys.ESCAPE).build().perform();
    Thread.sleep(3000);  
    
    //click on apply filter button
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[2]/div/button[2]/span")).click();
    Thread.sleep(5000);
    driver.findElement(By.xpath("//span[contains(text(),('Export'))]")).click();
    compareExcelFile readXlsx = new compareExcelFile();
	System.out.println("Rejected Data");
	readXlsx.readXLSXFile("C:\\Users\\lenovo\\git\\my-first-project\\Test Field Force\\Expense Rejected Data.xlsx");
	
	Thread.sleep(5000);
	 //click on clear filter button
   driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[2]/div/div/div/div/div[2]/button")).click();	  
   Thread.sleep(3000); 
    } 	    
    @Test(priority=5)
    public void verify_filter_for_pendingReimbursementData() throws InterruptedException {
    //click on filter
    driver.findElement(By.xpath("//span[contains(text(),('Filter'))]")).click();
    Thread.sleep(5000);
    //click on By status
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[1]/div/div[2]/div[2]/div/div/mat-form-field/div/div[1]/div/mat-select/div/div[1]/span")).click();
    Thread.sleep(3000);  
    //pending ReimbursementData
    driver.findElement(By.xpath("//*[@id=\"mat-option-5\"]/span")).click();
    Thread.sleep(2000);
    Actions act= new Actions(driver);
    act.sendKeys(Keys.ESCAPE).build().perform();
    Thread.sleep(3000);  
    
    //click on apply filter button
    driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[1]/div/div/div/form/div[2]/div/button[2]/span")).click();
    Thread.sleep(5000);
    driver.findElement(By.xpath("//span[contains(text(),('Export'))]")).click();
	compareExcelFile readXlsx = new compareExcelFile();
	System.out.println("Pending Reimbursement Data");
	readXlsx.readXLSXFile("C:\\Users\\lenovo\\git\\my-first-project\\Test Field Force\\Expense Pending Reimbursement Data.xlsx");
   
	Thread.sleep(5000);
	 //click on clear filter button
   driver.findElement(By.xpath("/html/body/kt-base/div[2]/div/div/div/div/kt-expense/kt-portlet/div/kt-filter/div[2]/div/div/div/div/div[2]/button")).click();	  
   Thread.sleep(3000); 
    } 
    @AfterTest
    public void closeBrowser() throws InterruptedException {
  	 Thread.sleep(2000);
  	  driver.close();
  	  System.out.println("AfterTest is running");
    }
}


