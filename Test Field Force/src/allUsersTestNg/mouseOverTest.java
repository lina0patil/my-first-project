package allUsersTestNg;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class mouseOverTest {
	public static WebDriver driver;
	
@BeforeTest
	public void beforeTest() throws InterruptedException {
//launching the browser
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
	ChromeOptions options= new ChromeOptions();
    Map<String, Object>prefs=new HashMap<String, Object>();
    prefs.put("credentials_enable_service", false);
    prefs.put("profile.password_manager_enabled", false);
    options.setExperimentalOption("prefs", prefs);
    options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
	driver = new ChromeDriver(options);
	driver.manage().window().maximize();
	      
//navigate to the web page
	driver.get("https://app.fieldforceconnect.com/auth/login");//to verify the title and url of login page
    String url= driver.getCurrentUrl();
	System.out.println(url);
	if(!url.equalsIgnoreCase("https://app.fieldforceconnect.com/auth/login")) {
	Assert.fail("url is not matched "); }
	else {
	System.out.println("url matched"); }
	String title= driver.getTitle();
	System.out.println(title);
	if(!title.equalsIgnoreCase("Field Force Connect")) {
	Assert.fail("title is not matched"); }
	else {
	System.out.println("title matched");}
		  
//login -enter the email and password & click on sign in button 
	driver.findElement(By.id("mat-input-0")).sendKeys("9519519519"); //email
	Thread.sleep(2000);
	driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
	Thread.sleep(2000);
	driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
	System.out.println("login done successfully");
	Thread.sleep(4000);
	  }
  @Test
  public void f() throws InterruptedException {
	 
	  //Identify the element on the hover of which you want to verify the pointer type
	 WebElement e= driver.findElement(By.xpath("//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'My Task')]"));
	 Thread.sleep(4000);
	  System.out.println("Cursor before hovering on: " + e.getCssValue("cursor"));
	  Thread.sleep(4000);
	  //Hover the mouse over that element
	  Actions builder = new Actions(driver);
	  Thread.sleep(4000);
	  builder.moveToElement(e);
	  Thread.sleep(4000);
	  builder.build().perform();
	  Thread.sleep(4000);
	//Check that the cusrsor does not change to pointer
	  String cursorTypeAfter = e.getCssValue("cursor");
	  System.out.println("Cursor after hovering on: " + cursorTypeAfter);
	  }
  
}
