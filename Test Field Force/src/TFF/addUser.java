package TFF;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class addUser {

    public static String ReturnMonthSelected(int mnth) {
        switch (mnth) {
            case 1:
                return "JAN";
            case 2:
                return "FEB";
            case 3:
                return "MAR";
            case 4:
                return "APR";
            case 5:
                return "MAY";
            case 6:
                return "JUN";
            case 7:
                return "JUL";
            case 8:
                return "AUG";
            case 9:
                return "SEP";
            case 10:
                return "OCT";
            case 11:
                return "NOV";
            case 12:
                return "DEC";
            default:
                return "Invalid input - Wrong month number.";
        }
    }
    

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\lenovo\\Driver/Chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://app.fieldforceconnect.com/auth/login");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        Scanner sc = new Scanner(System.in);
    	boolean IsBirthDateClicked = false; 

        driver.findElement(By.id("mat-input-0")).sendKeys("linapatil@nimapinfotech.com"); //email
        driver.findElement(By.id("mat-input-1")).sendKeys("12345678"); //password
        driver.findElement(By.id("kt_login_signin_submit")).click(); //sign in button
        // driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(5000);
        WebElement slider = driver.findElement(By.className("ps__thumb-y"));
        Actions act = new Actions(driver);
        act.clickAndHold(slider).moveByOffset(0, 250).release().perform();
        //settings
        Thread.sleep(3000);
        driver.findElement(By.xpath("//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")).click();
        //driver.findElement(By.className("kt-menu__link-text")).click();
        Thread.sleep(5000);
        act.clickAndHold(slider).moveByOffset(0, 50).release().perform();
        Thread.sleep(5000);
        //all users
        driver.findElement(By.xpath("//*[@id=\"kt_aside_menu\"]/ul/li[18]/div/ul/li[1]/a/span")).click();
        Thread.sleep(5000);
        //add user
        driver.findElement(By.xpath("//*[@id=\"kt_content\"]/div/kt-user/kt-user-list/kt-portlet/div/kt-portlet-header/div[4]/button[1]/span")).click();
        Thread.sleep(5000);

        //Name field
        System.out.println("Please enter the user name");
        String userName = sc.nextLine();
        System.out.println("userName is " + userName);
        WebElement name = driver.findElement(By.id("mat-input-6"));
        name.sendKeys(userName);

        //Employee no
        System.out.println("Please enter the Employee no");
        String employeeNo = sc.nextLine();
        System.out.println("employeeNo is " + employeeNo);
        WebElement no = driver.findElement(By.id("mat-input-7"));
        no.sendKeys(employeeNo);

        //country code
        driver.findElement(By.xpath("//*[@id=\"mat-select-7\"]/div/div[1]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"mat-option-360\"]/span")).click();
        Thread.sleep(3000);
           
        //mob no.
        System.out.println("Please enter the mob no");
        String mobNo = sc.nextLine();
        System.out.println("mob No is " + mobNo);
        WebElement mNo = driver.findElement(By.xpath("//*[@id=\"mat-input-8\"]"));
        mNo.sendKeys(mobNo);

        //contact no.
        System.out.println("Please enter the contact no");
        String contactNo = sc.nextLine();
        System.out.println("contact No is " + contactNo);
        WebElement cNo = driver.findElement(By.xpath("//*[@id=\"mat-input-9\"]"));
        cNo.sendKeys(contactNo);
        //user email
        System.out.println("Please enter the user email");
        String uemail = sc.nextLine();
        System.out.println(" user email is " + uemail);
        WebElement uemailId = driver.findElement(By.xpath("//*[@id=\"mat-input-10\"]"));
        uemailId.sendKeys(uemail);
        Thread.sleep(5000);

        //assign role
        driver.findElement(By.xpath("//*[@id=\"mat-select-8\"]/div/div[1]/span")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"mat-option-754\"]/span")).click();
        Thread.sleep(5000);
        //gender
        driver.findElement(By.xpath("//*[@id=\"mat-select-9\"]/div/div[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"mat-option-258\"]/span")).click();

        //date of birth
        System.out.println("Please enter Your Birth Year");
        int BirthYear = sc.nextInt();
        System.out.println(" user Birth Year is " + BirthYear);

        driver.findElement(By.xpath("//*[@id=\"mat-input-11\"]")).click();
        driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[1]")).click();
        
        while(true) {
        	if(IsBirthDateClicked) {
        		break;
        	}
        	WebElement Calelement = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[1]/span"));
            String dtr = Calelement.getText();
            System.out.println(dtr);
            String[] DateRangepartss = dtr.split(" � ");
            System.out.println(DateRangepartss[0]);
            System.out.println(DateRangepartss[1]);
            outerloop:       
        	if(BirthYear >= Integer.parseInt(DateRangepartss[0]) && BirthYear <= Integer.parseInt(DateRangepartss[1]) && !IsBirthDateClicked ) {
                    //Store each year in string array
                    List < WebElement > cols;
                    //Get value from Table each cell
                    WebElement t = driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-multi-year-view/table/tbody"));
                    // count rows with size() method
                    List < WebElement > rws = t.findElements(By.tagName("tr"));                          		
                    for (WebElement row: rws) {
                        List < WebElement > Cells = row.findElements(By.tagName("td"));
                        for (WebElement Cell: Cells) {
                            if (Cell.getText().contains(Integer.toString(BirthYear))) {
                                Cell.click();
                                Thread.sleep(5000);
                                System.out.println("Please enter Your Birth Month from 1 to 12");
                                int BirthMonth = sc.nextInt();
                                System.out.println(" user Birth Month is " + BirthMonth);
                                String bMonth = ReturnMonthSelected(BirthMonth);
                                WebElement tm = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-year-view/table/tbody"));
                                // count rows with size() method
                                List <WebElement> monthsrows = tm.findElements(By.tagName("tr"));
                                for (WebElement monthrow: monthsrows) {
                                    List < WebElement > MonthCells = monthrow.findElements(By.tagName("td"));
                                    for (WebElement Monthcell: MonthCells) {
                                        if (Monthcell.getText().contains(bMonth)) {
                                        	Monthcell.click();
                                            Thread.sleep(5000);
                                            System.out.println("Please enter Your Birth Date ");
                                            int BirthDay = sc.nextInt();
                                            System.out.println(" user Birth Date is " + BirthDay);
                                            WebElement bdatecalender = driver.findElement(By.xpath("/html/body/div[3]/div[4]/div/mat-datepicker-content/mat-calendar/div/mat-month-view/table/tbody"));
                                            // count rows with size() method
                                            List <WebElement> brows = bdatecalender.findElements(By.tagName("tr"));
                                            for(WebElement brow: brows){
                                                List<WebElement> bCells = brow.findElements(By.tagName("td"));
                                                for(WebElement bCell:bCells){
                                                	if (bCell.getText().contains(Integer.toString(BirthDay)))
                                                	{
                                                		bCell.click();
                                                     	IsBirthDateClicked = true;
                                                     	break outerloop;
                                                	}
                                                   
                                                }
                                            }                                                                                                                  
                                        }
                                    }
                                }
                            }
                        }
                    }
              		break;     
        	}
        	else {
        		driver.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/mat-calendar-header/div/div/button[2]")).click();
        		continue;
        	}
        }
   
        //country
        driver.findElement(By.xpath("//*[@id=\"mat-select-10\"]/div/div[1]/span")).click();
        driver.findElement(By.xpath("//*[@id=\"mat-option-607\"]/span")).click();
        /*
        Select sel= new Select(dropdown);
        sel.selectByVisibleText("India");*/

        //state  
     /*   List<WebElement> element= driver.findElements(By.xpath("//*[@id=\"mat-select-11\"]/div/div[1]/span"));
        for(int i= 0; i<element.size();i++) {
        	String state=element.get(i).getText();
        	if(state.equals("Maharashtra")) {
        		element.get(i).click();
        		break;
        	}
        }*/
      /*  driver.findElement(By.xpath("//*[@id=\"mat-select-11\"]/div/div[1]/span")).click();
        Thread.sleep(7000);
        driver.findElement(By.xpath("//*[@id=\"mat-input-13\"]")).sendKeys("Maharashtra");
      
       //city
        driver.findElement(By.xpath("//*[@id=\"mat-select-12\"]/div/div[1]/span")).click(); 
        Thread.sleep(7000);
        driver.findElement(By.xpath("//*[@id=\"mat-option-1212\"]/span")).click();
       */
        //zip code
        System.out.println("Please enter the zip code");
        String zipCode = sc.nextLine();
        System.out.println(" user zip code is " + zipCode);
        WebElement zCode = driver.findElement(By.xpath("//*[@id=\"mat-input-15\"]"));
        zCode.sendKeys(zipCode);
        Thread.sleep(5000);

        //street address
        System.out.println("Please enter the street address");
        String streetAddress = sc.nextLine();
        System.out.println(" user street address is " + streetAddress);
        WebElement sAddress = driver.findElement(By.xpath("//*[@id=\"mat-input-16\"]"));
        sAddress.sendKeys(streetAddress);
        Thread.sleep(5000);

        //Assign manager   
        driver.findElement(By.xpath("//*[@id=\"mat-select-14\"]/div/div[1]/span")).click();
        driver.findElement(By.xpath("//*[@id=\"mat-option-756\"]/span")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"mat-select-13\"]/div/div[1]/span")).click(); //territory
        Thread.sleep(5000);
        
        driver.findElement(By.xpath("//*[@id=\"mat-dialog-0\"]/kt-users-edit/div/div/div/button[2]/span")).click(); //add button
        Thread.sleep(2000);
        //String toastMessage = driver.findElement(By.className("toast-container")).getText();
        //System.out.println(toastMessage);

    }

}