package stepDefinitions;

import java.awt.AWTException;

import org.openqa.selenium.interactions.Actions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.ProductCatalogue_CategoryMaster;

public class ProductCatalogue_CategoryMasterSteps   extends OpenBrowser {
	ProductCatalogue_CategoryMaster categoryMaster;
	Actions as;

	@Given("User is on Dashboard page to check Category Master Module")
	public void user_is_on_dashboard_page_to_check_category_master_module() {
		categoryMaster= new ProductCatalogue_CategoryMaster(driver);
		as=new Actions(driver);
	}

	@And("Scroll upto Settings and click on that to check Category Master Module")
	public void scroll_upto_settings_and_click_on_that_to_check_category_master_module() throws InterruptedException {
		categoryMaster.VerticalSlider_Settings();
		categoryMaster.getSettings();
	}

	@And("Click on Product Catalogue link")
	public void click_on_product_catalogue_link() throws InterruptedException {
		categoryMaster.VerticalSlider_ProductCatalogue();
		categoryMaster.productCatalogueLink();
	}

	@And("Click on  Category Master link")
	public void click_on_category_master_link() {
		categoryMaster.categoryMasterLink();
	}

	@When("Click on add category button")
	public void click_on_add_category_button() {
		categoryMaster.addCategorybutton();
	}

	@And("Fill the details and add category")
	public void fill_the_details_and_add_category() {
		categoryMaster.enterCategoryDetails();
	}

	@Then("Check validation msg on Category Master Module")
	public void check_validation_msg_on_category_master_module() {
		categoryMaster.getVerifyMsg();
	}
	
	//search,edit, delete category
	@Given("Search category by entering category name")
	public void search_category_by_entering_category_name() throws InterruptedException {
		categoryMaster.searchCategory();
	}
	@When("Click on edit category button")
	public void click_on_edit_category_button() {
		categoryMaster.editCategorybutton();
	}

	@When("Edit category details and update category")
	public void edit_category_details_and_update_category() throws InterruptedException {
		categoryMaster.editEnterCategoryDetails();
	}
	@When("Search category by entering category name to delete category")
	public void search_category_by_entering_category_name_to_delete_category() throws InterruptedException {
		categoryMaster.editEnterCategoryDetails();
	}
	
	
//bulk upload, show report
	@When("Click on Bulk Upload button on Category Master Module")
	public void click_on_bulk_upload_button_on_category_master_module() {
		categoryMaster.bulkUploadCategoryButton();
	}

	@When("Click to download All Categories")
	public void click_to_download_all_categories() {
		categoryMaster.downloadAllCategories();
	}

	@When("Click to download sample file of Categories")
	public void click_to_download_sample_file_of_categories() {
		categoryMaster.downloadSampleFile();
	}

	@When("Click on attach excel file button to upload the multiple categories")
	public void click_on_attach_excel_file_button_to_upload_the_multiple_categories() throws InterruptedException, AWTException {
		categoryMaster.attachExcelFile();
	}

	@When("Click on Upload button to upload the multiple categories")
	public void click_on_upload_button_to_upload_the_multiple_categories() {
		categoryMaster.uploadbutton();
	}

	@When("Click on Show report button to view the category report")
	public void click_on_show_report_button_to_view_the_category_report() {
		categoryMaster.showReport_bulkUpload();
	}

	@When("Download the bulkupload report of categories")
	public void download_the_bulkupload_report_of_categories() {
		categoryMaster.dowloadBulkUploadReport();
	}

	@When("Download Category BulkUpload File Report")
	public void download_category_bulk_upload_file_report() {
		categoryMaster.dowloadBulkUploadFileReport();
	}

	@When("View the Category Report")
	public void view_the_category_report() {
		categoryMaster.viewReport();
	}
}
