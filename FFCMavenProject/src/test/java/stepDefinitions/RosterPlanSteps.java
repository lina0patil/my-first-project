package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.RosterPlan;
public class RosterPlanSteps extends OpenBrowser {
	RosterPlan rosterPlan;
	int j;
	@Given("User is on Dashboard page to check  roster plan")
	public void user_is_on_dashboard_page_to_check_roster_plan() throws InterruptedException, IOException, AWTException {
		Thread.sleep(2000);
		rosterPlan= new RosterPlan(driver);
		rosterPlan.ExcelConnect();
		rosterPlan.FetchExcelData(j);
	}

	@And("Click on Roster Plan")
	public void click_on_roster_plan() {
		rosterPlan.rosterPlan();
	}
	
	@And("Search User on Roster")
	public void search_user_on_roster() {
		rosterPlan.searchUserR();
	}

	@And("Select the checkbox on Roster plan")
	public void select_the_checkbox_on_Roster_plan() {
		rosterPlan.checkBox1();
	 
	}

	@When("Click on Select Date button on Roster plan")
	public void click_on_select_date_button() {
		rosterPlan.selectDateButton();
	}

	@And("Select date time and assign to user")
	public void select_date_time_and_assign_to_user() throws InterruptedException {
		rosterPlan.dateFilterRoster();
		rosterPlan.getDateFilter();
		//rosterPlan.selectTimeRoster();
		rosterPlan.assignToRoster();
	}

	@Then("Click on add button on Roster plan")
	public void click_on_add_button_on_Roster_plan() {
		rosterPlan.addRosterButton();
	}

	@And("Check the validation message of Roster Plan")
	public void check_the_validation_message_of_roster_plan() {
		rosterPlan.getVerifyRosterMsg();
	}
	
	
	//Filter and view
	@Given("Click on Filter button to filter users according to location, status and member")
	public void click_on_filter_button_to_filter_users_according_to_location_status_and_member() {
		rosterPlan.filterButton_RosterPlan();
	}

	@Given("Select the location, status and member of roster filter")
	public void select_the_location_status_and_member_of_roster_filter() throws InterruptedException {
		rosterPlan.filterByLocation_RosterPlan();
		rosterPlan.filterByStatus_RosterPlan();
		rosterPlan.filterByMember_RosterPlan();
	}

	@When("Apply it for filtering user on Roster plan")
	public void apply_it_for_filtering_user_on_roster_plan() {
		rosterPlan.applyfilterButton_RosterPlan();
	}

	@When("View the Roster details")
	public void view_the_roster_details() throws InterruptedException {
		Thread.sleep(3000);
		rosterPlan.viewRosterDetails();
	}

	@Then("Close Roster details")
	public void close_roster_details() throws InterruptedException {
		Thread.sleep(3000);
		rosterPlan.closeRosterDetails();
	}
	@And("Clear the filter on roster plan")
	public void clear_the_filter_on_roster_plan() throws InterruptedException {
		rosterPlan.clearFilter_Roster();
	}

}
