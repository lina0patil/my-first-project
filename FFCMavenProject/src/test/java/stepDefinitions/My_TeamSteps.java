package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.My_Team;

public class My_TeamSteps extends OpenBrowser {
	My_Team team;
	int j;
	@Given("User is on Dashboard page to check My Team module")
	public void user_is_on_dashboard_page_to_check_my_team_module() throws InterruptedException, IOException, AWTException {
	Thread.sleep(5000);
	team=new My_Team(driver);
	team.ExcelConnect();
	team.FetchExcelData(j);
	Thread.sleep(8000);
	}
	@And("Click on My Team")
	public void click_on_My_Team() {
	team.getMyTeam();
	}
	
	//Punch in -Out Status-Location
	@When("Click on Location and verify it")
	public void click_on_Location_and_verify_it() throws InterruptedException {
	team.getLocation();
	
	}
	@When("Search user")
	public void search_user() throws InterruptedException {	
    team.getSearchUser();
	}
	@And("Select User")
	public void select_User() throws InterruptedException {
	team.getUser();
	Thread.sleep(3000);
	}
	@And("Verify the from date filter")
	public void verify_the_from_date_filter() throws InterruptedException {
	team.getFromDateFilter();
	}
	@And("Verify the To date filter")
	public void verify_the_To_date_filter() throws InterruptedException{
	team.getToDateFilter();
	}
	@And("Check Apply button")
	public void check_Apply_button() throws InterruptedException {
	team.getApplyButton();
	Thread.sleep(3000);
	}
	@Then("Refresh the filter")
	public void refresh_the_filter() throws InterruptedException {
	team.getRefreshIcon();
	Thread.sleep(3000);
	}
	@And("Check the Punch In and Punch Out status")
	public void check_the_Punch_In_and_Punch_Out_status() {
	boolean punch_in=team.getPunchInStatus();
	String punch_In=team.getPunchInStatusText();
	if(punch_in) {
	String str = Boolean.toString(punch_in);
	System.out.println("Punch In Status is displayed= "+str);
	System.out.println("Punch In Status= "+punch_In);
	}
	else {
		System.out.println("Punch In Status is not displayed");
	}
	boolean punch_out=team.getPunchOutStatus();
	String punch_Out=team.getPunchOutStatusText();
	if(punch_out) {
	String str = Boolean.toString(punch_out);
	System.out.println("Punch Out Status is displayed= "+str);
	System.out.println("Punch Out Status= "+punch_Out);
	}
	else {
		System.out.println("Punch Out Status is not displayed");
	}
	}
	
	//Organization Hierarchy
	@When("Click on Organization Hierarchy icon and verify it")
	public void click_on_organization_hierarchy_icon_and_verify_it() {
    team.getOrganizationHierarchy();
	}
	@When("click on chevron to see hierarchy")
	public void click_on_chevron_to_see_hierarchy() throws InterruptedException {
	team.getChevron_right();
	}
	@Then("Clear the Search User")
	public void clear_the_search_user() throws InterruptedException {
	 team.getClearSearchUser();
	 Thread.sleep(2000);
	}
	
	//Global Map
	@When("Click on Global Map and verify it")
	public void click_on_global_map_and_verify_it() {
		team.globalMap();
	}

	@When("Select the date filter on map")
	public void select_the_date_filter_on_map() throws InterruptedException {
		team.dateFilter_globalMap();
		team.getDateFilter();
	}

	@When("Click on Location Pin Icon on global map")
	public void click_on_location_pin_icon_on_global_map() {
		team.locationPinIcon();
	}

	@When("Maximize the Map for full screen view")
	public void maximize_the_global_map_for_full_screen_view() {
		team.fullscreenView();
	}

	@When("Zoom Out and Zoom In for better view of map")
	public void zoom_out_and_zoom_in_for_better_view_of_global_map() throws InterruptedException {
		team.zoomOut_zoomIn();
	}

	@When("Click on Satellite on map")
	public void click_on_satellite_on_global_map() throws InterruptedException {
		team.satellite();
	}

	@Then("Minimize the map")
	public void minimize_the_global_map() {
		team.fullscreenView();
	}
	// Map
	@When("Click on Map and verify it")
	public void click_on__map_and_verify_it() {
		team.map();
	}
	
	
	//Attendence
	@When("Click on Attendance and verify it")
	public void click_on_attendance_and_verify_it() {
		team.attendance();
	}

	@When("Apply filter to view attendance status")
	public void apply_filter_to_view_attendance_status() throws InterruptedException {
		team.filter_atendance();
		team.getFromDateFilter();
		team.getToDateFilter();
		team.getApplyButton();
		Thread.sleep(3000);
	}
}
