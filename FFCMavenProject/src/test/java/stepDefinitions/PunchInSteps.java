package stepDefinitions;

import java.io.IOException;
import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.login;

public class PunchInSteps extends OpenBrowser{
	login lp;
    @Given("User is on Dashboard page P")
    public void user_is_on_dashboard_page_p() throws IOException {
		lp=new login(driver);
    }

    @When("Click on Punch In button")
    public void click_on_punch_in_button() {
    	lp.getPunchIn();
    }
	@Then("Click on Punch Out")
	public void click_on_punch_out() {
	   lp.getPunchOut();
	}
	@And("Send the report and done it")
	public void send_the_report_and_done_it() {
	   lp.sendReport_PunchOut();
	}
}
