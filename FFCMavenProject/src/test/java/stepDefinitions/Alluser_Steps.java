package stepDefinitions;


import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Alluser;

public class Alluser_Steps extends OpenBrowser{
	Alluser al;
	int j;
	@Given("User is scroll to Settings")
	public void user_is_scroll_to_settings() throws InterruptedException, IOException, AWTException {
		al=new Alluser(driver);
		al.ExcelConnect();
		al.FetchExcelData(j);
		al.VerticalSlider_Settings();
	}
	@And("Click on Settings")
	public void click_on_settings() {
		al.getSettings();
	}
	@And("Click on All User")
	public void click_on_all_user() throws InterruptedException {
		al.VerticalSlider_allUser();
		al.getAllUsers();
	}

	@And("Verify the Date of Joining Date Picker.")
	public void verify_the_date_of_joining_date_picker() throws InterruptedException {
		al.getDateOfJoining();
		al.getDateFilter();
	}

	@And("Verify the Last Date of Working Date Picker")
	public void verify_the_last_date_of_working_date_picker() throws InterruptedException {
		al.getLastdateOfWorking(); 
		al.getDateFilter2();
	}

	@Then("Verify Access Web Login functionality")
	public void verify_access_web_login_functionality() {
		al.getAccessWebLogin();
	}
	@And("Click on Update button")
	public void click_on_update_button() {
		al.getUpdate();  
	}
	@And("Verify the Update message")
	public void verfity_the_message() {
		al.getVerifyMsg();

	}
	@And("Sign Out")
	public void sign_out() throws InterruptedException {
		al.getUserProfile();
		al.getSignout();
	}

	@Given("User is on Login Page")
	public void user_is_on_login_page() {
		String title= driver.getTitle();
		System.out.println("Title of the page: "+title);
	}

	@When("User enters the valid login credentials")
	public void user_enters_the_valid_login_credentials() throws InterruptedException {
		al.getLogIn();
	}
	@When("Click on sign In and Sign In with OTP button")
	public void click_on_sign_in_and_sign_in_with_otp_button() {
		al.getOTP();
	}

	@Then("Check the Error message")
	public void check_the_error_message() {
		al.getErrorMessage();
	}
	@And("Close the All user browser")
	public void close_the_browser() throws InterruptedException {
		driver.close();
	}

	//add user
	@And("Click on add user")
	public void click_on_add_user() throws InterruptedException {
		al.addUser();
	}

	@And("Fill the user details")
	public void fill_the_user_details() throws InterruptedException {
		al.enterUserDetails();
		al.dateofBirth();
		al.getdateofBirth();
		al.getDateOfJoining();
		al.getDateFilter();
		al.enterUserDetails2();
	}

	@And("Click on add button for add new user")
	public void click_on_add_button_for_add_new_user() throws InterruptedException {
		al.addButton();
	}

	@And("Verify the validation message on all user module")
	public void verfity_the_validation_message_on_all_user_module() {
		al.getVerifyMsg();
	}

	//Edit user
	@Given("Search user by entering user name")
	public void search_user_by_entering_user_name() throws InterruptedException {
		al.searchUser();
	}

	@When("Click on Edit User")
	public void click_on_edit_user() throws InterruptedException {
		al.editUser();
	}
	@And("Edit the user details")
	public void edit_the_user_details() throws InterruptedException {
		al.editUserDetails();
		al.editdateofBirth();
		al.getdateofBirth();
		al.getDateOfJoining();
		al.getDateFilter();
		al.editlastDateofworking();
		al.getDateFilter2();
		al.editUserDetails2();
	}
	@And("Click on edit button for edit user")
	public void click_on_edit_button_for_edit_user() throws InterruptedException {
		al.updateButton();

	}
	@Given("Verify the validation message on edit user module")
	public void verify_the_validation_message_on_edit_user_module() throws Exception {
		al.VerifyMsg_edit();
	}
	//Bulk Upload
	@And("Click on bulk upload")
	public void click_on_bulk_upload() {
		al.bulkUploadButton();
	}

	@And("Click to download All Users")
	public void click_to_download_all_users() {
		al.downloadAllUserLink();
	}

	@And("Click to download sample file")
	public void click_to_download_sample_file() {
		al.downloadSampleFileLink();
	}

	@And("Get the input field Attach Excel File")
	public void get_the_input_field_attach_excel_file() throws InterruptedException, AWTException {
		al.attachExcelFileButton();
	}

	@When("Click the upload button")
	public void click_the_upload_button() {
		al.UploadButton();
	}
	@And("Click on export button to download user details")
	public void click_on_export_button_to_download_user_details() {
		al.exportButtonUser();
	}
	//show report
	@And("Click on show report")
	public void click_on_show_report() {
		al.showReportButton();
	}

	@And("Click to search report")
	public void click_to_search_report() {
		al.searchReport();
	}

	@And("Click to download bulkupload report")
	public void click_to_download_bulkupload_report() {
		al.dowloadBulkUploadReport();
	}

	@And("Click to download report file")
	public void click_to_download_report_file() throws InterruptedException, AWTException {
		al.downloadFileReport();
	}

	@And("Click on view report")
	public void click_on_view_report() {
		al.viewReport();
	}
	//assign manager, assign team, user status, filter
	@And("Assign the manager to the user and check validation msg")
	public void assign_the_manager_to_the_user_and_check_validation_msg() throws InterruptedException {
		al.assignManagericon();
	}

	@And("Assign the team to the user and check validation msg")
	public void assign_the_team_to_the_user_and_check_validation_msg() throws InterruptedException {
		al.assignTeamIcon();
	}
	@And("Remove the manager and check validation msg")
	public void remove_the_manager_and_check_validation_msg() throws InterruptedException {
		al.removeManager();
	}
	@And("Search the user and Deactivate that user and check validation msg")
	public void search_the_user_and_deactivate_that_user_and_check_validation_msg() throws InterruptedException {
		al.searchUser_AllUser();
		al.userStatus();
	}

	@And("Verify the filter on all user module")
	public void verify_the_filter_on_all_user_module() {
		al.filter_allUser();
	}

	@And("Select the user by inactive status")
	public void select_the_user_by_inactive_status() {
		al.byUserStatus();
	}

	@And("Apply it for filtering user")
	public void apply_it_for_filtering_user() {
		al.applyfilterbutton_allUser();
	}

	@And("Active the user status")
	public void active_the_user_status() throws InterruptedException {
		Thread.sleep(3000);
		al.userStatus();
	}

	@And("Clear the filter on all user module")
	public void clear_the_filter_on_all_user_module() {
		al.clearfilter_allUser();
	}
}
