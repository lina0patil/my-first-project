package stepDefinitions;

import java.io.IOException;

import org.junit.Assert;
import org.junit.runner.RunWith;
import java.time.Duration;
import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;

import pageClasses.login;

@RunWith(Cucumber.class)
public class LoginSteps extends OpenBrowser {
	login login;

	@Given("Launches the browser")
	public void launches_the_browser() throws IOException {
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30, 0));
		driver=OpenBrowser.getDriver1();
		login=new login(driver);
	}

	@And("User is on login page")
	public void user_is_on_login_page() {
		//to verify the title and url of login page
				String url= driver.getCurrentUrl();
				System.out.println(url);
				if(!url.equalsIgnoreCase("https://testffc.nimapinfotech.com/auth/login")) {
				Assert.fail("url is not matched "); }
				else {
				System.out.println("url is matched");
				}
			
	}
	@When("^User enter a credential \"(.+)\" \"(.+)\"$")
	public void user_enter_a_credential(String Username, String Password) throws Throwable {
		login.getUserName(Username);
		login.getPass(Password);    

	}
	@And("Click on Sign in button")
	public void click_on_sign_in_button() {
		login.getsignInbutton();
}
}


