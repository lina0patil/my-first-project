package stepDefinitions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Dashboard360;

public class Dashboard360Steps extends OpenBrowser{

	Dashboard360 d360;
	@Given("User is on dashboard360 page")
	public void user_is_on_dashboard360_page() throws InterruptedException {
	Thread.sleep(5000);
	d360=new Dashboard360(driver);
	d360.getDashboard360_link();
	Thread.sleep(8000);
	}

	@When("scroll to the Task against Lead tab")
	public void scroll_to_the_task_against_lead_tab() {
    d360.getScrolltoTaskAgainstLeadTab();
	}

	@And("Verify the Date filter dropdown")
	public void verify_the_date_filter_dropdown() throws InterruptedException {
	Thread.sleep(3000);
	d360.getFromDateFilter();
	Thread.sleep(3000);
	d360.getToDateFilter();
	}
	@And("User click on customer and proceed to Task Against Lead screen")
	public void user_click_on_customer_and_proceed_to_task_against_lead_screen() {
	d360.getCustomer();
	}
	
	@Then("Verify the filter dropdown")
	public void verify_the_filter_dropdown() throws InterruptedException {
    d360.getFilter();
	}
	@And("click on one of the task")
	public void click_on_one_of_the_task() {
	d360.getClickOnTask();
	}
	@And("fetch the data")
	public void fetch_the_data() {
	d360.getTitle();
	d360.getDescription();
	d360.getPriority();
	d360.getAssignedDate();
	d360.getDueDate();
	d360.getAssignedTo();
	d360.getCustomerName();
	d360.getMobileNo();
	}
	@And("Close the Dashboard360 browser")
	public void close_the_browser() throws InterruptedException {
	Thread.sleep(2000);
	driver.close();
	}
	
	
	//2nd Scenario
	@And("Check the From Date Filter Top5")
	public void check_the_from_date_filter_Top5() throws InterruptedException {
		Thread.sleep(2000);
		d360.getFromDateFilter_Top5();
	}

	@And("Check the To Date Filter Top5")
	public void check_the_to_date_filter_Top5() throws InterruptedException {
		d360.getToDateFilter_Top5();
	}

	@And("Click on View all pointer of Top {int} Absentees label")
	public void click_on_view_all_pointer_of_top_absentees_label(Integer int1) {
	d360.getViewAll();
	}

	@When("Verify the Working hours")
	public void verify_the_working_hours() {
	  d360.getWorkingHours();
	}

	@And("Check the filter on Working hours page")
	public void check_the_filter_on_working_hours_page() throws InterruptedException {
	 Thread.sleep(2000);
     d360.getFilterWorkingHours();
     d360.getFromDateFilterDropdown();
     d360.getToDateFilterDropdown();
     d360.getApplyButtonFilterDropdown();
   
	}
	@And("Get the Working hours")
	public void get_the_Working_hours() {
		  d360.getEligibleHours_Hrs();
		     d360.getCompletedHours_HH_MM();
		     d360.getLeftHours_HH_MM();
	}
	@Then("View the details of one of the user by clicking on view details")
	public void view_the_details_of_one_of_the_user_by_clicking_on_view_details() {
	  d360.getViewDetails();
	}

	@And("Edit the user by clicking on User name")
	public void edit_the_user_by_clicking_on_user_name() throws InterruptedException {
	d360.getUserEdit();
	d360.getEdit();
	driver.navigate().back();
	}
}
