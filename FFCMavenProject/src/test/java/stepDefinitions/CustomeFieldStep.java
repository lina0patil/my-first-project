package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.interactions.Actions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.CustomField;

public class CustomeFieldStep  extends OpenBrowser {
	CustomField customField;
	Actions as;
	int j;
	@Given("User is on Dashboard page to verify Custom Field Module")
	public void user_is_on_dashboard_page_to_verify_custom_field_module() throws IOException, InterruptedException, AWTException {
		customField= new CustomField(driver);
		as=new Actions(driver);
		customField.ExcelConnect();
		customField.FetchExcelData(j);
	}

	@And("Scroll upto Settings and click on that to check Custom Field module")
	public void scroll_upto_settings_and_click_on_that_to_check_custom_field_module() throws InterruptedException {
		customField.VerticalSlider_Settings();
		customField.getSettings();
	}

	@And("Click on Custom Field link")
	public void click_on_custom_field_link() throws InterruptedException {
		customField.VerticalSlider_Roles();
		customField.customFieldLink();
	}
	@When("Click on add custom field button")
	public void click_on_add_custom_field_button() {
		customField.addcustomfieldButton();
	}

	@And("Select Text custom field type and fill the required details, add it and check validation msg")
	public void select_text_custom_field_type_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.selectTextCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Text Custom Field");
		Thread.sleep(5000);
	}
	@When("Select Quantitative custom field type and fill the required details, add it and check validation msg")
	public void select_quantitative_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectQuantitativeCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Quantitative Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select YesNo custom field type and fill the required details, add it and check validation msg")
	public void select_yes_no_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectYesNoCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("YesNo Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select Date custom field type and fill the required details, add it and check validation msg")
	public void select_date_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectDateCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Date Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select Single custom field type and fill the required details, add it and check validation msg")
	public void select_single_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectSingleCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Single Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select Multiple custom field type and fill the required details, add it and check validation msg")
	public void select_multiple_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectMultipleCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Multiple Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select Image custom field type and fill the required details, add it and check validation msg")
	public void select_image_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectImageCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Image Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select CameraImage custom field type and fill the required details, add it and check validation msg")
	public void select_camera_image_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectCameraImageCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("CameraImage Custom Field");
		Thread.sleep(5000);
	}

	@Then("Select Signature custom field type and fill the required details, add it and check validation msg")
	public void select_signature_custom_field_type_and_fill_the_required_details_and_add_it_and_check_validation_msg() throws InterruptedException {
		customField.addcustomfieldButton();
		Thread.sleep(3000);
		customField.selectSignatureCustomFieldType();
		customField.mandatoryToggleButton();
		customField.addCustomType();
		customField.getVerifyMsg();
		System.out.println("Signature Custom Field");
		Thread.sleep(5000);
	}

	@When("Search and edit custom field and check validation msg, Delete custom field and check validation msg")
	public void search_and_edit_custom_field_and_check_validation_msg_delete_custom_field_and_check_validation_msg() throws InterruptedException {
		customField.searchByLabelNameT();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Text Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameQ();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Quantitative Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameY();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("YesNo Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameD();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Date Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameS();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Single Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameM();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Multiple Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameI();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Image Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameC();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Camera Image Custom Field");
		Thread.sleep(3000);
		
		customField.searchByLabelNameSt();
		customField.edit_CustomType();
		Thread.sleep(3000);
		customField.deleteRole();
		System.out.println("Signature Custom Field");
		Thread.sleep(3000);
		
	
	}
	
}
