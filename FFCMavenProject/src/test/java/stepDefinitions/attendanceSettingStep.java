package stepDefinitions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.AttendanceSetting;

public class attendanceSettingStep extends OpenBrowser {
	AttendanceSetting attendSetting;
	@Given("User is on Dashboard page to verify Attendance Setting Module")
	public void user_is_on_dashboard_page_to_verify_attendance_setting_module() {
		attendSetting =new AttendanceSetting(driver);
	}

	@And("Scroll upto Settings and click on that to check attendance module")
	public void scroll_upto_settings_and_click_on_that_to_check_attendance_module() throws InterruptedException {
		attendSetting.VerticalSlider_Settings();
		attendSetting.getSettings();
	}

	@And("Click on Attendance Setting link")
	public void click_on_attendance_setting_link() throws InterruptedException {
		attendSetting.VerticalSlider_attendanceSetting();
		attendSetting.attendenceSettingLink();
	}

	@And("Click on add new button to add new fencing location")
	public void click_on_add_new_button_to_add_new_fencing_location() {
		attendSetting.addNewFencingArea();
	}

	@And("Give the name for that fencing location")
	public void give_the_name_for_that_fencing_location() {
		attendSetting.enterPlaceName();
	}

	@And("Search and select the location")
	public void search_and_select_the_location() throws InterruptedException {
		attendSetting.searchLocation();
	}

	@And("Maximize the Map for full screen view on Attendance Setting Module")
	public void maximize_the_map_for_full_screen_view_on_attendance_setting_module() {
		attendSetting.fullscreenView();
	}

	@And("Set the correct location through location Pin Icon")
	public void set_the_correct_location_through_location_pin_icon() throws InterruptedException {
		attendSetting.locatinPin();
	}

	@And("Zoom Out and Zoom In for better view of map on Attendance Setting Module")
	public void zoom_out_and_zoom_in_for_better_view_of_map_on_attendance_setting_module() throws InterruptedException {
		attendSetting.zoomOut_zoomIn();
	}

	@And("Click on Satellite on map Attendance Setting Module")
	public void click_on_satellite_on_map_attendance_setting_module() throws InterruptedException {
		attendSetting.satellite();
	}

	@And("Minimize the map on Attendance Setting Module")
	public void minimize_the_map_on_attendance_setting_module() {
		attendSetting.fullscreenView();
	}
	@When("Click on add button to add new fencing location")
	public void click_on_add_button_to_add_new_fencing_location() {
		attendSetting.addFenceArea();
	}
	@And("Verify the validation message on Attendance setting module")
	public void verify_the_validation_message_on_attendance_setting_module() {
		attendSetting.getVerifyMsg();
	}
	//edit, delete
	@And("Search the fence area")
	public void search_the_fence_area() throws InterruptedException {
		attendSetting.searchFenceLocation();
	}

	@And("Edit that fence location")
	public void edit_that_fence_location() throws InterruptedException {
		attendSetting.editfenceLocation();
	}

	@And("Edit all the fence location details")
	public void edit_all_the_fence_location_details() throws InterruptedException {
		attendSetting.enterPlaceName_edit();
		attendSetting.searchLocation_edit();
		attendSetting.fullscreenView();
		attendSetting.locatinPin_edit();
		attendSetting.zoomOut_zoomIn();
		attendSetting.satellite();
		attendSetting.fullscreenView();
		attendSetting.updateFenceLocation();
	}
	@And("Delete that fence location")
	public void delete_that_fence_location() throws InterruptedException {
		Thread.sleep(3000);
		attendSetting.deleteFenceLocation();
	}

	//global setting
	@When("Scroll upto global setting on Attendance setting Module")
	public void scroll_upto_global_setting_on_attendance_setting_module() {
		attendSetting.globalSetting();
	}
	@When("Turn on the reminder punch in time toggle button and set the time on Attendance setting Module")
	public void turn_on_the_reminder_punch_in_time_toggle_button_and_set_the_time_on_attendance_setting_module() {
		attendSetting.reminderPunchInTimeToggle();
		attendSetting.setReminderPunchintime();
	}

	@And("Set total working Hours in day")
	public void set_set_total_working_hours_in_day() {
		attendSetting.setTotalWorkingHoursInDay();
	}

	@And("Set total working Hours in half day")
	public void set_total_working_hours_in_half_day() {
		attendSetting.setTotalWorkingHoursInHalfDay();
	}
	
	@And("Click on save button to save the global setting")
	public void click_on_save_button_to_save_the_global_setting() {
		attendSetting.saveGlobalSettingButton();
	}
	//add new holiday,remove
	@When("Click on Add new holiday button")
	public void click_on_add_new_holiday_button() {
		attendSetting.addNewHolidayButton();
	}

	@And("Enter holiday name and select date")
	public void enter_holiday_name_and_select_date() throws InterruptedException {
		attendSetting.addHoliday();
		attendSetting.holidayDate();
		attendSetting.getDateFilter();
		
	}

	@And("Click on done button to set the holiday")
	public void click_on_done_button_to_set_the_holiday() throws InterruptedException {
		Thread.sleep(3000);
		attendSetting.done_holiday();
		
		
	}
	@Then("Click on remove holiday icon")
	public void click_on_remove_holiday_icon() throws InterruptedException {
		Thread.sleep(3000);
		attendSetting.remove_holiday();
	}

}
