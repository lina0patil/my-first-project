package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.DynamicForm;

public class DynamicFormSteps extends OpenBrowser{
	DynamicForm dynamicF;
	int j;
	@Given("User is on Dashboard page to verify Dynamic Form")
	public void user_is_on_dashboard_page_to_verify_dynamic_form() throws IOException, InterruptedException, AWTException {
		dynamicF=new DynamicForm(driver);
		dynamicF.ExcelConnect();
		dynamicF.FetchExcelData(j);

	}

	@Given("Click on Dynamic form")
	public void click_on_dynamic_form() {
		dynamicF.getVisitForm();
	}

	@Given("Click on Add Form button to add Dynamic form")
	public void click_on_add_form_button_to_add_dynamic_form() {

	}

	@Given("Fill the Add Form Details and click on next D")
	public void fill_the_add_form_details_and_click_on_next_d() {
	
	}

	@Given("Click on add more section D")
	public void click_on_add_more_section_d() {
	
	}

	@When("Add the section title and done it D")
	public void add_the_section_title_and_done_it_d() {

	}

	@When("For add the components click on edit image on section D")
	public void for_add_the_components_click_on_edit_image_on_section_d() {

	}

	@When("Select the components D")
	public void select_the_components_d() {

	
	}

	@When("Fill the Components details D")
	public void fill_the_components_details_d() {
	  
	}

	@Then("Click on Publish form button D")
	public void click_on_publish_form_button_d() {
	  
	}

	@Then("verify the message D")
	public void verify_the_message_d() {
	 
	}

}
