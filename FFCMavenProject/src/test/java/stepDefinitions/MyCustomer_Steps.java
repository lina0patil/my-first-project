package stepDefinitions;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.myCustomer;

public class MyCustomer_Steps extends OpenBrowser{
	int j;
	myCustomer mycustomer;

	@Given("User is on dashboard page")
	public void user_is_on_dashboard_page() throws InterruptedException {
		Thread.sleep(2000);
		mycustomer= new myCustomer(driver);
	}

	@And("Click on my customer link")
	public void click_on_my_customer_link() throws IOException, InterruptedException, AWTException {
		mycustomer.getMyCustomer();
		mycustomer.ExcelConnect();
		mycustomer.FetchExcelData(j);
	}
	//add new customer
	@When("Click on new customer")
	public void click_on_new_customer() throws InterruptedException, AWTException {
		mycustomer.getNewCustomer();
	}

	@And("Enter the inputs")
	public void enter_the_inputs() throws InterruptedException, AWTException, IOException {
		mycustomer.getPrimaryContact();
		Thread.sleep(3000);
		mycustomer.getCustomerAddress();
		Thread.sleep(3000);
		mycustomer.getMoreInfo();
		Thread.sleep(3000);
		mycustomer.getPrimarySecondarySalesInfo();
		Thread.sleep(3000);
	}

	@And("Click on the Save button")
	public void click_on_the_save_button() {
		mycustomer.getSaveCustomer();
	}

	//Add Call logs
	@When("Click on add call logs Icon")
	public void click_on_add_call_logs_icon() {
		mycustomer.getAddCallLogsIcon();
	}
	@And("Fill the Call logs inputs")
	public void fill_the_call_logs_inputs() {
		mycustomer.getCallLogsFields();
	}

	@And("Click on the add call log button")
	public void click_on_the_add_call_log_button() {
		mycustomer.getAddCallLogsButton();
	}
	//Add Appointment
	@When("Click on add appointment Icon")
	public void click_on_add_appointment_icon() {
		mycustomer.getAddAppointmentIcon();
	}

	@And("Fill the appointment inputs")
	public void fill_the_appointment_inputs() throws InterruptedException {
		mycustomer.getDateFiltereAddAppointment();
		mycustomer.getDateFilter();
		mycustomer.getTimePicker();
		mycustomer.getAssignToUser();	
	}

	@And("Click on the add appointment button")
	public void click_on_the_add_appointment_button() {
		mycustomer.getAddAppointmentButton();
	}

	//edit customer >details
	@When("Click on company name")
	public void click_on_company_name() {
		mycustomer.getCompanyDetails();
	}

	@When("Click on details label")
	public void click_on_details_label() throws InterruptedException {
		mycustomer.getDetails();
	}

	@When("Click on edit button to update the customer details")
	public void click_on_edit_button_to_update_the_customer_details() throws InterruptedException, AWTException {
		mycustomer.getEditPrimaryContact();
		mycustomer.getEditCustomerAddress();
		Thread.sleep(3000);
		mycustomer.getEditMoreInfo();
		mycustomer.getEditPrimarySecondarySalesInfo();
	}

	//add Opportunity
	@When("Click on add Opportunity Icon")
	public void click_on_add_opportunity_icon() {
		mycustomer.getOpportunityIcon();
	}

	@When("Fill the Opportunity inputs")
	public void fill_the_opportunity_inputs() throws InterruptedException {
		mycustomer.getOtherOpportunitiesField();
		mycustomer.getDateFilter();
		mycustomer.getTags_Oppo();

	}

	@When("Click on the add Opportunity button")
	public void click_on_the_add_opportunity_button() throws InterruptedException {
		mycustomer.getAddOppoButton();
	}

	//export
	@When("Check the export")
	public void check_the_export() {
		mycustomer.getExport();
	}


	//Add Quick task
	@When("Click on Add Quick task Icon")
	public void click_on_add_quick_task_icon() {
		mycustomer.getAddQuickTaskIcon();
	}

	@When("Fill the quick task inputs")
	public void fill_the_quick_task_inputs() throws InterruptedException {
		mycustomer.getNextActionPoint();
		mycustomer.getDateFiltereAddQuickTask();
		mycustomer.getDateFilter();
		mycustomer.getTimePickerTask();
		mycustomer.getAssignToUserTask();
	}

	@When("Click on the Add Quick task button")
	public void click_on_the_add_quick_task_button() {
		mycustomer.getAddQuickTaskButton();
	}

	//delete
	@Given("Search customer by their name")
	public void search_customer_by_their_name() {
		mycustomer.getSearchByCustomerName();
	}


	@When("Select customer by clicking on checkbox")
	public void select_customer_by_clicking_on_checkbox() {
		mycustomer.getSelectCheckboxCustomer();
	}

	@When("Delete the selected customer")
	public void delete_the_selected_customer() {
		mycustomer.getdeleteCustomer();
	}

	//Timeline
	@When("User is on Timeline Screen")
	public void user_is_on_timeline_screen() {	
	}
	@When("Enter the text")
	public void enter_the_text() {
		mycustomer.getEntertext_Timeline();
	}
	@When("Apply Filter by status")
	public void apply_filter_by_status() throws InterruptedException {
		mycustomer.FilterByStatus_Timeline();	
	}
	@Then("Clear the Timeline filter")
	public void clear_the_timeline_filter() throws InterruptedException {
		mycustomer.Clearfilter_Timeline();
	}

	//Assign customer
	@When("Click on the assign customer button")
	public void click_on_the_assign_customer_button() throws InterruptedException {
		mycustomer.assignCustomerbutton();
	}

	@When("Search and assign customer")
	public void serach_Search_and_assign_customer() throws InterruptedException {
		mycustomer.searchCustomer();
	}


	//Bulk Upload
	@When("Click on Bulk Upload button on My customer")
	public void click_on_bulk_upload_button_on_my_customer() {
		mycustomer.BulkUploadbutton();
	}

	@And("Click to download All Customers")
	public void click_to_download_all_customers() throws InterruptedException {
		mycustomer.downloadAllCustomer();
	}

	@And("Click on attach excel file button to upload the multiple customer")
	public void click_on_attach_excel_file_button_to_upload_the_multiple_customer() throws InterruptedException, AWTException, IOException {
		mycustomer.attachExcelFile();
	}

	@And("Click on Upload button")
	public void click_on_upload_button() {
		mycustomer.uploadExcelFileButton();
	}


	//Show report-download report
	@And("Click on Show report button")
	public void click_on_show_report_button() {
		mycustomer.showReportButton();
	}
	@When("Search Customer report by file name, download report and view report")
	public void search_customer_report_by_file_name_download_report_and_view_report() {
		mycustomer.searchReport();
	}

	//Filter
	@When("Check the Filter on my customer")
	public void check_the_filter_on_my_customer() throws InterruptedException {
		mycustomer.filterButton_MyCustomer();
		mycustomer.filterByLocation_MyCustomer();
		mycustomer.filterByStatus_MyCustomer();
		mycustomer.filterBySource_MyCustomer();
		mycustomer.filterByStage_MyCustomer();
		mycustomer.filterByType_MyCustomer();
		mycustomer.filterByTerritory_MyCustomer();
		mycustomer.filterByMember_MyCustomer();
		mycustomer.filterByTag_MyCustomer();
	}
	@And("Apply Filter on my customer")
	public void apply_filter_on_my_customer() {
		mycustomer.applyfilterButton_MyCustomer();
	}
	@Then("clear Filter on my customer")
	public void clear_filter_on_my_customer() throws InterruptedException {
		mycustomer.clearFilter_MyCustomer();
	}


	@Then("Check validation msg on My Customer")
	public void check_validation_msg_on_my_customer() {
		mycustomer.getVerifyMsg();
	}
	
}
