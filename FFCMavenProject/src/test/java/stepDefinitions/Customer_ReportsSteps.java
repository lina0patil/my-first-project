package stepDefinitions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Customer_Reports;

public class Customer_ReportsSteps extends OpenBrowser{

	Customer_Reports reports;
	@Given("User is scroll to Reports")
	public void user_is_scroll_to_Reports() throws InterruptedException {
	Thread.sleep(2000);
	reports=new Customer_Reports(driver);
	reports.getVerticalSlider();
	Thread.sleep(5000);
	}

	@And("Click on Reports")
	public void click_on_Reports() throws InterruptedException {
	reports.getReports();
	}
	
    @And("Click on Customer")
	public void click_on_Customer() {
	reports.getCustomer();
	}

	@And("Again scroll to see all option in Reports")
	public void again_scroll_to_see_all_option_in_Reports() throws InterruptedException {
	reports.getVerticalSlider();
	}
	
	@When("Verify the Call Logs")
	public void verify_the_Call_Logs() {
	reports.getCallLogs();
	}

	@And("Check the From Date Filter on Reports")
	public void check_the_From_Date_Filter_on_Reports() throws InterruptedException {
	reports.getFromDateFilter();
	}

	@And("Check the To Date Filter on Reports")
	public void check_the_To_Date_Filter_on_Reports() throws InterruptedException {
	reports.getToDateFilter();
	
	}

	@And("Check the Select user dropdown on Reports")
	public void check_the_Select_user_dropdown_on_Reports() throws InterruptedException {
	
	reports.getSelectUser();
	}

	@And("Check the Select Manager dropdown on Reports")
	public void check_the_Select_Manager_dropdown_on_Reports() throws InterruptedException {
		
    reports.getSelectManager();
	}

	@And("Check the Apply button on Reports")
	public void check_the_Apply_button_on_Reports() throws InterruptedException {
	reports.getApplyButton();
	Thread.sleep(3000);
	}

	@And("Check the Export button on Reports")
	public void check_the_Export_button_on_Reports() throws InterruptedException {
	reports.getExport();
	Thread.sleep(3000);
	}
	@And("Click on Refresh on Customer")
	public void click_on_Refresh_on_Customer() throws InterruptedException {
	reports.getRefreshIcon();  
	}

	@Then("Click on one of the User on Reports")
	public void click_on_one_of_the_User_on_Reports() throws InterruptedException {
	reports.getManager(); 
	Thread.sleep(5000);
	}
	
	
	//Second Scenario
	@When("Verify the Customer added")
	public void verify_the_Customer_added() {
	reports.getCustomerAdded();
	}


	//Third Scenario
	@When("Verify the Visit Report")
	public void verify_the_Visit_Report() {
	reports.getVisitReport();
	}
}
