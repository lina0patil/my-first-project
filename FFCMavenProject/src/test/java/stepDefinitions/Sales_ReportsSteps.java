package stepDefinitions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Sales_Reports;

public class Sales_ReportsSteps extends OpenBrowser{
	Sales_Reports reports;
	@Given("User is scroll to Reports*")
	public void user_is_scroll_to_Reports() throws InterruptedException {
		Thread.sleep(2000);
		reports=new Sales_Reports(driver);
		reports.getVerticalSlider();
		Thread.sleep(3000);
	}
	@Given("Click on Reports*")
	public void click_on_Reports() {
		reports.getReports();
	}
	@Given("Again scroll to see all option in Sales Reports")
	public void again_scroll_to_see_all_option_in_Sales_Reports() throws InterruptedException {
		reports.getVerticalSlider();
	} 
	@And("Click on Sales")
	public void click_on_Sales() {
		reports.getSales();
	}
	@When("Verify the Sales by customer")
	public void verify_the_Sales_by_customer() {
		reports.getSalesByCustomer();	
	}
	@When("Check the From Date Filter on Sales")
	public void check_the_From_Date_Filter_on_Sales() throws InterruptedException {
		Thread.sleep(3000);
		reports.getFromDateFilter();
	}

	@When("Check the To Date Filter on Sales")
	public void check_the_To_Date_Filter_on_Sales() throws InterruptedException {
		reports.getToDateFilter();
	}

	@When("Check the Select user dropdown on Sales")
	public void check_the_Select_user_dropdown_on_Sales() throws InterruptedException {
		reports.getSelectUser();
	}

	@When("Check the Select Manager dropdown on Sales")
	public void check_the_Select_Manager_dropdown_on_Sales() throws InterruptedException {
		reports.getSelectManager();
	}
	@When("Check the Apply button on Sales")
	public void check_the_Apply_button_on_Sales() throws InterruptedException {
		reports.getApplyButton();
		Thread.sleep(2000);
	}
	@When("Check the Export button on Sales")
	public void check_the_Export_button_on_Sales() throws InterruptedException {
		reports.getExport();

	}
	@Then("Click on Refresh on Sales")
	public void click_on_Refresh_on_Sales() throws InterruptedException {
		reports.getRefreshIcon();  
		Thread.sleep(2000);
	}
	@And("Verify one of the Distributor on Sales")
	public void verify_one_of_the_Distributor_on_Sales() throws InterruptedException {
		reports.getDistributor(); 
		Thread.sleep(2000);
	}

	//2nd Scenario
	@When("Verify the Sales by Team")
	public void verify_the_Sales_by_Team() {
		reports.getSalesByTeam();	
	}
	@And("Verify one of the Distributor1 on Sales")
	public void verify_one_of_the_Distributor1_on_Sales() throws InterruptedException {
		reports.getDistributor1();  
	}
}
