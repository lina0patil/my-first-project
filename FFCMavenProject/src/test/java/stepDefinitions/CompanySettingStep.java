package stepDefinitions;

import java.awt.AWTException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.CompanySetting;

public class CompanySettingStep extends OpenBrowser{
	CompanySetting company;
	@Given("User is on Dashboard page to verify Company Setting Module")
	public void user_is_on_dashboard_page_to_verify_company_setting_module() {
	   company= new CompanySetting(driver);
	   
	}

	@And("Scroll upto Settings and click on that to check Company Setting module")
	public void scroll_upto_settings_and_click_on_that_to_check_company_setting_module() throws InterruptedException {
		company.VerticalSlider_Settings();
		company.getSettings();
	}

	@And("Click on Company Setting link")
	public void click_on_company_setting_link() throws InterruptedException {
		company.VerticalSlider_CompanySetting();
		company.companySettingLink();
	}

	@When("Fill the Company Setting deatils")
	public void fill_the_company_setting_deatils() throws InterruptedException, AWTException {
		company.companySettingDetail();
	}

	@And("Click on Update button to save changes in company setting")
	public void click_on_update_button_to_save_changes_in_company_setting() {
		company.update_CompanySetting();
	}

	@Then("Verify the validation message on Company Setting module")
	public void verify_the_validation_message_on_company_setting_module() {
		company.getVerifyMsg();
	}
	@And("Remove the logo")
	public void remove_the_logo() {
		company.removeLogo();
	}

	@And("Delete company setting")
	public void delete_company_setting() {
		company.delete_CompanySetting();
	}
}
