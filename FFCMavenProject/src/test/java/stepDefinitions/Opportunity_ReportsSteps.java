package stepDefinitions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import pageClasses.Opportunity_Reports;

public class Opportunity_ReportsSteps extends OpenBrowser{
	Opportunity_Reports reports;
	@Given("User is scroll to Opportunity Reports")
	public void user_is_scroll_to_Opportunity_Reports() throws InterruptedException {
		Thread.sleep(2000);
		reports=new Opportunity_Reports(driver);
		reports.getVerticalSlider();
		Thread.sleep(5000);
	}

	@And("Click on Opportunities Reports")
	public void click_on_Opportunity_Reports() {
		reports.getReports();
	}

	@And("Again scroll to see all option in Opportunity Reports")
	public void again_scroll_to_see_all_option_in_Opportunity_Reports() throws InterruptedException {
		reports.getVerticalSlider();
	}

	@And("Click on Opportunities")
	public void click_on_Opportunity() {
		reports.getOpportunity();
	}

	@When("Verify the Opportunity by Team")
	public void verify_the_Opportunity_by_Team() throws InterruptedException {
		reports.getOpportunityByTeam();
		Thread.sleep(3000);
	}

	@And("Check the From Date Filter on Opportunity")
	public void check_the_From_Date_Filter_on_Opportunity() throws InterruptedException {
		reports.getFromDateFilter();
		Thread.sleep(3000);
	}

	@And("Check the To Date Filter on Opportunity")
	public void check_the_To_Date_Filter_on_Opportunity() throws InterruptedException {
		reports.getToDateFilter();
	}

	@And("Check the Apply button on Opportunity")
	public void check_the_Apply_button_on_Opportunity() {
		reports.getApplyButton();
	}

	@And("Check the Export button on Opportunity")
	public void check_the_Export_button_on_Opportunity() throws InterruptedException {
		reports.getExport();
	}

	//2nd Scenario
	@When("Verify the Opportunity by Status")
	public void verify_the_Opportunity_by_Status() throws InterruptedException {
		reports.getOpportunityByStatus();
		Thread.sleep(3000);
	}
}
