package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Roles;

public class RolesStep  extends OpenBrowser {
	Roles roles;
	Actions as;
	int j;
	@Given("User is on Dashboard page to verify Roles Module")
	public void user_is_on_dashboard_page_to_verify_roles_module() throws IOException, InterruptedException, AWTException {
		roles= new Roles(driver);
		as=new Actions(driver);
		roles.ExcelConnect();
		roles.FetchExcelData(j);
	}
	@And("Scroll upto Settings and click on that to check Roles module")
	public void scroll_upto_settings_and_click_on_that_to_check_roles_module() throws InterruptedException {
		roles.VerticalSlider_Settings();
		roles.getSettings();
	}
	@And("Click on Roles link")
	public void click_on_roles_link() throws InterruptedException {
		roles.VerticalSlider_Roles();
		roles.rolesLink();
	}
	@When("Add new Role")
	public void add_new_role() throws InterruptedException {
		roles.addRole();
	}

	@Then("Verify the validation message on Roles module")
	public void verify_the_validation_message_on_company_setting_module() {
		roles.getVerifyMsg();
	}

	@And("edit the role name and description")
	public void edit_the_role_name_and_description() throws InterruptedException {
		roles.editRole_Name_Description();
		Thread.sleep(3000);
	}
	//permission
	@And("Click on Role name to verify module permission")
	public void click_on_role_name_to_verify_module_permission() throws InterruptedException {
		roles.roleName_Permission();
	}

	@And("Click on basic to check Basic Permissions and check validation msg")
	public void click_on_basic_to_check_basic_permissions_and_check_validation_msg() throws InterruptedException {
		roles.basicPermission();
		//Custometers
		roles.customersToggle_basicPermission();
		Thread.sleep(3000);
		roles.customers_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.customers_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.customers_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.customers_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.customers_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.customers_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Deals
		roles.DealsToggle_basicPermission();
		Thread.sleep(3000);
		roles.DealsDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.DealsDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.DealsDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.DealsDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Tasks
		roles.TasksToggle_basicPermission();
		Thread.sleep(3000);
		roles.TasksDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.TasksDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.TasksDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.TasksDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//attendance
		roles.AttendanceToggle_basicPermission();
		Thread.sleep(3000);
		roles.AttendanceDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.AttendanceDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.AttendanceDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.AttendanceDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Expense
		roles.ExpenseToggle_basicPermission();
		Thread.sleep(3000);
		roles.ExpenseDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ExpenseDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ExpenseDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ExpenseDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Orders
		roles.OrdersToggle_basicPermission();
		Thread.sleep(3000);
		roles.OrdersDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.OrdersDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.OrdersDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.OrdersDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.OrdersDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Catalogue
		roles.CatalogueToggle_basicPermission();
		Thread.sleep(3000);
		roles.CatalogueDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Dynamic Form
		roles.DynamicFormToggle_basicPermission();
		Thread.sleep(3000);
		roles.DynamicFormDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Visit Form
		roles.VisitFormToggle_basicPermission();
		Thread.sleep(3000);
		roles.VisitFormDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Communication
		roles.CommunicationToggle_basicPermission();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Referral
		roles.ReferralToggle_basicPermission();
		Thread.sleep(3000);
		roles.ReferralDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ReferralDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ReferralDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ReferralDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ReferralDropdown_basicPermission();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Leave Management
		roles.LeaveManagementToggle_basicPermission();
		Thread.sleep(3000);
		roles.LeaveManagementDropdown_basicPermission();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
	}

	@And("Click on Dashboard360 to check Dashboard360 Permissions and check validation msg")
	public void click_on_dashboard360_to_check_dashboard360_permissions_and_check_validation_msg() throws InterruptedException {
		Thread.sleep(3000);
		roles.dashboard360_Permission();
		//User driven
		roles.userDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.userDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.userDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.userDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.userDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.userDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Expense driven
		roles.ExpenseDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.ExpenseDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.ExpenseDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Customer driven
		roles.CustomerDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.CustomerDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.CustomerDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//opportunity driven
		roles.opportunityDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.opportunityDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.opportunityDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.opportunityDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.opportunityDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.opportunityDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Sales driven
		roles.salesDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.salesDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.salesDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.salesDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.salesDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Marketing driven
		roles.marketingDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.marketingDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.marketingDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.marketingDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Task driven
		roles.taskDrivenToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.taskDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.taskDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.taskDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.taskDrivenDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
	}

	@And("Click on reports to check Reports Permissions and check validation msg")
	public void click_on_reports_to_check_reports_permissions_and_check_validation_msg() throws InterruptedException {
		roles.reports_Permission();
		Thread.sleep(3000);
		//Customer report
		roles.customersReportToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.customersReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.customersReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Expense report
		roles.expenseReportToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.expenseReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.expenseReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Attendance report
		roles.attendanceReportToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.attendanceReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Sales Report
		roles.salesReportToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.salesReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.salesReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Deals report
		roles.dealsReportToggle_Dashboard360Permissions();
		Thread.sleep(3000);
		roles.dealsReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.dealsReportDropdown_Dashboard360Permissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
	}

	@And("Click on admin to check Admin Permissions and check validation msg")
	public void click_on_admin_to_check_admin_permissions_and_check_validation_msg() throws InterruptedException {
		roles.admin_Permission();
		Thread.sleep(3000);
		roles.getScrolltoTaskAgainstLeadTab();
		//Billing
		roles.billingToggle_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Integrations
		roles.integrationsToggle_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//User Management
		roles.userManagementToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.userManagementDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.userManagementDropdown_AdminPermissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//All Users Data
		roles.allUsersDataToggle_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Catalogue
		roles.catalogueToggleAdmin_AdminPermissions();
		Thread.sleep(3000);
		roles.catalogueDropdownAdmin_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.catalogueDropdownAdmin_AdminPermissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.catalogueDropdownAdmin_AdminPermissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Company Settings
		roles.companySettingsToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.companySettingsDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Expense Settings
		roles.expenseSettingsToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.expenseSettingsDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//CRM Settings
		roles.cRMSettingsToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.cRMSettingsDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Territory Master
		roles.territoryMasterToggle_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Attendance Settings
		roles.attendanceSettingsToggle_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Dynamic Form
		roles.dynamicFormToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.dynamicFormDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.dynamicFormDropdown_AdminPermissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Order Management
		roles.orderManagement_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Custom Fields
		roles.customFieldsToggle_AdminPermissions();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Visit Form
		roles.visitFormToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.visitFormDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		roles.visitFormDropdown_AdminPermissions();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Task Setting
		roles.taskSettingToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.taskSettingDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Deals and Pipeline Settings
		roles.dealsandPipelineSettingsToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.dealsandPipelineSettingsDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
		//Leave Settings
		roles.leaveSettingsToggle_AdminPermissions();
		Thread.sleep(3000);
		roles.leaveSettingsDropdown_AdminPermissions();
		as.sendKeys(Keys.ENTER).perform();
		roles.getVerifyMsg();
		Thread.sleep(3000);
	}
	
	@When("Click on delete role and confirm delete and check validation message on Roles module")
	public void click_on_delete_role_and_confirm_delete_and_check_validation_message_on_roles_module() throws InterruptedException {
		roles.deleteRole();
	}

}
