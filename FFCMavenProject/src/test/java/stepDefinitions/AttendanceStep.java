package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Attendance;

public class AttendanceStep extends OpenBrowser{
	int j;
	Attendance attend;
	@Given("User is on Dashboard page to verify My claim")
	public void user_is_on_dashboard_page_to_verify_my_claim() throws InterruptedException, IOException, AWTException {
		attend=new Attendance(driver);
		attend.ExcelConnect();
		attend.FetchExcelData(j);
	}

	@And("Click on Attendance link")
	public void click_on_attendance_link() {
		attend.getAttendenceLink();
	}

	@And("Click on Add new button to add claim on My Claims screen")
	public void click_on_add_new_button_to_add_claim_on_my_claims_screen() {
		attend.getAddNewClaim();
	}

	@And("Fill the claim details")
	public void fill_the_claim_details() throws InterruptedException {
		//attend.getselectPunchInDate();
		//attend.getDateFilter();
		//attend.getselectPunchOutDate();
	//	attend.getDateFilter();
		attend.getselectPunchIn_OutTime();
		attend.getReasonForClaim();
	}

	@When("Click on Done button to add the claim")
	public void click_on_done_button_to_add_the_claim() {
		attend.getDoneClaim();
	}

	@Then("Check the validation msg for attendance")
	public void check_the_validation_msg_for_attendance() {
		attend.getVerifyMsg();
	}
	
	
	//filter
	
	@And("Click on Filter button to filter Claims according to status and date")
	public void click_on_filter_button_to_filter_claims_according_to_status_and_date() {
		attend.getfilterclaimbutton();
	}

	@And("Select the status and date")
	public void select_the_status_and_date() throws InterruptedException {
		attend.getclaimByStatus();
		attend.getfromdatefilterclaim();
		attend.getDateFilter();
		attend.gettodatefilterclaim();
		attend.getDateFilter();
		
	}

	@When("Apply it for filtering claim")
	public void apply_it_for_filtering_claim() {
		attend.getapplyClaim();
	}
	@And("View the claim reason")
	public void view_the_claim_reason() throws InterruptedException {
		Thread.sleep(5000);
		attend.viewClaimReason();
	}

	@Then("Clear the filter claim")
	public void clear_the_filter_claim() throws InterruptedException {
		Thread.sleep(5000);
		attend.clearFilter();
	}

	
	
	//Team claims
	
	@Given("Click on Team Claims")
	public void click_on_team_claims() {
		attend.teamClaims();
	}

	@When("Search the claim by entering user name")
	public void search_the_claim_by_entering_user_name() {
		attend.searchuser();
	}

	@When("Accept the pending claim")
	public void accept_the_pending_claim() {
		attend.acceptClaim();	
	}

	@When("Click on Filter button to filter Claims according to member status and date")
	public void click_on_filter_button_to_filter_claims_according_to_member_status_and_date() {
		attend.getfilterclaimbutton();	
	}

	@When("Select the member status and date")
	public void select_the_member_status_and_date() throws InterruptedException {
		attend.getTeamclaimByStatus();
		attend.getfromdatefilterTeamclaim();
		attend.getDateFilter();
		attend.gettodatefilterTeamclaim();
		attend.getDateFilter();
	}

	@When("View the claim details and close it")
	public void view_the_claim_details_and_close_it() {
		attend.viewClaimDetails();	
	}

	@Given("Delete the pending claim and mention the reason")
	public void delete_the_pending_claim_and_mention_the_reason() throws InterruptedException {
		attend.deleteClaim();	
		
	}
	//Global map
	@When("Verify Global Map icon on attendance module")
	public void verify_global_map_icon_on_attendance_module() {
		attend.globalMap();
	}

	@When("Select the date to view the user location")
	public void select_the_date_to_view_the_user_location() throws InterruptedException {
		attend.dateFilter_globalMap();
		attend.getDateFilter();
	}

	@When("Maximize the Map for full screen view on Attendance Module")
	public void maximize_the_map_for_full_screen_view_on_attendance_module() {
		attend.fullscreenView();
	}

	@When("Zoom Out and Zoom In for better view of map on Attendance Module")
	public void zoom_out_and_zoom_in_for_better_view_of_map_on_attendance_module() throws InterruptedException {
		attend.zoomOut_zoomIn();
	}

	@When("Click on Satellite on map Attendance Module")
	public void click_on_satellite_on_map_attendance_module() throws InterruptedException {
		attend.satellite();
	}

	@Then("Minimize the map on Attendance Module")
	public void minimize_the_map_on_attendance_module() {
		attend.fullscreenView();
	}
	@Then("Click on view report icon on attendance module")
	public void click_on_view_report_icon_on_attendance_module() throws InterruptedException {
		Thread.sleep(3000);
		attend.viewReport();
	}
	@Then("Download the report on the basis of date User and Manager")
	public void download_the_report_on_the_basis_of_date_user_and_manager() throws InterruptedException {
		attend.selectFromDate_attendanceReport();
		attend.getDateFilter();
		attend.selectToDate_attendanceReport();
		attend.getDateFilter();
		attend.selectUser_Manager_attendanceReport();
		attend.downoad_attendanceReport();
	}
	@Then("Download the User not punch in report icon")
	public void download_the_user_not_punch_in_report_icon() throws InterruptedException {
		attend.userNotPunchInReport();
	}

	@Then("Check the claims icon")
	public void check_the_claims_icon() {
		attend.claimsIcon();
	}
	
	//User settings
	@And("Search the user by entering the user name")
	public void search_the_user_by_entering_the_user_name() throws InterruptedException {
		//attend.searchUser();
	}

	@When("Click on  user settings")
	public void click_on_user_settings() {
		attend.userSettings();
	}

	@And("Set the designation")
	public void set_the_designation() {
		attend.designation();
	}

	@And("Turn on the reminder punch in time toggle button and set the time")
	public void turn_on_the_reminder_punch_in_time_toggle_button_and_set_the_time() {
		attend.reminderPunchintoggle();
		attend.setReminderPunchintime();
	}

	@And("Set the Punch In With Geo-Fencing using map")
	public void set_the_punch_in_with_geo_fencing_using_map() throws InterruptedException {
		attend.punchInWithGeoFencing();
	}

	@And("Set the Punch Out With Geo-Fencing using map")
	public void set_the_punch_out_with_geo_fencing_using_map() throws InterruptedException {
		attend.punchOutWithGeoFencing();
	}

	@And("Set Monthly Leaves Allowed In Days")
	public void set_monthly_leaves_allowed_in_days() {
		attend.MonthlyLeavesAllowed();
	}
	@When("Click on Notify admin and assign new admin")
	public void click_on_notify_admin_and_assign_new_admin() {
		attend.notifyAdmin();
	}
	@Then("Notify the manager by email and mobile")
	public void notify_the_manager_by_email_and_mobile() {
		attend.notifyManager();
	}
	@And("Click on make admin toggle button")
	public void click_on_make_admin_toggle_button() {
		attend.makeAdmintoggle();
	}
	@And("Set the entire day tracking")
	public void set_the_entire_day_tracking() {
		attend.entireDayTrackingtoggle();
	}

	@And("Select the Weekly off day")
	public void select_the_weekly_off_day() {
		attend.weeklyOff();
	}

	@And("Select the day for alternate day off in month")
	public void select_the_day_for_alternate_day_off_in_month() {
		attend.alternateDayOffInMonth();
	}

	@And("Select alternate day off count")
	public void select_alternate_day_off_count() {
		attend.alternateDayOffCount();
	}
	@Then("Click on done user settings button to save changes")
	public void click_on_done_user_settings_button_to_save_changes() {
		attend.doneUserSettings();
	}
	//User Report
	@When("Click on  user report")
	public void click_on_user_report() {
		attend.userReport();
	}

	@When("Select date and enter the email id")
	public void select_date_and_enter_the_email_id() throws InterruptedException {
		attend.date_userReport();
		attend.getDateFilter();
		attend.emailId_userReport();		
	}

	@Then("Click on send button")
	public void click_on_send_button() {
		attend.sendButton();
	}
	//User Location
	@When("Click on user location and verify it")
	public void click_on_user_location_and_verify_it() throws InterruptedException {
		attend.userLocation();
	}
	@And("Search the user by entering the user name for user location")
	public void search_the_user_by_entering_the_user_name_for_user_location() throws InterruptedException {
		//attend.searchUser_userLocation();
	}
	@Then("Click on Location Pin Icon on user location")
	public void click_on_location_pin_icon_on_user_location() throws InterruptedException {
		attend.userLocationPin_userLocation();
	}
}
