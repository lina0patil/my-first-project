package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.MyTask;

public class MyTaskSteps extends OpenBrowser{
	int j;
	MyTask mytask;
	@Given("User is on Dashboard page to check the my task")
	public void user_is_on_dashboard_page_to_check_the_my_task() throws InterruptedException{
		Thread.sleep(2000);
		mytask=new MyTask(driver);
		
	}

	@And("Click on My Task link")
	public void click_on_my_task_link() throws IOException, InterruptedException, AWTException{
		mytask.getMyTaskLink();
		mytask.ExcelConnect();
		mytask.FetchExcelData(j);
	}

	@And("Click on Add Task button to add the task")
	public void click_on_add_task_button_to_add_the_task() throws InterruptedException {
		mytask.getAddTaskButton();
	}

	@And("Fill the task details on Assign to Others")
	public void fill_the_task_details_on_assign_to_others() throws InterruptedException, AWTException{
		mytask.getTaskDetails_AssignToOther();
		mytask.getDateFilter();
		mytask.getTaskDetails_AssignToOther2();

	}

	@When("Click on Add button to assign the task")
	public void click_on_add_button_to_assign_the_task() throws InterruptedException {
		mytask.getAddButton_AssignToOther();
	}

	@Then("Check the validation msg for task")
	public void check_the_validation_msg_for_task() {
		mytask.getVerifyMsg();
	}
	
	//assign to me
	@When("Click on Assign to me")
	public void click_on_assign_to_me() throws InterruptedException {
		mytask.getAssignToMeScreen();
	}

	@Given("Fill the task details on Assign to me")
	public void fill_the_task_details_on_assign_to_me() throws InterruptedException, AWTException {
		mytask.getTaskDetails_AssignToMe();
		mytask.getDateFilter();
		mytask.getTaskDetails_AssignToMe2();
	}
	
	//edit
	@When("Click on Title name to edit the task")
	public void click_on_title_name_to_edit_the_task() throws InterruptedException {
		mytask.getTitleNameToEditTask();
	}

	@Then("Click on Edit button to edit the task details")
	public void click_on_edit_button_to_edit_the_task_details() throws InterruptedException {
		mytask.getEditButtonToEditTaskdetails();
	}

	@And("Edit the task details on Assign to Me tab")
	public void edit_the_task_details_on_assign_to_me_tab() throws InterruptedException, AWTException {
		mytask.getEditTaskDetails_AssignToMe();
		mytask.getDateFilter();
		mytask.getEditTaskDetails_AssignToMe2();
	}
	@And("Add comment for that task")
	public void add_comment_for_that_task() throws InterruptedException, AWTException {
		mytask.addComment_AssignToMe();
	}
	@And("Click on cancel button")
	public void click_on_cancel_button() throws InterruptedException {
		mytask.cancelbutton();
	}
	//4 buttons
	@When("Search the added task by entering title name")
	public void serach_the_added_task_by_entering_title_name() throws InterruptedException {
		//mytask.search_Mytask();
	}

	@When("Accept that task")
	public void accepet_that_task() throws InterruptedException {
		mytask.acceptTask_Mytask();
	}

	@When("Select status as partial complete task and select percentage")
	public void select_status_as_partial_complete_task_and_select_percentage() throws InterruptedException {
		mytask.partialCompleteTask();
	}

	@When("Select status as task completed")
	public void select_status_as_task_completed() throws InterruptedException {
		mytask.completeTask();
	}

	@When("View task coverage and close it")
	public void view_task_coverage_and_close_it() throws InterruptedException {
		mytask.viewTaskCoverage();
	}

	@When("Archive the task")
	public void archive_the_task() throws Throwable {
		mytask.archiveTask();
	}

	@When("Go to Archive List tab and Unarchive that task")
	public void go_to_archive_list_tab_and_unarchive_that_task() throws Exception {
		mytask.archiveListScreen();
		mytask.unarchiveTask();
	}
	@When("Go to My task tab and Delete the task")
	public void go_to_my_task_tab_and_delete_the_task() throws InterruptedException {
		mytask.myTaskScreen();
		mytask.deletetask();
	}
	@When("Click on Filter button on MY Task to filter the task according to priority, member, status, and date")
	public void click_on_filter_button_on_my_task_to_filter_task_according_to_priority_member_status_and_date() throws InterruptedException {
		Thread.sleep(4000);
		mytask.filter_mytask();
	}

	@When("Select the priority, member, status, and date on my task filter")
	public void select_the_priority_member_status_and_date_on_my_task_filter() throws InterruptedException {
		mytask.filterbyStatus_MyTask();
		Thread.sleep(2000);
		mytask.filterbyPriority_MyTask();
		Thread.sleep(2000);
		mytask.filterByMember_MyTask();
		Thread.sleep(2000);
		mytask.fromDuedate_MyTask();
		Thread.sleep(2000);
		mytask.getDateFilter();
		Thread.sleep(2000);
		mytask.toDuedate_MyTask();
		Thread.sleep(2000);
		mytask.getDateFilter1();
		Thread.sleep(2000);
	}

	@When("Apply it for filtering taks on my task")
	public void apply_it_for_filtering_taks_on_my_task() throws InterruptedException {
		mytask.getApplyFilterButtonMytask();
		Thread.sleep(3000);
	}

	@Then("Clear the filter on my task")
	public void clear_the_filter_on_my_task() {
		mytask.getClearFilterMyTask();
	}

	//cc buttons
	@And("Fill the task details on Assign to Others on CC")
	public void fill_the_task_details_on_assign_to_others_on_CC() throws InterruptedException, AWTException{
		mytask.getTaskDetails_AssignToOtherCC();
		mytask.getDateFilter();
		mytask.getTaskDetails_AssignToOther2();

	}
	@When("View task coverage and close it on CC")
	public void view_task_coverage_and_close_it_on_cc() throws InterruptedException {
		mytask.viewTaskCoverage_CC();
	}
	@When("Archive the task on CC")
	public void archive_the_task_on_cc() throws Throwable {
		mytask.archiveTask_CC();
	}
	@When("Go to CC tab and Delete the task")
	public void Go_to_cc_tab_and_delete_the_task() throws InterruptedException {
		mytask.getCC_MyTask();
		mytask.deletetask_CC();
	}
	//Assign By Me
	@Given("Fill the task details on Assign to Others on Assigned By Me")
	public void fill_the_task_details_on_assign_to_others_on_assigned_by_me() throws InterruptedException, AWTException {
		mytask.getTaskDetails_AssignToOtherAssignByMe();
		mytask.getDateFilter();
		mytask.getTaskDetails_AssignToOther2();
	}

	@When("View task coverage and close it on Assigned By Me")
	public void view_task_coverage_and_close_it_on_assigned_by_me() throws InterruptedException {
		mytask.viewTaskCoverage_AssignByMe();
	}

	@When("Archive the task on Assigned By Me")
	public void archive_the_task_on_assigned_by_me() throws InterruptedException {
		mytask.archiveTask_AssignByMe();
	}

	@Then("Go to Assigned By Me tab and Delete the task")
	public void go_to_assigned_by_me_tab_and_delete_the_task() throws InterruptedException {
		mytask.getAssignedByMe_MyTask();
		mytask.deletetask_AssignByMe();
	}
	//calendar view
	@And("Click on Calendar View on My task")
	public void click_on_calendar_view_on_my_task() {
		mytask.CalenderView();
	}

	@And("Click on previous button and fetch that month")
	public void click_on_previous_button_and_fetch_that_month() throws InterruptedException {
		mytask.previousButton_text();
	}

	@And("Click on Today button and fetch that month")
	public void click_on_today_button_and_fetch_that_month() throws InterruptedException {
		mytask.toDayButton_text();
	}

	@And("Click on next button and fetch that month")
	public void click_on_next_button_and_fetch_that_month() throws InterruptedException {
		mytask.nextButton_text();
	}
	@And("Select an option as CC")
	public void select_an_option_as_cc() throws InterruptedException {
		mytask.selectOption();
	}

	@Then("Check the UI on weekly basis")
	public void check_the_ui_on_weekly_basis() throws InterruptedException {
		mytask.weekButton();
		mytask.previousButton_text();
		mytask.toDayButton_text();
		mytask.nextButton_text();
	}

	@And("Select an option as Assigned By Me")
	public void select_an_option_as_assigned_by_me() throws InterruptedException {
		mytask.selectOption1();
	}
	@And("Check the UI on daily basis")
	public void check_the_ui_on_daily_basis() throws InterruptedException {
		mytask.dayButton();
		mytask.previousButton_text();
		mytask.toDayButton_text();
		mytask.nextButton_text();
	}

	

	//export
	@And("Click on Assigned By Me")
	public void click_on_assigned_by_me() throws InterruptedException {
		mytask.assignedByMeScreen();
	}

	@And("Click on Archive List")
	public void click_on_archive_list() throws InterruptedException {
		mytask.archiveListScreen();
	}
	@Then("Check the Export on MY Task")
	public void check_the_export_on_my_task() {
		mytask.export_MyTask();
	}
	//My task filter

	
	//CC Filter
	@When("Click on CC tab on My task")
	public void click_on_cc_tab_on_my_task() {
		mytask.getCC_MyTask();
	}

	@When("Click on Filter button on MY Task to filter task according to status, priority, member.")
	public void click_on_filter_button_on_my_task_to_filter_task_according_to_status_priority_member() throws InterruptedException {
		mytask.filter_mytask();

	}

	@When("Select the status, priority, member on my task filter")
	public void select_the_status_priority_member_on_my_task_filter() throws InterruptedException {
		mytask.filterbyStatus_MyTask();
		mytask.filterbyPriority_MyTask();
		mytask.filterByMember_MyTask();
	}
	// Assigned By Me Filter
	@When("Click on Assigned By Me tab on My task")
	public void click_on_assigned_by_me_tab_on_my_task() {
		mytask.getAssignedByMe_MyTask();
	}
	
	//Settings
	@When("Check the Settings")
	public void check_the_settings() {
		mytask.settingButton();
	}

	@When("Add the percentage of task")
	public void add_the_percentage_of_task() {
		mytask.addPercentage();
	}

	@When("Select percentage and remove it")
	public void select_percentage_and_remove_it() {
		mytask.settingButton();
		mytask.select_removePercentage();
	}
	@Then("Close the settings popup")
	public void close_the_settings_popup() {
		mytask.closesetting();
	}


}
