package stepDefinitions;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Expense_Reports;

public class Expense_ReportsSteps extends OpenBrowser {

	Expense_Reports reports;
	@Given("User is scroll to Reports^")
	public void user_is_scroll_to_Reports() throws InterruptedException {
		Thread.sleep(2000);
		reports=new Expense_Reports(driver);
		reports.getVerticalSlider();
		Thread.sleep(5000);
	}

	@And("Click on Reports^")
	public void click_on_Reports() {
		reports.getReports();
	}

	@And("Again scroll to see all option in Expense Reports")
	public void again_scroll_to_see_all_option_in_Reports() throws InterruptedException {
		reports.getVerticalSlider();
	}

	@And("Click on Expense")
	public void click_on_Expense() {
		reports.getExpense();
	}

	@When("Verify the Expense by Team")
	public void verify_the_Expense_by_Team() throws InterruptedException {
		reports.getExpenseByTeam();
		Thread.sleep(3000);
	}

	@And("Check the From Date Filter on Expense")
	public void check_the_From_Date_Filter_on_Expense() throws InterruptedException {
		reports.getFromDateFilter();
		Thread.sleep(3000);
	}

	@And("Check the To Date Filter on Expense")
	public void check_the_To_Date_Filter_on_Expense() throws InterruptedException {
		reports.getToDateFilter();
	}

	@And("Check the Select user dropdown on Expense by Team")
	public void check_the_Select_user_dropdown_on_Expense_by_Team() throws InterruptedException {
		reports.getSelectUser();
	}

	@And("Check the Select Manager dropdown on Expense by Team")
	public void check_the_Select_Manager_dropdown_on_Expense_by_Team() throws InterruptedException {
		reports.getSelectManager();
	}

	@And("Check the Apply button on Expense")
	public void check_the_Apply_button_on_Expense() {
		reports.getApplyButton();
	}

	@And("Check the Export button on Expense by Team")
	public void check_the_Export_button_on_Expense_by_Team() throws InterruptedException {
		reports.getExport();
	}

	@Then("Click on Refresh on Expense by Team")
	public void click_on_Refresh_on_Expense_by_Team() throws InterruptedException {
		reports.getRefreshIcon(); 
	}

	@And("Verify one of the Manager on Expense by Team")
	public void verify_one_of_the_Manager_on_Expense_by_Team() throws InterruptedException {
		reports.getManager(); 
	}
	
	
	//2nd Scenario
	@When("Verify the Expense by Category")
	public void verify_the_Expense_by_Category() throws InterruptedException {
		reports.getExpenseByCategory();
		Thread.sleep(3000);
	}

	
}
