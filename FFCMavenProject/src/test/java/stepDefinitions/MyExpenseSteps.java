package stepDefinitions;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.ExcelCode;
import pageClasses.MyExpense;

public class MyExpenseSteps extends OpenBrowser{
	int j;
	MyExpense expense;
	ExcelCode readXlsx;
	@Given("User is on Dashboard page to check My Expense module")
	public void user_is_on_dashboard_page_to_check_my_expense_module() throws InterruptedException, IOException, AWTException {	
	Thread.sleep(3000);
	expense=new MyExpense(driver);
	expense.ExcelConnect();
	expense.FetchExcelData(j);
	}
	@When("Click on My Expense")
	public void click_on_my_expense() {
	expense.getMyExpense();
	}

	@And("Check the Export")
	public void check_the_export() {
	expense.getExport();
	}

	@And("Check Data")
	public void check_data() throws IOException {
	//copy path of excel
	String path="src\\test\\java\\utility\\Expense - Expected.xlsx";
 
    //create the object of file class to open xlsx file 
	File file=new File(path);
		    	
    //to fetch the date from excel sheet/ to read the file
    FileInputStream fs=new FileInputStream(file);
		    	
    XSSFWorkbook wb=new XSSFWorkbook(fs);
	 	    	
	XSSFSheet sheet=wb.getSheetAt(0);
	for(Row row :sheet) {
	if(row.getRowNum()>1) {
	break;
	}
	else {
	for(org.apache.poi.ss.usermodel.Cell cell :row) {
	System.out.println(cell.getStringCellValue());
	}
	}
	}
	}
	@Then("Verify the expense filter dropdown")
	public void verify_the_expense_filter_dropdown() throws InterruptedException {
    expense.getExpenseFilter();
	}
	@And("Select status")
	public void select_status() {
    expense.getByStatus();
	}
	@Then("Select status by pending_Apply it_Click on export_Clear the filter")
	public void select_status_by_pending_apply_it_click_on_export_clear_the_filter() throws InterruptedException {
	expense.getPending();
	Thread.sleep(2000);
	expense.getApplyFilterButton();
	Thread.sleep(3000);
	expense.getExport();
    System.out.println("Pending Data");
    readXlsx.readXLSXFile("src\\test\\java\\utility\\Expense Pending Data.xlsx");
	Thread.sleep(3000);
	expense.getClearFilter();
	Thread.sleep(2000);
	}

	@Then("Select status by rejected_Apply it_Click on export_Clear the filter")
	public void select_status_by_rejected_apply_it_click_on_export_clear_the_filter() throws InterruptedException {
	expense.getExpenseFilter();
	expense.getByStatus();
	expense.getRejected();
	Thread.sleep(2000);
	expense.getApplyFilterButton();
	Thread.sleep(3000);
	expense.getExport();
	System.out.println("Rejected Data");
	readXlsx.readXLSXFile("src\\test\\java\\utility\\Expense Rejected Data.xlsx");
	Thread.sleep(3000);
	expense.getClearFilter();
	Thread.sleep(2000);
	}

	@Then("Select status by pendingReimbursementData_Apply it_Click on export_Clear the filter")
	public void select_status_by_pending_reimbursement_data_apply_it_click_on_export_clear_the_filter() throws InterruptedException {
	expense.getExpenseFilter();
	expense.getByStatus();
	expense.getPendingReimbursementData();
	Thread.sleep(2000);
	expense.getApplyFilterButton();
	Thread.sleep(3000);
	expense.getExport();
	System.out.println("Pending Reimbursement Data");
	readXlsx.readXLSXFile("src\\test\\java\\utility\\Expense Pending Reimbursement Data.xlsx");
	Thread.sleep(5000);
	expense.getClearFilter();
	Thread.sleep(2000);
	}
	@And("Close the Expense browser")
	public void close_the_browser() throws InterruptedException {
	Thread.sleep(2000);
	driver.close();
	}
	
	
	//My claims> add new
	
	@And("Click on My Claims")
	public void click_on_my_claims() {
		expense.getmyClaims();
	}

	@And("Click on Add claim for the expense")
	public void click_on_add_claim_for_the_expense() {
		expense.getaddnewClaim();
	}

	@When("Enter the Expense data")
	public void enter_the_expense_data() throws AWTException, InterruptedException {
		expense.fillExpenseData();
		expense.getDateFilter();
	}

	@And("Click on Add button to add expense claim")
	public void click_on_add_button_to_add_expense_claim() {
		expense.getaddClaimButton();
	}

	@And("Check validation message for expense claim")
	public void check_validation_message_for_expense_claim() {
	    expense.getVerifyMsg();
	}
	
	//My claims> edit
	@And("Filter the claim by pending status on my claim")
	public void filter_the_claim_by_pending_status_on_my_claim() throws InterruptedException {
	expense.filter_MyClaim();
	expense.fromdate_MyClaim1();
	expense.getDateFilter();
	expense.todate_MyClaim2();
	expense.getDateFilter1();
	expense.getaaplyfilterButton_MyClaim();
	
	
	}
	@And("Search the claim by entering category name")
	public void search_the_claim_by_entering_category_name() {
		//expense.searchByCategoryName();
	}

	@When("Edit the claim by clicking on category name which is in pending status")
	public void edit_the_claim_by_clicking_on_category_name_which_is_in_pending_status() {
		expense.getCategoryName();
	}

	@And("Click on edit button to edit fields claim")
	public void click_on_edit_button_to_edit_fields_claim() throws InterruptedException, AWTException{
	
			expense.editClaimExpense();
			expense.getDateFilter();
	}

	@And("Click on Update button to update claim")
	public void click_on_update_button_to_update_claim() {
		expense.updateExpenseClaim();
	}
	@Then("View the logs and close it-Expense")
	public void view_the_logs_and_close_it_expense() throws InterruptedException {
		expense.getviewLogs();
	}
	@Then("Close the expense detail")
	public void close_the_expense_detail() throws InterruptedException {
		expense.CloseExpenseDetails();
	}
	
	//Claims for approval> edit
	@And("Filter the claim by pending status on Claims for approval")
	public void filter_the_claim_by_pending_status_on_claims_for_approval() {
		expense.filter_ClaimsForApproval();
	}
	@And("Edit the claim by clicking on Username which is in pending status")
	public void edit_the_claim_by_clicking_on_username_which_is_in_pending_status() {
		expense.userName();
	}

	@And("Click on edit button on claims for approval")
	public void click_on_edit_button_on_claims_for_approval() throws InterruptedException, AWTException {
		expense.editClaimExpense();
		
	}

	@Given("Click on Update button and check validation message on claims for approval")
	public void click_on_update_button_and_check_validation_message_on_claims_for_approval() {
	
	}

	//Take action
	@Given("Click on Username which is in pending status on Claims for approval")
	public void click_on_username_which_is_in_pending_status_on_claims_for_approval() {
		expense.userName();
		
	}

	@When("Click on Take action button on  Claims for approval")
	public void click_on_take_action_button_on_claims_for_approval() throws InterruptedException {
		expense.takeActionButton();
	}

	@When("Fill the Expense Detail Action")
	public void fill_the_expense_detail_action() {

	}

	@When("Click on Partial approve button and check validation message")
	public void click_on_partial_approve_button() throws InterruptedException {

	}
	
	//Accept-Delete claim button 
	@Given("Select the checkbox on Claims for approval which is in pending status")
	public void select_the_checkbox_on_claims_for_approval_which_is_in_pending_status() throws InterruptedException {
		expense.checkboxSelection();
	}

	@When("Click on Accept claim button on Claims for approval")
	public void click_on_accept_claim_button_on_claims_for_approval() throws InterruptedException {
		expense.acceptClaimButton();
	}

	@When("Click on Decline claim button on Claims for approval")
	public void click_on_Decline_claim_button_on_claims_for_approval() throws InterruptedException {
		expense.deleteClaimButton();
	}
	
	@When("Mention the reason for expense rejection and decline it")
	public void mention_the_reason_for_expense_rejection_and_decline_it() throws InterruptedException {
		expense.reasonForExpenseRejection();
	}
	
	//Accept-Delete claim image 
	@When("Click on Accept claim image on Claims for approval")
	public void click_on_accept_claim_image_on_claims_for_approval() throws InterruptedException {
		expense.acceptClaimImage();
	}

	@When("Click on Decline claim image on Claims for approval")
	public void click_on_decline_claim_image_on_claims_for_approval() throws InterruptedException {
		expense.declineClaimImage();
	}
	//export-Filter
	@When("Click on Filter button on Claims for approval to filter Claims according to location, member, status, category and date")
	public void click_on_filter_button_on_claims_for_approval_to_filter_claims_according_to_location_member_status_category_and_date() {
		expense.getExpenseFilter();
	}

	@When("Select the location, member, status, category and date on expense filter")
	public void select_the_location_member_status_category_and_date_on_expense_filter() throws InterruptedException {
		expense.filterByLocation_claimsForApproval();
		expense.filterByMember_claimsForApproval();
		expense.filterByStatus_claimsForApproval();
		expense.filterByCategory_claimsForApproval();
		expense.fromdate_claimsForApproval();
		expense.getDateFilter();
		expense.todate_claimsForApproval();
		expense.getDateFilter1();
	}

	@When("Apply it for filtering Claims on Claims for approval")
	public void apply_it_for_filtering_claims_on_claims_for_approval() {
		expense.getApplyFilterButton();
	}
	@Then("Clear the filter on Claims for approval")
	public void clear_the_filter_on_claims_for_approval() throws InterruptedException {
	  Thread.sleep(3000);
	  expense.getClearFilter();
	  
	}
}
