package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.Deals_Pipeline;

public class Deals_PipelineSteps extends OpenBrowser{
	Deals_Pipeline D_P;
	int j;
@Given("User is on dashboard page-D & P")
public void user_is_on_dashboard_page_d_p() throws InterruptedException {
	Thread.sleep(2000);
	D_P=new Deals_Pipeline(driver);
}
	@And("Click on Deals & Pipeline link")
	public void click_on_deals_pipeline_link() throws IOException, InterruptedException, AWTException {
		D_P=new Deals_Pipeline(driver);
		D_P.ExcelConnect();
		D_P.FetchExcelData(j);
		D_P.getDeals_Pipeline();
	}

	@When("Click on Add Opportunities")
	public void click_on_add_opportunities() throws InterruptedException {
		Thread.sleep(2000);
		D_P.getAddOpportunities();
	}

	@And("Fill the opportunities input")
	public void fill_the_opportunities_input() throws InterruptedException {	
		D_P.getOtherField();
		D_P.getDateFilter();
		D_P.getTags_Notes();
	}

	@And("Click on the Add button to Add Opportunity")
	public void click_on_the_add_button_to_add_opportunity() throws InterruptedException {
		D_P.getAddOppoButton();
	}

	@Then("Check validation msg of add opportunities")
	public void check_validation_msg_of_add_opportunities() {
		D_P.getVerifyMsg();
	}
	
	//Edit opportunity
	@When("Click on Edit Opportunities")
	public void click_on_edit_opportunities() throws InterruptedException {
		D_P.getEditOpportunityIcon();	
	}

	@When("Edit the opportunities input")
	public void edit_the_opportunities_input() throws InterruptedException {
		D_P.getEditOpportunity();
		D_P.getDateFilter();
		D_P.getTags_Notes();
	}
	@When("Click on the Update button to Edit Opportunity")
	public void click_on_the_update_button_to_edit_opportunity() throws InterruptedException {
		D_P.getUpdateOppoButton();
	}
	
}
