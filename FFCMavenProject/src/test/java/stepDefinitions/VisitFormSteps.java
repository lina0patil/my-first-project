package stepDefinitions;

import java.awt.AWTException;
import java.io.IOException;

import FFCClasses.OpenBrowser;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageClasses.VisitForm;

public class VisitFormSteps extends OpenBrowser {
	VisitForm visit;
	int j;
	@Given("User is on Dashboard page to verify Visit Form")
	public void user_is_on_dashboard_page_to_verify_visit_form() throws InterruptedException, IOException, AWTException {
		Thread.sleep(3000);
		visit=new VisitForm(driver);
		Thread.sleep(5000);
		visit.ExcelConnect();
		visit.FetchExcelData(j);
	}
	@And("Click on Visit form")
	public void click_on_visit_form() {
		visit.getVisitForm();
	}

	@And("Click on Add Form button to add visit form")
	public void click_on_add_form_button_to_add_visit_form() {
		visit.getAddForm();
	}

	@And("Fill the Add Form Details and click on next")
	public void fill_the_add_form_details_and_click_on_next() throws InterruptedException {
		visit.getFormDetails();
		visit.getStartDate();
		visit.getDateFilter();
		visit.getEndDate();
		visit.getDateFilter();
		visit.getToggleButton();
	}

	@And("Click on add more section")
	public void click_on_add_more_section() {
		visit.getAddMoreSection();
	}

	@When("add the section title and done it")
	public void add_the_section_title_and_done_it() {
		visit.getSection();
	}

	@And("for add the components click on edit image on section")
	public void for_add_the_components_click_on_edit_image_on_section() {
		visit.getEditForComponents();
	}

	@And("Select the components")
	public void select_the_components() {
		visit.getSelectComponent();
	}

	@And("Fill the Components details")
	public void fill_compo_details() {
		visit.getEnterFields();
	}

	@Then("Click on Publish form button")
	public void click_on_publish_form_button() {
		visit.getPublishForm();
	}

	@And("Verify the message")
	public void verify_the_message() {
		visit.getVerifyMsg();
	}


	//search and send report on mail and delete
	@When("Search form by Title name")
	public void search_form_by_title_name() {
		visit.getSearchByTitle();
	}

	@And("Click on Visit status")
	public void click_on_visit_status() {
		visit.getVisitStats();
	}

	@Then("Send report on mail")
	public void send_report_on_mail() throws InterruptedException {
		visit.getSendReportButton();
	}
	@Then("delete the visit form details")
	public void delete_the_visit_form_details() throws InterruptedException {
		visit.deletetask();
	}
	//Filter and export
	@And("Click on Title name")
	public void click_on_title_name() {
		visit.titleName();
	}

	@And("Click on Filter")
	public void click_on_filter() {
		visit.Filter();

	}

	@And("Filter by date and apply it")
	public void filter_by_date_and_apply_it() throws InterruptedException {
		visit.getStartDate();
		visit.getDateFilter1();
		visit.getEndDate();
		visit.getDateFilter();
		visit.applyFilter();
	}

	@Then("Click on export")
	public void click_on_export() {
		visit.applyExportButton();
	}
}
