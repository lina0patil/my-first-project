package TestRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


	@RunWith(Cucumber.class)
	@CucumberOptions(
			features={"src/test/resources/Features"},
			glue= {"stepDefinitions"}, tags="@all",
			plugin={"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
					"pretty","junit:target/JUnitReport.xml","json:target/JSONReport.json", "html:target/HtmlReport.html"},
			monochrome=true			
	)
	public class TestRunner {

		
	}


	