package FFCClasses;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.time.Duration;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


public class OpenBrowser {
	public static WebDriver driver;
	public static Properties property;
	public static FileInputStream fis;

	public static WebDriver getDriver1() throws IOException {
		property = new Properties();
		fis = new FileInputStream("src/test/java/FFCClasses/global.properties");
		property.load(fis);
		String projectPath=System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath+"/src/test/resources/Driver/chromedriver.exe");
		ChromeOptions options= new ChromeOptions();
		options.setExperimentalOption("excludeSwitches", new String[] {"enable-automation"});
		driver = new ChromeDriver(options);



		driver.get(property.getProperty("URL"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	
		return driver;
	}
	public static FileInputStream getExcel() throws IOException {
		property = new Properties();
		FileInputStream fis = new FileInputStream("src/test/java/FFCClasses/global.properties");
		property.load(fis);
		FileInputStream file = new FileInputStream("src/test/java/FFCClasses/DataSheet.xlsx");
		return file;
	}
}