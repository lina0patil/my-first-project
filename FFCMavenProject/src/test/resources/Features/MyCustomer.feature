Feature: feature to test My Customer module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  #Scenario: Verify the Add new customer My Customer with valid data
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on new customer
    #And Enter the inputs
    #And Click on the Save button
    #Then Check validation msg on My Customer
#
  #Scenario: Verify the Add Call logs on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on add call logs Icon
    #And Fill the Call logs inputs
    #And Click on the add call log button
    #Then Check validation msg on My Customer

  #Scenario: Verify the Add Appointment on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on add appointment Icon
    #And Fill the appointment inputs
    #And Click on the add appointment button
    #Then Check validation msg on My Customer
#
  #Scenario: Verify the Add OpportunityIcon on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on add Opportunity Icon
    #And Fill the Opportunity inputs
    #And Click on the add Opportunity button
    #Then Check validation msg on My Customer
#
  #Scenario: Verify the Add Quick task on My customer screen
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on Add Quick task Icon
    #And Fill the quick task inputs
    #And Click on the Add Quick task button
    #Then Check validation msg on My Customer
#
  #Scenario: Check Assign Customer on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #And Search customer by their name
    #And Select customer by clicking on checkbox
    #When Click on the assign customer button
    #And Search and assign customer
    #Then Check validation msg on My Customer
@mycustomer
  Scenario: Verify the edit Customer Details on My customer
    Given User is on dashboard page
    And Click on my customer link
    When Click on company name
    And Click on details label
    And Click on edit button to update the customer details
    And Click on the Save button
    Then Check validation msg on My Customer

  #Scenario: Verify the Timeline on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on company name
    #When User is on Timeline Screen
    #And Enter the text
    #And Apply Filter by status
    #Then Clear the Timeline filter




  #Scenario: Verify Bulk Upload on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on Bulk Upload button on My customer
    #And Click to download All Customers
    #And Click on attach excel file button to upload the multiple customer
    #And Click on Upload button
    #Then Check validation msg on My Customer
#
  #Scenario: Verify Customer Report on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Click on Bulk Upload button on My customer
    #And Click on Show report button
    #And Search Customer report by file name, download report and view report

  #Scenario: Verify the Export on My customer screen
    #Given User is on dashboard page
    #And Click on my customer link
    #When Check the export
#
  #Scenario: Verify Filter on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #When Check the Filter on my customer
    #And Apply Filter on my customer
    #Then clear Filter on my customer
#
  #Scenario: Check delete on My customer
    #Given User is on dashboard page
    #And Click on my customer link
    #And Search customer by their name
    #When Select customer by clicking on checkbox
    #And Delete the selected customer
    #Then Check validation msg on My Customer
