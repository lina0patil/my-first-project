Feature: feature to test ffc Reports> Opportunities Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify the Opportunity by Team on Opportunity
    Given User is scroll to Opportunity Reports
    And Click on Opportunities Reports
    And Again scroll to see all option in Opportunity Reports
    And Click on Opportunities
    When Verify the Opportunity by Team
    And Check the From Date Filter on Opportunity
    And Check the To Date Filter on Opportunity
    And Check the Apply button on Opportunity
    And Check the Export button on Opportunity

  Scenario: Verify the Opportunity by Status on Opportunity
    Given User is scroll to Opportunity Reports
    And Click on Opportunities Reports
    And Again scroll to see all option in Opportunity Reports
    And Click on Opportunities
    When Verify the Opportunity by Status
    And Check the From Date Filter on Opportunity
    And Check the To Date Filter on Opportunity
    And Check the Apply button on Opportunity
    And Check the Export button on Opportunity
