Feature: feature to test FFC Punch In & Out Status

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify Punch In Status
    Given User is on Dashboard page P
    When Click on Punch In button
    Then Click on Punch Out
    And Send the report and done it
