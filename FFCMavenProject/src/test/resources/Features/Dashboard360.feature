Feature: feature to test ffc dashboard360 functionality

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify the Task Against Lead tab in Dashboard360
    Given User is on dashboard360 page
    When scroll to the Task against Lead tab
    And Verify the Date filter dropdown
    And User click on customer and proceed to Task Against Lead screen
    Then Verify the filter dropdown
    And click on one of the task
    And fetch the data
    And Close the Dashboard360 browser

  Scenario: Verify Working hours in View All > Dashboard 360
    Given User is on dashboard360 page
    And Check the From Date Filter Top5
    And Check the To Date Filter Top5
    And Click on View all pointer of Top 5 Absentees label
    When Verify the Working hours
    And Check the filter on Working hours page
    And Get the Working hours
    Then View the details of one of the user by clicking on view details
    And Edit the user by clicking on User name
