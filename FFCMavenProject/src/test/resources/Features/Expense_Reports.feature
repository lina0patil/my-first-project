Feature: feature to test ffc Reports> Expense Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify the Expense by Team on Expense
    Given User is scroll to Reports^
    And Click on Reports^
    And Again scroll to see all option in Expense Reports
    And Click on Expense
    When Verify the Expense by Team
    And Check the From Date Filter on Expense
    And Check the To Date Filter on Expense
    And Check the Select user dropdown on Expense by Team
    And Check the Select Manager dropdown on Expense by Team
    And Check the Apply button on Expense
    And Check the Export button on Expense by Team
    Then Click on Refresh on Expense by Team
    And Verify one of the Manager on Expense by Team

  Scenario: Verify the Expense by Category on Expense
    Given User is scroll to Reports^
    And Click on Reports^
    And Again scroll to see all option in Expense Reports
    And Click on Expense
    When Verify the Expense by Category
    And Check the From Date Filter on Expense
    And Check the To Date Filter on Expense
