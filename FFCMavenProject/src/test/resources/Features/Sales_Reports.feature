Feature: feature to test ffc Reports> Sales Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify the Sales by Customer on Sales
    Given User is scroll to Reports*
    And Click on Reports*
    And Again scroll to see all option in Sales Reports
    And Click on Sales
    When Verify the Sales by customer
    And Check the From Date Filter on Sales
    And Check the To Date Filter on Sales
    And Check the Select user dropdown on Sales
    And Check the Select Manager dropdown on Sales
    And Check the Apply button on Sales
    And Check the Export button on Sales
    Then Click on Refresh on Sales
    And Verify one of the Distributor on Sales

  Scenario: Verify the Sales by Team on Sales
    Given User is scroll to Reports*
    And Click on Reports*
    And Again scroll to see all option in Sales Reports
    And Click on Sales
    When Verify the Sales by Team
    And Check the From Date Filter on Sales
    And Check the To Date Filter on Sales
    And Check the Select user dropdown on Sales
    And Check the Select Manager dropdown on Sales
    And Check the Apply button on Sales
    And Check the Export button on Sales
    Then Click on Refresh on Sales
    And Verify one of the Distributor1 on Sales
