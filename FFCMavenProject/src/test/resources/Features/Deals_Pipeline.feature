Feature: feature to test Deals & Pipeline module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify the Add Opportunities on Deals & Pipeline
    Given User is on dashboard page-D & P
    And Click on Deals & Pipeline link
    When Click on Add Opportunities
    And Fill the opportunities input
    And Click on the Add button to Add Opportunity
    Then Check validation msg of add opportunities

  Scenario: Verify the Edit Opportunities on Deals & Pipeline
    Given User is on dashboard page-D & P
    And Click on Deals & Pipeline link
    When Click on Edit Opportunities
    And Edit the opportunities input
    And Click on the Update button to Edit Opportunity
    Then Check validation msg of add opportunities
