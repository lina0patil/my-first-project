Feature: feature to test FFC Attendance Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button
#
  #Scenario: Verify My Claims on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #And Click on Add new button to add claim on My Claims screen
    #And Fill the claim details
    #When Click on Done button to add the claim
    #Then Check the validation msg for attendance
#
  #Scenario: Verify Filter My Claims on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #And Click on Filter button to filter Claims according to status and date
    #And Select the status and date
    #And Apply it for filtering claim
    #And View the claim reason
    #Then Clear the filter claim

  #Scenario: Verify Team Claims on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #And Click on Team Claims
    #And Click on Add new button to add claim on My Claims screen
    #And Fill the claim details
    #When Click on Done button to add the claim
    #And Check the validation msg for attendance
    #And Search the claim by entering user name
    #And Accept the pending claim
    #And View the claim details and close it
    #And Delete the pending claim and mention the reason
    #And Check the validation msg for attendance
    #And Click on Filter button to filter Claims according to member status and date
    #And Select the member status and date
    #And Apply it for filtering claim
    #Then Clear the filter claim

  #Scenario: Verify four Icons on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #When Verify Global Map icon on attendance module
    #And Select the date to view the user location
    #And Maximize the Map for full screen view on Attendance Module
    #And Zoom Out and Zoom In for better view of map on Attendance Module
    #And Click on Satellite on map Attendance Module
    #Then Minimize the map on Attendance Module
    #And Click on view report icon on attendance module
    #And Download the report on the basis of date User and Manager
    #And Download the User not punch in report icon
    #And Check the claims icon
#
  #Scenario: Verify User Settings on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #And Search the user by entering the user name
    #When Click on  user settings
    #And Set the designation
    #And Turn on the reminder punch in time toggle button and set the time
    #And Set the Punch In With Geo-Fencing using map
    #And Set the Punch Out With Geo-Fencing using map
    #And Set Monthly Leaves Allowed In Days
    #And Click on Notify admin and assign new admin
    #Then Notify the manager by email and mobile
    #And Click on make admin toggle button
    #And Set the entire day tracking
    #And Select the Weekly off day
    #And Select the day for alternate day off in month
    #And Select alternate day off count
    #Then Click on done user settings button to save changes
    #And Check the validation msg for attendance

  #Scenario: Verify User Report on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #And Search the user by entering the user name
    #When Click on  user report
    #And Select date and enter the email id
    #Then Click on send button
    #And Check the validation msg for attendance

  #Scenario: Verify User Location on Attendance Module
    #Given User is on Dashboard page to verify My claim
    #And Click on Attendance link
    #And Search the user by entering the user name for user location
    #When Click on user location and verify it
    #And Select the date to view the user location
    #And Click on Location Pin Icon on user location
    #And Maximize the Map for full screen view on Attendance Module
    #And Zoom Out and Zoom In for better view of map on Attendance Module
    #And Click on Satellite on map Attendance Module
    #Then Minimize the map on Attendance Module
