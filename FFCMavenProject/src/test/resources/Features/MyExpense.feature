Feature: feature to test FFC My Expense module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  #Scenario: Verify the export on Claims for approval screen according to status
    #Given User is on Dashboard page to check My Expense module
    #When Click on My Expense
    #And Check the Export
    #And Check Data
    #Then Verify the expense filter dropdown
    #And Select status
    #And Select status by pending_Apply it_Click on export_Clear the filter
    #And Select status by rejected_Apply it_Click on export_Clear the filter
    #And Select status by pendingReimbursementData_Apply it_Click on export_Clear the filter
    #And Close the Expense browser
#@Expense
  #Scenario: Verify Add Claim on My Expense module
    #Given User is on Dashboard page to check My Expense module
    #And Click on My Expense
    #And Click on My Claims
    #And Click on Add claim for the expense
    #When Enter the Expense data
    #And Click on Add button to add expense claim
    #Then Check validation message for expense claim

  #
  #Scenario: Verify edit Claim> My Claim on My Expense module
    #Given User is on Dashboard page to check My Expense module
    #And Click on My Expense
    #And Click on My Claims
    #And Filter the claim by pending status on my claim
    #And Search the claim by entering category name
    #When Edit the claim by clicking on category name which is in pending status
    #And Click on edit button to edit fields claim
    #And Click on Update button to update claim
    #And Check validation message for expense claim
    #Then View the logs and close it-Expense
    #And Close the expense detail

  #Scenario: Verify edit Claim> Claims for approval on My Expense module
    #Given User is on Dashboard page to check My Expense module
    #And Click on My Expense
    #And Filter the claim by pending status on Claims for approval
    #And Search the claim by entering category name
    #And Edit the claim by clicking on Username which is in pending status
    #And Click on edit button on claims for approval
    #And Click on Update button and check validation message on claims for approval
    #And Close the expense detail

  #Scenario: Verify Take action on claim > Claims for approval on My Expense module
    #Given User is on Dashboard page to check My Expense module
    #And Click on My Expense
    #And Filter the claim by pending status on Claims for approval
    #And Search the claim by entering category name
    #And Click on Username which is in pending status on Claims for approval
    #When Click on Take action button on  Claims for approval
    #And Fill the Expense Detail Action
    #And Click on Partial approve button and check validation message
    #And Close the expense detail

  Scenario: Verify Accept claim > Claims for approval on My Expense module
    Given User is on Dashboard page to check My Expense module
    And Click on My Expense
    And Filter the claim by pending status on Claims for approval
    When Click on Accept claim image on Claims for approval
    Then Check validation message for expense claim

  Scenario: Verify Decline claim > Claims for approval on My Expense module
    Given User is on Dashboard page to check My Expense module
    And Click on My Expense
    And Filter the claim by pending status on Claims for approval
    When Click on Decline claim image on Claims for approval
    And Mention the reason for expense rejection and decline it
    Then Check validation message for expense claim
#
  #Scenario: Verify Accept multiple claim > Claims for approval on My Expense module
    #Given User is on Dashboard page to check My Expense module
    #And Click on My Expense
    #And Select the checkbox on Claims for approval which is in pending status
    #When Click on Accept claim button on Claims for approval
    #Then Check validation message for expense claim
#
  #Scenario: Verify Decline multiple claim > Claims for approval on My Expense module
    #Given User is on Dashboard page to check My Expense module
    #And Click on My Expense
    #And Select the checkbox on Claims for approval which is in pending status
    #When Click on Decline claim button on Claims for approval
    #And Mention the reason for expense rejection and decline it
    #Then Check validation message for expense claim
#
  #Scenario: Verify the Filter on Claims for approval screen according to status
    #Given User is on Dashboard page to check My Expense module
    #When Click on My Expense
    #And Check the Export
    #And Click on Filter button on Claims for approval to filter Claims according to location, member, status, category and date
    #And Select the location, member, status, category and date on expense filter
    #When Apply it for filtering Claims on Claims for approval
    #Then Clear the filter on Claims for approval
