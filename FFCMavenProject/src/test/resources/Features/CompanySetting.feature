Feature: feature to test FFC Company Setting Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify Company Setting Module
    Given User is on Dashboard page to verify Company Setting Module
    And Scroll upto Settings and click on that to check Company Setting module
    And Click on Company Setting link
    When Fill the Company Setting deatils
    And Click on Update button to save changes in company setting
    Then Verify the validation message on Company Setting module
    And Remove the logo
    And Delete company setting
