Feature: feature to test FFC Dynamic Form Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify Add Form on Dynamic Form
    Given User is on Dashboard page to verify Dynamic Form
    And Click on Dynamic form
    And Click on Add Form button to add Dynamic form
    And Fill the Add Form Details and click on next D
    And Click on add more section D
    When Add the section title and done it D
    And For add the components click on edit image on section D
    And Select the components D
    And Fill the Components details D
    Then Click on Publish form button D
    And verify the message D
