Feature: feature to test FFC Custom Field Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify add Custom Field on Custom Field Module
    Given User is on Dashboard page to verify Custom Field Module
    And Scroll upto Settings and click on that to check Custom Field module
    And Click on Custom Field link
    When Click on add custom field button
    And Select Text custom field type and fill the required details, add it and check validation msg
    And Select Quantitative custom field type and fill the required details, add it and check validation msg
    And Select YesNo custom field type and fill the required details, add it and check validation msg
    And Select Date custom field type and fill the required details, add it and check validation msg
    And Select Single custom field type and fill the required details, add it and check validation msg
    And Select Multiple custom field type and fill the required details, add it and check validation msg
    And Select Image custom field type and fill the required details, add it and check validation msg
    And Select CameraImage custom field type and fill the required details, add it and check validation msg
    And Select Signature custom field type and fill the required details, add it and check validation msg

  @custom
  Scenario: Verify edit and delete custom type on Custom Field Module
    Given User is on Dashboard page to verify Custom Field Module
    When Scroll upto Settings and click on that to check Custom Field module
    And Click on Custom Field link
    Then Search and edit custom field and check validation msg, Delete custom field and check validation msg
    
