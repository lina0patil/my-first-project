Feature: feature to test ffc Reports> Customer Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify the Call logs on Customer
    Given User is scroll to Reports
    And Click on Reports
    And Click on Customer
    And Again scroll to see all option in Reports
    When Verify the Call Logs
    And Check the From Date Filter on Reports
    And Check the To Date Filter on Reports
    And Check the Select user dropdown on Reports
    And Check the Select Manager dropdown on Reports
    And Check the Apply button on Reports
    And Check the Export button on Reports
    And Click on Refresh on Customer
    Then Click on one of the User on Reports

  Scenario: Verify the Customer added on Customer
    Given User is scroll to Reports
    And Click on Reports
    And Click on Customer
    And Again scroll to see all option in Reports
    When Verify the Customer added
    And Check the From Date Filter on Reports
    And Check the To Date Filter on Reports
    And Check the Select user dropdown on Reports
    And Check the Select Manager dropdown on Reports
    And Check the Apply button on Reports
    And Check the Export button on Reports
    And Click on Refresh on Customer
    Then Click on one of the User on Reports

  Scenario: Verify the Visit Report on Customer
    Given User is scroll to Reports
    And Click on Reports
    And Click on Customer
    And Again scroll to see all option in Reports
    When Verify the Visit Report
    And Check the From Date Filter on Reports
    And Check the To Date Filter on Reports
    And Check the Select user dropdown on Reports
    And Check the Select Manager dropdown on Reports
    And Check the Apply button on Reports
    And Check the Export button on Reports
    And Click on Refresh on Customer
    Then Click on one of the User on Reports
