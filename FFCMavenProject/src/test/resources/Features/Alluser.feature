Feature: feature to test FFC All User Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  #Scenario: Verify add user on add user module
    #Given User is scroll to Settings
    #And Click on Settings
    #And Click on All User
    #And Click on add user
    #And Fill the user details
    #And Click on add button for add new user
    #And Verify the validation message on all user module

  #Scenario: Verify edit user on all user module
    #Given User is scroll to Settings
    #And Click on Settings
    #And Click on All User
    #And Search user by entering user name
    #And Click on Edit User
    #And Edit the user details
    #And Click on edit button for edit user
    #And Verify the validation message on edit user module

  #Scenario: Verify bulk upload on all user module
    #Given User is scroll to Settings
    #And Click on Settings
    #And Click on All User
    #And Click on bulk upload
    #And Click to download All Users
    #And Click to download sample file
    #And Get the input field Attach Excel File
    #When Click the upload button
    #And Verify the validation message on all user module
    #And Click on export button to download user details
    #And Click on bulk upload
    #And Click on show report
    #And Click to search report
    #And Click to download bulkupload report
    #And Click to download report file
    #And Click on view report
#@all
  #Scenario: Verify assign manager all user module
    #Given User is scroll to Settings
    #And Click on Settings
    #And Click on All User
    #When Assign the manager to the user and check validation msg
    #And Assign the team to the user and check validation msg
    #And Remove the manager and check validation msg
    #Then Search the user and Deactivate that user and check validation msg
    #And Verify the filter on all user module
    #And Select the user by inactive status
    #And Apply it for filtering user
    #And Active the user status
    #And Verify the validation message on all user module
    #And Clear the filter on all user module

    #
  #Scenario: Verify All User > Date of Joining, Last Date of Working, Access Web Login functionality
  #Given User is scroll to Settings
  #And Click on Settings
  #And Click on All User
  #When Click on Edit User
  #And Verify the Date of Joining Date Picker.
  #And Verify the Last Date of Working Date Picker
  #Then Verify Access Web Login functionality
  #And Click on Update button
  #And Verify the Update message
  #And Sign Out
  #Given User is on Login Page
  #When User enters the valid login credentials
  #And Click on sign In and Sign In with OTP button
  #Then Check the Error message
  #And Close the All user browser
