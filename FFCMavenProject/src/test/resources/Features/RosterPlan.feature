Feature: feature to test FFC My Roster Plan module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  #Scenario: Verify the Assign roster on Roster Plan
    #Given User is on Dashboard page to check  roster plan
    #And Click on Roster Plan
    #And Search User on Roster
    #And Select the checkbox on Roster plan
    #When Click on Select Date button on Roster plan
    #And Select date time and assign to user
    #Then Click on add button on Roster plan
    #And Check the validation message of Roster Plan
@Roster
  Scenario: Verify the Filter on Roster Plan
    Given User is on Dashboard page to check  roster plan
    And Click on Roster Plan
    And Click on Filter button to filter users according to location, status and member
    And Select the location, status and member of roster filter
    When Apply it for filtering user on Roster plan
    And Clear the filter on roster plan
    And View the Roster details
    Then Close Roster details
  
