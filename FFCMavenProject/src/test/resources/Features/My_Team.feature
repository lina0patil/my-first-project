Feature: feature to test FFC All User Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  #Scenario: Verify My Team > Location
    #Given User is on Dashboard page to check My Team module
    #And Click on My Team
    #When Click on Location and verify it
    #And Search user
    #And Select User
    #And Verify the from date filter
    #And Verify the To date filter
    #And Check Apply button
    #And Check the Punch In and Punch Out status
    #Then Refresh the filter

#
  #Scenario: Verify My Team > Organization Hierarchy
    #Given User is on Dashboard page to check My Team module
    #And Click on My Team
    #When Click on Organization Hierarchy icon and verify it
    #And Search user
    #And click on chevron to see hierarchy
    #Then Clear the Search User

  #Scenario: Verify My Team > Global Map
    #Given User is on Dashboard page to check My Team module
    #And Click on My Team
    #When Click on Global Map and verify it
    #And Select the date filter on map
    #And Click on Location Pin Icon on global map
    #And Maximize the Map for full screen view
    #And Zoom Out and Zoom In for better view of map
    #And Click on Satellite on map
    #Then Minimize the map
#
  #Scenario: Verify My Team > Map
    #Given User is on Dashboard page to check My Team module
    #And Click on My Team
    #When Click on Map and verify it
    #And Select the date filter on map
    #And Maximize the Map for full screen view
    #And Zoom Out and Zoom In for better view of map
    #And Click on Satellite on map
    #Then Minimize the map

  Scenario: Verify My Team > Attendence
    Given User is on Dashboard page to check My Team module
    And Click on My Team
    When Click on Attendance and verify it
    And Search user
    And Select User
    And Apply filter to view attendance status
    Then Refresh the filter
