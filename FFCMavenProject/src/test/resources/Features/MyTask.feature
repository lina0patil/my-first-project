Feature: feature to test FFC My Task Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  #Scenario: Verify Add task for Assign to Others
    #Given User is on Dashboard page to check the my task
    #And Click on My Task link
    #And Click on Add Task button to add the task
    #And Fill the task details on Assign to Others
    #When Click on Add button to assign the task
    #Then Check the validation msg for task
#
  #Scenario: Verify Add task for Assign to me
    #Given User is on Dashboard page to check the my task
    #And Click on My Task link
    #And Click on Add Task button to add the task
    #When Click on Assign to me
    #And Fill the task details on Assign to me
    #When Click on Add button to assign the task
    #Then Check the validation msg for task
#
  #Scenario: Verify Edit task for Assign to Me
    #Given User is on Dashboard page to check the my task
    #And Click on My Task link
    #And Click on Title name to edit the task
    #And Click on Edit button to edit the task details
    #When Edit the task details on Assign to Me tab
    #Then Check the validation msg for task
    #And Add comment for that task
    #And Click on cancel button

  #Scenario: Verify four buttons on My task
    #Given User is on Dashboard page to check the my task
    #And Click on My Task link
    #And Check the Export on MY Task
    #And Click on Add Task button to add the task
    #And Fill the task details on Assign to Others
    #When Click on Add button to assign the task
    #And Check the validation msg for task
    #And Search the added task by entering title name
    #And Accept that task
    #And Check the validation msg for task
    #And Select status as partial complete task and select percentage
    #And Check the validation msg for task
    #And Select status as task completed
    #And Check the validation msg for task
    #And View task coverage and close it
    #And Archive the task
    #And Check the validation msg for task
    #And Go to Archive List tab and Unarchive that task
    #And Check the validation msg for task
    #And Go to My task tab and Delete the task
    #And Check the validation msg for task
    #And Click on Filter button on MY Task to filter the task according to priority, member, status, and date
    #And Select the priority, member, status, and date on my task filter
    #When Apply it for filtering taks on my task
    #Then Clear the filter on my task
#
  Scenario: Verify CC on My Task
    Given User is on Dashboard page to check the my task
    And Click on My Task link
    And Click on CC tab on My task
    And Click on Add Task button to add the task
    And Fill the task details on Assign to Others on CC
    When Click on Add button to assign the task
    And Check the validation msg for task
    And Click on CC tab on My task
    When View task coverage and close it on CC
    And Archive the task on CC
    And Check the validation msg for task
    And Go to Archive List tab and Unarchive that task
    And Check the validation msg for task
    Then Go to CC tab and Delete the task
    And Check the validation msg for task
    And Click on Filter button on MY Task to filter task according to status, priority, member.
    And Select the status, priority, member on my task filter
    When Apply it for filtering taks on my task
    Then Clear the filter on my task

  Scenario: Verify Assigned By Me on My Task
    Given User is on Dashboard page to check the my task
    And Click on My Task link
    And Click on Assigned By Me
    And Check the Export on MY Task
    And Click on Add Task button to add the task
    And Fill the task details on Assign to Others on Assigned By Me
    When Click on Add button to assign the task
    And Check the validation msg for task
    And Click on Assigned By Me tab on My task
    When View task coverage and close it on Assigned By Me
    And Archive the task on Assigned By Me
    And Check the validation msg for task
    And Go to Archive List tab and Unarchive that task
    And Check the validation msg for task
    Then Go to Assigned By Me tab and Delete the task
    And Check the validation msg for task
    And Click on Filter button on MY Task to filter the task according to priority, member, status, and date
    And Select the priority, member, status, and date on my task filter
    When Apply it for filtering taks on my task
    Then Clear the filter on my task
    
    Scenario: Verify Calendar View on My Task
    Given User is on Dashboard page to check the my task
    And Click on My Task link
    And Click on Calendar View on My task
    And Click on Add Task button to add the task
    And Fill the task details on Assign to Others on Assigned By Me
    When Click on Add button to assign the task
    And Check the validation msg for task
     And Click on Calendar View on My task
   And Click on previous button and fetch that month
   And Click on Today button and fetch that month
    And Click on next button and fetch that month
    And Select an option as CC
    Then Check the UI on weekly basis 
     And Select an option as Assigned By Me
    And Check the UI on daily basis
    

  #Scenario: Verify Export and setting on Archive List
    #Given User is on Dashboard page to check the my task
    #And Click on My Task link
    #And Click on Archive List
    #And Check the Export on MY Task
    #When Check the Settings
    #And Add the percentage of task
    #And Check the validation msg for task
    #And Select percentage and remove it
    #And Check the validation msg for task
   #Then Close the settings popup

