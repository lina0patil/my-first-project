Feature: feature to test FFC Attendance Setting Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify Attendance Setting Module
    Given User is on Dashboard page to verify Attendance Setting Module
    And Scroll upto Settings and click on that to check attendance module
    And Click on Attendance Setting link
    And Click on add new button to add new fencing location
    And Give the name for that fencing location
    And Search and select the location
    And Maximize the Map for full screen view on Attendance Setting Module
    And Set the correct location through location Pin Icon
    And Zoom Out and Zoom In for better view of map on Attendance Setting Module
    And Click on Satellite on map Attendance Setting Module
    And Minimize the map on Attendance Setting Module
    When Click on add button to add new fencing location
    And Verify the validation message on Attendance setting module
    And Search the fence area
    And Edit that fence location
    And Edit all the fence location details
    And Verify the validation message on Attendance setting module
    And Delete that fence location
    Then Verify the validation message on Attendance setting module

  @atten
  Scenario: Verify Global Setting on Attendance setting Module
    Given User is on Dashboard page to verify Attendance Setting Module
    And Scroll upto Settings and click on that to check attendance module
    And Click on Attendance Setting link
    When Scroll upto global setting on Attendance setting Module
    And Turn on the reminder punch in time toggle button and set the time on Attendance setting Module
    And Set total working Hours in day
    And Set total working Hours in half day
    And Click on save button to save the global setting
    Then Verify the validation message on Attendance setting module

  Scenario: Verify Add new holiday and remove on Attendance setting Module
    Given User is on Dashboard page to verify Attendance Setting Module
    And Scroll upto Settings and click on that to check attendance module
    And Click on Attendance Setting link
    When Click on Add new holiday button
    And Enter holiday name and select date
    And Click on done button to set the holiday
    Then Click on remove holiday icon
    And Verify the validation message on Attendance setting module
