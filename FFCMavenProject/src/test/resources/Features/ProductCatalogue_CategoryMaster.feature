Feature: feature to test FFC Product Catalogue Module

  Background: 
    Given Launches the browser
    And User is on login page
    When User enter a credential "linapatil@nimapinfotech.com" "12345678"
    And Click on Sign in button

  Scenario: Verify add Category Master on Product Catalogue Module
    Given User is on Dashboard page to check Category Master Module
    And Scroll upto Settings and click on that to check Category Master Module
    And Click on Product Catalogue link
    And Click on  Category Master link
    When Click on add category button
    And Fill the details and add category
    Then Check validation msg on Category Master Module

  Scenario: Verify search,edit,delete Category Master on Product Catalogue Module
    Given User is on Dashboard page to check Category Master Module
    And Scroll upto Settings and click on that to check Category Master Module
    And Click on Product Catalogue link
    And Click on  Category Master link
    And Search category by entering category name
    When Click on edit category button
    And Edit category details and update category
    Then Check validation msg on Category Master Module
    And Search category by entering category name to delete category
    Then Check validation msg on Category Master Module

  Scenario: Verify bulk upload Category Master on Product Catalogue Module
    Given User is on Dashboard page to check Category Master Module
    And Scroll upto Settings and click on that to check Category Master Module
    And Click on Product Catalogue link
    And Click on  Category Master link
    When Click on Bulk Upload button on Category Master Module
    And Click to download All Categories
    And Click to download sample file of Categories
    And Click on attach excel file button to upload the multiple categories
    And Click on Upload button to upload the multiple categories
    Then Check validation msg on Category Master Module
    And Click on Show report button to view the category report
    And Download the bulkupload report of categories
    And Download Category BulkUpload File Report
    And View the Category Report
