package pageClasses;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Dashboard360 {
	    public WebDriver driver;
		Actions as;
		WebDriverWait wait;
		int timeout=10;
		//create constructor
		public Dashboard360(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		}
		@FindBy(xpath="//span[contains(text(),'Dashboard 360')]")
		WebElement dashboard360_link;
	   
		@FindBy(xpath="//div[@class='row']/child::div[15]")
		WebElement scrolltoTaskAgainstLeadTab;     //teamwise task overview
		
		@FindBy(xpath="//*[.='Customer2 5']")
		WebElement customer;
		
		@FindBy(xpath="//div[@id='mat-tab-label-1-1']//button[@type='button']")
		WebElement fromDateFilter;
		
		@FindBy(xpath="//div[@id='mat-tab-label-1-1']//button[2]")
		WebElement toDateFilter;
		              
		@FindBy(xpath="//button[@aria-label='Choose month and year']")
		WebElement chooseMonth_Year;
		    
		@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
		WebElement chooseYear;
		    
		@FindBy(css="tbody[role='grid']")
		WebElement tBody;
		              
		@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
		WebElement previousYear;
		    
	    @FindBy(xpath="//button[@aria-label='Next year']//span[@class='mat-button-wrapper']")
		WebElement nextYear;
		    
		@FindBy(xpath="//mat-cell[normalize-space()='test2']")
		WebElement clickOnTask;
		
		@FindBy(xpath="//p[normalize-space()='test2']")
		WebElement title;
		
		@FindBy(xpath="//p[normalize-space()='dashboard360']")
		WebElement description;
		
		@FindBy(xpath="//p[normalize-space()='Low Priority']")
		WebElement priority;
		
		@FindBy(xpath="//p[normalize-space()='21 Feb 2022 01:40 PM']")
		WebElement assignedDate;
		
		@FindBy(xpath="//p[normalize-space()='23 Feb 2022']")
		WebElement dueDate;
		
		@FindBy(xpath="//p[contains(@class,'kt-pointer')]")
		WebElement assignedTo;
		
		@FindBy(xpath="//p[normalize-space()='Customer2']")
		WebElement customerName;
		
		@FindBy(xpath="//div[contains(@class,'col-4')]//p[contains(text(),'-')]")
		WebElement mobileNo;
		
		@FindBy(xpath="//kt-filter[@filterformat='task-table']//span[@class='mat-button-wrapper'][normalize-space()='Filter']")
		WebElement filterButton;
		
		@FindBy(xpath="//span[contains(text(),('Priority'))]")
		WebElement byPriority;
		
		@FindBy(xpath="//span[contains(text(),(' Low Priority '))]")
		WebElement lowPriority;
		
		@FindBy(xpath="//form[@class='kt-form ng-valid ng-star-inserted ng-touched ng-dirty']//span[@class='mat-button-wrapper'][normalize-space()='Apply']")
		WebElement applyButton;
		
		@FindBy(xpath="(//span[@class='mat-button-wrapper'])[4]")
		WebElement fromDateFilter_Top5;
		
		@FindBy(xpath="(//span[@class='mat-button-wrapper'])[5]")
		WebElement toDateFilter_Top5;
	
		@FindBy(xpath="(//a[contains(text(),'View All')])[1]")
		WebElement viewAll;

		@FindBy(xpath="//div[contains(text(),'Working Hours')]")
		WebElement  workingHours ;
		
		@FindBy(xpath="//span[normalize-space()='Filter']")
		WebElement  filterDropdown;
		
		@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
		WebElement  fromDateFilterDropdown;
		
		@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
		WebElement  toDateFilterDropdown;
		
		@FindBy(xpath="//span[contains(text(),'Apply')]")
		WebElement applyButtonFilterDropdown;
		
		@FindBy(xpath="(//mat-cell[@role='gridcell'][normalize-space()='View Details'])[2]")
		WebElement viewDetails;
		
		@FindBy(xpath="//span[normalize-space()='18']")
		WebElement eligibleHours;
		
		@FindBy(xpath="//span[normalize-space()='02:50']")
		WebElement completedHours;
		
		@FindBy(xpath="//span[normalize-space()='16:10']")
		WebElement leftHours;
		
		
		@FindBy(xpath="//mat-cell[contains(text(),(' LinaTestDashboard'))][1]")
		WebElement userEdit;
		
		@FindBy(xpath="//span[contains(text(),(' Edit '))]")
		WebElement edit;
		
		@FindBy(xpath="//input[@id='mat-input-105']")
		WebElement mNo;
		
		@FindBy(xpath="//span[contains(text(),(' Update'))]")
		WebElement update;
		
		public void getDashboard360_link() {
			as=new Actions(driver);
		    wait = new WebDriverWait(driver, timeout);
	    dashboard360_link.click();	
		}
		public void getScrolltoTaskAgainstLeadTab() {
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", scrolltoTaskAgainstLeadTab);
		}
	
		public void getCustomer() {
	    customer.click();	
		}
		
		public void getClickOnTask() {
		clickOnTask.click();
		}
		public void getTitle() {
		String tasktitle=title.getText();
		System.out.println("Title ="+tasktitle);
			}
		public void getDescription() {
		String taskdescription=description.getText();
		System.out.println("Description = "+taskdescription);
			}
		public void getPriority() {
		String taskpriority=priority.getText();
		 System.out.println("Priority = "+taskpriority);
			}
		public void getAssignedDate() {
		String taskassignedDate=assignedDate.getText();
		 System.out.println("Assigned Date = "+taskassignedDate);
			}
		public void getDueDate() {
		String taskdueDate=dueDate.getText();
		System.out.println("Due Date = "+taskdueDate);
			}
		public void getAssignedTo() {
		String taskassignedTo=assignedTo.getText();
		 System.out.println("Assigned To = "+taskassignedTo);
		    }
		public void getCustomerName() {
		String taskcustomerName=customerName.getText();
		System.out.println("Customer Name = "+taskcustomerName);
			}
		public void getMobileNo() {
		String taskmobileNo=mobileNo.getText();
		System.out.println("Mobile No = "+taskmobileNo);
			}
		public void getViewAll() {
			viewAll.click();	
			}
		
		public void getWorkingHours() {
			workingHours.click();	
			}
		public void getFilterWorkingHours() {
		    filterDropdown.click();	
			}
		public void getApplyButtonFilterDropdown() {
			applyButtonFilterDropdown.click();	
			}
		public void getViewDetails() {
			viewDetails.click();	
			}
		public void getEligibleHours_Hrs() {
		String eligibleHours_Hrs=eligibleHours.getText();
		System.out.println("Eligible Hours_Hrs = "+eligibleHours_Hrs);
				}
		public void getCompletedHours_HH_MM() {
			String completedHours_HH_MM=completedHours.getText();
			System.out.println("Completed Hours_HH_MM = "+completedHours_HH_MM);
					}
		public void getLeftHours_HH_MM() {
			String leftHours_HH_MM=leftHours.getText();
			System.out.println("Left Hours_HH_MM = "+leftHours_HH_MM);
					}
		public void getUserEdit() throws InterruptedException {
		    userEdit.click();	
		    Thread.sleep(3000);
			}
		public void getEdit() throws InterruptedException {
		    edit.click();
		    Thread.sleep(3000);
		    mNo.clear();
		    mNo.sendKeys("2345678765");
		    Thread.sleep(3000);
		    update.click();
			}
		
		public void getFromDateFilter() throws InterruptedException {
		fromDateFilter.click();	
		
		 boolean IsFromDateClicked=false;
		    int fromYear = 2022;
		    chooseMonth_Year.click();
		    
		    while(true) {
		    	if(IsFromDateClicked) {
		    		break;
		    	}                                                           
		    	WebElement fromCalelement = chooseYear;
		        String fdtr = fromCalelement.getText();
		       // System.out.println(fdtr);
		        String[] fDateRangepartss = fdtr.split(" � ");
		      //  System.out.println(fDateRangepartss[0]);
		      //  System.out.println(fDateRangepartss[1]);
		        outerloop:       
		    	if(fromYear >= Integer.parseInt(fDateRangepartss[0]) && fromYear <= Integer.parseInt(fDateRangepartss[1]) && !IsFromDateClicked ) {
		                //Store each year in string array
		                List < WebElement > fcols;
		                //Get value from Table each cell
		                WebElement ft = tBody;
		                // count rows with size() method
		                List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
		                for (WebElement row: frws) {
		                    List < WebElement > fCells = row.findElements(By.tagName("td"));
		                    for (WebElement fCell: fCells) {
		                        if (fCell.getText().contains(Integer.toString(fromYear))) {
		                            fCell.click();
		                            Thread.sleep(2000);
		                            int fromMonth = 01;
		                            String fMonth = ReturnMonthSelected(fromMonth);
		                            WebElement tm =tBody;
		                            // count rows with size() method
		                            List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
		                            for (WebElement fmonthrow: fmonthsrows) {
		                                List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
		                                for (WebElement fMonthcell: fMonthCells) {
		                                    if (fMonthcell.getText().contains(fMonth)) {
		                                    	fMonthcell.click();
		                                        Thread.sleep(2000);
		                                        int fromDay = 1;
		                                        WebElement fdatecalender = tBody;
		                                        // count rows with size() method
		                                        List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
		                                        for(WebElement fmrow: fmrows){
		                                            List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
		                                            for(WebElement fmCell:fmCells){
		                                            	if (fmCell.getText().contains(Integer.toString(fromDay)))
		                                            	{
		                                            		fmCell.click();
		                                            		IsFromDateClicked = true;
		                                                 	break outerloop;
		                                            	}
		                                               
		                                            }
		                                        }                                                                                                                  
		                                    }
		                                }
		                            }
		                        }
		                    }
		                }
		          		break;     
		    	}
		    	else {
		    		if((Integer.parseInt(fDateRangepartss[0])-fromYear)>0){
		    			previousYear.click();
		        		continue;
		    		}
		    		else {
		    			nextYear.click();
		        		continue;
		    		}                 
		    	}
		    } 
		    Thread.sleep(2000);
			}
		
		public void getToDateFilter() throws InterruptedException {
			toDateFilter.click();	
		
			 boolean IsToDateClicked=false;
			    int toYear = 2022;
			    chooseMonth_Year.click();
			    
			    while(true) {
			    	if(IsToDateClicked) {
			    		break;
			    	}
			    	WebElement toCalelement = chooseYear;
			        String todtr = toCalelement.getText();
			     //   System.out.println(todtr);
			        String[] toDateRangepartss = todtr.split(" � ");
			      //  System.out.println(toDateRangepartss[0]);
			      //  System.out.println(toDateRangepartss[1]);
			        outerloop:       
			    	if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
			                //Store each year in string array
			                List < WebElement > tocols;
			                //Get value from Table each cell
			                WebElement toTbodyYear = tBody;
			                // count rows with size() method
			                List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
			                for (WebElement row: trws) {
			                    List < WebElement > tCells = row.findElements(By.tagName("td"));
			                    for (WebElement tCell: tCells) {
			                        if (tCell.getText().contains(Integer.toString(toYear))) {
			                            tCell.click();
			                            Thread.sleep(2000);
			                            int toMonth = 02;
			                            String tMonth = ReturnMonthSelected(toMonth);
			                            WebElement toTbodyMonth = tBody;
			                            // count rows with size() method
			                            List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
			                            for (WebElement tmonthrow: tmonthsrows) {
			                                List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
			                                for (WebElement tMonthcell: tMonthCells) {
			                                    if (tMonthcell.getText().contains(tMonth)) {
			                                    	tMonthcell.click();
			                                        Thread.sleep(2000);
			                                        int toDay = 28;
			                                        WebElement toTbodyDate = tBody;
			                                        // count rows with size() method
			                                        List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
			                                        for(WebElement tmrow: tmrows){
			                                            List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
			                                            for(WebElement tmCell:tmCells){
			                                            	if (tmCell.getText().contains(Integer.toString(toDay)))
			                                            	{
			                                            		tmCell.click();
			                                            		IsToDateClicked = true;
			                                                 	break outerloop;
			                                            	}
			                                               
			                                            }
			                                        }                                                                                                                  
			                                    }
			                                }
			                            }
			                        }
			                    }
			                }
			          		break;     
			    	}
			    	else {
			    		if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
			    			previousYear.click();
			        		continue;
			    		}
			    		else {
			    			nextYear.click();
			        		continue;
			    		}                 
			    	}
			    }
			    
			    Thread.sleep(3000);
				}
		  public static String ReturnMonthSelected(int mnth) {
		      switch (mnth) {
		          case 1:
		              return "JAN";
		          case 2:
		              return "FEB";
		          case 3:
		              return "MAR";
		          case 4:
		              return "APR";
		          case 5:
		              return "MAY";
		          case 6:
		              return "JUN";
		          case 7:
		              return "JUL";
		          case 8:
		              return "AUG";
		          case 9:
		              return "SEP";
		          case 10:
		              return "OCT";
		          case 11:
		              return "NOV";
		          case 12:
		              return "DEC";
		          default:
		              return "Invalid input - Wrong month number.";
		      }
	}

		  public void getFilter() throws InterruptedException {
			 Thread.sleep(5000);  
		  //click on filter
		    filterButton.click();
		    Thread.sleep(5000);
		    //click on By Priority
		    byPriority.click();
		    Thread.sleep(3000);  
		    //low priority
		    lowPriority.click();
		    //click on apply filter button
		    applyButton.click();
		    Thread.sleep(2000);
		}
		  public void getFromDateFilter_Top5() throws InterruptedException {
			  fromDateFilter_Top5.click();	
				
				 boolean IsFromDateClicked=false;
				    int fromYear = 2022;
				    chooseMonth_Year.click();
				    
				    while(true) {
				    	if(IsFromDateClicked) {
				    		break;
				    	}                                                           
				    	WebElement fromCalelement = chooseYear;
				        String fdtr = fromCalelement.getText();
				       // System.out.println(fdtr);
				        String[] fDateRangepartss = fdtr.split(" � ");
				      //  System.out.println(fDateRangepartss[0]);
				      //  System.out.println(fDateRangepartss[1]);
				        outerloop:       
				    	if(fromYear >= Integer.parseInt(fDateRangepartss[0]) && fromYear <= Integer.parseInt(fDateRangepartss[1]) && !IsFromDateClicked ) {
				                //Store each year in string array
				                List < WebElement > fcols;
				                //Get value from Table each cell
				                WebElement ft = tBody;
				                // count rows with size() method
				                List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
				                for (WebElement row: frws) {
				                    List < WebElement > fCells = row.findElements(By.tagName("td"));
				                    for (WebElement fCell: fCells) {
				                        if (fCell.getText().contains(Integer.toString(fromYear))) {
				                            fCell.click();
				                            Thread.sleep(2000);
				                            int fromMonth = 01;
				                            String fMonth = ReturnMonthSelected(fromMonth);
				                            WebElement tm =tBody;
				                            // count rows with size() method
				                            List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
				                            for (WebElement fmonthrow: fmonthsrows) {
				                                List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
				                                for (WebElement fMonthcell: fMonthCells) {
				                                    if (fMonthcell.getText().contains(fMonth)) {
				                                    	fMonthcell.click();
				                                        Thread.sleep(2000);
				                                        int fromDay = 1;
				                                        WebElement fdatecalender = tBody;
				                                        // count rows with size() method
				                                        List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
				                                        for(WebElement fmrow: fmrows){
				                                            List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
				                                            for(WebElement fmCell:fmCells){
				                                            	if (fmCell.getText().contains(Integer.toString(fromDay)))
				                                            	{
				                                            		fmCell.click();
				                                            		IsFromDateClicked = true;
				                                                 	break outerloop;
				                                            	}
				                                               
				                                            }
				                                        }                                                                                                                  
				                                    }
				                                }
				                            }
				                        }
				                    }
				                }
				          		break;     
				    	}
				    	else {
				    		if((Integer.parseInt(fDateRangepartss[0])-fromYear)>0){
				    			previousYear.click();
				        		continue;
				    		}
				    		else {
				    			nextYear.click();
				        		continue;
				    		}                 
				    	}
				    } 
				    Thread.sleep(2000);
					}
				
				public void getToDateFilter_Top5() throws InterruptedException {
					toDateFilter_Top5.click();	
				
					 boolean IsToDateClicked=false;
					    int toYear = 2022;
					    chooseMonth_Year.click();
					    
					    while(true) {
					    	if(IsToDateClicked) {
					    		break;
					    	}
					    	WebElement toCalelement = chooseYear;
					        String todtr = toCalelement.getText();
					     //   System.out.println(todtr);
					        String[] toDateRangepartss = todtr.split(" � ");
					      //  System.out.println(toDateRangepartss[0]);
					      //  System.out.println(toDateRangepartss[1]);
					        outerloop:       
					    	if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
					                //Store each year in string array
					                List < WebElement > tocols;
					                //Get value from Table each cell
					                WebElement toTbodyYear = tBody;
					                // count rows with size() method
					                List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
					                for (WebElement row: trws) {
					                    List < WebElement > tCells = row.findElements(By.tagName("td"));
					                    for (WebElement tCell: tCells) {
					                        if (tCell.getText().contains(Integer.toString(toYear))) {
					                            tCell.click();
					                            Thread.sleep(2000);
					                            int toMonth = 03;
					                            String tMonth = ReturnMonthSelected(toMonth);
					                            WebElement toTbodyMonth = tBody;
					                            // count rows with size() method
					                            List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
					                            for (WebElement tmonthrow: tmonthsrows) {
					                                List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
					                                for (WebElement tMonthcell: tMonthCells) {
					                                    if (tMonthcell.getText().contains(tMonth)) {
					                                    	tMonthcell.click();
					                                        Thread.sleep(2000);
					                                        int toDay = 23;
					                                        WebElement toTbodyDate = tBody;
					                                        // count rows with size() method
					                                        List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
					                                        for(WebElement tmrow: tmrows){
					                                            List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
					                                            for(WebElement tmCell:tmCells){
					                                            	if (tmCell.getText().contains(Integer.toString(toDay)))
					                                            	{
					                                            		tmCell.click();
					                                            		IsToDateClicked = true;
					                                                 	break outerloop;
					                                            	}
					                                               
					                                            }
					                                        }                                                                                                                  
					                                    }
					                                }
					                            }
					                        }
					                    }
					                }
					          		break;     
					    	}
					    	else {
					    		if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
					    			previousYear.click();
					        		continue;
					    		}
					    		else {
					    			nextYear.click();
					        		continue;
					    		}                 
					    	}
					    }
					    
					    Thread.sleep(3000);
				}
					    public void getFromDateFilterDropdown() throws InterruptedException {
					    	fromDateFilterDropdown.click();	
							
							 boolean IsFromDateClicked=false;
							    int fromYear = 2022;
							    chooseMonth_Year.click();
							    
							    while(true) {
							    	if(IsFromDateClicked) {
							    		break;
							    	}                                                           
							    	WebElement fromCalelement = chooseYear;
							        String fdtr = fromCalelement.getText();
							       // System.out.println(fdtr);
							        String[] fDateRangepartss = fdtr.split(" � ");
							      //  System.out.println(fDateRangepartss[0]);
							      //  System.out.println(fDateRangepartss[1]);
							        outerloop:       
							    	if(fromYear >= Integer.parseInt(fDateRangepartss[0]) && fromYear <= Integer.parseInt(fDateRangepartss[1]) && !IsFromDateClicked ) {
							                //Store each year in string array
							                List < WebElement > fcols;
							                //Get value from Table each cell
							                WebElement ft = tBody;
							                // count rows with size() method
							                List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
							                for (WebElement row: frws) {
							                    List < WebElement > fCells = row.findElements(By.tagName("td"));
							                    for (WebElement fCell: fCells) {
							                        if (fCell.getText().contains(Integer.toString(fromYear))) {
							                            fCell.click();
							                            Thread.sleep(2000);
							                            int fromMonth = 01;
							                            String fMonth = ReturnMonthSelected(fromMonth);
							                            WebElement tm =tBody;
							                            // count rows with size() method
							                            List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
							                            for (WebElement fmonthrow: fmonthsrows) {
							                                List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
							                                for (WebElement fMonthcell: fMonthCells) {
							                                    if (fMonthcell.getText().contains(fMonth)) {
							                                    	fMonthcell.click();
							                                        Thread.sleep(2000);
							                                        int fromDay = 1;
							                                        WebElement fdatecalender = tBody;
							                                        // count rows with size() method
							                                        List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
							                                        for(WebElement fmrow: fmrows){
							                                            List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
							                                            for(WebElement fmCell:fmCells){
							                                            	if (fmCell.getText().contains(Integer.toString(fromDay)))
							                                            	{
							                                            		fmCell.click();
							                                            		IsFromDateClicked = true;
							                                                 	break outerloop;
							                                            	}
							                                               
							                                            }
							                                        }                                                                                                                  
							                                    }
							                                }
							                            }
							                        }
							                    }
							                }
							          		break;     
							    	}
							    	else {
							    		if((Integer.parseInt(fDateRangepartss[0])-fromYear)>0){
							    			previousYear.click();
							        		continue;
							    		}
							    		else {
							    			nextYear.click();
							        		continue;
							    		}                 
							    	}
							    } 
							    Thread.sleep(2000);
								}
							
							public void getToDateFilterDropdown() throws InterruptedException {
								toDateFilterDropdown.click();	
							
								 boolean IsToDateClicked=false;
								    int toYear = 2022;
								    chooseMonth_Year.click();
								    
								    while(true) {
								    	if(IsToDateClicked) {
								    		break;
								    	}
								    	WebElement toCalelement = chooseYear;
								        String todtr = toCalelement.getText();
								     //   System.out.println(todtr);
								        String[] toDateRangepartss = todtr.split(" � ");
								      //  System.out.println(toDateRangepartss[0]);
								      //  System.out.println(toDateRangepartss[1]);
								        outerloop:       
								    	if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
								                //Store each year in string array
								                List < WebElement > tocols;
								                //Get value from Table each cell
								                WebElement toTbodyYear = tBody;
								                // count rows with size() method
								                List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
								                for (WebElement row: trws) {
								                    List < WebElement > tCells = row.findElements(By.tagName("td"));
								                    for (WebElement tCell: tCells) {
								                        if (tCell.getText().contains(Integer.toString(toYear))) {
								                            tCell.click();
								                            Thread.sleep(2000);
								                            int toMonth = 03;
								                            String tMonth = ReturnMonthSelected(toMonth);
								                            WebElement toTbodyMonth = tBody;
								                            // count rows with size() method
								                            List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
								                            for (WebElement tmonthrow: tmonthsrows) {
								                                List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
								                                for (WebElement tMonthcell: tMonthCells) {
								                                    if (tMonthcell.getText().contains(tMonth)) {
								                                    	tMonthcell.click();
								                                        Thread.sleep(2000);
								                                        int toDay = 23;
								                                        WebElement toTbodyDate = tBody;
								                                        // count rows with size() method
								                                        List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
								                                        for(WebElement tmrow: tmrows){
								                                            List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
								                                            for(WebElement tmCell:tmCells){
								                                            	if (tmCell.getText().contains(Integer.toString(toDay)))
								                                            	{
								                                            		tmCell.click();
								                                            		IsToDateClicked = true;
								                                                 	break outerloop;
								                                            	}
								                                               
								                                            }
								                                        }                                                                                                                  
								                                    }
								                                }
								                            }
								                        }
								                    }
								                }
								          		break;     
								    	}
								    	else {
								    		if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
								    			previousYear.click();
								        		continue;
								    		}
								    		else {
								    			nextYear.click();
								        		continue;
								    		}                 
								    	}
								    }
								    
								    Thread.sleep(3000);
									}			

		  }

