package pageClasses;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyTask {
	public WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
	public MyTask(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	//assign to others
	@FindBy(xpath="//span[normalize-space()='My Task']")
	WebElement myTaskLink;
	@FindBy(xpath="//span[normalize-space()='Add Task']")
	WebElement AddTaskButton;
	@FindBy(xpath="//input[@formcontrolname='Title']")
	WebElement title;
	@FindBy(xpath="//textarea[@placeholder='Description*']")
	WebElement description;
	@FindBy(xpath="//input[@class='kt-pointer mat-input-element mat-form-field-autofill-control cdk-text-field-autofill-monitored']")
	WebElement attachFile;
	@FindBy(xpath="//mat-select[@placeholder='Lead/Customer Name']//div[@class='mat-select-value']")
	WebElement customer_leadName;
	@FindBy(css="input[name='searchBar'][autocomplete='off']")
	WebElement search;
	@FindBy(xpath="//mat-option[1]//span[1]")
	WebElement leadname;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]")
	WebElement selectDueDate;
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[8]/div/div/kt-tag/div/mat-form-field/div/div[1]/div/input")
	WebElement nextYear;
	@FindBy(xpath="//div[@class='mat-select-trigger']//span[contains(text(),'Select Priority')]")
	WebElement selectPriority;
	@FindBy(xpath="//input[@placeholder='Add Users*']")
	WebElement addUsers;
	@FindBy(xpath="//kt-add-members-dialog//input[@aria-invalid='false']")
	WebElement searchUsers;
	//@FindBy(xpath="//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
	//WebElement LinaPatilCheckBox;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement Done;
	@FindBy(xpath="//input[@formcontrolname='TaskDisplayOwners']")
	WebElement addCCMembers;
	@FindBy(xpath="//kt-add-members-dialog//input[@aria-invalid='false']")
	WebElement searchCCMembers;
	//@FindBy(xpath="(//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin'])[3]")
	//WebElement linatest3CheckBox;
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement addButton;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']")
	WebElement dropdownList;
	@FindBy(xpath="//div[@dir='ltr']//div//div//div//mat-form-field//div//div//div//input[@placeholder='Search']")
	WebElement searchLeadName;
	@FindBy(xpath="(//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin'])[1]")
	WebElement assignUserCheckbox;
	@FindBy(xpath="//div[@class='kt-form__actions kt-form__actions--sm']//span[@class='mat-button-wrapper'][normalize-space()='Cancel']")
	WebElement cancelUser;



	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("My Task");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	public void getMyTaskLink() {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(myTaskLink))).click();
	}
	public void getAddTaskButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AddTaskButton))).click();

	}
	public void getTaskDetails_AssignToOther() throws InterruptedException, AWTException {
		String titlename = sheet.getRow(2).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(title))).sendKeys(titlename);
		String descript = sheet.getRow(2).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).sendKeys(descript);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachFile))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("marketing.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customer_leadName))).click();
		Thread.sleep(3000);
		try {

			if(dropdownList.isDisplayed())
			{
				String searchleadname = sheet.getRow(2).getCell(2).toString();
				searchLeadName.sendKeys(searchleadname);
				as.sendKeys(Keys.ENTER).build().perform();
				System.out.println("Lead Name is selected");
			}}
		catch (Exception e) {
			System.out.println("Lead Name is not selected");
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectDueDate))).click();

	}
	public void getTaskDetails_AssignToOther2() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectPriority))).click();
		as.sendKeys(Keys.ENTER).build().perform();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addUsers))).click();
		Thread.sleep(2000);
		try {

			if(assignUserCheckbox.isDisplayed())
			{
				String searchuser = sheet.getRow(2).getCell(3).toString();
				searchUsers.sendKeys(searchuser);
				as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Done))).click();
				System.out.println("Assign user is selected");
			}}
		catch (Exception e) {

			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelUser))).click();
			System.out.println("Assign user is not selected");
		}

		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addCCMembers))).click();
		Thread.sleep(2000);
		try {

			if(assignUserCheckbox.isDisplayed())
			{
				String searchCCmember = sheet.getRow(2).getCell(2).toString();
				searchCCMembers.sendKeys(searchCCmember);
				as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Done))).click();
				System.out.println("Member is selected");
			}}
		catch (Exception e) {

			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelUser))).click();
			System.out.println("Member is not selected");
		}
	}
	public void getAddButton_AssignToOther() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addButton))).click();
	}

	//Assign to me
	@FindBy(xpath="//div[contains(text(),'Assign to Me')]")
	WebElement assignToMe;

	public void getAssignToMeScreen() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignToMe))).click();
	}
	public void getTaskDetails_AssignToMe() throws InterruptedException, AWTException {
		Thread.sleep(3000);
		String titlename = sheet.getRow(6).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(title))).sendKeys(titlename);
		Thread.sleep(3000);
		String descript = sheet.getRow(6).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).sendKeys(descript);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachFile))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("marketing.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);

		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customer_leadName))).click();
		Thread.sleep(3000);
		try {

			if(dropdownList.isDisplayed())
			{
				String searchleadname = sheet.getRow(6).getCell(2).toString();
				searchLeadName.sendKeys(searchleadname);
				as.sendKeys(Keys.ENTER).build().perform();
				System.out.println("Lead Name is selected");
			}}
		catch (Exception e) {
			System.out.println("Lead Name is not selected");
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectDueDate))).click();
	}
	public void getTaskDetails_AssignToMe2() throws InterruptedException {				
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectPriority))).click();
		as.sendKeys(Keys.ENTER).build().perform();	
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addCCMembers))).click();
		Thread.sleep(2000);
		try {

			if(assignUserCheckbox.isDisplayed())
			{
				String searchCCmember = sheet.getRow(2).getCell(3).toString();
				searchCCMembers.sendKeys(searchCCmember);
				as.sendKeys(Keys.ENTER).build().perform();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Done))).click();
				System.out.println("Member is selected");
			}}
		catch (Exception e) {

			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelUser))).click();
			System.out.println("Member is not selected");
		}
	}

	//edit
	@FindBy(xpath="//mat-table/mat-row[1]/mat-cell[@class='kt-block-inline highlight mat-cell cdk-column-Title mat-column-Title ng-star-inserted'][1]")
	WebElement titleName;
	@FindBy(xpath="//button[.='Edit Task']")
	WebElement EditTaskButton;
	//@FindBy(xpath="//label[.='Select Priority']")
	@FindBy(xpath="//mat-select[@aria-label='Select Priority']//div[@aria-hidden='true']//div//div")
	WebElement SelectPriority1;
	@FindBy(xpath="(//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin'])[1]")
	WebElement linaCheckBox;
	@FindBy(xpath="//kt-add-members-dialog//span[contains(text(),'Done')]")
	WebElement doneMember;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement edituserButton;
	@FindBy(xpath="//mat-icon[@role='img']")
	WebElement attachFile2;
	@FindBy(xpath="//textarea[@placeholder='Enter your comment...']")
	WebElement comment;
	@FindBy(xpath="//span[normalize-space()='Add Comment']")
	WebElement addCommentButton;
	@FindBy(xpath="//span[normalize-space()='Cancel']")
	WebElement cancelButton;

	public void getTitleNameToEditTask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(titleName))).click();
	}
	public void getEditButtonToEditTaskdetails() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(EditTaskButton))).click();
	}
	public void getEditTaskDetails_AssignToMe() throws InterruptedException, AWTException {
		Thread.sleep(3000);
		String titlename = sheet.getRow(10).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(title))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(title))).sendKeys(titlename);
		Thread.sleep(3000);
		String descript = sheet.getRow(10).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).sendKeys(descript);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachFile))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("tracking.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customer_leadName))).click();
		Thread.sleep(3000);
		try {

			if(dropdownList.isDisplayed())
			{
				String searchlead = sheet.getRow(10).getCell(2).toString();
				search.sendKeys(searchlead);
				as.sendKeys(Keys.ENTER).build().perform();
				System.out.println("Lead Name is selected");
			}}
		catch (Exception e) {
			System.out.println("Lead Name is not selected");
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectDueDate))).click();
		
	}
	public void getEditTaskDetails_AssignToMe2() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(SelectPriority1))).click();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();	
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addCCMembers))).click();
		Thread.sleep(2000);
		try {

			if(assignUserCheckbox.isDisplayed())
			{
				String searchCCmember = sheet.getRow(10).getCell(3).toString();
				searchCCMembers.sendKeys(searchCCmember);
				as.sendKeys(Keys.ENTER).build().perform();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(doneMember))).click();
				System.out.println("Member is selected");
			}}
		catch (Exception e) {

			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelUser))).click();
			System.out.println("Member is not selected");
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(edituserButton))).click();
	}
	public void addComment_AssignToMe() throws InterruptedException, AWTException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachFile2))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("reicept.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		String addcomment = sheet.getRow(10).getCell(4).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(comment))).sendKeys(addcomment);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addCommentButton))).click();

	}
	public void cancelbutton() throws InterruptedException{
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelButton))).click();

	}

	//my task
	@FindBy(xpath="(//input[@name='searchBar'])[1]")
	WebElement search_Mytask;
	@FindBy(xpath="(//button[@type='button'])[12]")
	WebElement acceptTask;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/kt-mytask[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[7]/span[1]/img[1]")
	WebElement partialCompleteTask;
	@FindBy(xpath="//div[@class='partialBlocks ng-star-inserted']")
	WebElement selectPercentage;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement done;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/kt-mytask[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[6]/span[1]/img[1]")
	WebElement completeTask;
	@FindBy(xpath="//span[normalize-space()='Yes']")
	WebElement yes;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/kt-mytask[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[4]/span[1]/img[1]")
	WebElement viewTaskCoverage;                                                                                                                                                                                                       
	@FindBy(xpath="//div[@class='kt-portlet__head kt-portlet__head__custom']//i[@class='fa fa-times pull-right']")
	WebElement CloseTaskCoverage;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/kt-mytask[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[2]/span[1]/img[1]")
	WebElement archiveTask;
	@FindBy(xpath="//div[contains(text(),'Archive List')]")
	WebElement archiveListScreen;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[4]/div[1]/kt-archivelist[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[1]/span[1]/img[1]")
	WebElement unarchiveTask;
	@FindBy(xpath="//div[@class='mat-tab-label-content'][normalize-space()='My Task']")
	WebElement myTaskScreen;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[1]/div[1]/kt-mytask[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[5]/span[1]/img[1]")
	WebElement deletetask;
	@FindBy(xpath="//span[normalize-space()='Delete']")
	WebElement deleteButton;


	public void search_Mytask() throws InterruptedException {
		String searchTask = sheet.getRow(14).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search_Mytask))).sendKeys(searchTask);	
		Thread.sleep(3000);
	}
	public void acceptTask_Mytask() throws InterruptedException {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(acceptTask))).click();
	}
	public void partialCompleteTask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(partialCompleteTask))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectPercentage))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(done))).click();
		Thread.sleep(3000);
	}
	public void completeTask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(completeTask))).click();	
		yes.click();
		Thread.sleep(3000);
	}
	public void viewTaskCoverage() throws InterruptedException {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewTaskCoverage)));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", viewTaskCoverage);
		Thread.sleep(3000);
		CloseTaskCoverage.click();
	}
	public void archiveTask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(archiveTask))).click();	
		yes.click();
		Thread.sleep(3000);
	}	
	public void unarchiveTask() throws InterruptedException {
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(unarchiveTask))).click();
			yes.click();
			Thread.sleep(3000);
		}
		catch(Exception e) {

		}

	}	
	public void myTaskScreen() {
		myTaskScreen.click();	
	}
	public void deletetask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deletetask))).click();	
		Thread.sleep(3000);
		deleteButton.click();
	}	

	//settings
	@FindBy(xpath="//span[normalize-space()='Setting']")
	WebElement settingsButton;
	@FindBy(xpath="//div[normalize-space()='+ Add New']")
	WebElement addNewPercentage;
	@FindBy(xpath="//input[@placeholder='Value']")
	WebElement value;
	@FindBy(xpath="//div[@class='partialBlocks isSelected']")
	WebElement selectper;
	@FindBy(xpath="//body//kt-base//div[@x-placement='bottom-right']//div//div//div//div//div[1]//div[1]")
	WebElement removePercentage;
	@FindBy(xpath="//i[@class='fa fa-times']")
	WebElement closeSetting;

	public void settingButton() {
		settingsButton.click();	
	}

	public void addPercentage() {
		addNewPercentage.click();	
		value.sendKeys("30");
		done.click();
	}
	public void select_removePercentage() {
		wait.until(ExpectedConditions.invisibilityOf(selectper));
		selectper.click();

		//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(removePercentage)));
		//wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(viewTaskCoverage));
		wait.until(ExpectedConditions.invisibilityOf(removePercentage));
		removePercentage.click();	
	}
	public void closesetting() {
		closeSetting.click();
	}


	//cc
	@FindBy(xpath="//button[@aria-label='Open calendar']//span[@class='mat-button-wrapper']//*[name()='svg']")
	WebElement selectDuedate_CC; 
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[2]/div[1]/kt-cc[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[3]/span[1]/img[1]")
	WebElement viewTaskCoverage_CC;  
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[2]/div[1]/kt-cc[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[1]/span[1]/img[1]")
	WebElement archiveTask_CC;
	@FindBy(xpath="//mat-row[1]//mat-cell[8]//button[6]//span[1]//img[1]")
	WebElement deletetask_CC;
	public void viewTaskCoverage_CC() throws InterruptedException {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(viewTaskCoverage_CC));
		//wait.until(ExpectedConditions.invisibilityOf(viewTaskCoverage_CC));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", viewTaskCoverage_CC);
		Thread.sleep(3000);
		CloseTaskCoverage.click();
	}
	public void archiveTask_CC() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(archiveTask_CC))).click();	
		yes.click();
		Thread.sleep(3000);
	}
	public void getTaskDetails_AssignToOtherCC() throws InterruptedException, AWTException {
		String titlename = sheet.getRow(2).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(title))).sendKeys(titlename);
		String descript = sheet.getRow(2).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).sendKeys(descript);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachFile))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("marketing.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customer_leadName))).click();
		Thread.sleep(3000);
		try {

			if(dropdownList.isDisplayed())
			{
				String searchleadname = sheet.getRow(2).getCell(2).toString();
				searchLeadName.sendKeys(searchleadname);
				as.sendKeys(Keys.ENTER).build().perform();
				System.out.println("Lead Name is selected");
			}}
		catch (Exception e) {
			System.out.println("Lead Name is not selected");
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectDuedate_CC))).click();
	}
	public void deletetask_CC() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deletetask_CC))).click();	
		Thread.sleep(3000);
		deleteButton.click();
	}
	//Assign By Me
	@FindBy(xpath="//mat-tab-body//div//form//div//div//mat-form-field//div//div//div//mat-datepicker-toggle//button[@aria-label='Open calendar']//span//*[name()='svg']")
	WebElement selectDuedate_AssignByMe; 
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[3]/div[1]/kt-assignedby[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[2]/span[1]/img[1]")
	WebElement viewTaskCoverage_AssignByMe;  
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[3]/div[1]/kt-assignedby[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[1]/span[1]/img[1]")
	WebElement archiveTask_AssignByMe;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-task[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/mat-tab-group[1]/div[1]/mat-tab-body[3]/div[1]/kt-assignedby[1]/mat-drawer-container[1]/mat-drawer-content[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[8]/button[3]/span[1]/img[1]")
	WebElement deletetask_AssignByMe;
	public void viewTaskCoverage_AssignByMe() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewTaskCoverage_AssignByMe)));
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(viewTaskCoverage_AssignByMe));
		//wait.until(ExpectedConditions.invisibilityOf(viewTaskCoverage_CC));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click()", viewTaskCoverage_AssignByMe);
		Thread.sleep(3000);
		CloseTaskCoverage.click();
	}
	public void archiveTask_AssignByMe() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(archiveTask_AssignByMe))).click();	
		yes.click();
		Thread.sleep(3000);
	}
	public void getTaskDetails_AssignToOtherAssignByMe() throws InterruptedException, AWTException {		
		String titlename = sheet.getRow(2).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(title))).sendKeys(titlename);
		String descript = sheet.getRow(2).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).sendKeys(descript);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachFile))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("marketing.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customer_leadName))).click();
		Thread.sleep(3000);
		try {

			if(dropdownList.isDisplayed())
			{
				String searchleadname = sheet.getRow(2).getCell(2).toString();
				searchLeadName.sendKeys(searchleadname);
				as.sendKeys(Keys.ENTER).build().perform();
				System.out.println("Lead Name is selected");
			}}
		catch (Exception e) {
			System.out.println("Lead Name is not selected");
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectDuedate_AssignByMe))).click();
	}
	public void deletetask_AssignByMe() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deletetask_AssignByMe))).click();	
		Thread.sleep(3000);
		deleteButton.click();
	}


	//Calender View
	@FindBy(xpath="//div[contains(text(),'Calendar View')]")
	WebElement calenderView;
	@FindBy(xpath="//div[normalize-space()='Previous']")
	WebElement previousbutton;
	@FindBy(xpath="//div[normalize-space()='Next']")
	WebElement nextbutton;
	@FindBy(xpath="//div[normalize-space()='Today']")
	WebElement toDaybutton;
	@FindBy(xpath="//div[normalize-space()='Week']")
	WebElement weekbutton;
	@FindBy(xpath="//div[normalize-space()='Day']")
	WebElement daybutton;
	@FindBy(xpath="//mat-tab-body//div//kt-task-calendar//div//div//div//h3")
	WebElement fetchtext;
	@FindBy(xpath="//div[@class='mat-select-arrow']")
	WebElement selectOption;

	public void CalenderView(){
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(calenderView))).click();
	}
	public void previousButton_text() throws InterruptedException{
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(previousbutton))).click();
		String text=fetchtext.getText();
		System.out.println("Previous= " + text);
		Thread.sleep(3000);
	}
	public void toDayButton_text() throws InterruptedException{
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(toDaybutton))).click();
		String text=fetchtext.getText();
		System.out.println("Today= " + text);
		Thread.sleep(3000);
	}
	public void nextButton_text() throws InterruptedException{
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(nextbutton))).click();
		String text=fetchtext.getText();
		System.out.println("Next= " + text);
		Thread.sleep(3000);
	}
	public void weekButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(weekbutton))).click();
		Thread.sleep(3000);
	}
	public void dayButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(daybutton))).click();
		Thread.sleep(3000);
	}
	public void selectOption() throws InterruptedException {
		as.click(selectOption).perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
	}
	public void selectOption1() throws InterruptedException {
		as.click(selectOption).perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
	}





	//export
	@FindBy(xpath="//div[contains(text(),'Assigned By Me')]")
	WebElement assignedByMeScreen;
	@FindBy(xpath="//span[normalize-space()='Export']")
	WebElement export_MyTask;

	public void assignedByMeScreen() {
		assignedByMeScreen.click();
	}
	public void archiveListScreen() {
		archiveListScreen.click();
	}
	public void export_MyTask() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(export_MyTask))).click();
	}

	//filter
	@FindBy(xpath="//kt-filter[@filterformat='mat-table']//span[contains(text(),'Filter')]")
	WebElement filter_mytask;
	@FindBy(xpath="//kt-filter[@filterformat='mat-table']//div[@aria-expanded='false']//div[@aria-labelledby='filterDropdown']//div//div//form//div//div//div//div//div//mat-form-field//div//div//div//mat-select[@placeholder='Status']//div[@aria-hidden='true']//div//span[contains(text(),'Status')]")
	WebElement byStatusMyTask;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Member')]")
	WebElement byMemberMyTask;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Priority')]")
	WebElement byPriorityMyTask;
	@FindBy(xpath="(//*[name()='svg'][@fill='currentColor'])[1]")
	WebElement fromdate_MyTask;
	@FindBy(xpath="(//*[name()='svg'][@fill='currentColor'])[2]")
	WebElement todate_MyTask;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Member')]")
	WebElement applyFilterButtonMyTask;
	@FindBy(xpath="//span[contains(text(),('Apply '))]")
	WebElement applyFilterButton;
	@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
	WebElement clearFilter;
	@FindBy(xpath="//div[contains(text(),'CC')]")
	WebElement cc_MyTask;
	@FindBy(xpath="//div[contains(text(),'Assigned By Me')]")
	WebElement AssignedByMe_MyTask;
	public void filter_mytask(){
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(filter_mytask))).click();
	}
	public void filterbyStatus_MyTask() throws InterruptedException {
		as.click(byStatusMyTask).perform();
		as.sendKeys(Keys.ENTER).build().perform();
	}
	public void filterbyPriority_MyTask() {
		as.click(byPriorityMyTask).perform();
		as.sendKeys(Keys.ENTER).build().perform();
	}
	public void filterByMember_MyTask() {
		as.click(byMemberMyTask).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void fromDuedate_MyTask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fromdate_MyTask))).click();
	}

	public void toDuedate_MyTask() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(todate_MyTask))).click();
	}
	public void getApplyFilterButtonMytask() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(applyFilterButton))).click();

	}
	public void getClearFilterMyTask() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(clearFilter))).click();
	
	}
	public void getCC_MyTask() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cc_MyTask))).click();
	
	}
	public void getAssignedByMe_MyTask() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AssignedByMe_MyTask))).click();
	
	}

	//=================================================================//
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void getVerifyMsg() {
		try {
			if (toastMessage.isDisplayed()) {
				System.out.println("msg is appearing");
				String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vmsg);
			}
			} 
			catch (Exception e) {
				System.out.println("Validation msg is not displayed");
			} 
	}

	public void getDateFilter() throws InterruptedException {
		//  Thread.sleep(2000);

		boolean IsDateClicked=false;
		int Year = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			// System.out.println(fDateRangepartss[0]);
			// System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(Year))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 9;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 1;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public void getDateFilter1() throws InterruptedException {
		//  Thread.sleep(2000);

		boolean IsDateClicked=false;
		int Year = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			// System.out.println(fDateRangepartss[0]);
			// System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(Year))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 12;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 31;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}

}
