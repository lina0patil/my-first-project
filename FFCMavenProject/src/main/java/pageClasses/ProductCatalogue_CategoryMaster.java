package pageClasses;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductCatalogue_CategoryMaster {
	public WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	public ProductCatalogue_CategoryMaster(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(className="ps__thumb-y")
	WebElement verticalSlider;
	@FindBy(xpath="//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")
	WebElement settings;
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	@FindBy(xpath="//span[normalize-space()='Product Catalogue']")
	WebElement productCatalogue;
	@FindBy(xpath="//span[normalize-space()='Category Master']")
	WebElement categoryMaster;
	@FindBy(xpath="//span[normalize-space()='Add Category']")
	WebElement addCategory;
	@FindBy(xpath="//input[@formcontrolname='Name']")
	WebElement enterCategoryName;
	@FindBy(xpath="//textarea[@placeholder='Description*']")
	WebElement description;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Select Parent Category')]")
	WebElement selectParentCategory;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']")
	WebElement parentCategoryDropdownList;
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement add;
	public void getVerifyMsg1() {
		String vMsg = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
		System.out.println("Validation Message: "+vMsg);	
	}
	public void getVerifyMsg() {
		try {
			if(toastMessage.isDisplayed()==true) {
				String vMsg = wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vMsg);
			}
		}
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 

	}

	public void VerticalSlider_Settings() throws InterruptedException {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 250).release().perform();
	}

	public void getSettings() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(settings))).click();
	}
	public void VerticalSlider_ProductCatalogue() throws InterruptedException {
		as=new Actions(driver);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 150).release().perform();
	}
	public void productCatalogueLink() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(productCatalogue))).click();

	}
	public void categoryMasterLink() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(categoryMaster))).click();

	}
	public void addCategorybutton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addCategory))).click();

	}
	public void enterCategoryDetails() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterCategoryName))).sendKeys("category L");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).sendKeys("testing");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectParentCategory))).click();

		try {
			Boolean dropdownPresent = parentCategoryDropdownList.isDisplayed();
			if(dropdownPresent==true)
			{
				System.out.println("Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
			}
		}
		catch (Exception e) {
			System.out.println("Dropdown is not appearing");
		} 
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(add))).click();
	}
	//search,edit, delete category
	@FindBy(xpath="(//span[@class='mat-button-wrapper'][normalize-space()='Edit'])[1]")
	WebElement editCategory;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement done;
	@FindBy(xpath="//input[@name='searchBar']")
	WebElement search;
	@FindBy(xpath="(//span[@class='mat-button-wrapper'][normalize-space()='Delete'])[1]")
	WebElement delete;
	
	public void searchCategory() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).sendKeys("category");
		Thread.sleep(3000);

	}
	public void editCategorybutton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editCategory))).click();

	}
	public void editEnterCategoryDetails() throws InterruptedException {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterCategoryName))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterCategoryName))).sendKeys("update category");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(description))).clear();
		description.sendKeys("testing");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(done))).click();
	}
	public void deleteCategory() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).sendKeys("update category");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(delete))).click();
	}
	
	//Bulk upload, show report
	@FindBy(xpath="//span[normalize-space()='Bulk Upload']")
	WebElement bulkUpload;
	@FindBy(xpath="//b[normalize-space()='Click Here']")
	WebElement downloadAllCategories;
	@FindBy(xpath="//a[normalize-space()='Click to download sample file']")
	WebElement downloadSampleFile;
	@FindBy(xpath="//span[normalize-space()='Attach Excel File']")
	WebElement attachExcelFile;
	@FindBy(xpath="//span[normalize-space()='Upload']")
	WebElement uploadbutton;
	@FindBy(xpath="//span[normalize-space()='Show Report']")
	WebElement showReport;
	@FindBy(xpath="(//i[@mattooltip='Dowload BulkUpload Report'])[1]")
	WebElement dowloadBulkUploadReport;
	@FindBy(xpath="(//span[contains(text(),'File')])[1]")
	WebElement dowloadBulkUploadFileReport;
	@FindBy(xpath="(//span[contains(text(),'ViewReport')])[1]")
	WebElement viewReport;
	public void bulkUploadCategoryButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(bulkUpload))).click();
	}
	public void downloadAllCategories() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(downloadAllCategories))).click();

	}
	public void downloadSampleFile() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(downloadSampleFile))).click();

	}
	public void attachExcelFile() throws InterruptedException, AWTException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachExcelFile))).click();
		Thread.sleep(3000);
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("category.xlsx");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
	}
	public void uploadbutton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(uploadbutton))).click();

	}
	public void showReport_bulkUpload() {

		 try {
			 if(addCategory.isDisplayed()==true) {
				 System.out.println("Category bulk uploaded successfully");
				 wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(bulkUpload))).click();
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(showReport))).click();
				}
		    } catch (Exception e) {
		    	 System.out.println("Category bulk upload failed");
		    	wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(showReport))).click();
		    } 
	}
	public void dowloadBulkUploadReport() {
		 try {
			 if(dowloadBulkUploadReport.isDisplayed()==true) {
				 wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dowloadBulkUploadReport))).click();
				}
		    } catch (Exception e) {
		      System.out.println("Report is not available");
		    } 
	}
	public void dowloadBulkUploadFileReport() {
		 try {
			 if(dowloadBulkUploadFileReport.isDisplayed()==true) {
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dowloadBulkUploadFileReport))).click();
				}
		    } catch (Exception e) {
		      System.out.println("Report is not available");
		    } 
	}
	public void viewReport() {
		 try {
			 if(viewReport.isDisplayed()==true) {
				 wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewReport))).click();
				}
		    } catch (Exception e) {
		      System.out.println("Report is not available");
		    } 
	}
	
		
}
