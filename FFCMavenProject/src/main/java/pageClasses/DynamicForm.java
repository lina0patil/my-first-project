package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DynamicForm {
	public WebDriver driver;
	WebDriverWait wait;
	Actions as;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
    //create constructor
    public DynamicForm(WebDriver driver) {
	this.driver=driver;
	PageFactory.initElements(driver, this);
    }

    @FindBy(xpath="//span[normalize-space()='Dynamic Form']")
    WebElement DynamicForm;
    
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void getVerifyMsg() {
		
		try {
			if (toastMessage.isDisplayed()) {
			System.out.println("msg is appearing");
			String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
			System.out.println("Validation Message: "+vmsg);
			}
			} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
			} 	
		}

		public void ExcelConnect() throws IOException {
			FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
			wb = new XSSFWorkbook(file);
			sheet = wb.getSheet("My Customer");
		}
		public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
			ExcelConnect();
			j = n;
		}

   
	public void getVisitForm() {
		as=new Actions(driver);
	    wait = new WebDriverWait(driver, timeout);
		DynamicForm.click();	
	}	
}
