package pageClasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class login {
	public WebDriver driver;
	//create constructor
	public login(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="mat-input-0")
	WebElement username;
	@FindBy(id="mat-input-1")
	WebElement password;
	@FindBy(id="kt_login_signin_submit")
	WebElement signInbutton;
	@FindBy(xpath="//span[contains(text(),'Punch In')]")
	WebElement punchIn;
	@FindBy(xpath="//span[contains(text(),'Punch Out')]")
	WebElement punchOut;
	@FindBy(xpath="//textarea[@placeholder='Report']")
	WebElement punchOutReport;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement punchOutDone;
	public void getUserName(String Username) {
		username.sendKeys(Username);	
	}
	public void getPass(String Password) {
		password.sendKeys(Password);		
	}
	public void getsignInbutton() {
		signInbutton.click();	
	}
	public void getPunchIn() {
		int timeout=6;
		final WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.refreshed(
				ExpectedConditions.elementToBeClickable(punchIn)));
		punchIn.click();	
	}
	public void getPunchOut() {
		punchOut.click();	
	}
	public void sendReport_PunchOut() {
		punchOutReport.sendKeys("Testing");
		punchOutDone.click();
	}
}
