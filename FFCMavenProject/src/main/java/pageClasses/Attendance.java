package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Attendance {
	public WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
	public Attendance(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[normalize-space()='Attendance']")
	WebElement attendenceLink;
	@FindBy(xpath="//span[normalize-space()='Add New']")
	WebElement addNewClaim;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]")
	WebElement selectPunchInDate;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[4]")
	WebElement selectPunchOutDate;
	//@FindBy(xpath="//span[.='Select Punch in Time*']//label")
	@FindBy(xpath="//input[@formcontrolname='PunchInTime']")
	WebElement selectPunchInTime;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='HH']")
	WebElement PunchInHH;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='MM']")
	WebElement PunchInMM;
	@FindBy(xpath="//div[@class='ngb-tp-meridian ng-star-inserted']")
	WebElement AM_PM;
	@FindBy(xpath="//button[normalize-space()='PM']")
	WebElement punchInPM;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//span[@class='mat-button-wrapper'][normalize-space()='Done']")
	WebElement PunchInDoneButton;
	//@FindBy(xpath="//span[.='Select Punch Out Time*']")
	@FindBy(xpath="//input[@formcontrolname='PunchOutTime']")
	WebElement selectPunchOutTime;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='HH']")
	WebElement PunchOutHH;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='MM']")
	WebElement PunchOutMM;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//button[@type='button'][normalize-space()='AM']")
	WebElement PunchOutPM;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//span[@class='mat-button-wrapper'][normalize-space()='Done']")
	WebElement PunchOutDoneButton;
	@FindBy(xpath="//textarea[@formcontrolname='Reason']")
	WebElement reasonForClaim;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-claims-dialog/div/form/div[2]/div/button[2]/span")
	WebElement DoneClaim;
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[8]/div/div/kt-tag/div/mat-form-field/div/div[1]/div/input")
	WebElement nextYear;



	//filter

	@FindBy(xpath="//span[normalize-space()='Filter']")
	WebElement filterclaimbutton;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[1]")
	WebElement claimByStatus;
	@FindBy(xpath="//mat-option[@value='1']//mat-pseudo-checkbox[@class='mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted']")
	WebElement pendingclaim;
	@FindBy(xpath="(//mat-pseudo-checkbox[@class='mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted mat-pseudo-checkbox-checked'])[1]")
	WebElement approvedclaim;
	@FindBy(css="(//mat-pseudo-checkbox[@class='mat-option-pseudo-checkbox mat-pseudo-checkbox ng-star-inserted'])[2]")
	WebElement rejectedclaim;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
	WebElement fromdatefilterclaim;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
	WebElement todatefilterclaim;
	@FindBy(xpath="//span[normalize-space()='Apply']")
	WebElement applyClaim;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//button//span[contains(text(),'Increment hours')]")
	WebElement Incrementhours;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//button//span[contains(text(),'Decrement hours')]")
	WebElement Decrementhours;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//button//span[contains(text(),'Increment minutes')]")
	WebElement Incrementminutes;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//button//span[contains(text(),'Decrement minutes')]")
	WebElement Decrementminutes;
	@FindBy(xpath="//mat-row[1]//mat-cell[4]//span[1]//b[1]")
	WebElement viewClaimreason;
	@FindBy(xpath="//span[normalize-space()='Cancel']")
	WebElement cancelreasonbutton;
	@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
	WebElement clearFilter;


	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("Attendance");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	public void getAttendenceLink() {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attendenceLink))).click();
	}
	public void getAddNewClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addNewClaim))).click();
	}
	public void getselectPunchInDate() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectPunchInDate))).click();
	}
	public void getselectPunchOutDate() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectPunchOutDate))).click();
	}
	public void getselectPunchIn_OutTime() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectPunchOutTime))).click();
		//as.moveToElement(selectPunchOutTime).perform();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(PunchOutHH))).clear();
		Thread.sleep(3000);
		PunchOutHH.sendKeys("9");
		Thread.sleep(3000);
		PunchOutMM.clear();
		Thread.sleep(3000);
		PunchOutMM.sendKeys("20");
		Thread.sleep(3000);
		String verifyPM=AM_PM.getText();
		if(verifyPM.contentEquals("PM")) {
			System.out.println("Time in PM");
		}else {
			PunchOutPM.click();
		}

		PunchOutDoneButton.click();


		Thread.sleep(3000);
		selectPunchInTime.click();

		/*			String hour="06";
		System.out.println("HH :-"+ PunchInHH.getAttribute("value"));

		while(true) {
		boolean IsPunchInHH = PunchInHH.getAttribute("value")==hour && PunchInHH.getAttribute("value")!="" ?true :false;
		if(!IsPunchInHH)
				{
			Thread.sleep(3000);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Incrementhours))).click();

		}
		else {
			break;
		}    	
		}*/


		PunchInHH.clear();
		// String PunchInhh = sheet.getRow(3).getCell(1).toString();
		PunchInHH.sendKeys("10");

		PunchInMM.clear();
		//  String PunchInmm = sheet.getRow(3).getCell(2).toString();
		PunchInMM.sendKeys("30");
		String verifyAM=AM_PM.getText();
		if(verifyAM.contentEquals("AM")) {
			System.out.println("Time in AM");
		}else {
			punchInPM.click();
		}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(PunchInDoneButton))).click();
		Thread.sleep(3000);

	}
	public void getReasonForClaim() {
		String reasonClaim = sheet.getRow(3).getCell(0).toString();
		reasonForClaim.sendKeys(reasonClaim);
	}
	public void getDoneClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(DoneClaim))).click();
	}


	//Filter
	public void getfilterclaimbutton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(filterclaimbutton))).click();
	}
	public void getclaimByStatus() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(claimByStatus))).click();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getfromdatefilterclaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fromdatefilterclaim))).click();
	}
	public void gettodatefilterclaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(todatefilterclaim))).click();
	}

	public void getapplyClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(applyClaim))).click();
	}

	public void viewClaimReason() {
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewClaimreason))).click();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelreasonbutton))).click();
		}
		catch(Exception e) {
			System.out.println("There is no claim");
		}
	}

	public void clearFilter() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(clearFilter))).click();
	}


	//Team claims
	@FindBy(xpath="//div[contains(text(),'Team Claims')]")
	WebElement teamClaims;
	@FindBy(xpath="//kt-portlet-header[@ktsticky='[object Object]']//div//div//mat-form-field//div//div//div//input[@name='searchBar']")
	WebElement searchuser;
	@FindBy(xpath="(//input[@type='image'])[1]")
	WebElement acceptClaim;
	@FindBy(xpath="//mat-select[@placeholder='Member']//div[@class='mat-select-arrow-wrapper']")
	WebElement teamClaimBymember;
	@FindBy(xpath="//input[@aria-label='dropdown search']")
	WebElement searchMember;
	@FindBy(xpath="//mat-row[1]//mat-cell[1]//div[1]//div[1]")
	WebElement viewClaimDetails;
	@FindBy(xpath="//div[@class='kt-portlet__head kt-portlet__head__custom']//i[@class='fa fa-times pull-right']")
	WebElement cancelClaimDetails;
	@FindBy(xpath="(//input[@type='image'])[2]")
	WebElement deleteClaim;
	@FindBy(xpath="//textarea[@placeholder='Reason For Rejection']")
	WebElement reasonForClaimRejection;
	@FindBy(xpath="//span[normalize-space()='Submit']")
	WebElement submit;



	public void teamClaims() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(teamClaims))).click();
	}
	public void searchuser() {
		String search = sheet.getRow(7).getCell(0).toString();
		searchuser.sendKeys(search);
	}
	public void acceptClaim() {
		try {
			wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(acceptClaim)).click();
		}
		catch(Exception e) {
			System.out.println("Claim is not available");
		}
	}
	public void filterTeamClaim() {
		filterclaimbutton.click();
	}
	public void getTeamclaimByMember() {
		teamClaimBymember.click();
		searchMember.sendKeys("Lina");
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getTeamclaimByStatus() {
		claimByStatus.click();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getfromdatefilterTeamclaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fromdatefilterclaim))).click();
		
	}
	public void gettodatefilterTeamclaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(todatefilterclaim))).click();
	
	}

	public void getapplyfilterTeamClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(applyClaim))).click();
	}

	public void viewClaimDetails() {
		try {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(viewClaimDetails)).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelClaimDetails))).click();
	}
		catch(Exception e) {
			System.out.println("Claim is not available");
		}
	}

	public void deleteClaim() throws InterruptedException {
	
		try {
			wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(deleteClaim)).click();
			String claimRejectionreason = sheet.getRow(7).getCell(1).toString();
			reasonForClaimRejection.sendKeys(claimRejectionreason);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(submit))).click();
		}
			catch(Exception e) {
				System.out.println("Claim is not available");
				Thread.sleep(2000);
			}
	}

	//Global map
	@FindBy(xpath="//body/kt-base/div/div/div[@id='kt_wrapper']/div[@id='kt_content']/div/ng-component/kt-attendance-list/div/div/kt-portlet/div/div/div/div/img[4]")
	WebElement globalMap;
	@FindBy(xpath="//button[@aria-label='Open calendar']//span//*[name()='svg']")
	WebElement dateFilter_globalMap;
	@FindBy(xpath="//button[@title='Toggle fullscreen view']")
	WebElement fullscreenView;
	@FindBy(xpath="//button[@title='Zoom out']")
	WebElement zoomOut;
	@FindBy(xpath="//button[@title='Zoom in']")
	WebElement zoomIn;
	@FindBy(xpath="//button[@title='Show satellite imagery']")
	WebElement satellite;
	@FindBy(xpath="//body/kt-base/div/div/div[@id='kt_wrapper']/div[@id='kt_content']/div/ng-component/kt-attendance-list/div/div/kt-portlet/div/div/div/div/img[3]")
	WebElement viewReport;
	@FindBy(xpath="//body//div//div[@dir='ltr']//div//div//div//div//div[1]//mat-form-field[1]//div[1]//div[1]//div[2]//mat-datepicker-toggle[1]//button[1]//span[1]//*[name()='svg']")
	WebElement fromdate_attendanceReport;
	@FindBy(xpath="//div[@dir='ltr']//div[2]//mat-form-field[1]//div[1]//div[1]//div[2]//mat-datepicker-toggle[1]//button[1]//span[1]//*[name()='svg']")
	WebElement todate_attendanceReport;
	@FindBy(xpath="//mat-select[@placeholder='Select User']//div[2]")
	WebElement selectUser_attendanceReport;
	@FindBy(xpath="//mat-select[@placeholder='Select Manager']//div[2]")
	WebElement selectManager_attendanceReport;
	@FindBy(xpath="//span[normalize-space()='Download']")
	WebElement download_attendanceReport;
	@FindBy(xpath="//body/kt-base/div/div/div[@id='kt_wrapper']/div[@id='kt_content']/div/ng-component/kt-attendance-list/div/div/kt-portlet/div/div/div/div/img[2]")
	WebElement userNotPunchInReport;
	@FindBy(xpath="//div[@class='kt-pull-right']//img[@class='kt-pointer']")
	WebElement claimsIcon;
	@FindBy(xpath="//div[@class='cdk-overlay-pane cdk-overlay-pane-select-search']")
	WebElement dropdownList;
	@FindBy(xpath="//input[@aria-label='dropdown search']")
	WebElement dropdownSearch;

	public void globalMap() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(globalMap))).click();
	}

	public void dateFilter_globalMap() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dateFilter_globalMap))).click();
	}

	public void fullscreenView() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fullscreenView))).click();
	}
	public void zoomOut_zoomIn() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(zoomOut))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(zoomIn))).click();
	}

	public void satellite() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(satellite))).click();
		Thread.sleep(3000);
	}
	public void viewReport() {
	  
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewReport))).click();
		
	}
	public void selectFromDate_attendanceReport() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fromdate_attendanceReport))).click();
	}
	public void selectToDate_attendanceReport() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(todate_attendanceReport))).click();
	}
	public void selectUser_Manager_attendanceReport() {
		as.click(selectUser_attendanceReport).perform();
		try {
	        if(dropdownList.isDisplayed())
	        {
	        	String searchUser = sheet.getRow(10).getCell(1).toString();
				dropdownSearch.sendKeys(searchUser);
	            System.out.println("User dropdown is appearing");
	            as.sendKeys(Keys.TAB).build().perform(); 
	        }}
			catch (Exception e) {
				System.out.println("User dropdown is not appearing");
			}
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectManager_attendanceReport))).click();
		try {
			   if(dropdownList.isDisplayed()) {
				String searchManager = sheet.getRow(10).getCell(0).toString();
				dropdownSearch.sendKeys(searchManager);
	            System.out.println("Manager dropdown is appearing");
	            as.sendKeys(Keys.TAB).build().perform(); 
	        }
			   }
			catch (Exception e) {
				System.out.println("Manager dropdown is not appearing");
			}
		
	}
	public void downoad_attendanceReport() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(download_attendanceReport)).click();

	}
	public void userNotPunchInReport() throws InterruptedException {
		Thread.sleep(4000);
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(userNotPunchInReport)).click();

	}
	public void claimsIcon() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(claimsIcon)).click();

	}

	//User settings
	@FindBy(xpath="//input[@name='searchBar']")
	WebElement searchUser;
	//@FindBy(xpath="//div[@class='kt-pointer font highlight']//img[1]")
	@FindBy(xpath="//kt-portlet-body/div/div/div[1]/div[1]/div[1]/img[1][@mattooltip='User Settings'][1]")
	WebElement userSettings;
	@FindBy(xpath="//input[@placeholder='Designation']")
	WebElement designation;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[1]")
	WebElement reminderPunchintoggle;
	@FindBy(xpath="//div[@class='kt-pointer punchTime dropdown-toggle']")
	WebElement reminderPunchintime;
	@FindBy(xpath="//input[@placeholder='HH']")
	WebElement hhPunchIn;
	@FindBy(xpath="//input[@placeholder='MM']")
	WebElement mmPunchIn;
	@FindBy(xpath="//button[normalize-space()='AM']")
	WebElement amPunchIn;
	@FindBy(xpath="//button[normalize-space()='PM']")
	WebElement pmPunchIn;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//span[contains(text(),'Done')]")
	//@FindBy(xpath="//kt-time-picker[@x-placement='top-left']//span[contains(text(),'Done')]")
	WebElement donetime;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[2]")
	WebElement punchInWithGeoFencing;
	@FindBy(xpath="//span[normalize-space()='Add More']")
	WebElement addMorefenceArea;
	@FindBy(xpath="//div[@class='kt-portlet__head-label float-right']//button[@type='button']")
	WebElement addMorefenceArea2;
	@FindBy(xpath="//button[@type='button']//span[@class='mat-button-wrapper'][normalize-space()='Add More']")
	WebElement addMorefenceAreaOut;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//kt-add-new-user-geofence-dialog//div//div//div//div//div//div//div//mat-form-field//div//div//div//input[@name='searchBar']")
	WebElement searchfenceArea;
	@FindBy(xpath="//div[@class='mat-checkbox-inner-container']")
	WebElement checkbox;
	@FindBy(xpath="//kt-add-new-user-geofence-dialog[@class='ng-star-inserted']//span[@class='mat-button-wrapper'][normalize-space()='Done']")
	WebElement doneArea;
	@FindBy(xpath="//body/div/div[@dir='ltr']/div/mat-dialog-container[@aria-modal='true']/kt-add-user-geofence-dialog/div/div/div/div/div/div/div[1]/div[1]")
	WebElement selectArea;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//span[contains(text(),'Done')]")
	WebElement done;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[3]")
	WebElement punchOutWithGeoFencing;
	@FindBy(xpath="//mat-select[@formcontrolname='MonthlyLeavesAllowed']//div[@aria-hidden='true']//div//div")
	WebElement MonthlyLeavesAllowed;
	@FindBy(xpath="//label[@for='mat-slide-toggle-4-input']//div[@class='mat-slide-toggle-thumb']")
	WebElement notifyAdmin;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//kt-assign-admin-dialog//div//div//div//div//div//div//div//mat-form-field//div//div//div//input[@name='searchBar']")
	WebElement searchAdmin;
	@FindBy(xpath="//kt-assign-admin-dialog//span[contains(text(),'Done')]")
	WebElement doneNewadmin;
	//@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//span[contains(text(),'Done')]")
	@FindBy(xpath="//button[@class='mat-raised-button mat-button-base mat-primary']//span[@class='mat-button-wrapper'][normalize-space()='Done']")
	WebElement doneAssignAdmin;
	@FindBy(xpath="//span[normalize-space()='Configure']")
	WebElement notifyManager;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[7]")
	WebElement emailPunchIn;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[8]")
	WebElement mobilePunchIn;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[9]")
	WebElement emailPunchOut;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[10]")
	WebElement mobilePunchout;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[5]")
	WebElement makeAdmintoggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[6]")
	WebElement entireDayTrackingtoggle;
	@FindBy(xpath="//mat-select[@placeholder='Weekly Off']//div[@aria-hidden='true']//div//div")
	WebElement weeklyOff;
	@FindBy(xpath="//mat-select[@placeholder='Alternate Day Off In Month']//div[@class='mat-select-arrow']")
	WebElement alternateDayOffInMonth;
	@FindBy(xpath="//mat-select[@placeholder='Alternate Day Off Count']//div[@class='mat-select-arrow']")
	WebElement alternateDayOffCount;
	@FindBy(xpath="//button[@class='float-right mat-raised-button mat-button-base mat-primary']//span[@class='mat-button-wrapper'][normalize-space()='Done']")
	WebElement doneUserSettings;


	public void searchUser() throws InterruptedException {
		String searchuser = sheet.getRow(10).getCell(0).toString();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(searchUser)).sendKeys(searchuser);
	}
	public void userSettings() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(userSettings)).click();
	}
	public void designation() {
		String design = sheet.getRow(14).getCell(1).toString();
		designation.sendKeys(design);
	}
	public void reminderPunchintoggle() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(reminderPunchintoggle)).click();
	}
	public void setReminderPunchintime() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(reminderPunchintime)).click();
		hhPunchIn.sendKeys("9");
		mmPunchIn.sendKeys("30");
		String verifyAM=AM_PM.getText();
		if(verifyAM.contentEquals("AM")) {
			System.out.println("Time in AM");
		}else {
			pmPunchIn.click();
		}
		//PunchInDoneButton.click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(PunchInDoneButton)).click();
		//wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(donetime)).click();

	}
	public void punchInWithGeoFencing() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(punchInWithGeoFencing))).click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(addMorefenceArea)).click();
		Thread.sleep(3000);
		try {
	       if(checkbox.isDisplayed())
	        {
	        	//String searchfence = sheet.getRow(14).getCell(2).toString();
	    		//searchfenceArea.sendKeys(searchfence);
	            System.out.println("Dropdown is appearing");
	            as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
	        }}
			catch (Exception e) {
				System.out.println("Dropdown is not appearing");
			}
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(doneArea)).click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(selectArea)).click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(done)).click();
	}
	public void punchOutWithGeoFencing() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(punchOutWithGeoFencing))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addMorefenceArea2))).click();
		//as.click(addMorefenceArea2).perform();
		Thread.sleep(3000);
		try {			
	        if(checkbox.isDisplayed())
	        {
	        	//String searchfence = sheet.getRow(14).getCell(2).toString();
	    		//searchfenceArea.sendKeys(searchfence);
	            System.out.println("Dropdown is appearing");
	            as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
	            
	        }}
			catch (Exception e) {
				System.out.println("Dropdown is not appearing");
			}
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(doneArea)).click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(selectArea)).click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(done)).click();
	}
	public void MonthlyLeavesAllowed() {
		as.click(MonthlyLeavesAllowed).perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	}
	public void notifyAdmin() {
		notifyAdmin.click();
		addMorefenceAreaOut.click();
		try {			
	        if(checkbox.isDisplayed())
	        {
	        	//String searchad = sheet.getRow(14).getCell(3).toString();
	    	//	searchAdmin.sendKeys(searchad);
	            System.out.println("Dropdown is appearing");
	            as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
	            
	        }}
			catch (Exception e) {
				System.out.println("Dropdown is not appearing");
			}
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(doneNewadmin)).click();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(doneAssignAdmin)).click();
	
	}
	public void notifyManager() {
	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(notifyManager)).click();
	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(emailPunchIn)).click();
	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(mobilePunchIn)).click();
	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(emailPunchOut)).click();
	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(mobilePunchout)).click();
	wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(done)).click();
	
	}
	public void makeAdmintoggle() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(makeAdmintoggle)).click();

	}
	public void entireDayTrackingtoggle() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(entireDayTrackingtoggle)).click();
		
	}
	public void weeklyOff() {
		as.click(weeklyOff).perform();
		//as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void alternateDayOffInMonth() {
		as.click(alternateDayOffInMonth).perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	}
	public void alternateDayOffCount() {
		as.click(alternateDayOffCount).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();

	}
	public void doneUserSettings() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(doneUserSettings)).click();
	}

	//User report
	@FindBy(xpath="//kt-portlet-body/div/div/div[1]/div[1]/div[1]/img[3]")
	WebElement userReport;
	@FindBy(xpath="//button[@aria-label='Open calendar']//span//*[name()='svg']")
	WebElement date_userReport;
	@FindBy(xpath="//input[@placeholder='Enter Your Email ID']")
	WebElement emailId_userReport;
	@FindBy(xpath="//span[contains(text(),'Send')]")
	WebElement sendButton;

	public void userReport() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(userReport)).click();
	}
	public void date_userReport() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(date_userReport)).click();
	}
	public void emailId_userReport() {
		String email = sheet.getRow(18).getCell(0).toString();
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(emailId_userReport)).sendKeys(email);
	}
	public void sendButton() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(sendButton)).click();
	}

	//User Location
	@FindBy(xpath="//div[@class='kt-pointer font highlight']//img[2]")
	WebElement userLocation;
	@FindBy(xpath="//img[@src='https://maps.gstatic.com/mapfiles/transparent.png']")
	WebElement userLocationPin;

	public void userLocation() throws InterruptedException {
		Thread.sleep(5000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userLocation)));
		userLocation.click();
	}

	public void searchUser_userLocation() throws InterruptedException {
		String searchuser = sheet.getRow(7).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchUser))).sendKeys(searchuser);
	}
	public void userLocationPin_userLocation() throws InterruptedException {
		try {
			wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(userLocationPin)).click();
		}
		catch(Exception e) {
			System.out.println("user location pin is not displayed");
		}
	}

	//==============================================================
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void getVerifyMsg() {
		try {
			if (toastMessage.isDisplayed()) {
				System.out.println("msg is appearing");
				String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vmsg);
			}
		} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 
	}

	public void getDateFilter() throws InterruptedException {
		//  Thread.sleep(2000);

		boolean IsDateClicked=false;
		int Year = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			// System.out.println(fDateRangepartss[0]);
			// System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(Year))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 5;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 16;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}
}
