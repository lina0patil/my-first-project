package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VisitForm {
	public WebDriver driver;
	WebDriverWait wait;
	Actions as;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
	//create constructor
	public VisitForm(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[normalize-space()='Visit Form']")
	WebElement VisitForm;
	@FindBy(xpath="//span[normalize-space()='Add Form']")
	WebElement AddFormButton;
	@FindBy(xpath="//input[@placeholder='Form Title*']")
	WebElement FormTitle;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
	WebElement startDate;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
	WebElement endDate;
	@FindBy(xpath="//tbody/tr/td/form/div/div/div/div/mat-slide-toggle/label[@for='mat-slide-toggle-1-input']/div/div/div[1]")
	WebElement IsOneTimeToggleButton;
	@FindBy(xpath="//tbody/tr/td/form/div/div/div/div/mat-slide-toggle/label[@for='mat-slide-toggle-2-input']/div/div/div[1]")
	WebElement IsGeoTaggingToggleButton;
	@FindBy(xpath="//span[normalize-space()='Next']")
	WebElement nextButton;
	@FindBy(xpath="//span[normalize-space()='+ Add More Section']")
	WebElement AddMoreSection;
	@FindBy(xpath="//input[@placeholder='Enter Section Title*']")
	WebElement EnterSectionTitle;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement donebutton;
	@FindBy(xpath="//tbody//tr//img[1]")
	WebElement editPointer;
	@FindBy(xpath="//button[normalize-space()='Text']")
	WebElement component;
	@FindBy(xpath="//input[@placeholder='Enter Label']")
	WebElement enterLabel;
	@FindBy(xpath="//input[@placeholder='Enter Placeholder']")
	WebElement enterPlaceholder;
	@FindBy(xpath="//tbody/tr/td/div/div/form[@autocomplete='off']/div[@formarrayname='Options']/div/div/mat-slide-toggle[@color='primary']/label[@for='mat-slide-toggle-3-input']/div/div/div[1]")
	WebElement mandatoryToggleButon;
	@FindBy(xpath="//span[normalize-space()='Publish Form']")
	WebElement publishForm;


	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[8]/div/div/kt-tag/div/mat-form-field/div/div[1]/div/input")
	WebElement nextYear;

	//search and send report on mail
	@FindBy(xpath="//input[@name='searchBar']")
	WebElement searchByTitle;
	@FindBy(xpath="//span[normalize-space()='Visit Stats']")
	WebElement VisitStats;
	@FindBy(xpath="//input[@placeholder='Email Report']")
	WebElement EmailReport;
	@FindBy(xpath="//span[normalize-space()='Send']")
	WebElement SendReportButton;

	//Filter and export
	@FindBy(xpath="//kt-portlet-body/div[@class='row']/div[1]/div[1]/div[1]")
	WebElement titleName;
	@FindBy(xpath="//span[normalize-space()='Filter']")
	WebElement Filter;
	@FindBy(xpath="//span[normalize-space()='Apply']")
	WebElement applyFilterbutton;
	@FindBy(xpath="//span[normalize-space()='Export']")
	WebElement exportButton;

	@FindBy(id="toast-container")
	WebElement toastMessage;
	public void getVerifyMsg() {
		try {
			if (toastMessage.isDisplayed()) {
				System.out.println("msg is appearing");
				String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vmsg);
			}
		} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 	
	}

	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("Visit Form");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}

	public void getVisitForm() {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.elementToBeClickable(VisitForm)).click();	
	}	
	public void getAddForm() {
		wait.until(ExpectedConditions.elementToBeClickable(AddFormButton)).click();	
	}
	public void getFormDetails() {
		String Formtitle = sheet.getRow(4).getCell(0).toString();
		wait.until(ExpectedConditions.elementToBeClickable(FormTitle)).sendKeys(Formtitle);
	}
	public void getStartDate() {
		wait.until(ExpectedConditions.elementToBeClickable(startDate)).click();	
	}
	public void getEndDate() {
		wait.until(ExpectedConditions.elementToBeClickable(endDate)).click();		
	}
	public void getToggleButton() {
		wait.until(ExpectedConditions.elementToBeClickable(IsOneTimeToggleButton)).click();	
		wait.until(ExpectedConditions.elementToBeClickable(IsGeoTaggingToggleButton)).click();	
		wait.until(ExpectedConditions.elementToBeClickable(nextButton)).click();	
	}
	public void getAddMoreSection() {
		wait.until(ExpectedConditions.elementToBeClickable(AddMoreSection)).click();	
	}
	public void getSection() {
		String SectionTitle = sheet.getRow(4).getCell(1).toString();
		EnterSectionTitle.sendKeys(SectionTitle);
		wait.until(ExpectedConditions.elementToBeClickable(donebutton)).click();	
	}
	public void getEditForComponents() {
		wait.until(ExpectedConditions.elementToBeClickable(editPointer)).click();		
	}
	public void getSelectComponent() {
		wait.until(ExpectedConditions.elementToBeClickable(component)).click();	
	}
	public void getEnterFields() {
		String label = sheet.getRow(4).getCell(2).toString();
		enterLabel.sendKeys(label);	
		String placeholder = sheet.getRow(4).getCell(3).toString();
		enterPlaceholder.sendKeys(placeholder);
		wait.until(ExpectedConditions.elementToBeClickable(mandatoryToggleButon)).click();	
	}
	public void getPublishForm() {
		wait.until(ExpectedConditions.elementToBeClickable(publishForm)).click();	
	}

	//search and send report on mail
	public void getSearchByTitle() {
		searchByTitle.sendKeys("Visit");
	}
	public void getVisitStats() {
		wait.until(ExpectedConditions.elementToBeClickable(VisitStats)).click();
	}
	public void getSendReportButton() throws InterruptedException {
		String emailid = sheet.getRow(8).getCell(0).toString();
		EmailReport.sendKeys(emailid);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(SendReportButton)).click();
	}

	//Filter and export
	public void titleName() {
		wait.until(ExpectedConditions.elementToBeClickable(titleName)).click();
	}
	public void Filter() {
		wait.until(ExpectedConditions.elementToBeClickable(Filter)).click();
	}
	public void applyFilter() {
		wait.until(ExpectedConditions.elementToBeClickable(applyFilterbutton)).click();
	}
	public void applyExportButton() {
		try {
		wait.until(ExpectedConditions.elementToBeClickable(exportButton)).click();}
		catch(Exception e) {
			System.out.println("Export button is not available");
		}
	}
	@FindBy(xpath="//kt-portlet-body/div[@class='row']/div[1]/div[1]/div[2]/div[2]/img[1]")
	WebElement deletevisit;
	@FindBy(xpath="//span[normalize-space()='Delete']")
	WebElement deleteButton;
	
	public void deletetask() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deletevisit))).click();	
		Thread.sleep(3000);
		deleteButton.click();
	}	

	public void getDateFilter() throws InterruptedException {
		boolean IsToDateClicked=false;
		int toYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsToDateClicked) {
				break;
			}
			WebElement toCalelement = chooseYear;
			String todtr = toCalelement.getText();
			//   System.out.println(todtr);
			String[] toDateRangepartss = todtr.split(" – ");
			//  System.out.println(toDateRangepartss[0]);
			//  System.out.println(toDateRangepartss[1]);
			outerloop:       
				if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
					//Store each year in string array
					List < WebElement > tocols;
					//Get value from Table each cell
					WebElement toTbodyYear = tBody;
					// count rows with size() method
					List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
					for (WebElement row: trws) {
						List < WebElement > tCells = row.findElements(By.tagName("td"));
						for (WebElement tCell: tCells) {
							if (tCell.getText().contains(Integer.toString(toYear))) {
								tCell.click();
								Thread.sleep(2000);
								int toMonth = 12;
								String tMonth = ReturnMonthSelected(toMonth);
								WebElement toTbodyMonth = tBody;
								// count rows with size() method
								List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
								for (WebElement tmonthrow: tmonthsrows) {
									List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
									for (WebElement tMonthcell: tMonthCells) {
										if (tMonthcell.getText().contains(tMonth)) {
											tMonthcell.click();
											Thread.sleep(2000);
											int toDay = 10;
											WebElement toTbodyDate = tBody;
											// count rows with size() method
											List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
											for(WebElement tmrow: tmrows){
												List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
												for(WebElement tmCell:tmCells){
													if (tmCell.getText().contains(Integer.toString(toDay)))
													{
														tmCell.click();
														IsToDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public void getDateFilter1() throws InterruptedException {
		boolean IsToDateClicked=false;
		int toYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsToDateClicked) {
				break;
			}
			WebElement toCalelement = chooseYear;
			String todtr = toCalelement.getText();
			//   System.out.println(todtr);
			String[] toDateRangepartss = todtr.split(" – ");
			//  System.out.println(toDateRangepartss[0]);
			//  System.out.println(toDateRangepartss[1]);
			outerloop:       
				if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
					//Store each year in string array
					List < WebElement > tocols;
					//Get value from Table each cell
					WebElement toTbodyYear = tBody;
					// count rows with size() method
					List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
					for (WebElement row: trws) {
						List < WebElement > tCells = row.findElements(By.tagName("td"));
						for (WebElement tCell: tCells) {
							if (tCell.getText().contains(Integer.toString(toYear))) {
								tCell.click();
								Thread.sleep(2000);
								int toMonth = 5;
								String tMonth = ReturnMonthSelected(toMonth);
								WebElement toTbodyMonth = tBody;
								// count rows with size() method
								List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
								for (WebElement tmonthrow: tmonthsrows) {
									List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
									for (WebElement tMonthcell: tMonthCells) {
										if (tMonthcell.getText().contains(tMonth)) {
											tMonthcell.click();
											Thread.sleep(2000);
											int toDay = 10;
											WebElement toTbodyDate = tBody;
											// count rows with size() method
											List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
											for(WebElement tmrow: tmrows){
												List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
												for(WebElement tmCell:tmCells){
													if (tmCell.getText().contains(Integer.toString(toDay)))
													{
														tmCell.click();
														IsToDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}

}
