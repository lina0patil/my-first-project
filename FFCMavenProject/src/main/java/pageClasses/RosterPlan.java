package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RosterPlan {
	WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
    int j;
	int i;
	//create constructor
	public RosterPlan(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[normalize-space()='Roster Plan']")
	WebElement rosterPlan;
	@FindBy(xpath="//body//kt-base//div//div//div//div//div//kt-roster-plan//mat-drawer-container//mat-drawer-content//kt-portlet//div//kt-portlet-header[@ktsticky='[object Object]']//div//div//mat-form-field//div//div//div//input[@placeholder='Search']")
	WebElement searchUserR; 
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/kt-roster-plan[1]/mat-drawer-container[1]/mat-drawer-content[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/div[1]/mat-table[1]/mat-row[1]/mat-cell[1]/mat-checkbox[1]/label[1]/div[1]")
	WebElement checkBox1; 
	@FindBy(xpath="//span[normalize-space()='Select Date']")
	WebElement selectDateButton; 
	@FindBy(xpath="//button[@aria-label='Open calendar']//span//*[name()='svg']")
	WebElement dateFilterRoster; 
	@FindBy(xpath="//span[normalize-space()='Select time']")
	WebElement selectTimeRoster; 
	@FindBy(xpath="//input[@placeholder='HH']")
	WebElement timeHH;
	@FindBy(xpath="//input[@placeholder='MM']")
	WebElement timeMM;
	@FindBy(xpath="//button[@class='btn btn-outline-primary']")
	WebElement PM_AM;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement timeDoneButton;
	@FindBy(xpath="//mat-select[@placeholder='Assign To']//div[@aria-hidden='true']//div//span//span")
	WebElement assignToRoster; 
	@FindBy(xpath="//input[@autocomplete='off']")
	WebElement searchAssignUserR; 
	@FindBy(xpath="//i[@aria-hidden='true']")
	WebElement plusRoster; 
	@FindBy(xpath="//div[normalize-space()='Remove']")
	WebElement removeRoster; 
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement addRosterButton; 
	@FindBy(xpath="//span[normalize-space()='Cancel']")
	WebElement cancelButton; 
	
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']")
	WebElement dropdownList;
	
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[8]/div/div/kt-tag/div/mat-form-field/div/div[1]/div/input")
	WebElement nextYear;
	
	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("Roster Plan");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	
	public void rosterPlan() {
		as=new Actions(driver);
	    wait = new WebDriverWait(driver, timeout);
		rosterPlan.click();	
	}
	public void searchUserR() {
		String searchUser = sheet.getRow(3).getCell(0).toString();
		searchUserR.sendKeys(searchUser);	
	}
	public void checkBox1() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(checkBox1))).click();
	}
	
	public void selectDateButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectDateButton))).click();
			
	}
	public void dateFilterRoster() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dateFilterRoster))).click();
		
	}
	public void selectTimeRoster() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectTimeRoster))).click();
		timeHH.clear();
		timeHH.sendKeys("12");
		timeMM.clear();
		timeMM.sendKeys("10");
		PM_AM.click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(timeDoneButton))).click();
	}
	public void assignToRoster() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignToRoster))).click();
		Thread.sleep(3000);	
		try {

	        if(dropdownList.isDisplayed())
	        {	
	        	String searchUser = sheet.getRow(3).getCell(0).toString();
	        	searchAssignUserR.sendKeys(searchUser);
	        	Thread.sleep(3000);	
	        	as.sendKeys(Keys.ENTER).build().perform();
	        	 System.out.println("Assign user is selected");
	    		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(plusRoster))).click();
	           
	        }}
			catch (Exception e) {
				System.out.println("Assign user is not selected");
			}
	}
	public void addRosterButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addRosterButton))).click();	
	}
	
	//Filter and view
	@FindBy(xpath="//kt-filter[@filterformat='mat-table']//span[contains(text(),'Filter')]")
	WebElement filterButtonR; 
	@FindBy(xpath="//mat-select[@aria-label='Country']//div[@class='mat-select-arrow-wrapper']")
	WebElement byCountry;
	@FindBy(xpath="//span[normalize-space()='India']")
	WebElement india;
	@FindBy(xpath="//mat-select[@aria-label='State']//div[@class='mat-select-arrow-wrapper']")
	WebElement byState;
	@FindBy(xpath="//span[contains(text(),'Madhya Pradesh ')]")
	WebElement MadhyaPradesh;
	@FindBy(xpath="//mat-select[@placeholder='City']//div[@class='mat-select-arrow']")
	WebElement byCity;
	@FindBy(xpath="//span[contains(text(),'Gogaon ')]")
	WebElement gogaon;
	@FindBy(xpath="//div[@class='mat-form-field-infix']//input[@type='text']")
	WebElement searchCity;
	@FindBy(xpath="//mat-select[@aria-label='Locality']//div[@class='mat-select-arrow']")
	WebElement localityF;
	@FindBy(xpath="//mat-drawer-content//mat-select[@placeholder='Status']//div[2]")
	WebElement byStatus;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Member')]")
	WebElement byMember;
	@FindBy(xpath="//input[@aria-label='dropdown search']")
	WebElement searchMember;
	@FindBy(xpath="//kt-filter[@filterformat='mat-table']//div//div//div//div//span[contains(text(),'Apply')]")
	WebElement applyFilterButtonR;
	@FindBy(xpath="(//mat-cell[@role='gridcell'][normalize-space()='View'])[1]")
	WebElement viewRosterDetails; 
	@FindBy(xpath="//span[normalize-space()='Close']")
	WebElement closeRosterDetails; 
	@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
	WebElement clearFilter_Roster; 
	
	public void filterButton_RosterPlan() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(filterButtonR))).click();
	}
	public void filterByLocation_RosterPlan() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(byCountry))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(india))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(byState))).click();
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", MadhyaPradesh);
		MadhyaPradesh.click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(byCity))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(gogaon))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(localityF))).click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
			

	}
	public void filterByStatus_RosterPlan() {
		as.click(byStatus).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	
	public void filterByMember_RosterPlan() {
		as.click(byMember).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void applyfilterButton_RosterPlan() {
		applyFilterButtonR.click();
	}

	public void viewRosterDetails() throws InterruptedException {
		viewRosterDetails.click();
		Thread.sleep(5000);
	}
	public void closeRosterDetails() {
	closeRosterDetails.click();
	}
	public void clearFilter_Roster() throws InterruptedException {
		Thread.sleep(2000);
		clearFilter_Roster.click();
		}
	public void getVerifyRosterMsg() {
		try {
			if (toastMessage.isDisplayed()) {
				System.out.println("msg is appearing");
				String vMsg = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vMsg);
			}
			} 
			catch (Exception e) {
				System.out.println("Validation msg is not displayed");
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancelButton))).click();
			} 
	}
	public void getDateFilter() throws InterruptedException {
		boolean IsDateClicked=false;
		int Year = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			// System.out.println(fDateRangepartss[0]);
			// System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(Year))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 6;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 30;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}
}
