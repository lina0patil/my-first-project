package pageClasses;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Customer_Reports {
	public WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	//create constructor
	public Customer_Reports(WebDriver driver) {
	this.driver=driver;
	PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//div[@class='ps__thumb-y']")
	WebElement verticalSlider;
	
	@FindBy(xpath="//span[contains(text(),'Reports')]")
	WebElement reports;
	
	@FindBy(xpath="//span[normalize-space()='Customer']")
	WebElement customer;
	 
    @FindBy(xpath="//span[normalize-space()='Call Logs']")
    WebElement callLogs;
  
    @FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
    WebElement fromDateFilter;
    
    @FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
    WebElement toDateFilter;
    
    @FindBy(xpath="//button[@aria-label='Choose month and year']")
    WebElement chooseMonth_Year;
    
    @FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
    WebElement chooseYear;
    
    @FindBy(css="tbody[role='grid']")
    WebElement tBody;
    
    @FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
    WebElement previousYear;
    
    @FindBy(xpath="//button[@aria-label='Next year']//span[@class='mat-button-wrapper']")
    WebElement nextYear;
    
    @FindBy(xpath="//div[@class='mat-select-arrow-wrapper']//div[@class='mat-select-arrow']")
    WebElement selectUser;
    
    @FindBy(xpath="(//div[@class='mat-select-arrow'])[2]")
    WebElement selectManager;
    
    @FindBy(xpath="//span[normalize-space()='Apply']")
    WebElement applyButton;
    
    @FindBy(xpath="//span[contains(text(),('Export'))]")
    WebElement export;
    
    @FindBy(xpath="//div[normalize-space()='Rijo Standard']//span[@class='ng-star-inserted']")
    WebElement manager;
    
    @FindBy(xpath="//img[@class='action-btn kt-common-image color-change']")
    WebElement refreshIcon;
    
    @FindBy(xpath="//span[normalize-space()='Customer Added']")
    WebElement customerAdded;
    
    @FindBy(xpath="//span[normalize-space()='Visit Report']")
    WebElement visitReport;
    
	public void getVerticalSlider() throws InterruptedException {
	 Thread.sleep(4000);
	//move down the slider by using method
	as.clickAndHold(verticalSlider).moveByOffset(0, 300).release().perform();
	Thread.sleep(4000);
	
    }
	public void getReports() {
		as=new Actions(driver);
	    wait = new WebDriverWait(driver, timeout);
	reports.click();
	}
	public void getCustomer() {
	customer.click();
	}
	public void getCallLogs() {
	callLogs.click();
	}
	public void getSelectUser() throws InterruptedException {
	selectUser.click();
	Thread.sleep(3000);
	as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getSelectManager() throws InterruptedException {
	selectManager.click();
	Thread.sleep(3000);
	as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getApplyButton() {
	applyButton.click();	
	}
	public void getExport() {
	export.click();
	}
	public void getManager() throws InterruptedException {
	JavascriptExecutor js=(JavascriptExecutor)driver;
	js.executeScript("arguments[0].scrollIntoView(true);", manager);
	Thread.sleep(3000);
	manager.click();
	}
	public void getRefreshIcon() throws InterruptedException {
	wait.until(ExpectedConditions.elementToBeClickable (refreshIcon));
	refreshIcon.click();
	}
	public void getCustomerAdded() {
	customerAdded.click();
	}
	public void getVisitReport() {
	visitReport.click();
	}
	public void getFromDateFilter() throws InterruptedException {
		fromDateFilter.click();
		 boolean IsFromDateClicked=false;
		    int fromYear = 2022;
		    chooseMonth_Year.click();
		    
		    while(true) {
		    	if(IsFromDateClicked) {
		    		break;
		    	}
		    	WebElement fromCalelement = chooseYear;
		        String fdtr = fromCalelement.getText();
		       // System.out.println(fdtr);
		        String[] fDateRangepartss = fdtr.split(" � ");
		      //  System.out.println(fDateRangepartss[0]);
		      //  System.out.println(fDateRangepartss[1]);
		        outerloop:       
		    	if(fromYear >= Integer.parseInt(fDateRangepartss[0]) && fromYear <= Integer.parseInt(fDateRangepartss[1]) && !IsFromDateClicked ) {
		                //Store each year in string array
		                List < WebElement > fcols;
		                //Get value from Table each cell
		                WebElement ft = tBody;
		                // count rows with size() method
		                List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
		                for (WebElement row: frws) {
		                    List < WebElement > fCells = row.findElements(By.tagName("td"));
		                    for (WebElement fCell: fCells) {
		                        if (fCell.getText().contains(Integer.toString(fromYear))) {
		                            fCell.click();
		                            Thread.sleep(2000);
		                            int fromMonth = 01;
		                            String fMonth = ReturnMonthSelected(fromMonth);
		                            WebElement tm = tBody;
		                            // count rows with size() method
		                            List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
		                            for (WebElement fmonthrow: fmonthsrows) {
		                                List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
		                                for (WebElement fMonthcell: fMonthCells) {
		                                    if (fMonthcell.getText().contains(fMonth)) {
		                                    	fMonthcell.click();
		                                        Thread.sleep(2000);
		                                        int fromDay = 1;
		                                        WebElement fdatecalender = tBody;
		                                        // count rows with size() method
		                                        List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
		                                        for(WebElement fmrow: fmrows){
		                                            List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
		                                            for(WebElement fmCell:fmCells){
		                                            	if (fmCell.getText().contains(Integer.toString(fromDay)))
		                                            	{
		                                            		fmCell.click();
		                                            		IsFromDateClicked = true;
		                                                 	break outerloop;
		                                            	}
		                                               
		                                            }
		                                        }                                                                                                                  
		                                    }
		                                }
		                            }
		                        }
		                    }
		                }
		          		break;     
		    	}
		    	else {
		    		if((Integer.parseInt(fDateRangepartss[0])-fromYear)>0){
		    			previousYear.click();
		        		continue;
		    		}
		    		else {
		    			nextYear.click();
		        		continue;
		    		}                 
		    	}
		    }
		    
		    Thread.sleep(3000);
		}
	public void getToDateFilter() throws InterruptedException {
		toDateFilter.click();	
		
			 boolean IsToDateClicked=false;
			    int toYear = 2022;
			    chooseMonth_Year.click();
			    
			    while(true) {
			    	if(IsToDateClicked) {
			    		break;
			    	}
			    	WebElement toCalelement = chooseYear;
			        String todtr = toCalelement.getText();
			     //   System.out.println(todtr);
			        String[] toDateRangepartss = todtr.split(" � ");
			      //  System.out.println(toDateRangepartss[0]);
			      //  System.out.println(toDateRangepartss[1]);
			        outerloop:       
			    	if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
			                //Store each year in string array
			                List < WebElement > tocols;
			                //Get value from Table each cell
			                WebElement toTbodyYear = tBody;
			                // count rows with size() method
			                List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
			                for (WebElement row: trws) {
			                    List < WebElement > tCells = row.findElements(By.tagName("td"));
			                    for (WebElement tCell: tCells) {
			                        if (tCell.getText().contains(Integer.toString(toYear))) {
			                            tCell.click();
			                            Thread.sleep(2000);
			                            int toMonth = 03;
			                            String tMonth = ReturnMonthSelected(toMonth);
			                            WebElement toTbodyMonth = tBody;
			                            // count rows with size() method
			                            List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
			                            for (WebElement tmonthrow: tmonthsrows) {
			                                List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
			                                for (WebElement tMonthcell: tMonthCells) {
			                                    if (tMonthcell.getText().contains(tMonth)) {
			                                    	tMonthcell.click();
			                                        Thread.sleep(2000);
			                                        int toDay = 9;
			                                        WebElement toTbodyDate = tBody;
			                                        // count rows with size() method
			                                        List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
			                                        for(WebElement tmrow: tmrows){
			                                            List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
			                                            for(WebElement tmCell:tmCells){
			                                            	if (tmCell.getText().contains(Integer.toString(toDay)))
			                                            	{
			                                            		tmCell.click();
			                                            		IsToDateClicked = true;
			                                                 	break outerloop;
			                                            	}
			                                               
			                                            }
			                                        }                                                                                                                  
			                                    }
			                                }
			                            }
			                        }
			                    }
			                }
			          		break;     
			    	}
			    	else {
			    		if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
			    			previousYear.click();
			        		continue;
			    		}
			    		else {
			    			nextYear.click();
			        		continue;
			    		}                 
			    	}
			    }
			    
			    Thread.sleep(3000);
				}
		  public static String ReturnMonthSelected(int mnth) {
		      switch (mnth) {
		          case 1:
		              return "JAN";
		          case 2:
		              return "FEB";
		          case 3:
		              return "MAR";
		          case 4:
		              return "APR";
		          case 5:
		              return "MAY";
		          case 6:
		              return "JUN";
		          case 7:
		              return "JUL";
		          case 8:
		              return "AUG";
		          case 9:
		              return "SEP";
		          case 10:
		              return "OCT";
		          case 11:
		              return "NOV";
		          case 12:
		              return "DEC";
		          default:
		              return "Invalid input - Wrong month number.";
		      }
		      
		  }
}
