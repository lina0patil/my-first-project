package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Deals_Pipeline {
	public WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
    int j;
	int i;
	public Deals_Pipeline(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		}
	
	@FindBy(xpath="//span[text()='Deals & Pipeline']")
	WebElement Deals_PipelineLink;
	@FindBy(xpath="//span[text()=' Add Opportunities ']")
    WebElement addOpportunities;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[1]/mat-form-field/div/div[1]/div/mat-select/div/div[1]")
    WebElement Lead_CustomerName;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Lead/Customer Name*')]")
    WebElement search;
	@FindBy(xpath="//input[@placeholder='Opportunity Name*']")
    WebElement OpportunityName;
	@FindBy(xpath="//mat-select[@placeholder='Stage*']//div[@class='mat-select-arrow']")
    WebElement stage;
	@FindBy(xpath="//input[@placeholder='Assign Products']")
    WebElement AssignProducts;
	@FindBy(xpath="//mat-checkbox[@class='mat-checkbox mat-accent']")
    WebElement AssignProductscheckbox;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//input[@placeholder='Search']")
	WebElement searchProduct;
	@FindBy(xpath="//span[contains(text(),' Done')]")
    WebElement Done;
	@FindBy(xpath="//div[@class='kt-form__actions kt-form__actions--sm']//span[@class='mat-button-wrapper'][normalize-space()='Cancel']")
    WebElement cancel;
	@FindBy(xpath="//mat-select[@placeholder='Assigned To*']//div[@class='mat-select-arrow-wrapper']")
    WebElement AssignTo;
	@FindBy(xpath="//div[@dir='ltr']//div//div//div//mat-form-field//div//div//div//input[@placeholder='Search']")
    WebElement SearchAssignTo;
	@FindBy(xpath="(//input[@placeholder='Value (in रु)*'])")
    WebElement value;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]")
    WebElement ClosedateFilter;
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[8]/div/div/kt-tag/div/mat-form-field/div/div[1]/div/input")
	WebElement nextYear;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//kt-add-opportunities//div//div//form//div//div//div//div//div//kt-tag[@placeholder='Tag']//div//mat-form-field//div//div//div//input[@placeholder='Tag']")
	WebElement tag;
	@FindBy(xpath="(//textarea[@formcontrolname='notes'])")
	WebElement notes;
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement addoppButton;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']")
	WebElement dropdownList;
	
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;
		
	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("Deals & Pipeline");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	public void getDeals_Pipeline() {
		as=new Actions(driver);
	    wait = new WebDriverWait(driver, timeout);
		Deals_PipelineLink.click();	
	}
	public void getAddOpportunities() throws InterruptedException {
		addOpportunities.click();
	
	}
	public void getOtherField() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Lead_CustomerName))).click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	        	//String searchleadname = sheet.getRow(2).getCell(6).toString();
	        	//searchLeadName.sendKeys(searchleadname);
	            as.sendKeys(Keys.ENTER).build().perform();
	            System.out.println("Lead Name is selected");
	        }}
			catch (Exception e) {
				System.out.println("Lead Name is not selected");
			}

		String OppoName = sheet.getRow(2).getCell(0).toString();
		OpportunityName.sendKeys(OppoName);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(stage))).click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	            as.sendKeys(Keys.ENTER).build().perform();
	            System.out.println("Stage type is selected");
	        }}
			catch (Exception e) {
				System.out.println("Stage type is not selected");
			}
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AssignProducts))).click();
		Thread.sleep(3000);
		try {
	        if(AssignProductscheckbox.isDisplayed())
	        {
	        	//String searchproduct = sheet.getRow(2).getCell(4).toString();
	    		//searchProduct.sendKeys(searchproduct);
	            as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
	    		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Done))).click();
	    		   System.out.println("Assign product is selected");
	        }
	        }
			catch (Exception e) {
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancel))).click();
				System.out.println("Assign product is not selected");
			}

		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AssignTo))).click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	        	//String SearchassignTo = sheet.getRow(2).getCell(5).toString();
	    		//SearchAssignTo.sendKeys(SearchassignTo);
	          
	            as.sendKeys(Keys.ENTER).build().perform();	
	            System.out.println("Assign user is selected");
	        }
	        }
			catch (Exception e) {
				System.out.println("Assign user is not selected");
				
			}
		Thread.sleep(3000);
		String values = sheet.getRow(2).getCell(3).toString();
		value.sendKeys(values);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ClosedateFilter))).click();
	}
		public void getTags_Notes() throws InterruptedException {
	    Thread.sleep(3000);
		String tags = sheet.getRow(2).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tag))).sendKeys(tags);
		tag.sendKeys(Keys.ENTER);
		String note = sheet.getRow(2).getCell(2).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(notes))).sendKeys(note);
		
	
	}
	public void getAddOppoButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addoppButton))).click();
	}
	public void getVerifyMsg() {
	try {
		if (toastMessage.isDisplayed()) {
			System.out.println("msg is appearing");
			String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
			System.out.println("Validation Message: "+vmsg);
		}
		} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 
	}
	
	//Edit opportunity
		@FindBy(xpath="//img[@alt='edit']")
		WebElement editOpportunityIcon;
		@FindBy(xpath="//div[@dir='ltr']//div//div//div//mat-form-field//div//div//div//input[@placeholder='Search']")
		WebElement searchLeadName;
		@FindBy(xpath="//span[contains(text(),' Lina Customer ')] ")
		WebElement LinaCustomer;
		@FindBy(xpath="//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
		WebElement dairyproductsCheckbox;
		@FindBy(xpath="//span[normalize-space()='Update']")
		WebElement updateOppoButton;
		@FindBy(xpath="(//label[@id='mat-form-field-label-37'])[1]")
		WebElement tagsEdit;
	//edit opportunity
	public void getEditOpportunityIcon() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editOpportunityIcon))).click();
	
	}
	public void getEditOpportunity() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Lead_CustomerName))).click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	        	//String searchleadname = sheet.getRow(2).getCell(6).toString();
	        	//searchLeadName.sendKeys(searchleadname);
	       
	            as.sendKeys(Keys.ENTER).build().perform();
	            System.out.println("Lead Name is selected");
	        }}
			catch (Exception e) {
				System.out.println("Lead Name is not selected");
			}
		
		String OppoName = sheet.getRow(6).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(OpportunityName))).clear();
		OpportunityName.sendKeys(OppoName);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(stage))).click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	            
	            as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	            System.out.println("Stage type is selected");
	        }}
			catch (Exception e) {
				System.out.println("Stage type is not selected");
			}
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AssignProducts))).click();
		Thread.sleep(3000);
		try {
	        if(AssignProductscheckbox.isDisplayed())
	        {
	        	//String searchproduct = sheet.getRow(2).getCell(4).toString();
	    		//searchProduct.sendKeys(searchproduct);
	        
	            as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
	    		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Done))).click();
	    	    System.out.println("Assign product is selected");
	    		
	        }
	        }
			catch (Exception e) {
				
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cancel))).click();
				System.out.println("Assign product is not selected");
			}

		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AssignTo))).click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	        	//String SearchassignTo = sheet.getRow(2).getCell(5).toString();
	    		//SearchAssignTo.sendKeys(SearchassignTo);
	           
	            as.sendKeys(Keys.ENTER).build().perform();	
	            System.out.println("Assign user is selected");
	        }
	        }
			catch (Exception e) {
				System.out.println("Assign user is not selected");
				
			}
		Thread.sleep(3000);
		String values = sheet.getRow(6).getCell(3).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(value))).clear();
		value.sendKeys(values);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ClosedateFilter))).click();
	}
		public void getEditTags_Notes() throws InterruptedException {
	    Thread.sleep(3000);
	    String tags = sheet.getRow(6).getCell(1).toString();
	    wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tagsEdit))).sendKeys(tags);
		String searchproduct = sheet.getRow(5).getCell(0).toString();
		tagsEdit.sendKeys(Keys.ENTER);

		String note = sheet.getRow(6).getCell(2).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(notes))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(notes))).sendKeys(note);
	
			
		
	}
	public void getUpdateOppoButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(updateOppoButton))).click();
	}
	
	public void getDateFilter() throws InterruptedException {
		//  Thread.sleep(2000);
		
		 boolean IsDateClicked=false;
		    int Year = 2022;
		    chooseMonth_Year.click();
		    
		    while(true) {
		    	if(IsDateClicked) {
		    		break;
		    	}
		    	WebElement fromCalelement = chooseYear;
		        String fdtr = fromCalelement.getText();
		      // System.out.println(fdtr);
		        String[] fDateRangepartss = fdtr.split(" – ");
		      // System.out.println(fDateRangepartss[0]);
		      // System.out.println(fDateRangepartss[1]);
		        outerloop:       
		    	if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
		                //Store each year in string array
		                List < WebElement > fcols;
		                //Get value from Table each cell
		                WebElement ft = tBody;
		                // count rows with size() method
		                List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
		                for (WebElement row: frws) {
		                    List < WebElement > fCells = row.findElements(By.tagName("td"));
		                    for (WebElement fCell: fCells) {
		                        if (fCell.getText().contains(Integer.toString(Year))) {
		                            fCell.click();
		                            Thread.sleep(2000);
		                            int fromMonth = 6;
		                            String fMonth = ReturnMonthSelected(fromMonth);
		                            WebElement tm = tBody;
		                            // count rows with size() method
		                            List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
		                            for (WebElement fmonthrow: fmonthsrows) {
		                                List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
		                                for (WebElement fMonthcell: fMonthCells) {
		                                    if (fMonthcell.getText().contains(fMonth)) {
		                                    	fMonthcell.click();
		                                        Thread.sleep(2000);
		                                        int fromDay = 19;
		                                        WebElement fdatecalender = tBody;
		                                        // count rows with size() method
		                                        List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
		                                        for(WebElement fmrow: fmrows){
		                                            List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
		                                            for(WebElement fmCell:fmCells){
		                                            	if (fmCell.getText().contains(Integer.toString(fromDay)))
		                                            	{
		                                            		fmCell.click();
		                                            		IsDateClicked = true;
		                                                 	break outerloop;
		                                            	}
		                                               
		                                            }
		                                        }                                                                                                                  
		                                    }
		                                }
		                            }
		                        }
		                    }
		                }
		          		break;     
		    	}
		    	else {
		    		if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
		    			previousYear.click();
		        		continue;
		    		}
		    		else {
		    			nextYear.click();
		        		continue;
		    		}                 
		    	}
		    }
		    
		    Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
	      switch (mnth) {
	          case 1:
	              return "JAN";
	          case 2:
	              return "FEB";
	          case 3:
	              return "MAR";
	          case 4:
	              return "APR";
	          case 5:
	              return "MAY";
	          case 6:
	              return "JUN";
	          case 7:
	              return "JUL";
	          case 8:
	              return "AUG";
	          case 9:
	              return "SEP";
	          case 10:
	              return "OCT";
	          case 11:
	              return "NOV";
	          case 12:
	              return "DEC";
	          default:
	              return "Invalid input - Wrong month number.";
	      }
	      
	  }
}
