package pageClasses;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MyExpense {
	WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
	//create constructor
	public MyExpense(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[contains(text(),('My Expense'))]")
	WebElement myExpense;

	@FindBy(xpath="//span[contains(text(),('Export'))]")
	WebElement export;

	@FindBy(xpath="//span[contains(text(),('Filter'))]")
	WebElement expenseFilter;

	@FindBy(xpath="//span[contains(text(),('Apply '))]")
	WebElement applyFilterButton;

	@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
	WebElement clearFilter;

	@FindBy(xpath="//mat-select[@aria-label='Status']//div[@class='mat-select-arrow-wrapper']")
	WebElement byStatus;
	@FindBy(xpath="//mat-select[@aria-label='Category']//div[@class='mat-select-arrow-wrapper']")
	WebElement byCategory;

	@FindBy(xpath="//span[contains(text(),(' Pending '))]")
	WebElement pending;

	@FindBy(xpath="//span[contains(text(),(' Rejected '))]")
	WebElement rejected;

	@FindBy(xpath="//span[contains(text(),(' Pending Reimbursement '))]")
	WebElement pendingReimbursementData;

	//My claim> add claim
	@FindBy(xpath="//div[contains(text(),'My Claims')]")
	WebElement myClaims;
	@FindBy(xpath="//span[normalize-space()='Add Claim']")
	WebElement addnewClaim;
	@FindBy(xpath="//mat-select[@placeholder='Category*']//div[@class='mat-select-arrow-wrapper']")
	WebElement category;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[3]")
	WebElement transactionDate;
	@FindBy(xpath="//input[@placeholder='Amount*']")
	WebElement ammount;
	@FindBy(xpath="//input[@placeholder='Event']")
	WebElement event;
	@FindBy(xpath="//textarea[@placeholder='Description']")
	WebElement description;
	@FindBy(xpath="//div[normalize-space()='Attach Receipts']")
	WebElement attachReceipts;
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement  AddClaimbutton;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']")
	WebElement dropdownList;

	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-add-opportunities/div/div/form/div[1]/div[2]/div[8]/div/div/kt-tag/div/mat-form-field/div/div[1]/div/input")
	WebElement nextYear;

	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;
	public void getVerifyMsg() {
		try {
			if (toastMessage.isDisplayed()) {
				System.out.println("msg is appearing");
				String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vmsg);
			}
		} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 	
	}

	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("My Expense");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}

	public void getMyExpense() {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(myExpense))).click();
	}	
	public void getExport() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(export))).click();

	}
	public void getExpenseFilter() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(expenseFilter))).click();
	}
	public void getApplyFilterButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(applyFilterButton))).click();
	}
	public void getClearFilter() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(clearFilter))).click();
	}
	public void getByStatus() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(byStatus))).click();
	}
	public void getPending(){
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(pending))).click();
		as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getRejected() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(rejected))).click();
		as.sendKeys(Keys.ESCAPE).build().perform(); 
	}
	public void getPendingReimbursementData() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(pendingReimbursementData))).click();
		as.sendKeys(Keys.ESCAPE).build().perform(); 
	}

	//My claims> add new
	public void getmyClaims() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(myClaims))).click();
	}
	public void getaddnewClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addnewClaim))).click();
	}
	public void fillExpenseData() throws AWTException, InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(category))).click();
		Thread.sleep(3000);
		try {

			if(dropdownList.isDisplayed())
			{
				System.out.println("Category dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
			}}
		catch (Exception e) {
			System.out.println("Category dropdown is not appearing");
		}

		String amount = sheet.getRow(3).getCell(0).toString();
		ammount.sendKeys(amount);
		String eve = sheet.getRow(3).getCell(1).toString();
		event.sendKeys(eve);
		String descript = sheet.getRow(3).getCell(2).toString();
		description.sendKeys(descript);
		attachReceipts.click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("reicept.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		transactionDate.click();

	}
	public void getaddClaimButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AddClaimbutton))).click();
	}

	//My claims> edit
	@FindBy(xpath="(//*[name()='svg'][@fill='currentColor'])[1]")
	WebElement dateFilter_MyClaim1;
	@FindBy(xpath="(//*[name()='svg'][@fill='currentColor'])[2]")
	WebElement dateFilter_MyClaim2;
	@FindBy(xpath="//input[@name='searchBar']")
	WebElement searchByCategoryName;
	@FindBy(xpath="//mat-row[1]//mat-cell[2]")
	WebElement categoryName;
	@FindBy(xpath="//body/div/div[@dir='ltr']/div/mat-dialog-container[@aria-modal='true']/kt-expense-edit-dialog/div/form/div/div/div/div[1]/mat-form-field[1]/div[1]/div[1]/div[1]")
	WebElement category1;
	@FindBy(xpath="//span[normalize-space()='Food']")
	WebElement food;
	@FindBy(xpath="//span[normalize-space()='Edit Claim']")
	WebElement editClaimExpense;
	@FindBy(xpath="//span[normalize-space()='Update']")
	WebElement updateExpenseClaim;
	@FindBy(xpath="//span[normalize-space()='View Logs']")
	WebElement viewLogs;
	@FindBy(xpath="//kt-expense-log-dialog//span[contains(text(),'Close')]")
	WebElement closeViewlogsbutton;
	@FindBy(xpath="//span[normalize-space()='Close']")
	WebElement CloseExpenseDetailsButton;
	@FindBy(xpath="//div[@class='cdk-overlay-container']")
	WebElement checkbox;


	//My claims> edit
	public void filter_MyClaim() {
		expenseFilter.click();
		as.click(byStatus).perform();
		as.sendKeys(Keys.ENTER).sendKeys(Keys.TAB).build().perform();
		as.click(byCategory).perform();
		try {
			if(checkbox.isDisplayed())
			{
				System.out.println("Category Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.TAB).build().perform();        

			}}
		catch (Exception e) {
			System.out.println("Category Dropdown is not appearing");
		}

	}

	public void fromdate_MyClaim1() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dateFilter_MyClaim1))).click();
	}
	public void todate_MyClaim2() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dateFilter_MyClaim2))).click();
	}
	public void getaaplyfilterButton_MyClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(applyFilterButton))).click();
	}

	public void searchByCategoryName() {
		String searchCategory = sheet.getRow(6).getCell(0).toString();
		searchByCategoryName.sendKeys(searchCategory);
	}
	public void getCategoryName() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(categoryName))).click();

	}
	public void editClaimExpense() throws InterruptedException, AWTException {
		try {

			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editClaimExpense))).click();
			Thread.sleep(3000);
			as.click(category1).perform();
			Thread.sleep(3000);
			try {

				if(dropdownList.isDisplayed())
				{
					System.out.println("Category dropdown is appearing");
					as.sendKeys(Keys.ENTER).build().perform();
				}}
			catch (Exception e) {
				System.out.println("Category dropdown is not appearing");
			}

			String amount = sheet.getRow(11).getCell(0).toString();
			ammount.clear();
			ammount.sendKeys(amount);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(event))).clear();
			String eve = sheet.getRow(11).getCell(1).toString();
			event.sendKeys(eve);
			event.sendKeys("lunch");
			description.clear();
			String descript = sheet.getRow(11).getCell(2).toString();
			description.sendKeys(descript);

			attachReceipts.click();
			Robot robot = new Robot();
			StringSelection strSel = new StringSelection("tracking.jpg");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
			Thread.sleep(3000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(3000);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(3000);
			transactionDate.click();
			getDateFilter();
		    updateExpenseClaim();
		    getVerifyMsg();
		}
		catch (Exception e) {
			System.out.println("Edit button is not available");
		}
		
		
	}
	public void updateExpenseClaim() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(updateExpenseClaim))).click();
	}
	public void getviewLogs() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewLogs))).click();
		Thread.sleep(3000);
		closeViewlogsbutton.click();
	}
	public void CloseExpenseDetails() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CloseExpenseDetailsButton))).click();
	}

	//Claims for approval> edit	
	@FindBy(xpath="//mat-row[1]//mat-cell[3]")
	WebElement userName;

	public void filter_ClaimsForApproval() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(expenseFilter))).click();

		as.click(byStatus).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();

	/*	as.click(byCategory).perform();
		try {
			if(checkbox.isDisplayed())
			{
				System.out.println("Category Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.TAB).build().perform();        

			}}
		catch (Exception e) {
			System.out.println("Category Dropdown is not appearing");
		}*/
		applyFilterButton.click();


	}
	public void userName() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userName))).click();

	}
	//Take Action
	@FindBy(xpath="//span[normalize-space()='Take Action']")
	WebElement takeActionButton;
	@FindBy(xpath="//span[normalize-space()='Change']")
	WebElement changeAmountButton;
	@FindBy(xpath="//input[@formcontrolname='ApprovedAmount']")
	WebElement ApprovedAmount;
	@FindBy(xpath="//textarea[@formcontrolname='Reason']")
	WebElement reasonForPartialApprove;
	@FindBy(xpath="//span[normalize-space()='Partial Approve']")
	WebElement partialApproveButton;
	public void takeActionButton() throws InterruptedException {
		Thread.sleep(3000);	
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(takeActionButton))).click();
			getExpenseDetailAction();
			partialApproveButton();
			getVerifyMsg();
		}
		catch (Exception e) {
			System.out.println("Take Action button is not available");
		}
		
	}

	public void getExpenseDetailAction() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(changeAmountButton))).click();
		ApprovedAmount.clear();
		String requestedAmount = sheet.getRow(15).getCell(0).toString();
		ApprovedAmount.sendKeys(requestedAmount);
		String reasonforApprove = sheet.getRow(15).getCell(1).toString();
		reasonForPartialApprove.sendKeys(reasonforApprove);

	} 

	public void partialApproveButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(partialApproveButton))).click();
	}
	//Accept-Delete claim
	@FindBy(xpath="//mat-row[1]//mat-cell[1]")
	WebElement checkox1;
	@FindBy(xpath="(//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin'])[4]")
	WebElement checkox2;
	@FindBy(xpath="//span[normalize-space()='Decline']")
	WebElement declineClaimButton;
	@FindBy(xpath="//span[normalize-space()='Accept']")
	WebElement acceptClaimButton;
	@FindBy(xpath="//textarea[@formcontrolname='Reason']")
	WebElement reasonForRejection;
	@FindBy(xpath="//button[@type='button']//span[contains(text(),'Decline')]")
	WebElement declineClaimButton1;

	@FindBy(xpath="(//input[@type='image'])[2]")
	WebElement declineClaimImage;
	@FindBy(xpath="(//input[@type='image'])[1]")
	WebElement acceptClaimImage;

	public void checkboxSelection() throws InterruptedException {
		Thread.sleep(2000);
		try {
			checkox1.click();
			checkox2.click();
		}
		catch(Exception e) {
			System.out.println("There is no claim");
		}
		
	}
	public void acceptClaimButton() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(acceptClaimButton)).click();
	}
	public void deleteClaimButton() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(declineClaimButton)).click();
	}

	public void reasonForExpenseRejection() throws InterruptedException {
		String reasonforRej = sheet.getRow(19).getCell(0).toString();
		wait.until(ExpectedConditions.elementToBeClickable(reasonForRejection)).sendKeys(reasonforRej);
		Thread.sleep(3000);
		declineClaimButton1.click();
	}

	public void acceptClaimImage() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(acceptClaimImage)).click();
	}
	public void declineClaimImage() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(declineClaimImage)).click();
	}



	//Export-filter

	@FindBy(xpath="//mat-select[@aria-label='Country']//div[@class='mat-select-arrow-wrapper']")
	WebElement byCountry;
	@FindBy(xpath="//span[normalize-space()='India']")
	WebElement india;
	@FindBy(xpath="//mat-select[@aria-label='State']//div[@class='mat-select-arrow-wrapper']")
	WebElement byState;
	@FindBy(xpath="//span[normalize-space()='Maharashtra']")
	WebElement maharashtra;
	@FindBy(xpath="//mat-select[@placeholder='Status']//div[@class='mat-select-arrow-wrapper']")
	WebElement byStatus1;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Member')]")
	WebElement byMember;
	@FindBy(xpath="//mat-select[@placeholder='Category']//div[@class='mat-select-arrow-wrapper']")
	WebElement byCategory1;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
	WebElement fromdate_ClaimsForApproval;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
	WebElement todate_ClaimsForApproval;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Member')]")
	WebElement applyFilterButtonCFA;
	public void filterByLocation_claimsForApproval() throws InterruptedException {	
		byCountry.click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(india))).click();		
		Thread.sleep(3000);
		byState.click();
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", maharashtra);
		maharashtra.click();
	}
	public void filterByMember_claimsForApproval() {
		as.click(byMember).perform();
		try {
			if(checkbox.isDisplayed())
			{
				System.out.println("Member Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.TAB).build().perform();    

			}}
		catch (Exception e) {
			System.out.println("Member Dropdown is not appearing");
		}
		
	}
	public void filterByStatus_claimsForApproval() {
		as.click(byStatus1).perform();

		try {
			if(checkbox.isDisplayed())
			{
				System.out.println("Status Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.TAB).build().perform();

			}}
		catch (Exception e) {
			System.out.println("Status Dropdown is not appearing");
		}
		
	}
	

	public void filterByCategory_claimsForApproval() {
		as.click(byCategory1).perform();

		try {
			if(checkbox.isDisplayed())
			{
				System.out.println("Category Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
				as.sendKeys(Keys.TAB).build().perform();

			}}
		catch (Exception e) {
			System.out.println("Category Dropdown is not appearing");
		}
	}
	public void todate_claimsForApproval() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(todate_ClaimsForApproval))).click();
	}
	public void fromdate_claimsForApproval() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fromdate_ClaimsForApproval))).click();
	}

	public void getDateFilter() throws InterruptedException {
		boolean IsToDateClicked=false;
		int toYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsToDateClicked) {
				break;
			}
			WebElement toCalelement = chooseYear;
			String todtr = toCalelement.getText();
			//   System.out.println(todtr);
			String[] toDateRangepartss = todtr.split(" – ");
			//  System.out.println(toDateRangepartss[0]);
			//  System.out.println(toDateRangepartss[1]);
			outerloop:       
				if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
					//Store each year in string array
					List < WebElement > tocols;
					//Get value from Table each cell
					WebElement toTbodyYear = tBody;
					// count rows with size() method
					List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
					for (WebElement row: trws) {
						List < WebElement > tCells = row.findElements(By.tagName("td"));
						for (WebElement tCell: tCells) {
							if (tCell.getText().contains(Integer.toString(toYear))) {
								tCell.click();
								Thread.sleep(2000);
								int toMonth = 03;
								String tMonth = ReturnMonthSelected(toMonth);
								WebElement toTbodyMonth = tBody;
								// count rows with size() method
								List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
								for (WebElement tmonthrow: tmonthsrows) {
									List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
									for (WebElement tMonthcell: tMonthCells) {
										if (tMonthcell.getText().contains(tMonth)) {
											tMonthcell.click();
											Thread.sleep(2000);
											int toDay = 1;
											WebElement toTbodyDate = tBody;
											// count rows with size() method
											List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
											for(WebElement tmrow: tmrows){
												List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
												for(WebElement tmCell:tmCells){
													if (tmCell.getText().contains(Integer.toString(toDay)))
													{
														tmCell.click();
														IsToDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public void getDateFilter1() throws InterruptedException {
		boolean IsToDateClicked=false;
		int toYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsToDateClicked) {
				break;
			}
			WebElement toCalelement = chooseYear;
			String todtr = toCalelement.getText();
			//   System.out.println(todtr);
			String[] toDateRangepartss = todtr.split(" – ");
			//  System.out.println(toDateRangepartss[0]);
			//  System.out.println(toDateRangepartss[1]);
			outerloop:       
				if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
					//Store each year in string array
					List < WebElement > tocols;
					//Get value from Table each cell
					WebElement toTbodyYear = tBody;
					// count rows with size() method
					List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
					for (WebElement row: trws) {
						List < WebElement > tCells = row.findElements(By.tagName("td"));
						for (WebElement tCell: tCells) {
							if (tCell.getText().contains(Integer.toString(toYear))) {
								tCell.click();
								Thread.sleep(2000);
								int toMonth = 04;
								String tMonth = ReturnMonthSelected(toMonth);
								WebElement toTbodyMonth = tBody;
								// count rows with size() method
								List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
								for (WebElement tmonthrow: tmonthsrows) {
									List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
									for (WebElement tMonthcell: tMonthCells) {
										if (tMonthcell.getText().contains(tMonth)) {
											tMonthcell.click();
											Thread.sleep(2000);
											int toDay = 28;
											WebElement toTbodyDate = tBody;
											// count rows with size() method
											List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
											for(WebElement tmrow: tmrows){
												List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
												for(WebElement tmCell:tmCells){
													if (tmCell.getText().contains(Integer.toString(toDay)))
													{
														tmCell.click();
														IsToDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}




}
