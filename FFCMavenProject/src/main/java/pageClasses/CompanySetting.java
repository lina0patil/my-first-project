package pageClasses;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CompanySetting {
	Actions as;
	WebDriver driver;
	WebDriverWait wait;
	int timeout=10;
	//create constructor
	public CompanySetting(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(className="ps__thumb-y")
	WebElement verticalSlider;
	@FindBy(xpath="//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")
	WebElement settings;
	@FindBy(xpath="//span[normalize-space()='Company Setting']")
	WebElement companySetting;
	@FindBy(xpath="//input[@formcontrolname='CompanyName']")
	WebElement companyName;
	@FindBy(xpath="//input[@formcontrolname='GroupName']")
	WebElement groupName;
	@FindBy(xpath="//input[@formcontrolname='TimeZone']")
	WebElement timeZone;
	@FindBy(xpath="//input[@placeholder='Search']")
	WebElement search;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement done;
	@FindBy(xpath="//input[@formcontrolname='Currency']")
	WebElement selectCurrency;
	@FindBy(xpath="//img[@alt='Your Company Logo']")
	WebElement companyLogo;
	@FindBy(xpath="//span[normalize-space()='Update']")
	WebElement update_CompanySetting;
	@FindBy(xpath="//span[normalize-space()='Delete Company']")
	WebElement delete_CompanySetting;
	@FindBy(xpath="//span[normalize-space()='Delete']")
	WebElement delete;
	@FindBy(xpath="//div[normalize-space()='REMOVE']")
	WebElement removeLogo;
	
	
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void VerticalSlider_Settings() throws InterruptedException {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 350).release().perform();
	}

	public void getSettings() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(settings))).click();
	}
	public void VerticalSlider_CompanySetting() throws InterruptedException {
		as=new Actions(driver);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 200).release().perform();
	}
	public void companySettingLink() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companySetting))).click();
	
	}
	public void companySettingDetail() throws InterruptedException, AWTException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companyName))).sendKeys("Lina");
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(groupName))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(groupName))).sendKeys("FFC Team");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(timeZone))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).sendKeys("India");
		as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
		done.click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectCurrency))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).sendKeys("India");
		as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
		done.click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companyLogo))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("companyLogo.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		
	}
	/*public void groupName() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(groupName))).sendKeys("FFC Team");
	
	}
	public void timeZone() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(timeZone))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).sendKeys("India");
		as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
		done.click();
		
	
	}
	public void selectCurrency() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectCurrency))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(search))).sendKeys("India");
		as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
		done.click();
	
	}
	public void companyLogo() throws AWTException, InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companyLogo))).click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("companyLogo.png");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
	}*/
	public void update_CompanySetting() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(update_CompanySetting))).click();
	
	}
	public void delete_CompanySetting() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(delete_CompanySetting))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(delete))).click();
	
	}
	public void getVerifyMsg() {
		if (toastMessage.isDisplayed()) {
			System.out.println("msg is appearing");
			String vMsg = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
			System.out.println("Validation Message: "+vMsg);

		} else {
			System.out.println("msg is not appearing");
		}
		
	}
	public void removeLogo() {
		System.out.println("remove logo");
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(removeLogo))).click();

	
	}	

}
