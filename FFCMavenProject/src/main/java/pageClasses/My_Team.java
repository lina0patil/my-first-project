package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class My_Team {
	WebDriver driver;
	WebDriverWait wait;
	Actions as;
	Select select;
	int timeout=10;
	XSSFWorkbook wb;
	XSSFSheet sheet;
    int j;
	int i;
	//create constructor
	public My_Team(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[contains(text(),('My Team'))]")
	WebElement myTeam; 
	@FindBy(xpath="//div[contains(text(),'Location')]")
	WebElement location;    
	@FindBy(xpath="//div[contains(text(),('Lina Patil'))]")
	WebElement usereName;   
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
	WebElement fromDateFilter;   
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
	WebElement toDateFilter;  
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;   
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;   
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;   
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;   
	@FindBy(xpath="//button[@aria-label='Next year']//span[@class='mat-button-wrapper']")
	WebElement nextYear;   
	@FindBy(xpath="//span[normalize-space()='Apply']")
	WebElement applyButton;   
	@FindBy(xpath="//img[@class='kt-common-image kt-pull-right kt-pointer']")
	WebElement refreshIcon;   
	@FindBy(xpath="//table[@class=\"table custom-table-fixed-head\"]//tr[@class='punchin ng-star-inserted']/td[4]")
	WebElement punchInStatus;    
	@FindBy(xpath="//table[@class=\"table custom-table-fixed-head\"]//tr[@class='punchout ng-star-inserted']/td[4]")
	WebElement punchOutStatus;    
	@FindBy(xpath="//input[@autocomplete='off'][@placeholder='Search']")
	WebElement searchUser;  
	@FindBy(xpath="//button[@class='mat-icon-button mat-button-base mat-primary']//span[@class='mat-button-wrapper']")
	WebElement organizationHierarchy;   
	@FindBy(xpath="//mat-icon[contains(text(),(' chevron_right '))]")
	WebElement chevron_right;
	
	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("My Team");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	public void getMyTeam() {
		as=new Actions(driver);
	    wait = new WebDriverWait(driver, timeout);
		myTeam.click();	
	}
	public void getLocation() {
		location.click();
	}
	public void getUser() {
		try {
			usereName.click();
		}
		catch(Exception e){
			System.out.println("User is not available");
		}
			
	}
	public void getApplyButton() {
		applyButton.click();	
	}
	public boolean getPunchInStatus() {
		return punchInStatus.isDisplayed();

	}
	public String getPunchInStatusText() {
		return punchInStatus.getText();	
	}
	public boolean getPunchOutStatus() {
		return punchOutStatus.isDisplayed();	
	}
	public String getPunchOutStatusText() {
		return punchOutStatus.getText();	
	}
	public void getRefreshIcon() {
		refreshIcon.click();
	}
	public void getSearchUser() throws InterruptedException {
		String searchuser = sheet.getRow(2).getCell(0).toString();
	wait.until(ExpectedConditions.elementToBeClickable (searchUser)).sendKeys(searchuser);
	}
	public void getClearSearchUser() throws InterruptedException {
		Thread.sleep(5000);
		searchUser.clear();;
	}
	public void getOrganizationHierarchy() {
		organizationHierarchy.click();
	}
	public void getChevron_right() throws InterruptedException {
	
		try {
			chevron_right.click();
			Thread.sleep(3000);
			chevron_right.click();
			Thread.sleep(3000);
			chevron_right.click();
			Thread.sleep(3000);
			chevron_right.click();
			Thread.sleep(3000);
		}
		catch(Exception e){
			System.out.println("Organization Hierarchy is not available");
		}
	}

	//================================================================================================================//
//global map
	@FindBy(xpath="//div[contains(text(),'Global Map')]")
	WebElement globalMap;
	@FindBy(xpath="//button[@aria-label='Open calendar']//span//*[name()='svg']")
	WebElement dateFilter_globalMap;
	@FindBy(xpath="//img[@src='https://maps.gstatic.com/mapfiles/transparent.png']")
	WebElement locationPinIcon;
	@FindBy(xpath="//button[@title='Toggle fullscreen view']")
	WebElement fullscreenView;
	@FindBy(xpath="//button[@title='Zoom out']")
	WebElement zoomOut;
	@FindBy(xpath="//button[@title='Zoom in']")
	WebElement zoomIn;
	@FindBy(xpath="//button[@title='Show satellite imagery']")
	WebElement satellite;
//Map
	@FindBy(xpath="//div[@class='mat-tab-label-content'][normalize-space()='Map']")
	WebElement map;

	public void globalMap() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(globalMap))).click();
	}
	public void map() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(map))).click();

	}
	public void dateFilter_globalMap() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dateFilter_globalMap))).click();
	}

	public void locationPinIcon() {
		try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(locationPinIcon))).click();
		}
		catch(Exception e){
			System.out.println("Location pin is not available");
		}
	}

	public void fullscreenView() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fullscreenView))).click();
	}
	public void zoomOut_zoomIn() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(zoomOut))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(zoomIn))).click();
	}

	public void satellite() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(satellite))).click();
		Thread.sleep(3000);
	}
	//================================================================================================================//
	
	//Attendance
	@FindBy(xpath="//div[contains(text(),'Attendance')]")
	WebElement attendance;
	@FindBy(xpath="//span[normalize-space()='Filter']")
	WebElement filter_atendance;
	public void attendance() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attendance))).click();
	}
	public void filter_atendance() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(filter_atendance))).click();
	}
	
	


	public void getDateFilter() throws InterruptedException {
		boolean IsFromDateClicked=false;
		int fromYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsFromDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			//  System.out.println(fDateRangepartss[0]);
			//  System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(fromYear >= Integer.parseInt(fDateRangepartss[0]) && fromYear <= Integer.parseInt(fDateRangepartss[1]) && !IsFromDateClicked ) {
					 //Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(fromYear))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 04;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 21;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsFromDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-fromYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}


	public void getFromDateFilter() throws InterruptedException {
		fromDateFilter.click();	
		boolean IsFromDateClicked=false;
		int fromYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsFromDateClicked) {
				break; 
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			//  System.out.println(fDateRangepartss[0]);
			//  System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(fromYear >= Integer.parseInt(fDateRangepartss[0]) && fromYear <= Integer.parseInt(fDateRangepartss[1]) && !IsFromDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(fromYear))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 01;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 1;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsFromDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-fromYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}

	public void getToDateFilter() throws InterruptedException {
		toDateFilter.click();	

		boolean IsToDateClicked=false;
		int toYear = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsToDateClicked) {
				break;
			}
			WebElement toCalelement = chooseYear;
			String todtr = toCalelement.getText();
			//   System.out.println(todtr);
			String[] toDateRangepartss = todtr.split(" – ");
			//  System.out.println(toDateRangepartss[0]);
			//  System.out.println(toDateRangepartss[1]);
			outerloop:       
				if(toYear >= Integer.parseInt(toDateRangepartss[0]) && toYear <= Integer.parseInt(toDateRangepartss[1]) && !IsToDateClicked ) {
					//Store each year in string array
					List < WebElement > tocols;
					//Get value from Table each cell
					WebElement toTbodyYear = tBody;
					// count rows with size() method
					List < WebElement > trws = toTbodyYear.findElements(By.tagName("tr"));                          		
					for (WebElement row: trws) {
						List < WebElement > tCells = row.findElements(By.tagName("td"));
						for (WebElement tCell: tCells) {
							if (tCell.getText().contains(Integer.toString(toYear))) {
								tCell.click();
								Thread.sleep(2000);
								int toMonth = 03;
								String tMonth = ReturnMonthSelected(toMonth);
								WebElement toTbodyMonth = tBody;
								// count rows with size() method
								List <WebElement> tmonthsrows = toTbodyMonth.findElements(By.tagName("tr"));
								for (WebElement tmonthrow: tmonthsrows) {
									List < WebElement > tMonthCells = tmonthrow.findElements(By.tagName("td"));
									for (WebElement tMonthcell: tMonthCells) {
										if (tMonthcell.getText().contains(tMonth)) {
											tMonthcell.click();
											Thread.sleep(2000);
											int toDay = 3;
											WebElement toTbodyDate = tBody;
											// count rows with size() method
											List <WebElement> tmrows = toTbodyDate.findElements(By.tagName("tr"));
											for(WebElement tmrow: tmrows){
												List<WebElement> tmCells = tmrow.findElements(By.tagName("td"));
												for(WebElement tmCell:tmCells){
													if (tmCell.getText().contains(Integer.toString(toDay)))
													{
														tmCell.click();
														IsToDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(toDateRangepartss[0])-toYear)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}
}
