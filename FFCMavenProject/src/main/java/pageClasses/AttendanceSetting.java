package pageClasses;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AttendanceSetting {
	Actions as;
	WebDriver driver;
	WebDriverWait wait;
	int timeout=10;
	//create constructor
	public AttendanceSetting(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(className="ps__thumb-y")
	WebElement verticalSlider;
	@FindBy(xpath="//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")
	WebElement settings;
	@FindBy(xpath="//span[normalize-space()='Attendance Setting']")
	WebElement attendanceSetting;
	@FindBy(xpath="//div[@class='kt-portlet']//span[@class='mat-button-wrapper'][normalize-space()='Add New']")
	WebElement addNewFencingArea;
	@FindBy(xpath="//input[@placeholder='Enter Place Name*']")
	WebElement enterPlaceName;
	@FindBy(xpath="//input[@placeholder='Search Location']")
	WebElement searchLocation;
	@FindBy(xpath="(//button[@title='Toggle fullscreen view'])[2]")
	WebElement fullscreenView;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//div[@role='button']//img")
	WebElement locationPinIcon;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//button[@title='Zoom out']")
	WebElement zoomOut;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//button[@title='Zoom in']")
	WebElement zoomIn;
	@FindBy(xpath="//button[@title='Show satellite imagery']")
	WebElement satellite;
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement addFenceArea;

	
	
	
	@FindBy(xpath="//input[@name='searchBar']")
	WebElement searchFenceLocation;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-attendance-setting[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/img[1]")
	WebElement editFenceLocation;
	@FindBy(xpath="//span[normalize-space()='Update']")
	WebElement updateFenceLocation;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-attendance-setting[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/img[2]")
	WebElement deleteFenceLocation;

	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void VerticalSlider_Settings() throws InterruptedException {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 350).release().perform();
	}

	public void getSettings() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(settings))).click();
	}
	public void VerticalSlider_attendanceSetting() throws InterruptedException {
		as=new Actions(driver);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 200).release().perform();
	}
	public void attendenceSettingLink() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attendanceSetting))).click();
	
	}
	public void addNewFencingArea() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addNewFencingArea))).click();
	
	}
	public void enterPlaceName() {
		enterPlaceName.sendKeys("Tirth2Work");
	}
	public void searchLocation() throws InterruptedException {
		searchLocation.sendKeys("Teerth Technospace, Veerbhadra Nagar Road");
		Thread.sleep(3000);
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	}
	public void fullscreenView() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(fullscreenView))).click();
	}
	public void locatinPin() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(locationPinIcon)));
		as.clickAndHold(locationPinIcon).moveByOffset(250, 0);
	}
	public void zoomOut_zoomIn() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(zoomOut))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(zoomIn))).click();

	}

	public void satellite() throws InterruptedException {
		satellite.click();
		Thread.sleep(3000);
	}
	
	public void addFenceArea() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(addFenceArea)).click();
	}
	
	//search,edit,delete
	public void searchFenceLocation() throws InterruptedException {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(searchFenceLocation)).sendKeys("Tirth");
		
	}
	
	public void editfenceLocation() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(editFenceLocation)).click();
	}
	public void enterPlaceName_edit() throws InterruptedException {
		enterPlaceName.clear();
		Thread.sleep(1000);
		enterPlaceName.sendKeys("Parel");
	}
	public void searchLocation_edit() throws InterruptedException {
		searchLocation.sendKeys("Parel, Mumbai");
		Thread.sleep(3000);
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	}

	public void locatinPin_edit() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(locationPinIcon)));
		as.clickAndHold(locationPinIcon).moveByOffset(200, 0).release().perform();
	}
	public void updateFenceLocation() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(updateFenceLocation)).click();
	}
	public void deleteFenceLocation() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(deleteFenceLocation)).click();
	}
	
	//Global setting
	@FindBy(xpath="//p[.='Set Reminder Punch in Time']")
	WebElement scrolltillGlobalSetting;
	@FindBy(xpath="//div[@class='mat-slide-toggle-thumb']")
	WebElement reminderPunchinTimeToggle;
	@FindBy(xpath="//div[@class='kt-pointer punchTime dropdown-toggle']")
	WebElement reminderPunchintime;
	@FindBy(xpath="//input[@placeholder='HH']")
	WebElement hhPunchIn;
	@FindBy(xpath="//input[@placeholder='MM']")
	WebElement mmPunchIn;
	@FindBy(xpath="//button[normalize-space()='AM']")
	WebElement amPunchIn;
	@FindBy(xpath="//button[normalize-space()='PM']")
	WebElement pmPunchIn;
	@FindBy(xpath="//ngb-timepicker[@class='ng-valid ng-touched ng-dirty']//input[@placeholder='MM']")
	WebElement PunchInMM;
	@FindBy(xpath="//div[@class='ngb-tp-meridian ng-star-inserted']")
	WebElement AM_PM;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//span[contains(text(),'Done')]")
	WebElement donetime;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-attendance-setting[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/mat-form-field[1]/div[1]/div[1]/div[1]/mat-select[1]/div[1]/div[2]/div[1]")
	WebElement setTotalWorkingHoursInDay;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']//div//div")
	WebElement dropdownOptionList;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-attendance-setting[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[2]/mat-form-field[1]/div[1]/div[1]/div[1]/mat-select[1]/div[1]/div[2]/div[1]")
	WebElement setTotalWorkingHoursInHalfDay;
	@FindBy(xpath="//span[normalize-space()='Save']")
	WebElement saveGlobalSettingButton;
	
	//Global setting
	public void globalSetting() {
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", scrolltillGlobalSetting);
		
	}
	public void reminderPunchInTimeToggle() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(reminderPunchinTimeToggle)).click();
	}
	public void setReminderPunchintime() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(reminderPunchintime)).click();
		hhPunchIn.sendKeys("9");
		mmPunchIn.sendKeys("30");
		String verifyAM=AM_PM.getText();
		if(verifyAM.contentEquals("AM")) {
			System.out.println("Time in AM");
		}else {
			pmPunchIn.click();
		}
		donetime.click();

	}
	public void setTotalWorkingHoursInDay() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(setTotalWorkingHoursInDay)).click();
		List<WebElement> dropdownOptList=wait.until(ExpectedConditions.visibilityOfAllElements(dropdownOptionList));
		System.out.println(dropdownOptList.size());
		int count=0;
		for(WebElement ele:dropdownOptList){
		String currentOption=ele.getText();
		if(currentOption.contains("9")){
		ele.click();
		count++;
		break;
		}
		}
		if(count!=0){
		System.out.println("Working hours has been selected");}
		else{System.out.println("option you want to select is not available in the DropDown");
		}
	}
	public void setTotalWorkingHoursInHalfDay() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(setTotalWorkingHoursInHalfDay)).click();
		List<WebElement> dropdownOptList=wait.until(ExpectedConditions.visibilityOfAllElements(dropdownOptionList));
		System.out.println(dropdownOptList.size());
		int count=0;
		for(WebElement ele:dropdownOptList){
		String currentOption=ele.getText();
		if(currentOption.contains("4")){
		ele.click();
		count++;
		break;
		}
		}
		if(count!=0){
		System.out.println("Working hours has been selected");}
		else{System.out.println("option you want to select is not available in the DropDown");
		}
	}
	public void saveGlobalSettingButton() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(saveGlobalSettingButton)).click();
	}

	//Add holiday
	@FindBy(xpath="//ng-component//div//div//div//div//span[contains(text(),'Add New')]")
	WebElement addNewHolidayButton;
	@FindBy(xpath="//input[@placeholder='Add Holiday*']")
	WebElement addHoliday;
	@FindBy(xpath="//button[@aria-label='Open calendar']//span//*[name()='svg']")
	WebElement holidayDate;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//span[contains(text(),'Done')]")
	WebElement done_holiday;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//span[contains(text(),'Done')]")
	WebElement cancel_holiday;
	@FindBy(xpath="//div[contains(text(),'-')]")
	WebElement remove_holiday;
	
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="//button[@aria-label='Next year']//span[@class='mat-button-wrapper']")
	WebElement nextYear;

	public void addNewHolidayButton() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(addNewHolidayButton)).click();
	}
	public void addHoliday() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(addHoliday)).sendKeys("Saturday");
	}
	public void holidayDate() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(holidayDate)).click();
	}
	public void done_holiday() throws InterruptedException {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(done_holiday)).click();
		/*boolean vMsg;
		 * vMsg = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(toastMessage)).isDisplayed();
		if(vMsg==true) {
			Thread.sleep(2000);
			System.out.println("Validation Message: "+vMsg);
			}
		else {
			Thread.sleep(2000);
			
			cancel_holiday.click();
		}*/
	}
	public void remove_holiday() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(remove_holiday)).click();
	}
	public void getVerifyMsg() {
		if (toastMessage.isDisplayed()) {
			System.out.println("msg is appearing");
			String vMsg = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
			System.out.println("Validation Message: "+vMsg);

		} else {
			System.out.println("msg is not appearing");
		}
		
	}
	public void getDateFilter() throws InterruptedException {
		//  Thread.sleep(2000);
		boolean IsDateClicked=false;
		int Year = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" � ");
			// System.out.println(fDateRangepartss[0]);
			// System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(Year))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 12;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 24;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}
}
