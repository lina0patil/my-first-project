package pageClasses;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;

import io.cucumber.messages.internal.com.google.protobuf.Duration;
import java.io.File;

public class myCustomer {
	public WebDriver driver;
	Actions as;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
    int j;
	int i;
	WebDriverWait wait;
	int timeout=10;
	public myCustomer(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//add customer
	@FindBy(xpath="//span[text()='My Customers']")
	WebElement myCustomer;
	@FindBy(xpath="//span[text()=' New Customer ']")
	WebElement newCustomer;	
	@FindBy(xpath="//input[@formcontrolname='LeadName']")
	WebElement customerName;
	@FindBy(xpath="//input[@formcontrolname='RefNo']")
	WebElement refNo;
	@FindBy(xpath="//input[@formcontrolname='PersonName']")
	WebElement contactPersonName;
	@FindBy(xpath="//input[@formcontrolname='MobileNo']")
	WebElement mobileNumber;
	@FindBy(xpath="//input[@formcontrolname='ContactNo']")
	WebElement telephoneNumber;
	@FindBy(xpath="//input[@formcontrolname='Email']")
	WebElement email;
	@FindBy(xpath="//input[@formcontrolname='PersonDesignation']")
	WebElement contactPersonDesignation;
	@FindBy(xpath="//input[@formcontrolname='Address']")
	WebElement address;
	@FindBy(xpath="//button[@class='float-right mat-raised-button mat-button-base mat-primary']//span[contains(text(),'Done')]")
	WebElement doneButton;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[6]/div[2]/mat-form-field/div/div[1]/div/mat-select/div")
	WebElement country;
	@FindBy(xpath="//body[1]/div[3]/div[4]/div[1]/div[1]/div[1]")
	WebElement countryList;
	@FindBy(xpath="//span[contains(text(),'India ')]")
	WebElement India;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//mat-form-field//div//div//div//div//span[contains(text(),'State')]")
	WebElement state;
	@FindBy(xpath="//span[contains(text(),'Gogaon ')]")
	WebElement gogaon;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//mat-form-field//div//div//div//div//span[contains(text(),'City')]")
	WebElement city;
	@FindBy(xpath="//span[contains(text(),'Pune ')]")
	WebElement Pune;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//mat-form-field//div//div//div//div//span[contains(text(),'Locality')]")
	WebElement locality;
	@FindBy(xpath="//input[@formcontrolname='PinCode']")
	WebElement pincode;
	@FindBy(xpath="//span[contains(text(),'Customer Source')]")
	WebElement customerSource;
	@FindBy(xpath="//span[contains(text(),'Email newsletter ')]")
	WebElement Emailnewsletter;
	@FindBy(xpath="//span[contains(text(),'Customer Stage')]")
	WebElement customerStage;
	@FindBy(xpath="//span[contains(text(),'Oppourtunity ')]")
	WebElement opportunity;
	@FindBy(xpath="//kt-tag[@formcontrolname='LeadTags']//input[@placeholder='Tags']")
	WebElement tags;
	@FindBy(xpath="//span[contains(text(),'Select Territory')]")
	WebElement selectTerritory;
	@FindBy(xpath="//div[@class='cdk-overlay-pane']")
	WebElement dropdownList;
	@FindBy(xpath="//input[@formcontrolname='Website']")
	WebElement website;
	@FindBy(xpath="//input[@formcontrolname='OtherInfo']")
	WebElement otherInformation;
	@FindBy(xpath="//input[@placeholder='Attach Front Visiting Card']")
	WebElement attachFrontVisitingCard;
	@FindBy(xpath="//input[@placeholder='Attach Back Visiting Card']")
	WebElement attachBackVisitingCard;
	@FindBy(xpath="//div[@class='mat-checkbox-inner-container']")
	WebElement checkBox;
	@FindBy(xpath="//span[contains(text(),'Customer Type')]")
	WebElement customerType;
	@FindBy(xpath="//span[contains(text(),'Retailer / Sub Dealer ')]")
	WebElement Retailer;
	@FindBy(xpath="//span[contains(text(),'Distributor/Customer Parent Name')]")
	WebElement DistributorName;
	@FindBy(xpath="//div[@formarrayname='Customfields'][1]")
	WebElement customfields;
	@FindBy(xpath="//div//input[@placeholder='Value*']")
	WebElement customfieldType;
	@FindBy(xpath="//span[contains(text(),' Save ')]")
	WebElement save;

	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("My Customer");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	public void getMyCustomer() {
		as=new Actions(driver);
	    wait = new WebDriverWait(driver, timeout);
		myCustomer.click();	
	}
	public void getNewCustomer() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(newCustomer))).click();
		Thread.sleep(5000);
	}
	public void getPrimaryContact() throws InterruptedException, AWTException, IOException {
		String CustomerName = sheet.getRow(1).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customerName))).sendKeys(CustomerName);
		String refNumber = sheet.getRow(1).getCell(1).toString();
		refNo.sendKeys(refNumber);
		String contactPersonN = sheet.getRow(1).getCell(2).toString();
		contactPersonName.sendKeys(contactPersonN);	
		String mobileNo = sheet.getRow(1).getCell(3).toString();
		mobileNumber.sendKeys(mobileNo);
		String telephoneNo = sheet.getRow(1).getCell(4).toString();
		telephoneNumber.sendKeys(telephoneNo);	
		String emailId = sheet.getRow(1).getCell(5).toString();
		email.sendKeys(emailId);
		String contactPDesignation = sheet.getRow(1).getCell(6).toString();
		contactPersonDesignation.sendKeys(contactPDesignation);	  
		
	}

	public void getCustomerAddress() throws InterruptedException {
		address.click();	
		Thread.sleep(5000);
		doneButton.click();
		country.click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(India))).click();
		/*
		List<WebElement> allOptions = wait.until(ExpectedConditions.visibilityOfAllElements(countryList));
		Thread.sleep(5000);
		System.out.println(allOptions.size());
		 for(int i = 0; i<=allOptions.size(); i++) {
             
             
	            if(allOptions.get(i).getText().equals("India")) {
	                // System.out.println("dropdown values are " + allOptions);
	                allOptions.get(i).click();
	                break;
	                 
	            }
		}
		
		*/
		
		Thread.sleep(3000);
		state.click();
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", MadhyaPradesh);
		MadhyaPradesh.click();
		Thread.sleep(2000);
		city.click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(gogaon))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(locality))).click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
		String pin = sheet.getRow(1).getCell(7).toString();
		pincode.sendKeys(pin);	
	}

	public void getMoreInfo() throws InterruptedException, AWTException {
		customerSource.click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Emailnewsletter))).click();
		customerStage.click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		//opportunity.click();
		Thread.sleep(5000);
		String tag = sheet.getRow(1).getCell(8).toString();
		tags.sendKeys(tag);
		tags.sendKeys(Keys.ENTER);
		selectTerritory.click();
		Thread.sleep(3000);
			try {
			Boolean dropdownPresent = dropdownList.isDisplayed();

	        if(dropdownPresent==true)
	        {
	            System.out.println("SelectTerritory dropdown is appearing");
	            as.sendKeys(Keys.ENTER).build().perform();
	        }}
			catch (Exception e) {
				System.out.println("SelectTerritory dropdown is not appearing");
			}
		
	    String webSite = sheet.getRow(1).getCell(9).toString();
		website.sendKeys(webSite);
		String otherInfo = sheet.getRow(1).getCell(10).toString();
		otherInformation.sendKeys(otherInfo);
		attachFrontVisitingCard.click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("aadhar-card.jpeg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		attachBackVisitingCard.click();
		StringSelection strSel2 = new StringSelection("aadhar card back.jpeg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		checkBox.click();
	}
	public void getPrimarySecondarySalesInfo() throws InterruptedException {
		customerType.click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		DistributorName.click();
		Thread.sleep(3000);
		try {

	        if(dropdownList.isDisplayed())
	        {
	            System.out.println("Distributor Name is appearing");
	            as.sendKeys(Keys.ENTER).build().perform();
	        }}
			catch (Exception e) {
				System.out.println("Distributor Name is not appearing");
			}
		Thread.sleep(3000);
		try {
	        if(customfields.isDisplayed())
	        {
	        	wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customfieldType))).sendKeys("Custom field type");
	        	
	        }}
			catch (Exception e) {
				System.out.println("Custom field is not appearing");
			}
		Thread.sleep(3000);
	}
	public void getSaveCustomer() {
	   	wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(save))).click();
	}
	public void getVerifyMsg() {
		try {
		if (toastMessage.isDisplayed()) {
			System.out.println("msg is appearing");
			String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
			System.out.println("Validation Message: "+vmsg);
		}
		} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 
		
	}

	//===========================================================================================================================================//

	//add call logs
	@FindBy(xpath="//button[@mattooltip='Add Call Log']//img[@class='customerIcons']")
	WebElement addCallLogsIcon;
	@FindBy(xpath="//input[@placeholder='Lead/Customer Status']")
	WebElement CustomerStatus;
	@FindBy(xpath="//div[@class='kt-list-timeline__item ng-star-inserted']//div")
	List<WebElement> CustomerStatusList;
	
	@FindBy(xpath="//div[contains(text(),'Success')]")
	WebElement success;
	@FindBy(xpath="//input[@placeholder='Meeting Objective']")
	WebElement MeetingObjective;
	@FindBy(xpath="//button[@class='mat-raised-button mat-button-base mat-primary']//span[contains(text(),'Done')]")
	WebElement doneMeetingObjective;
	@FindBy(xpath="//textarea[@placeholder='Write Meeting Notes*']")
	WebElement WriteMeetingNotes;
	@FindBy(xpath="//div[@class='kt-padding-0 dropdown-menu show']//button[@type='button']")
	WebElement addCallLogsButton;


	//Add Call logs
	public void getAddCallLogsIcon() {
		addCallLogsIcon.click();
	}
	public void getCallLogsFields() {
		//Click on dropdown web element.
		CustomerStatus.click();
		//Store all dropdown options into List
		List<WebElement> allOptions = wait.until(ExpectedConditions.visibilityOfAllElements(CustomerStatusList));
		System.out.println(allOptions.size());
		int count=0;
		for(WebElement ele:allOptions){
		String currentOption=ele.getText();
		if(currentOption.contains("Success")){
		ele.click();
		count++;
		break;
		}
		}
		if(count!=0){
		System.out.println("Success has been selected in the DropDown");}
		else{System.out.println("option you want to select is not available in the DropDown");
		}

		
	
		
		MeetingObjective.click();
		doneMeetingObjective.click();
		WriteMeetingNotes.sendKeys("sdyytyut uytiuyiu"); 
	}
	public void getAddCallLogsButton() {
		addCallLogsButton.click();
	}
	//===========================================================================================================================================//

	//add apointment
	@FindBy(xpath="//button[@mattooltip='Add Appointment']//img[@class='customerIcons']")
	WebElement AddAppointmentIcon;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[1]")
	WebElement datefilter;
	@FindBy(xpath="//button[@aria-label='Choose month and year']")
	WebElement chooseMonth_Year;
	@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
	WebElement chooseYear;  
	@FindBy(css="tbody[role='grid']")
	WebElement tBody;  
	@FindBy(xpath="//button[@aria-label='Previous year']//span[@class='mat-button-wrapper']")
	WebElement previousYear;
	@FindBy(xpath="//button[@aria-label='Next year']//span[@class='mat-button-wrapper']")
	WebElement nextYear;
	@FindBy(xpath="//div[@class='kt-pointer punchTime dropdown-toggle']//span[contains(text(),'Select time')]")
	WebElement selectTime;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='HH']")
	WebElement hour;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='MM']")
	WebElement minute;
	@FindBy(xpath="//ngb-timepicker[@class='ng-valid ng-touched ng-dirty']//button[@type='button'][normalize-space()='AM']")
	WebElement pm;
	@FindBy(xpath="//button[@class='float-right kt-margin-t-5 mat-raised-button mat-button-base mat-primary']//span[contains(text(),'Done')]")
	WebElement doneTimePicker;
	@FindBy(xpath="//input[@placeholder='Assign To']")
	WebElement AssignTo;
	@FindBy(xpath="//div[@class='kt-list-timeline__items ng-star-inserted']")
	WebElement AssignToDropdown;
	@FindBy(css="input[placeholder='Search'][name='searchBar'][type='text']")
	WebElement searchUser;
	@FindBy(xpath="//button[@class='mat-raised-button mat-button-base mat-primary']//span[contains(text(),'Done')]")
	WebElement DoneUser;
	@FindBy(xpath="//button[@class='mat-raised-button mat-button-base']//span[contains(text(),'Cancel')]")
	WebElement cancelUser;	
	@FindBy(xpath="/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-portlet-body/div/mat-table/mat-row[1]/mat-cell[2]/kt-customer-action[2]/div[2]/div/div/div/form/div[2]/div/button/span")
	WebElement addAppointmentButton;

	//Add Appointment
	public void getAddAppointmentIcon() {
		AddAppointmentIcon.click();
	}
	public void getDateFiltereAddAppointment() {
		datefilter.click();
	}

	public void getTimePicker() {
		selectTime.click();
		hour.clear();
		hour.sendKeys("02");
		minute.clear();
		minute.sendKeys("30");
		pm.click();
		doneTimePicker.click();
	}
	public void getAssignToUser() throws InterruptedException {
		AssignTo.click();
		Thread.sleep(3000);
		String searchAssignUser = sheet.getRow(5).getCell(1).toString();
		searchUser.sendKeys(searchAssignUser);
		 try {
			 if(AssignToDropdown.isDisplayed()==true) {
				
				 as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
				 DoneUser.click();
				}
		    } catch (Exception e) {
		      System.out.println("Dropdown is not available");
		      cancelUser.click();
		    } 
	}
	public void getAddAppointmentButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addAppointmentButton))).click();

	}

	//===========================================================================================================================================//
	//Add quick task icon
	@FindBy(xpath="//button[@mattooltip='Add Quick Task']//img[@class='customerIcons']")
	WebElement AddQuicktaskIcon;
	@FindBy(xpath="//div[@x-placement='bottom-left']//div//div//form//div//div//div//kt-select-list-input[@placeholder='Next Action Point*']//div//mat-form-field//div//div//div//input[@placeholder='Next Action Point*']")
	WebElement nextActionPoint_task;
	@FindBy(xpath="//div[contains(text(),'Email The Documents')]")
	WebElement emailTheDocuments;
	@FindBy(xpath="//div[@x-placement='bottom-left']//div//div//form//div//div//div//kt-date-time-picker[@placeholder='Date & Time']//div//mat-form-field[@appearance='standard']//div//div//div//mat-datepicker-toggle//span//*[name()='svg']")
	WebElement DateFiltertask;
	@FindBy(xpath="//div[@x-placement='bottom-left']//div//div//form//div//div//div//span[contains(text(),'Select time')]")
	WebElement TimeFiltertask;
	@FindBy(xpath="//kt-time-picker[@class='dropdown-menu ng-star-inserted show']//input[@placeholder='HH']")
	WebElement hhtask;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//input[@placeholder='MM']")
	WebElement mmtask;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//button[@type='button'][normalize-space()='AM']")
	WebElement pmtask;
	@FindBy(xpath="//kt-time-picker[@x-placement='bottom-left']//span[contains(text(),'Done')]")
	WebElement doneTimePickerTaskButton;
	@FindBy(xpath="//kt-customer-action[@actionname='quickTask']//div[@aria-expanded='false']//div[@x-placement='bottom-left']//div//div//form//div//div//div//kt-single-check-list-input[@placeholder='Assign To']//div//mat-form-field//div//div//div//input[@placeholder='Assign To']")
	WebElement AssignToTask;
	@FindBy(css="input[placeholder='Search'][name='searchBar'][type='text']")
	WebElement searchAssignTo;
	@FindBy(xpath="//div[@class='mat-radio-inner-circle']")
	WebElement LinaPatilRadioButton;
	@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//span[contains(text(),'Done')]")
	WebElement DoneAssignToButton;
	@FindBy(xpath="//div[@class='dropdown-inline show dropdown']//div[@class='kt-portlet__foot kt-portlet__no-border kt-portlet__foot--fit text-right']//button[1]//span[1]")
	WebElement AddQuickTaskButton;

	//Add Quick task
	public void getAddQuickTaskIcon() {
		AddQuicktaskIcon.click();
	}
	public void getNextActionPoint() {
		nextActionPoint_task.click();
		emailTheDocuments.click();
	}
	public void getDateFiltereAddQuickTask() {
		DateFiltertask.click();
	}

	public void getTimePickerTask() throws InterruptedException {
		TimeFiltertask.click();
		Thread.sleep(3000);
		hhtask.clear();
		hhtask.sendKeys("02");
		mmtask.clear();
		mmtask.sendKeys("30");
		pmtask.click();
		doneTimePickerTaskButton.click();
	}
	public void getAssignToUserTask() throws InterruptedException {
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AssignToTask))).click();
		Thread.sleep(3000);
		String searchAssignUser = sheet.getRow(5).getCell(1).toString();
		searchAssignTo.sendKeys(searchAssignUser);
		 try {
			 if(AssignToDropdown.isDisplayed()==true) {
				 Thread.sleep(3000);
					as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
					DoneAssignToButton.click();
				}
		    } catch (Exception e) {
		      System.out.println("Dropdown is not available");
		      cancelUser.click();
		    } 
	}
	public void getAddQuickTaskButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AddQuickTaskButton))).click();
	}
	//===========================================================================================================================================//
	//add opportunity icon
	@FindBy(xpath="//button[@mattooltip='Add Opportunity']//img[@class='customerIcons']")
	WebElement AddOpportunityIcon;
	@FindBy(xpath="/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-portlet-body/div/mat-table/mat-row[1]/mat-cell[2]/kt-customer-action[3]/div[3]/div/div/div/form/div[1]/div/div[1]/mat-form-field/div/div[1]/div/input")
	WebElement OpportunityName;
	@FindBy(xpath="/html/body/kt-base/div[2]/div/div/div/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-portlet-body/div/mat-table/mat-row[1]/mat-cell[2]/kt-customer-action[3]/div[3]/div/div/div/form/div[1]/div/div[2]/mat-form-field/div/div[1]/div")
	WebElement stage;
	@FindBy(xpath="//span[contains(text(),'Won')]")
	WebElement Won;
	@FindBy(xpath="(//div[@class='mat-form-field-infix'])[23]")
	WebElement AssignToOpportunity;
	@FindBy(xpath="//button[@class='mat-raised-button mat-button-base mat-primary']//span[contains(text(),'Done')]")
	WebElement Done;
	@FindBy(xpath="(//input[@placeholder='Value*'])")
	WebElement value;
	@FindBy(xpath="(//*[name()='svg'][@class='mat-datepicker-toggle-default-icon ng-star-inserted'])[2]")
	WebElement dateFilterOppo;
	@FindBy(xpath="(//input[@placeholder='Tags'])[1]")
	WebElement tagsOppo;
	@FindBy(xpath="//div[@class='dropdown-inline show dropdown']//div[@class='kt-portlet__foot kt-portlet__no-border kt-portlet__foot--fit text-right']//button[1]//span[1]")
	WebElement addoppButton;
	@FindBy(css="input[placeholder='Search'][type='text']")
	WebElement searchassignuserO;
	@FindBy(xpath="//div[@class='kt-list-timeline__items ng-star-inserted']")
	WebElement searchassignuserDropdown;
	
	//Add Opportunity
	public void getOpportunityIcon() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AddOpportunityIcon))).click();

	}
	public void getOtherOpportunitiesField() throws InterruptedException {
		String OppoName = sheet.getRow(9).getCell(0).toString();
		OpportunityName.sendKeys(OppoName);
		
		stage.click();
		as.sendKeys(Keys.ENTER).build().perform();
		//Won.click();
		Thread.sleep(2000);
		AssignToOpportunity.click();
		
		String searchAssignUser = sheet.getRow(5).getCell(1).toString();
		searchassignuserO.sendKeys(searchAssignUser);
		 try {
			 if(searchassignuserDropdown.isDisplayed()==true) {
				
				 as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
				 Done.click();
				}
		    } catch (Exception e) {
		      System.out.println("Dropdown is not available");
		      cancelUser.click();
		    } 
		 String valueO = sheet.getRow(9).getCell(1).toString();
			
		 value.sendKeys(valueO);

		dateFilterOppo.click();
		Thread.sleep(3000);
	}
	
	public void getTags_Oppo() throws InterruptedException {
		String tagsOppoturnity = sheet.getRow(9).getCell(2).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(tagsOppo))).sendKeys(tagsOppoturnity);

	}
	public void getAddOppoButton() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addoppButton))).click();
	}

	//===========================================================================================================================================//
	//details screen> edit customer
	@FindBy(xpath="//mat-row[@role=\"row\"][1]//mat-cell[@role='gridcell'][3]")
	WebElement companyName;
	@FindBy(xpath="//div[contains(text(),'Details')]")
	WebElement deatils;
	@FindBy(xpath="//span[normalize-space()='Edit']")
	WebElement editButton;
	@FindBy(xpath="//body/div/div[@dir='ltr']/div/mat-dialog-container[@aria-modal='true']/kt-customers-edit-dialog/div/form/div/div/div/mat-form-field/div/div/div/mat-select[@placeholder='State']/div[@aria-hidden='true']/div[1]")
	WebElement editState;
	@FindBy(xpath="//span[contains(text(),'Madhya Pradesh ')]")
	WebElement MadhyaPradesh;
	@FindBy(xpath="//body/div/div[@dir='ltr']/div/mat-dialog-container[@aria-modal='true']/kt-customers-edit-dialog/div/form/div/div/div/mat-form-field/div/div/div/mat-select[@placeholder='City']/div[@aria-hidden='true']/div[1]")
	WebElement editCity;
	@FindBy(xpath="//span[contains(text(),'Bhopal ')]")
	WebElement Bhopal;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[8]/div[1]/mat-form-field/div/div[1]/div")
	WebElement customerSource1;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[8]/div[2]/mat-form-field/div/div[1]/div")
	WebElement customerStage1;

	@FindBy(xpath="//span[contains(text(),'Enquiry ')]")
	WebElement enquiry;
	@FindBy(xpath="//kt-tag[@formcontrolname='LeadTags']//input[@placeholder='Tags']")
	WebElement editTags;
	@FindBy(xpath="//span[contains(text(),'Distributor / Super Stockist ')]")
	WebElement Distributor_SuperStockisr;
	@FindBy(xpath="//input[@formcontrolname='Website']")
	WebElement website_edit;
	@FindBy(xpath="(//span[contains(text(),'×')])[1]")
	WebElement crossPointerAadhar;
	@FindBy(xpath="(//span[contains(text(),'×')])[2]")
	WebElement crossPointerAadhar2;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[8]/div[7]/kt-image-upload/div/mat-form-field/div/div[1]/div")
	WebElement attachFrontVisitingCard1;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[8]/div[8]/kt-image-upload/div/mat-form-field/div/div[1]/div")
	WebElement attachBackVisitingCard1;
	//	@FindBy(xpath="//div[@class='mat-checkbox-inner-container']//input[@tabindex='0']")
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[8]/div[9]/mat-checkbox/label/div")
	WebElement checkBox1;
	@FindBy(xpath="//div[@class='mat-form-field-infix']//mat-select[@formcontrolname='LeadTypeId']")
	WebElement customerType1;
	@FindBy(xpath="/html/body/div[3]/div[2]/div/mat-dialog-container/kt-customers-edit-dialog/div[1]/form/div/div[10]/div[2]/mat-form-field/div/div[1]/div")
	WebElement DistributorName1;

	//edit customer> details
	public void getCompanyDetails() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companyName))).click();
	}
	public void getDetails() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deatils))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editButton))).click();
	}

	public void getEditPrimaryContact() throws InterruptedException, AWTException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customerName))).clear();
		String custName = sheet.getRow(15).getCell(0).toString();
		customerName.sendKeys(custName);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(refNo))).clear();
		String referenceNo = sheet.getRow(15).getCell(1).toString();
		refNo.sendKeys(referenceNo);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(contactPersonName))).clear();
		String conPersonName = sheet.getRow(15).getCell(2).toString();
		contactPersonName.sendKeys(conPersonName);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(mobileNumber))).clear();
		String mobNumber = sheet.getRow(15).getCell(3).toString();
		mobileNumber.sendKeys(mobNumber);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(telephoneNumber))).clear();
		String telNumber = sheet.getRow(15).getCell(4).toString();
		telephoneNumber.sendKeys(telNumber);

		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(email))).clear();
		String emailId = sheet.getRow(15).getCell(5).toString();
		email.sendKeys(emailId);
		
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(contactPersonDesignation))).clear();
		String contactDesig = sheet.getRow(15).getCell(6).toString();
		contactPersonDesignation.sendKeys(contactDesig);	   
	}

	public void getEditCustomerAddress() throws InterruptedException {
		address.click();	
		Thread.sleep(5000);
		doneButton.click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(country))).click();
	
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(India))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editState))).click();
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", MadhyaPradesh);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(MadhyaPradesh))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editCity))).click();
		Thread.sleep(3000);
		js.executeScript("arguments[0].scrollIntoView(true);", gogaon);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(gogaon))).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(locality))).click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(pincode))).clear();
		String pin = sheet.getRow(15).getCell(7).toString();
		pincode.sendKeys(pin);
		
	}

	public void getEditMoreInfo() throws InterruptedException, AWTException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
	//	js.executeScript("arguments[0].click();", customerSource);
		js.executeScript("arguments[0].scrollIntoView(true);", customerSource1);
		js.executeScript("window.scrollBy(0,500)");
		
		customerSource1.click();
		Thread.sleep(3000);
		try {
	        if(dropdownList.isDisplayed())
	        {
	            System.out.println("CustomerSource dropdown is appearing");
	        	as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	        }}
			catch (Exception e) {
				System.out.println("CustomerSource dropdown is not appearing");
			}
		Thread.sleep(3000);
		as.click(customerStage1).perform();
		Thread.sleep(3000);
		try {
	        if(dropdownList.isDisplayed())
	        {
	            System.out.println("CustomerStage dropdown is appearing");
	        	as.sendKeys(Keys.ENTER).build().perform();
	        }}
			catch (Exception e) {
				System.out.println("CustomerStage dropdown is not appearing");
			}	
		Thread.sleep(3000);
		String eTag = sheet.getRow(15).getCell(8).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editTags))).sendKeys(eTag);
		editTags.sendKeys(Keys.ENTER);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(selectTerritory))).click();
		Thread.sleep(3000);
		try {

        if(dropdownList.isDisplayed())
        {
            System.out.println("SelectTerritory dropdown is appearing");
            as.sendKeys(Keys.ENTER).build().perform();
        }}
		catch (Exception e) {
			System.out.println("SelectTerritory dropdown is not appearing");
		}
		
		String websiteEdit = sheet.getRow(15).getCell(9).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(website_edit))).clear();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(website_edit))).sendKeys(websiteEdit);
			
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(otherInformation))).clear();
		String otherInfo = sheet.getRow(15).getCell(10).toString();
		otherInformation.sendKeys(otherInfo);
		Thread.sleep(3000);
		
		//crossPointerAadhar.click();
		attachFrontVisitingCard1.click();
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("aadhar-card.jpeg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		/*crossPointerAadhar2.click();
		attachBackVisitingCard1.click();
		StringSelection strSel2 = new StringSelection("aadhar card back.jpg");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(5000);*/
		checkBox1.click();
	}
	public void getEditPrimarySecondarySalesInfo() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customerType1))).click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		DistributorName1.click();
		Thread.sleep(3000);
		try {
	        if(dropdownList.isDisplayed())
	        {
	            System.out.println("DistributorName dropdown is appearing");
	            as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	        }}
			catch (Exception e) {
				System.out.println("DistributorName dropdown is not appearing");
			}
		Thread.sleep(3000);
		try {
	        if(customfields.isDisplayed())
	        {
	        	wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customfieldType))).sendKeys("Custom field type");
	        	
	        }}
			catch (Exception e) {
				System.out.println("Custom field is not appearing");
			}
		Thread.sleep(3000);
	}

	//===========================================================================================================================================//
	//export
	@FindBy(xpath="//span[text()='Export']")
	WebElement export;
	//export
	public void getExport() {
		export.click();
	}

	//===========================================================================================================================================//
	//Delete
	@FindBy(xpath="(//input[@name='searchBar'][@placeholder='Search'])[2]")
	WebElement searchByCustomerName;
	@FindBy(xpath="//kt-portlet-body//mat-row[1]//mat-cell[1]//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
	WebElement CheckboxCustomer;
	@FindBy(xpath="//button[@mattooltip='Delete selected customers']//span[@class='mat-button-wrapper']")
	WebElement DeleteCustomerButton;
	@FindBy(xpath="//button[@class='mat-button mat-button-base mat-primary ng-star-inserted']//span[@class='mat-button-wrapper'][normalize-space()='Delete']")
	WebElement ConfirmDeleteCustomerButton;
	//delete
	public void getSearchByCustomerName() {
		String searchCustomer = sheet.getRow(11).getCell(1).toString();
		searchByCustomerName.sendKeys(searchCustomer);
	}
	public void getSelectCheckboxCustomer() {
		try {
			Thread.sleep(3000);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CheckboxCustomer))).click();
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Customer check box is not available");
		}
		
	}
	public void getdeleteCustomer() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(DeleteCustomerButton))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ConfirmDeleteCustomerButton))).click();
	}

	//===========================================================================================================================================//
	//Timeline
	@FindBy(xpath="//textarea[@placeholder='Enter your Text']")
	WebElement enterText;
	@FindBy(xpath="//button[@class='mat-button-mt-4 dropdown-toggle mat-raised-button mat-button-base kt-customer-filter-btn mat-primary']//span[@class='mat-button-wrapper'][normalize-space()='Filter']")
	WebElement filterT;
	@FindBy(xpath="//mat-tab-body/div/kt-timeline/div/div/kt-filter[@filterformat='customer-timeline']/div[@aria-expanded='false']/div[@aria-labelledby='filterDropdown']/div/div/form/div/div/div/div/div/mat-form-field/div/div[1]/div[1]")
	WebElement CustomerByStatus;
	@FindBy(xpath="//kt-filter[@filterformat='customer-timeline']//div//div//div//div//span[contains(text(),'Apply')]")
	WebElement applyFilterT;
	@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
	WebElement clearFilterT;

	//Timeline
	public void getEntertext_Timeline() {
		//enterText.sendKeys("email doc");
		//enterText.sendKeys(Keys.ENTER);
		//enterText.sendKeys(Keys.ENTER);
	}
	public void FilterByStatus_Timeline() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(filterT))).click();
		Thread.sleep(3000);
		as.click(CustomerByStatus).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
		Thread.sleep(3000);
		applyFilterT.click();
	}

	public void Clearfilter_Timeline() throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(clearFilterT))).click();
	}

	//===========================================================================================================================================//
	//Assign customer
	@FindBy(xpath="//span[normalize-space()='Assign customer']")
	WebElement assignCustomerbutton;
	@FindBy(css="input[name='searchBar'][type='text']")
	WebElement serachC;
	@FindBy(xpath="//div[@class='mat-radio-inner-circle']")
	WebElement radiobuttonC;
	@FindBy(xpath="//span[normalize-space()='Assign']")
	WebElement AssignCButton;
	//Assign Customer
	public void assignCustomerbutton() {
		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignCustomerbutton))).click();
		} catch (InterruptedException e) {
		System.out.println("Assign customer button is not available");
		}
		
	}

	public void searchCustomer() throws InterruptedException {
		String searchCustomer = sheet.getRow(11).getCell(1).toString();
		serachC.sendKeys(searchCustomer);
		 try {
			 if(AssignToDropdown.isDisplayed()==true) {
				
				 as.sendKeys(Keys.TAB).sendKeys(Keys.SPACE).perform();
				 AssignCButton.click();
				}
		    } catch (Exception e) {
		      System.out.println("Dropdown is not available");
		      cancelUser.click();
		    } 

	}


	//===========================================================================================================================================//
	//Bulk Upload
	@FindBy(xpath="//span[normalize-space()='Bulk Upload']")
	WebElement bulkUploadbutton;
	@FindBy(xpath="//b[normalize-space()='Click Here']")
	WebElement downloadAllCustomer;
	@FindBy(xpath="//span[normalize-space()='Attach Excel File']")
	WebElement attachExcelFileButton;
	@FindBy(xpath="//span[normalize-space()='Upload']")
	WebElement uploadExcelButton;

	//Bulk Upload
	public void BulkUploadbutton() {
		bulkUploadbutton.click();
	}
	public void downloadAllCustomer() throws InterruptedException {
		downloadAllCustomer.click();
		Thread.sleep(2000);
	}
	public void attachExcelFile() throws InterruptedException, AWTException, IOException {
		attachExcelFileButton.click();
		Thread.sleep(3000);
		Robot robot = new Robot();
		StringSelection strSel = new StringSelection("BulkUpload-Leads for testing.xlsx");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		
		
		/*
		String path = new File(".").getCanonicalPath();
		String filepath=path+"/src/test/java/utility/BulkUpload-Leads for testing.xlsx";
		//String bulkFile = sheet.getRow(19).getCell(1).toString();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachExcelFileButton))).sendKeys(filepath);
	*/
	}
	public void uploadExcelFileButton() {
		uploadExcelButton.click();
	}

	//===========================================================================================================================================//
	//show report-download report
	@FindBy(xpath="//span[normalize-space()='Show Report']")
	WebElement showReportbutton;
	@FindBy(xpath="//input[@aria-invalid='false']")
	WebElement searchByFileName;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-bulk-report-customer[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/div[1]/table[1]/tbody[1]/tr[1]/td[5]/button[3]/span[1]")
	WebElement downloadExcelFile;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-bulk-report-customer[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/div[1]/table[1]/tbody[1]/tr[1]/td[5]/button[2]/span[1]/i[1]")
	WebElement downloadExcelReport;
	@FindBy(xpath="//body[1]/kt-base[1]/div[2]/div[1]/div[1]/div[1]/div[1]/ng-component[1]/kt-bulk-report-customer[1]/kt-portlet[1]/div[1]/kt-portlet-body[1]/div[1]/table[1]/tbody[1]/tr[1]/td[5]/button[1]/span[1]")
	WebElement viewReport;

	//show report-download report-view report

	public void showReportButton() {
		showReportbutton.click();
	}
	public void searchReport() {
	
		//String searchFileName = sheet.getRow(17).getCell(1).toString();
		 try {
			 searchByFileName.sendKeys("2022");
			 downloadExcelFile();
			 viewReport();
		 }	
		    catch (Exception e) {
		      System.out.println("report is not available");
		    } 
	}
	public void downloadExcelFile() throws InterruptedException {
		downloadExcelFile.click();
		Thread.sleep(3000);
		downloadExcelReport.click();
	}
	public void viewReport() {
		viewReport.click();
	}


	//Filter
	@FindBy(xpath="//kt-filter[@filterformat='mat-table']//span[contains(text(),'Filter')]")
	WebElement filterButtonC;
	@FindBy(xpath="//mat-select[@aria-label='Country']//div[@class='mat-select-arrow-wrapper']")
	WebElement byCountry;
	@FindBy(xpath="//div[@class='mat-form-field-infix']//input[@type='text']")
	WebElement searchCountry;
	@FindBy(xpath="//mat-select[@aria-label='State']//div[@class='mat-select-arrow-wrapper']")
	WebElement byState;
	@FindBy(xpath="//div[@class='mat-form-field-infix']//input[@type='text']")
	WebElement searchState;
	@FindBy(xpath="//mat-select[@placeholder='City']//div[@class='mat-select-arrow']")
	WebElement byCity;
	@FindBy(xpath="//div[@class='mat-form-field-infix']//input[@type='text']")
	WebElement searchCity;
	@FindBy(xpath="//mat-select[@aria-label='Locality']//div[@class='mat-select-arrow']")
	WebElement localityF;
	@FindBy(xpath="//body/kt-base/div/div/div/div/div/ng-component/kt-customers-list/mat-drawer-container/mat-drawer-content/kt-portlet/div/kt-filter[@filterformat='mat-table']/div[@aria-expanded='false']/div[@aria-labelledby='filterDropdown']/div/div/form/div/div/div/div/div/mat-form-field/div/div/div/mat-select[@placeholder='Status']/div[@aria-hidden='true']/div[1]")
	WebElement byStatus;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Source')]")
	WebElement bySource;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Stage')]")
	WebElement byStage;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Type')]")
	WebElement byType;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Territory')]")
	WebElement byTerritory;
	@FindBy(xpath="//div[@aria-hidden='true']//div//span[contains(text(),'Member')]")
	WebElement byMember;
	@FindBy(xpath="//input[@aria-label='dropdown search']")
	WebElement searchMember;
	@FindBy(xpath="//input[@placeholder='Tag']")
	WebElement byTag;
	@FindBy(xpath="//kt-filter[@filterformat='mat-table']//div//div//div//div//span[contains(text(),'Apply')]")
	WebElement applyFilterButtonC;
	@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
	WebElement clearFilter_MyCustomer; 

	//Filter

	public void filterButton_MyCustomer() {
		filterButtonC.click();
	}
	public void filterByLocation_MyCustomer() throws InterruptedException {
		Actions as=new Actions(driver);
		Thread.sleep(3000);
		byCountry.click();
		searchCountry.sendKeys(" India ");
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		byState.click();
		searchState.sendKeys("Madhya");
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		Thread.sleep(3000);
		byCity.click();
		searchCity.sendKeys("Burhanpur");
		Thread.sleep(3000);
		as.sendKeys(Keys.ENTER).build().perform();
		localityF.click();

	}
	public void filterByStatus_MyCustomer() {
		as.click(byStatus).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void filterBySource_MyCustomer() {
		as.click(bySource).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void filterByStage_MyCustomer() {
		as.click(byStage).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void filterByType_MyCustomer() {
		as.click(byType).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void filterByTerritory_MyCustomer() {
		byTerritory.click();
	}
	public void filterByMember_MyCustomer() {
		as.click(byMember).perform();
		as.sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		as.sendKeys(Keys.TAB).build().perform();
	}
	public void filterByTag_MyCustomer() {
		as.click(byTag).sendKeys("filter").build().perform();
		as.sendKeys(Keys.ENTER).build().perform();
	}
	public void applyfilterButton_MyCustomer() {
		applyFilterButtonC.click();
	}

	public void clearFilter_MyCustomer() throws InterruptedException {
		Thread.sleep(2000);
		clearFilter_MyCustomer.click();
		}



	//=====================//
	public void getDateFilter() throws InterruptedException {
		//  Thread.sleep(2000);
		boolean IsDateClicked=false;
		int Year = 2022;
		chooseMonth_Year.click();

		while(true) {
			if(IsDateClicked) {
				break;
			}
			WebElement fromCalelement = chooseYear;
			String fdtr = fromCalelement.getText();
			// System.out.println(fdtr);
			String[] fDateRangepartss = fdtr.split(" – ");
			// System.out.println(fDateRangepartss[0]);
			// System.out.println(fDateRangepartss[1]);
			outerloop:       
				if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
					//Store each year in string array
					List < WebElement > fcols;
					//Get value from Table each cell
					WebElement ft = tBody;
					// count rows with size() method
					List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
					for (WebElement row: frws) {
						List < WebElement > fCells = row.findElements(By.tagName("td"));
						for (WebElement fCell: fCells) {
							if (fCell.getText().contains(Integer.toString(Year))) {
								fCell.click();
								Thread.sleep(2000);
								int fromMonth = 9;
								String fMonth = ReturnMonthSelected(fromMonth);
								WebElement tm = tBody;
								// count rows with size() method
								List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
								for (WebElement fmonthrow: fmonthsrows) {
									List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
									for (WebElement fMonthcell: fMonthCells) {
										if (fMonthcell.getText().contains(fMonth)) {
											fMonthcell.click();
											Thread.sleep(2000);
											int fromDay = 20;
											WebElement fdatecalender = tBody;
											// count rows with size() method
											List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
											for(WebElement fmrow: fmrows){
												List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
												for(WebElement fmCell:fmCells){
													if (fmCell.getText().contains(Integer.toString(fromDay)))
													{
														fmCell.click();
														IsDateClicked = true;
														break outerloop;
													}

												}
											}                                                                                                                  
										}
									}
								}
							}
						}
					}
					break;     
				}
				else {
					if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
						previousYear.click();
						continue;
					}
					else {
						nextYear.click();
						continue;
					}                 
				}
		}

		Thread.sleep(3000);
	}
	public static String ReturnMonthSelected(int mnth) {
		switch (mnth) {
		case 1:
			return "JAN";
		case 2:
			return "FEB";
		case 3:
			return "MAR";
		case 4:
			return "APR";
		case 5:
			return "MAY";
		case 6:
			return "JUN";
		case 7:
			return "JUL";
		case 8:
			return "AUG";
		case 9:
			return "SEP";
		case 10:
			return "OCT";
		case 11:
			return "NOV";
		case 12:
			return "DEC";
		default:
			return "Invalid input - Wrong month number.";
		}

	}
}