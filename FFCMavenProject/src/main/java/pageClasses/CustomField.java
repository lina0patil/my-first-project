package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomField {
	public WebDriver driver;
	Actions as;
	WebDriverWait wait;
	int timeout=10;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
	public CustomField(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(className="ps__thumb-y")
	WebElement verticalSlider;
	@FindBy(xpath="//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")
	WebElement settings;
	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	@FindBy(xpath="//span[normalize-space()='Custom Field']")
	WebElement customfield;
	@FindBy(xpath="//span[normalize-space()='Add Custom Field']")
	WebElement addcustomfieldButton;
	@FindBy(xpath="//button[normalize-space()='Text']")
	WebElement text_CustomType;
	@FindBy(xpath="//input[@formcontrolname='enterLabel']")
	WebElement enterLabel;
	@FindBy(xpath="//input[@formcontrolname='enterPlaceholder']")
	WebElement enterPlaceholder;
	@FindBy(xpath="//div[@class='mat-slide-toggle-thumb']")
	WebElement mandatoryToggle;
	@FindBy(xpath="//span[normalize-space()='Add']")
	WebElement addCustomType;
	public void getVerifyMsg() {	
	 try {
			 String vMsg = wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vMsg);
	    } catch (Exception e) {
	      System.out.println("Validation msg is not displayed");
	    } 
	
    }
	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("Custom Field");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}

	public void VerticalSlider_Settings() throws InterruptedException {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 250).release().perform();
	}

	public void getSettings() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(settings))).click();
	}
	public void VerticalSlider_Roles() throws InterruptedException {
		as=new Actions(driver);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 150).release().perform();
	}
	public void customFieldLink() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customfield))).click();

	}
	public void addcustomfieldButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addcustomfieldButton))).click();

	}
	public void selectTextCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(text_CustomType))).click();
		String label = sheet.getRow(2).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);
		String placeholder = sheet.getRow(2).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterPlaceholder))).sendKeys(placeholder);

	}
	public void mandatoryToggleButton() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(mandatoryToggle))).click();

	}
	public void addCustomType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addCustomType))).click();

	}
	@FindBy(xpath="//button[normalize-space()='Quantitative']")
	WebElement quantitative_CustomType;
	public void selectQuantitativeCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(quantitative_CustomType))).click();
		String label = sheet.getRow(3).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);
		String placeholder = sheet.getRow(3).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterPlaceholder))).sendKeys(placeholder);

	}
	@FindBy(xpath="//button[normalize-space()='YesNo']")
	WebElement YesNo_CustomType;
	public void selectYesNoCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(YesNo_CustomType))).click();
		String label = sheet.getRow(4).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);

	}
	@FindBy(xpath="//button[normalize-space()='Date']")
	WebElement Date_CustomType;
	public void selectDateCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Date_CustomType))).click();
		String label = sheet.getRow(5).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);

	}
	@FindBy(xpath="//button[normalize-space()='Single']")
	WebElement Single_CustomType;
	@FindBy(xpath="//label[normalize-space()='+ Add Option']")
	WebElement add_Option;
	@FindBy(xpath="//input[@placeholder='Enter Options 1']")
	WebElement enter_Option;
	public void selectSingleCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Single_CustomType))).click();
		String label = sheet.getRow(6).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(add_Option))).click();
		String option = sheet.getRow(6).getCell(2).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enter_Option))).sendKeys(option);
	}
	@FindBy(xpath="//button[normalize-space()='Multiple']")
	WebElement Multiple_CustomType;
	public void selectMultipleCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Multiple_CustomType))).click();
		String label = sheet.getRow(7).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(add_Option))).click();
		String option = sheet.getRow(7).getCell(2).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enter_Option))).sendKeys(option);

	}
	@FindBy(xpath="//button[normalize-space()='Image']")
	WebElement image_CustomType;
	public void selectImageCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(image_CustomType))).click();
		String label = sheet.getRow(8).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);

	}
	@FindBy(xpath="//button[normalize-space()='Camera Image']")
	WebElement cameraImage_CustomType;
	public void selectCameraImageCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cameraImage_CustomType))).click();
		String label = sheet.getRow(9).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);
	}
	@FindBy(xpath="//button[normalize-space()='Signature']")
	WebElement Signature_CustomType;
	public void selectSignatureCustomFieldType() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(Signature_CustomType))).click();
		String label = sheet.getRow(10).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).sendKeys(label);
	}
	@FindBy(xpath="//input[@name='searchBar']")
	WebElement searchByLabelName;
	@FindBy(xpath="//img[@alt='edit']")
	WebElement edit_CustomType;
	@FindBy(xpath="//span[normalize-space()='Update']")
	WebElement update_CustomType;
	public void searchByLabelNameT() {
		String label = sheet.getRow(2).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).sendKeys(label);
	}
	public void searchByLabelNameQ() {
		String label = sheet.getRow(3).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameY() {
		String label = sheet.getRow(4).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameD() {
		String label = sheet.getRow(5).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameS() {
		String label = sheet.getRow(6).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameM() {
		String label = sheet.getRow(7).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameI() {
		String label = sheet.getRow(8).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameC() {
		String label = sheet.getRow(9).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}
	public void searchByLabelNameSt() {
		String label = sheet.getRow(10).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchByLabelName))).clear();
		searchByLabelName.sendKeys(label);
	}

	public void edit_CustomType() {
	
	 try {
		 if(edit_CustomType.isDisplayed()==true) {
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(edit_CustomType))).click();
				String label = sheet.getRow(14).getCell(0).toString();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(enterLabel))).clear();
				enterLabel.sendKeys(label);
				mandatoryToggleButton();
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(update_CustomType))).click();
			    getVerifyMsg();
			    Thread.sleep(2000);
			}
	    } catch (Exception e) {
	      System.out.println("Edit custom type is not available");
	    } 
	}
	@FindBy(xpath="//img[@alt='delete']")
	WebElement deleteRole;
	@FindBy(xpath="//span[normalize-space()='Delete']")
	WebElement confirmdeleteRole;
	public void deleteRole() throws InterruptedException {
		
		 try {
			 if(deleteRole.isDisplayed()==true) {
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deleteRole))).click();
					Thread.sleep(3000);
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(confirmdeleteRole))).click();
					getVerifyMsg();
				}
		    } catch (Exception e) {
		      System.out.println("Delete custom type is not available");
		    } 
		
	}
	
}
