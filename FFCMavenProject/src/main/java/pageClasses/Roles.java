package pageClasses;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Roles {
	Actions as;
	WebDriver driver;
	WebDriverWait wait;
	int timeout=20;
	Select select;
	XSSFWorkbook wb;
	XSSFSheet sheet;
	int j;
	int i;
	//create constructor
	public Roles(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(className="ps__thumb-y")
	WebElement verticalSlider;
	@FindBy(xpath="//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")
	WebElement settings;
	@FindBy(xpath="//span[normalize-space()='Roles']")
	WebElement roles;
	@FindBy(xpath="//span[normalize-space()='Add Role']")
	WebElement addRoleButton;
	@FindBy(xpath="//input[@formcontrolname='RoleName']")
	WebElement roleName;
	@FindBy(xpath="//mat-select[@aria-label='Clone Role*']//div[@aria-hidden='true']//div//div")
	WebElement cloneRole;
	@FindBy(xpath="//textarea[@placeholder='Role Description*']")
	WebElement roleDescription;
	@FindBy(xpath="//span[normalize-space()='Done']")
	WebElement doneRole;
	@FindBy(xpath="//mat-row[3]//mat-cell[6]//span[1]//button[1]//span[1]")
	WebElement editRole;
	


	@FindBy(xpath="//div[@id='toast-container']")
	WebElement toastMessage;

	public void getVerifyMsg() {

		try {
			if (toastMessage.isDisplayed()) {
				System.out.println("msg is appearing");
				String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
				System.out.println("Validation Message: "+vmsg);
			}
		} 
		catch (Exception e) {
			System.out.println("Validation msg is not displayed");
		} 	
	}

	public void ExcelConnect() throws IOException {
		FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
		wb = new XSSFWorkbook(file);
		sheet = wb.getSheet("Roles");
	}
	public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
		ExcelConnect();
		j = n;
	}
	
	public void VerticalSlider_Settings() throws InterruptedException {
		as=new Actions(driver);
		wait = new WebDriverWait(driver, timeout);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 250).release().perform();
	}

	public void getSettings() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(settings))).click();
	}
	public void VerticalSlider_Roles() throws InterruptedException {
		as=new Actions(driver);
		//move down the slider by using method
		as.clickAndHold(verticalSlider).moveByOffset(0, 150).release().perform();
	}
	public void rolesLink() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(roles))).click();

	}
	public void addRole() throws InterruptedException {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addRoleButton))).click();
		String rolename = sheet.getRow(3).getCell(0).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(roleName))).sendKeys(rolename);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cloneRole))).click();
		Thread.sleep(3000);
		as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).perform();
		String roleDescrip = sheet.getRow(3).getCell(1).toString();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(roleDescription))).sendKeys(roleDescrip);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(doneRole))).click();
		
	}
	public void editRole_Name_Description() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(editRole))).click();
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(roleName))).clear();
		String rolename = sheet.getRow(7).getCell(0).toString();
		roleName.sendKeys(rolename);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(roleDescription))).clear();
		String roleDescrip = sheet.getRow(7).getCell(1).toString();
		roleDescription.sendKeys(roleDescrip);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(doneRole))).click();
		
	}
	//permission
	//@FindBy(xpath="//mat-row[3]//mat-cell[1]//button[@color='primary']")
	@FindBy(xpath="//mat-row[3]//mat-cell[1]")
	WebElement roleName_Permission;
	
	@FindBy(xpath="//li[normalize-space()='Admin']")
	WebElement admin_Permission;
	//Basic permission
	@FindBy(xpath="//li[normalize-space()='Basic']")
	WebElement basicPermission;
	@FindBy(xpath="//kt-portlet-body/div/div/div/div/div/div/div/div/mat-slide-toggle/label[@for='mat-slide-toggle-18-input']/div/div/div[1]")
	WebElement customersToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[1]")
	WebElement customersDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[2]")
	WebElement DealsToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[2]")
	WebElement DealsDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[3]")
	WebElement TasksToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[3]")
	WebElement TasksDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[4]")
	WebElement AttendanceToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[4]")
	WebElement AttendanceDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[5]")
	WebElement ExpenseToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[5]")
	WebElement ExpenseDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[6]")
	WebElement OrdersToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[6]")
	WebElement OrdersDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[7]")
	WebElement CatalogueToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[7]")
	WebElement CatalogueDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[8]")
	WebElement DynamicFormToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[8]")
	WebElement DynamicFormDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[9]")
	WebElement VisitFormToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[9]")
	WebElement VisitFormDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[10]")
	WebElement CommunicationToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[11]")
	WebElement ReferralToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[10]")
	WebElement ReferralDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[12]")
	WebElement LeaveManagementToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow'])[11]")
	WebElement LeaveManagementDropdown;
	
	public void roleName_Permission () throws InterruptedException {
		Thread.sleep(3000);
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(roleName_Permission))).click();
	}
	//Basic Permissions
	public void basicPermission () {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(basicPermission))).click();	
	}
	public void customersToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customersToggle))).click();
	}
	public void customers_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customersDropdown))).click();
	}
	public void DealsToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(DealsToggle))).click();
	}
	public void DealsDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(DealsDropdown))).click();
	}
	public void TasksToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(TasksToggle))).click();
	}
	public void TasksDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(TasksDropdown))).click();
	}
	public void AttendanceToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AttendanceToggle))).click();
	}
	public void AttendanceDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(AttendanceDropdown))).click();
	}
	public void ExpenseToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ExpenseToggle))).click();
	}
	public void ExpenseDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ExpenseDropdown))).click();
	}
	public void OrdersToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(OrdersToggle))).click();
	}
	public void OrdersDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(OrdersDropdown))).click();
	}
	public void CatalogueToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CatalogueToggle))).click();
	}
	public void CatalogueDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CatalogueDropdown))).click();
	}
	public void DynamicFormToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(DynamicFormToggle))).click();
	}
	public void DynamicFormDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(DynamicFormDropdown))).click();
	}
	public void VisitFormToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(VisitFormToggle))).click();
	}
	public void VisitFormDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(VisitFormDropdown))).click();
	}
	public void CommunicationToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CommunicationToggle))).click();
	}
	public void ReferralToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ReferralToggle))).click();
	}
	public void ReferralDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ReferralDropdown))).click();
	}
	public void LeaveManagementToggle_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(LeaveManagementToggle))).click();
	}
	public void LeaveManagementDropdown_basicPermission() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(LeaveManagementDropdown))).click();
	}

	//Dashboard360 Permissions
	@FindBy(xpath="//li[normalize-space()='Dashboard360']")
	WebElement dashboard360_Permission;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[13]")
	WebElement userDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[12]")
	WebElement userDrivenDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[14]")
	WebElement ExpenseDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[13]")
	WebElement ExpenseDrivenDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[15]")
	WebElement CustomerDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[14]")
	WebElement CustomerDrivenDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[16]")
	WebElement opportunityDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[15]")
	WebElement opportunityDrivenDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[17]")
	WebElement salesDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[16]")
	WebElement salesDrivenDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[18]")
	WebElement marketingDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[17]")
	WebElement marketingDrivenDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[19]")
	WebElement taskDrivenToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[18]")
	WebElement taskDrivenDropdown;
	//Dashboard360 Permissions
	public void dashboard360_Permission () {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dashboard360_Permission))).click();	
	}
	public void userDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userDrivenToggle))).click();
	}
	public void userDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userDrivenDropdown))).click();
	}
	public void ExpenseDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ExpenseDrivenToggle))).click();
	}
	public void ExpenseDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(ExpenseDrivenDropdown))).click();
	}
	public void CustomerDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CustomerDrivenToggle))).click();
	}
	public void CustomerDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(CustomerDrivenDropdown))).click();
	}
	public void opportunityDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(opportunityDrivenToggle))).click();
	}
	public void opportunityDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(opportunityDrivenDropdown))).click();
	}
	public void salesDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(salesDrivenToggle))).click();
	}
	public void salesDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(salesDrivenDropdown))).click();
	}
	public void marketingDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(marketingDrivenToggle))).click();
	}
	public void marketingDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(marketingDrivenDropdown))).click();
	}
	public void taskDrivenToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(taskDrivenToggle))).click();
	}
	public void taskDrivenDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(taskDrivenDropdown))).click();
	}
	
	
	//Reports Permissions
	@FindBy(xpath="//li[contains(text(),'Reports')]")
	WebElement reports_Permission;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[20]")
	WebElement customersReportToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[19]")
	WebElement customersReportDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[21]")
	WebElement expenseReportToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[20]")
	WebElement expenseReportDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[22]")
	WebElement attendanceReportToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[21]")
	WebElement attendanceReportDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[23]")
	WebElement salesReportToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[22]")
	WebElement salesReportDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[24]")
	WebElement dealsReportToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[23]")
	WebElement dealsReportDropdown;
	public void reports_Permission () {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(reports_Permission))).click();
	}
	public void customersReportToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customersReportToggle))).click();
	}
	public void customersReportDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customersReportDropdown))).click();
	}
	
	public void expenseReportToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(expenseReportToggle))).click();
	}
	public void expenseReportDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(expenseReportDropdown))).click();
	}
	
	public void attendanceReportToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attendanceReportToggle))).click();
	}
	public void attendanceReportDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attendanceReportDropdown))).click();
	}
	
	public void salesReportToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(salesReportToggle))).click();
	}
	public void salesReportDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(salesReportDropdown))).click();
	}
	public void dealsReportToggle_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dealsReportToggle))).click();
	}
	public void dealsReportDropdown_Dashboard360Permissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dealsReportDropdown))).click();
	}
	
	//Admin
	@FindBy(xpath="//b[normalize-space()='Admin Permissions']")
	WebElement adminPermission;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[25]")
	WebElement billingToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[26]")
	WebElement integrationsToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[27]")
	WebElement userManagementToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[24]")
	WebElement userManagementDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[28]")
	WebElement allUsersDataToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[29]")
	WebElement catalogueToggleAdmin;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[25]")
	WebElement catalogueDropdownAdmin;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[30]")
	WebElement companySettingsToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[26]")
	WebElement companySettingsDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[31]")
	WebElement expenseSettingsToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[27]")
	WebElement expenseSettingsDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[32]")
	WebElement cRMSettingsToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[28]")
	WebElement cRMSettingsDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[33]")
	WebElement territoryMasterToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[34]")
	WebElement attendanceSettingsToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[35]")
	WebElement dynamicFormToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[29]")
	WebElement dynamicFormDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[36]")
	WebElement orderManagementToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[37]")
	WebElement customFieldsToggle;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[38]")
	WebElement visitFormToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[30]")
	WebElement visitFormDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[39]")
	WebElement taskSettingToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[31]")
	WebElement taskSettingDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[40]")
	WebElement dealsandPipelineSettingsToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[32]")
	WebElement dealsandPipelineSettingsDropdown;
	@FindBy(xpath="(//div[@class='mat-slide-toggle-thumb'])[41]")
	WebElement leaveSettingsToggle;
	@FindBy(xpath="(//div[@class='mat-select-arrow-wrapper'])[33]")
	WebElement leaveSettingsDropdown;
	public void getScrolltoTaskAgainstLeadTab() {
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView(true);", adminPermission);
		}
	public void admin_Permission () {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(admin_Permission))).click();
	}
	public void billingToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(billingToggle))).click();
	}
	public void integrationsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(integrationsToggle))).click();
	}
	public void userManagementToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userManagementToggle))).click();
	}
	public void userManagementDropdown_AdminPermissions() {
		wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userManagementDropdown))).click();
	}
	public void allUsersDataToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(allUsersDataToggle))).click();
	}
	public void catalogueToggleAdmin_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(catalogueToggleAdmin))).click();
	}
	public void catalogueDropdownAdmin_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(catalogueDropdownAdmin))).click();
	}
	public void companySettingsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companySettingsToggle))).click();
	}
	public void companySettingsDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(companySettingsDropdown))).click();
	}
	public void expenseSettingsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(expenseSettingsToggle))).click();
	}
	public void expenseSettingsDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(expenseSettingsDropdown))).click();
	}
	public void cRMSettingsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cRMSettingsToggle))).click();
	}
	public void cRMSettingsDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(cRMSettingsDropdown))).click();
	}
	public void territoryMasterToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(territoryMasterToggle))).click();
	}
	public void attendanceSettingsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attendanceSettingsToggle))).click();
	}
	public void dynamicFormToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dynamicFormToggle))).click();
	}
	public void dynamicFormDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dynamicFormDropdown))).click();
	}
	public void orderManagement_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(orderManagementToggle))).click();
	}
	public void customFieldsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(customFieldsToggle))).click();
	}
	public void visitFormToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(visitFormToggle))).click();
	}
	public void visitFormDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(visitFormDropdown))).click();
	}
	public void taskSettingToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(taskSettingToggle))).click();
	}
	public void taskSettingDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(taskSettingDropdown))).click();
	}
	public void dealsandPipelineSettingsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dealsandPipelineSettingsToggle))).click();
	}
	public void dealsandPipelineSettingsDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dealsandPipelineSettingsDropdown))).click();
	}
	public void leaveSettingsToggle_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(leaveSettingsToggle))).click();
	}
	public void leaveSettingsDropdown_AdminPermissions() {
		wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(leaveSettingsDropdown))).click();
	}

	
	//delete
	@FindBy(xpath="//span[normalize-space()='Delete']")
	WebElement deleteRole;
	@FindBy(xpath="//button[@class='mat-button mat-button-base mat-primary ng-star-inserted']//span[@class='mat-button-wrapper'][normalize-space()='Delete']")
	WebElement confirmdeleteRole;
	public void deleteRole() throws InterruptedException {
		
		 try {
			 if(deleteRole.isDisplayed()==true) {
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deleteRole))).click();
					Thread.sleep(3000);
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(confirmdeleteRole))).click();
					getVerifyMsg();
				}
		    } catch (Exception e) {
		      System.out.println("Delete role button is not available");
		    } 
		
	}
}

