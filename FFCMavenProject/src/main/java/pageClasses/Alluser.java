package pageClasses;
	import java.awt.AWTException;
	import java.awt.Robot;
	import java.awt.Toolkit;
	import java.awt.datatransfer.StringSelection;
	import java.awt.event.KeyEvent;
	import java.io.FileInputStream;
	import java.io.IOException;
	import java.util.List;
	import java.util.Scanner;
	import java.util.concurrent.TimeUnit;

	import org.apache.poi.xssf.usermodel.XSSFSheet;
	import org.apache.poi.xssf.usermodel.XSSFWorkbook;
	import org.openqa.selenium.By;
	import org.openqa.selenium.Keys;
	import org.openqa.selenium.StaleElementReferenceException;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.chrome.ChromeDriver;
	import org.openqa.selenium.interactions.Actions;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.PageFactory;
	import org.openqa.selenium.support.ui.ExpectedConditions;
	import org.openqa.selenium.support.ui.Select;
	import org.openqa.selenium.support.ui.WebDriverWait;

	public class Alluser {

		public WebDriver driver;
		Actions as;
		WebDriverWait wait;
		int timeout=20;
		String vMsg;
		String validation;
		Select select;
		XSSFWorkbook wb;
		XSSFSheet sheet;
		int j;
		int i;
		//create constructor
		public Alluser(WebDriver driver) {
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}

		@FindBy(className="ps__thumb-y")
		WebElement verticalSlider;
		@FindBy(xpath="//a[@class='kt-menu__link kt-menu__toggle']//span[contains(text(),'Settings')]")
		WebElement settings;
		@FindBy(xpath="//span[normalize-space()='All Users']")
		WebElement allUsers;
		@FindBy(xpath="//span[contains(text(),' Edit ')]")
		WebElement edit;//user1
		@FindBy(xpath="//*[.='Date of Joining']")
		WebElement dateOfJoining;
		@FindBy(xpath="//*[.='Last Date of Working']")
		WebElement lastdateOfWorking;

		@FindBy(xpath="//span[normalize-space()='Update']")
		WebElement update;
		@FindBy(xpath="//span[@class='kt-header__topbar-username kt-hidden-mobile']")
		WebElement userProfile;
		@FindBy(xpath="//a[contains(text(),'Sign Out')]")
		WebElement signout;
		@FindBy(id="mat-input-21")
		WebElement username;
		@FindBy(id="mat-input-22")
		WebElement password;
		@FindBy(xpath="kt_login_signin_submit")
		WebElement signInbutton;
		@FindBy(xpath="//button[contains(text(),'Sign In With OTP')]")
		WebElement signInbuttonWithOTP;
		@FindBy(xpath="kt_login_signin_submit")
		WebElement submitbutton;
		@FindBy(xpath="mat-input-23")
		WebElement otp;
		@FindBy(xpath="//div[@class='alert-text']")
		WebElement errorMessage;
		@FindBy(xpath="//button[@aria-label='Choose month and year']")
		WebElement chooseMonth_Year;
		@FindBy(css="button[aria-label='Choose date'] span[class='mat-button-wrapper']")
		WebElement chooseYear;  
		@FindBy(css="tbody[role='grid']")
		WebElement tBody;  
		@FindBy(xpath="//button[@aria-label='Previous 20 years']")
		WebElement previousYear;
		@FindBy(xpath="//button[@aria-label='Next 20 years']")
		WebElement nextYear;
		@FindBy(xpath="//div[@id='toast-container']")
		WebElement toastMessage;

		public void getVerifyMsg() {

			try {
				if (toastMessage.isDisplayed()) {
					System.out.println("msg is appearing");
					String vmsg=wait.until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
					System.out.println("Validation Message: "+vmsg);
				}
			} 
			catch (Exception e) {
				System.out.println("Validation msg is not displayed");
			} 	
		}

		public void ExcelConnect() throws IOException {
			FileInputStream file = new FileInputStream("src/test/java/ExcelFiles/DataSheet.xlsx");
			wb = new XSSFWorkbook(file);
			sheet = wb.getSheet("All User");
		}
		public void FetchExcelData(int n) throws IOException, InterruptedException, AWTException {
			ExcelConnect();
			j = n;
		}

		public void VerticalSlider_Settings() throws InterruptedException {
			as=new Actions(driver);
			wait = new WebDriverWait(driver, timeout);
			//move down the slider by using method
			as.clickAndHold(verticalSlider).moveByOffset(0, 250).release().perform();
		}

		public void getSettings() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(settings)));
			settings.click();
		}
		public void VerticalSlider_allUser() throws InterruptedException {
			as=new Actions(driver);
			//move down the slider by using method
			as.clickAndHold(verticalSlider).moveByOffset(0, 150).release().perform();
		}
		public void getAllUsers() {
			wait.until(ExpectedConditions.elementToBeClickable(allUsers)).click();
		}

		public void getAccessWebLogin() {
			wait.until(ExpectedConditions.elementToBeClickable(accessWebLogin)).click();
		}
		public void getUpdate() {
			wait.until(ExpectedConditions.elementToBeClickable(update)).click();	
		}

		public void getUserProfile() {
			// as.moveToElement(element).click().build().perform();	
			wait.until(ExpectedConditions.elementToBeClickable(userProfile)).click();
		}
		public void getSignout() {
			wait.until(ExpectedConditions.elementToBeClickable(signout)).click();
		}
		public void getLogIn() throws InterruptedException { 
			username.sendKeys("testlead12@nimapinfotech.com");
			password.sendKeys("123456789");
			wait.until(ExpectedConditions.elementToBeClickable(signInbutton)).click();
			wait.until(ExpectedConditions.elementToBeClickable(signInbuttonWithOTP)).click();
		}
		public void getOTP() {
			Scanner sca = new Scanner(System.in);
			System.out.println("Please enter the OTP");
			String userInput= sca.nextLine();
			System.out.println("OTP is " + userInput);
			otp.sendKeys(userInput);
		}
		public void getErrorMessage() {
			String message1 = errorMessage.getText();
			System.out.println("Error validation message: "+message1);
		}
		public void getDateOfJoining() throws InterruptedException {
			dateOfJoining.click();
		}
		public void getLastdateOfWorking() throws InterruptedException {
			lastdateOfWorking.click();
		}

		//add User
		@FindBy(xpath="//span[contains(text(),' Add User ')]")
		WebElement addUser;
		@FindBy(xpath="//input[@placeholder='Name*']")
		WebElement name;
		@FindBy(xpath="//input[@placeholder='Employee No']")
		WebElement employeeNo;
		@FindBy(xpath="//input[@placeholder='Mobile Number*']")
		WebElement mobNo;
		@FindBy(xpath="//input[@placeholder='Contact Number']")
		WebElement contactNo;
		@FindBy(xpath="//input[@placeholder='Email*']")
		WebElement email;
		@FindBy(xpath="//mat-select[@placeholder='Assign Role']//div[@class='mat-select-arrow-wrapper']")
		WebElement assignRole;
		@FindBy(xpath="//mat-select[@placeholder='Gender']//div[@class='mat-select-arrow-wrapper']")
		WebElement gender;
		@FindBy(xpath="//div[9]//mat-form-field[1]//div[1]//div[1]//div[2]//mat-datepicker-toggle[1]//button[1]//span[1]//*[name()='svg']")
		WebElement dateofBirth;
		@FindBy(xpath="//body/div/div[@dir='ltr']/div/mat-dialog-container[@aria-modal='true']/kt-users-edit/div/form/div/div/div/mat-form-field/div/div/div/mat-select[@placeholder='Country']/div[@aria-hidden='true']/div[1]")
		WebElement country;
		@FindBy(css="input[name='searchBar'][type='text']")
		WebElement search;
		@FindBy(xpath="//body/div/div[@dir='ltr']/div/mat-dialog-container[@aria-modal='true']/kt-users-edit/div/form/div/div/div/mat-form-field/div/div/div/mat-select[@placeholder='State']/div[@aria-hidden='true']/div[1]")
		WebElement state;
		@FindBy(xpath="//mat-dialog-container[@aria-modal='true']//mat-select[@placeholder='City']//div[@aria-hidden='true']//div//div")
		WebElement city;
		@FindBy(xpath="//input[@placeholder='Zip Code']")
		WebElement zipCode;
		@FindBy(xpath="//input[@placeholder='Street Address']")
		WebElement streetAddress;
		@FindBy(xpath="//mat-select[@placeholder='Assign Manager']//div[@class='mat-select-arrow']")
		WebElement assignManager;
		@FindBy(xpath="//input[@type='text']")
		WebElement searchDrop;
		@FindBy(xpath="//div[@class='cdk-overlay-pane']")
		WebElement assignManagerDropdownList;	
		@FindBy(xpath="//mat-select[@placeholder='Territory']//div[@class='mat-select-arrow']")
		WebElement territory;
		@FindBy(xpath="//div[@class='cdk-overlay-pane']")
		WebElement territoryDropdownList;
		@FindBy(xpath="//label[@for='mat-slide-toggle-10-input']//div[@class='mat-slide-toggle-thumb']")
		WebElement accessWebLogin;
		@FindBy(xpath="//span[normalize-space()='Add']")
		WebElement addButton;
		public void addUser() throws InterruptedException {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addUser))).click();
		}
		public void enterUserDetails() throws InterruptedException {
			String nam = sheet.getRow(3).getCell(0).toString();
			name.sendKeys(nam);
			String employee = sheet.getRow(3).getCell(1).toString();
			employeeNo.sendKeys(employee);
			String mobno = sheet.getRow(3).getCell(2).toString();
			mobNo.sendKeys(mobno);
			String contact = sheet.getRow(3).getCell(3).toString();
			contactNo.sendKeys(contact);
			String emailid = sheet.getRow(3).getCell(4).toString();
			email.sendKeys(emailid);
			as.click(assignRole).perform();
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			as.click(gender).perform();
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		}
		public void dateofBirth() throws InterruptedException {
			dateofBirth.click();
		}
		public void enterUserDetails2() throws InterruptedException {
			country.click();
			String coun = sheet.getRow(3).getCell(5).toString();
			search.sendKeys(coun);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(3000);
			state.click();
			String sta = sheet.getRow(3).getCell(6).toString();
			search.sendKeys(sta);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(3000);
			city.click();
			String cit = sheet.getRow(3).getCell(7).toString();
			search.sendKeys(cit);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			String zipcode = sheet.getRow(3).getCell(8).toString();
			zipCode.sendKeys(zipcode);
			String streetAdd = sheet.getRow(3).getCell(9).toString();
			streetAddress.sendKeys(streetAdd);
			assignManager.click();
	        try {
			if(assignManagerDropdownList.isDisplayed())
			{
				String manager = sheet.getRow(3).getCell(10).toString();
				searchDrop.sendKeys(manager);
				System.out.println("Assign manager Dropdown is appearing");
				as.sendKeys(Keys.ENTER).build().perform();
			}
	        }
	        catch(Exception e) {
				System.out.println("Assign manager Dropdown is not appearing");
			}

	    	wait.until(ExpectedConditions.elementToBeClickable(territory)).click();
	    	try {
			Boolean dropdownPresent = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(territoryDropdownList)).isDisplayed();
			if(dropdownPresent==true)
			{	
				String terri = sheet.getRow(3).getCell(11).toString();
				searchDrop.sendKeys(terri);
				Thread.sleep(3000);
				System.out.println("Territory dropdown is appearing");
				as.sendKeys(Keys.ENTER).sendKeys(Keys.TAB).build().perform();
			}
	    	}
			catch(Exception e){
				System.out.println("Territory dropdown is not appearing");
			}

			//wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(accessWebLogin)));
			//accessWebLogin.click();

		}

		public void addButton() throws InterruptedException {
			addButton.click();
		}

		//edit user

		@FindBy(xpath="//span[normalize-space()='Update']")
		WebElement updateButton;
		@FindBy(xpath="//input[@placeholder='Search']")
		WebElement searchUser;
		@FindBy(xpath="//mat-select[@aria-label='Country Code']//div[@aria-hidden='true']//div//div")
		WebElement countryCode;
		@FindBy(xpath="(//*[name()='svg'][@fill='currentColor'])[3]")
		WebElement lastDateOfWorking;
		@FindBy(xpath="//input[@aria-label='dropdown search']")
		WebElement countryCodesearch;
		@FindBy(xpath="//span[@class='text-danger ng-star-inserted']")
		WebElement errorMsg;
		@FindBy(xpath="//span[normalize-space()='Cancel']")
		WebElement cancelButton;

		public void searchUser() throws InterruptedException {
			String searchuser = sheet.getRow(6).getCell(0).toString();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchUser))).sendKeys(searchuser);
		}
		public void editUser() throws InterruptedException {
			wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(edit))).click();
		}
		public void editUserDetails() throws InterruptedException {
			Thread.sleep(3000);
			name.clear();
			String nameU = sheet.getRow(10).getCell(0).toString();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(name))).sendKeys(nameU);
			employeeNo.clear();
			String employee = sheet.getRow(10).getCell(1).toString();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(employeeNo))).sendKeys(employee);

			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(countryCode))).click();
			String countrycode = sheet.getRow(10).getCell(2).toString();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(countryCodesearch))).sendKeys(countrycode);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(mobNo))).clear();
			String mob = sheet.getRow(10).getCell(3).toString();
			mobNo.sendKeys(mob);
			contactNo.clear();
			String contact = sheet.getRow(10).getCell(4).toString();
			contactNo.sendKeys(contact);
			email.clear();
			String emailid = sheet.getRow(10).getCell(5).toString();
			email.sendKeys(emailid);
			as.click(assignRole).perform();
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			as.click(gender).perform();
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		}
		public void editdateofBirth() throws InterruptedException {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(dateofBirth))).click();
		}
		public void editlastDateofworking() throws InterruptedException {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(lastDateOfWorking))).click();
		}
		public void editUserDetails2() throws InterruptedException {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(country))).click();
			String coun = sheet.getRow(10).getCell(6).toString();
			search.sendKeys(coun);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(3000);
			state.click();
			String stat = sheet.getRow(10).getCell(7).toString();
			search.sendKeys(stat);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			Thread.sleep(3000);
			city.click();
			String cit = sheet.getRow(10).getCell(8).toString();
			search.sendKeys(cit);
			as.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
			zipCode.clear();
			Thread.sleep(3000);
			String zipc = sheet.getRow(10).getCell(9).toString();
			zipCode.sendKeys(zipc);
			streetAddress.clear();
			Thread.sleep(3000);
			String street = sheet.getRow(10).getCell(10).toString();
			streetAddress.sendKeys(street);

			territory.click();
			try {
			Boolean dropdownPresent = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(territoryDropdownList)).isDisplayed();

			if(dropdownPresent==true)
			{
				String terri = sheet.getRow(10).getCell(11).toString();
				searchDrop.sendKeys(terri);
				Thread.sleep(3000);
				System.out.println("Territory dropdown is appearing");
				as.sendKeys(Keys.ENTER).sendKeys(Keys.TAB).build().perform();
			}
			}
			catch(Exception e){
				System.out.println("Territory dropdown is not appearing");
			}
			//accessWebLogin.click();

		}

		public void updateButton() throws InterruptedException {
			updateButton.click();

		}

		public void VerifyMsg_edit() throws InterruptedException {
			try {
			if (toastMessage.isDisplayed()) {
			vMsg = new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(toastMessage)).getText();
			if(vMsg.contentEquals("User Updated Successfully")) {
				System.out.println("Validation Message: "+vMsg);
				System.out.println("Edit User is closed");
			}
			}}
			catch(Exception e) {
				Thread.sleep(2000);
				validation =wait.until(ExpectedConditions.elementToBeClickable(errorMsg)).getText();
				System.out.println(validation);
				cancelButton.click();
			}
			

		}

		//Bulk upload
		@FindBy(xpath="//span[contains(text(),'Bulk Upload')]")
		WebElement bulkUploadButton;
		@FindBy(xpath="//b[normalize-space()='Click Here']")
		WebElement downloadAllUserLink;
		@FindBy(xpath="//a[contains(text(),'Click to download sample file')]")
		WebElement downloadSampleFileLink;
		@FindBy(xpath="//button[@class='mat-raised-button mat-stroked-button mat-button-base ng-star-inserted']//span[contains(text(),' Attach Excel File')]")
		WebElement attachExcelFileButton;
		@FindBy(xpath="//span[contains(text(),'Upload')]")
		WebElement UploadButton;
		@FindBy(xpath="//span[normalize-space()='Export']")
		WebElement exportButtonUser;
		public void bulkUploadButton() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(bulkUploadButton))).click();
		}
		public void downloadAllUserLink()  {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(downloadAllUserLink))).click();
		}
		public void downloadSampleFileLink() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(downloadSampleFileLink))).click();
		}

		public void attachExcelFileButton() throws InterruptedException, AWTException {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(attachExcelFileButton))).click();

			Robot robot = new Robot();
			StringSelection strSel = new StringSelection("Bulk Upload - CRM Team for testing");
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSel, null);
			Thread.sleep(3000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(3000);
			robot.keyRelease(KeyEvent.VK_ENTER);
		}
		public void UploadButton()  {
			UploadButton.click();
		}
		public void exportButtonUser()  {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(exportButtonUser))).click();
		}
		//Show report
		@FindBy(xpath="//span[contains(text(), 'Show Report')]")
		WebElement showReportButton;
		@FindBy(xpath="//input[@placeholder='Search']")
		WebElement searchReport;
		@FindBy(xpath="(//i[@mattooltip='Dowload BulkUpload Report'])[1]")
		WebElement dowloadBulkUploadReport;
		@FindBy(xpath="//mat-row[1]//mat-cell[5]//i[2]")
		WebElement downloadFile;
		@FindBy(xpath="(//span[contains(text(),'ViewReport')])[1]")
		WebElement viewReport;

		public void showReportButton() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(showReportButton))).click();
		}
		public void searchReport() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchReport))).sendKeys("20220519");
		}
		public void dowloadBulkUploadReport() {
			try {
			wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(dowloadBulkUploadReport)).click();
		    }
			catch(Exception e) {
			System.out.println("There is no report");	
			}
			}
		public void downloadFileReport() throws InterruptedException, AWTException {
		
			try {
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(downloadFile))).click();
				Thread.sleep(3000);
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				robot.keyPress(KeyEvent.VK_ENTER);
				Thread.sleep(3000);
				robot.keyRelease(KeyEvent.VK_ENTER);
			    }
				catch(Exception e) {
				System.out.println("There is no report");	
				}
				
		}
		public void viewReport() {
			try {
				wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(viewReport))).click();
			    }
				catch(Exception e) {
				System.out.println("There is no report");	
				}
		}
		//assign manager, assign team
		@FindBy(xpath="//i[@aria-hidden='true']")
		WebElement assignManagericon;
		@FindBy(xpath="//form[@autocomplete='off']//div//div//div//div//mat-form-field//div//div//div//input[@placeholder='Search']")
		WebElement searchManager;
		@FindBy(xpath="//span[normalize-space()='Assign']")
		WebElement assignButton;
		@FindBy(xpath="//span[normalize-space()='Assign Team']")
		WebElement assignTeam;
		@FindBy(xpath="//span[normalize-space()='�']")
		WebElement removeManager;
		@FindBy(xpath="//span[normalize-space()='Delete']")
		WebElement deleteassignManager;
		@FindBy(xpath="//kt-portlet-body/div/mat-table[@role='grid']/mat-row[@role='row']/mat-cell[@role='gridcell']/mat-slide-toggle/label/div/div/div[1]")
		WebElement userStatus;
		@FindBy(xpath="//span[normalize-space()='OK']")
		WebElement userStatusconfirmation;
		@FindBy(xpath="//span[normalize-space()='Filter']")
		WebElement filter_allUser;
		@FindBy(xpath="//mat-select[@aria-label='By User Status']//div[@aria-hidden='true']//div//div")
		WebElement byUserStatus;
		@FindBy(xpath="//div[@class='cdk-overlay-pane']")
		List<WebElement> userStatusList;
		@FindBy(xpath="//span[normalize-space()='Apply']")
		WebElement applyfilterbutton_allUser;
		@FindBy(xpath="//h6[normalize-space()='Clear Filter']")
		WebElement clearfilter_allUser;
		@FindBy(xpath="//input[@name='searchBar']")
		WebElement searchUser_alluser;
		@FindBy(xpath="(//div[@class='mat-radio-outer-circle'])[1]")
		WebElement radioDrop;
		@FindBy(xpath="(//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin'])[1]")
		WebElement checkbox;
		@FindBy(xpath="//span[normalize-space()='Cancel']")
		WebElement cancel;
		@FindBy(xpath="//form[@autocomplete='off']//div//div//div//div//mat-form-field//div//div//div//input[@placeholder='Search']")
		WebElement searchTeam;

		public void assignManagericon() throws InterruptedException {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignManagericon))).click();
			Thread.sleep(1000);
			try {
				if(radioDrop.isDisplayed()) {
					//String searchMan = sheet.getRow(17).getCell(0).toString();
					//searchManager.sendKeys(searchMan);
					Thread.sleep(3000);
					as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
					wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignButton))).click();
					getVerifyMsg();
				}
			}
			catch(Exception e) {
				System.out.println("dropdown is not appearing");
				cancel.click();
			}
	
		}

		public void assignTeamIcon() throws InterruptedException {

			try {
				wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignTeam))).click();
				if(radioDrop.isDisplayed()) {
					//String searchTeam = sheet.getRow(17).getCell(1).toString();
					//searchTeam.sendKeys(searchTeam);
					Thread.sleep(3000);
					as.sendKeys(Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.SPACE).build().perform();
					wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(assignButton))).click();
					getVerifyMsg();
				}
			}
			catch(Exception e) {
				System.out.println("dropdown is not appearing");
				cancel.click();
			}
		}
		
		public void removeManager() throws InterruptedException {
			try {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(removeManager))).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(deleteassignManager))).click();
			getVerifyMsg();
		}
			catch(Exception e) {
				System.out.println("Manager is not assigned");
			}
		}
		public void searchUser_AllUser() {
			String searchUse = sheet.getRow(6).getCell(0).toString();
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(searchUser_alluser))).sendKeys(searchUse);
		}
		public void userStatus() {
			try {
			wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userStatus))).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(userStatusconfirmation))).click();
			getVerifyMsg();
			}
			catch(Exception e) {
			}
			}
		
		public void filter_allUser() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(filter_allUser))).click();
		}
		public void byUserStatus() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(byUserStatus))).click();

			//Store all dropdown options into List
			List<WebElement> allOptions = wait.until(ExpectedConditions.visibilityOfAllElements(userStatusList));
			System.out.println(allOptions.size());
			int count=0;
			for(WebElement ele:allOptions){
				String currentOption=ele.getText();
				if(currentOption.contains("Inactive Users")){
					ele.click();
					count++;
					break;
				}
			}
			if(count!=0){
				System.out.println("Inactive Users has been selected in the DropDown");}
			else{System.out.println("option you want to select is not available in the DropDown");
			}
		}
		public void applyfilterbutton_allUser() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(applyfilterbutton_allUser))).click();
		}
		public void clearfilter_allUser() {
			wait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(clearfilter_allUser))).click();
		}


		//==========================================================================
		public void getdateofBirth() throws InterruptedException {
			boolean IsDateClicked=false;
			int Year = 1997;
			chooseMonth_Year.click();

			while(true) {
				if(IsDateClicked) {
					break;
				}
				WebElement fromCalelement = chooseYear;
				String fdtr = fromCalelement.getText();
				// System.out.println(fdtr);
				String[] fDateRangepartss = fdtr.split(" – ");
				// System.out.println(fDateRangepartss[0]);
				// System.out.println(fDateRangepartss[1]);
				outerloop:       
					if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
						//Store each year in string array
						List < WebElement > fcols;
						//Get value from Table each cell
						WebElement ft = tBody;
						// count rows with size() method
						List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
						for (WebElement row: frws) {
							List < WebElement > fCells = row.findElements(By.tagName("td"));
							for (WebElement fCell: fCells) {
								if (fCell.getText().contains(Integer.toString(Year))) {
									fCell.click();
									Thread.sleep(2000);
									int fromMonth = 9;
									String fMonth = ReturnMonthSelected(fromMonth);
									WebElement tm = tBody;
									// count rows with size() method
									List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
									for (WebElement fmonthrow: fmonthsrows) {
										List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
										for (WebElement fMonthcell: fMonthCells) {
											if (fMonthcell.getText().contains(fMonth)) {
												fMonthcell.click();
												Thread.sleep(2000);
												int fromDay = 19;
												WebElement fdatecalender = tBody;
												// count rows with size() method
												List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
												for(WebElement fmrow: fmrows){
													List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
													for(WebElement fmCell:fmCells){
														if (fmCell.getText().contains(Integer.toString(fromDay)))
														{
															fmCell.click();
															IsDateClicked = true;
															break outerloop;
														}

													}
												}                                                                                                                  
											}
										}
									}
								}
							}
						}
						break;     
					}
					else {
						if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
							previousYear.click();
							continue;
						}
						else {
							nextYear.click();
							continue;
						}                 
					}
			}

			Thread.sleep(3000);
		}
		public void getDateFilter() throws InterruptedException {
			boolean IsDateClicked=false;
			int Year = 2021;
			chooseMonth_Year.click();

			while(true) {
				if(IsDateClicked) {
					break;
				}
				WebElement fromCalelement = chooseYear;
				String fdtr = fromCalelement.getText();
				// System.out.println(fdtr);
				String[] fDateRangepartss = fdtr.split(" – ");
				// System.out.println(fDateRangepartss[0]);
				// System.out.println(fDateRangepartss[1]);
				outerloop:       
					if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
						//Store each year in string array
						List < WebElement > fcols;
						//Get value from Table each cell
						WebElement ft = tBody;
						// count rows with size() method
						List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
						for (WebElement row: frws) {
							List < WebElement > fCells = row.findElements(By.tagName("td"));
							for (WebElement fCell: fCells) {
								if (fCell.getText().contains(Integer.toString(Year))) {
									fCell.click();
									Thread.sleep(2000);
									int fromMonth = 12;
									String fMonth = ReturnMonthSelected(fromMonth);
									WebElement tm = tBody;
									// count rows with size() method
									List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
									for (WebElement fmonthrow: fmonthsrows) {
										List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
										for (WebElement fMonthcell: fMonthCells) {
											if (fMonthcell.getText().contains(fMonth)) {
												fMonthcell.click();
												Thread.sleep(2000);
												int fromDay = 1;
												WebElement fdatecalender = tBody;
												// count rows with size() method
												List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
												for(WebElement fmrow: fmrows){
													List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
													for(WebElement fmCell:fmCells){
														if (fmCell.getText().contains(Integer.toString(fromDay)))
														{
															fmCell.click();
															IsDateClicked = true;
															break outerloop;
														}

													}
												}                                                                                                                  
											}
										}
									}
								}
							}
						}
						break;     
					}
					else {
						if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
							previousYear.click();
							continue;
						}
						else {
							nextYear.click();
							continue;
						}                 
					}
			}

			Thread.sleep(3000);
		}



		public void getDateFilter2() throws InterruptedException {
			boolean IsDateClicked=false;
			int Year = 2022;
			chooseMonth_Year.click();

			while(true) {
				if(IsDateClicked) {
					break;
				}
				WebElement fromCalelement = chooseYear;
				String fdtr = fromCalelement.getText();
				// System.out.println(fdtr);
				String[] fDateRangepartss = fdtr.split(" – ");
				// System.out.println(fDateRangepartss[0]);
				// System.out.println(fDateRangepartss[1]);
				outerloop:       
					if(Year >= Integer.parseInt(fDateRangepartss[0]) && Year <= Integer.parseInt(fDateRangepartss[1]) && !IsDateClicked ) {
						//Store each year in string array
						List < WebElement > fcols;
						//Get value from Table each cell
						WebElement ft = tBody;
						// count rows with size() method
						List < WebElement > frws = ft.findElements(By.tagName("tr"));                          		
						for (WebElement row: frws) {
							List < WebElement > fCells = row.findElements(By.tagName("td"));
							for (WebElement fCell: fCells) {
								if (fCell.getText().contains(Integer.toString(Year))) {
									fCell.click();
									Thread.sleep(2000);
									int fromMonth = 4;
									String fMonth = ReturnMonthSelected(fromMonth);
									WebElement tm = tBody;
									// count rows with size() method
									List <WebElement> fmonthsrows = tm.findElements(By.tagName("tr"));
									for (WebElement fmonthrow: fmonthsrows) {
										List < WebElement > fMonthCells = fmonthrow.findElements(By.tagName("td"));
										for (WebElement fMonthcell: fMonthCells) {
											if (fMonthcell.getText().contains(fMonth)) {
												fMonthcell.click();
												Thread.sleep(2000);
												int fromDay = 1;
												WebElement fdatecalender = tBody;
												// count rows with size() method
												List <WebElement> fmrows = fdatecalender.findElements(By.tagName("tr"));
												for(WebElement fmrow: fmrows){
													List<WebElement> fmCells = fmrow.findElements(By.tagName("td"));
													for(WebElement fmCell:fmCells){
														if (fmCell.getText().contains(Integer.toString(fromDay)))
														{
															fmCell.click();
															IsDateClicked = true;
															break outerloop;
														}

													}
												}                                                                                                                  
											}
										}
									}
								}
							}
						}
						break;     
					}
					else {
						if((Integer.parseInt(fDateRangepartss[0])-Year)>0){
							previousYear.click();
							continue;
						}
						else {
							nextYear.click();
							continue;
						}                 
					}
			}

			Thread.sleep(3000);
		}
		public static String ReturnMonthSelected(int mnth) {
			switch (mnth) {
			case 1:
				return "JAN";
			case 2:
				return "FEB";
			case 3:
				return "MAR";
			case 4:
				return "APR";
			case 5:
				return "MAY";
			case 6:
				return "JUN";
			case 7:
				return "JUL";
			case 8:
				return "AUG";
			case 9:
				return "SEP";
			case 10:
				return "OCT";
			case 11:
				return "NOV";
			case 12:
				return "DEC";
			default:
				return "Invalid input - Wrong month number.";   }
		}

	

}
